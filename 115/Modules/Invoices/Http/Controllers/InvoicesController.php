<?php

namespace Modules\Invoices\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\Units;
use App\Models\ProductVariations;
use App\Models\Challans;
use App\Models\ChallanEntries;
use App\Models\ChallanItems;
use App\Models\Accounts;
use App\Models\JournalEntries;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Response;
use DB;
use View;

class InvoicesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        if ((isset($_GET['invoice_id'])))
        {
            $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $_GET['invoice_id'])
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.type as product_type',
                                                    'units.name as unit_name',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                    'sizes.height as height',
                                                    'sizes.width as width',
                                                    'product_entries.name as item_name')
                                            ->get();

            $entries_count          = $find_invoice_entries->count();
            $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
            $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();
            $units                  = Units::orderBy('id', 'ASC')->get();
            $variations             = ProductVariations::orderBy('id', 'ASC')->get();

            return view('invoices::index', compact('variations', 'units', 'paid_accounts', 'find_invoice_entries', 'entries_count', 'accounts'));
        }
        else
        {
            $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
            $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();
            $units                  = Units::orderBy('id', 'ASC')->get();
            $variations             = ProductVariations::orderBy('id', 'ASC')->get();

            return view('invoices::index', compact('variations', 'units' ,'paid_accounts', 'accounts'));
        }
    }

    public function AllSales()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('invoices::all_sales', compact('paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('invoices::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'customer_id'           => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'customer_id'           => 'required|integer',
            'reference_id'          => 'nullable|integer',
            'customer_name'         => 'nullable|string|max:255',
            'customer_phone'        => 'nullable|numeric',
            'invoice_note'          => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->customer_address          = $data['customer_address'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->labor_cost                = $data['labor_cost'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->account_id                = $data['account_id'];
            $invoice->branch_id                 = $branch_id;
            $invoice->created_by                = $user_id;

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    if ($data['pcs'][$key] != null)
                    {
                        $cal_pcs = $data['pcs'][$key] != 'NaN' ? $data['pcs'][$key] : 0;
                    }
                    else
                    {
                        $cal_pcs = 0;
                    }

                    if ($data['cartoon'][$key] != null)
                    {
                        $cal_cartoon = $data['cartoon'][$key] != 'NaN' ? $data['cartoon'][$key] : 0;
                    }
                    else
                    {
                        $cal_cartoon = 0;
                    }

                    $invoice_entries[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'customer_id'        => $invoice['customer_id'],
                        'pcs'                => $cal_pcs,
                        'cartoon'            => $cal_cartoon,
                        'reference_id'       => $data['reference_id'],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['amount'][$key],
                        'discount_type'      => $data['discount_type'][$key],
                        'discount_amount'    => $data['discount'][$key] != null ? $data['discount'][$key] : 0,
                        'is_collected'       => $data['is_collected'][$key],
                        'branch_id'          => $branch_id,
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $challan_items[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $cal_pcs,
                        'cartoon'            => $cal_cartoon,
                        'invoice_quantity'   => $data['quantity'][$key],
                        'branch_id'          => $branch_id,
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('invoice_entries')->insert($invoice_entries);
                DB::table('challan_items')->insert($challan_items);

                if (isset($data['generate_challan']))
                {
                    stockOut($data, $item_id=null);

                    $data_find                          = Challans::orderBy('created_at', 'DESC')->first();
                    $challan_number                     = $data_find != null ? $data_find['challan_number'] + 1 : 1;

                    $challan                            = new Challans;;
                    $challan->challan_number            = $challan_number;
                    $challan->date                      = date('Y-m-d', strtotime($data['selling_date']));
                    $challan->invoice_id                = $invoice->id;
                    $challan->branch_id                 = $branch_id;
                    $challan->created_by                = $user_id;

                    if ($challan->save())
                    {
                        foreach ($data['product_entries'] as $key1 => $value1)
                        {
                            $product_buy_price1 = ProductEntries::find($value1);

                            if ($data['pcs'][$key1] != null)
                            {
                                $cal_pcs1 = $data['pcs'][$key1] != 'NaN' ? $data['pcs'][$key1] : 0;
                            }
                            else
                            {
                                $cal_pcs1 = 0;
                            }

                            if ($data['cartoon'][$key1] != null)
                            {
                                $cal_cartoon1 = $data['cartoon'][$key1] != 'NaN' ? $data['cartoon'][$key1] : 0;
                            }
                            else
                            {
                                $cal_cartoon1 = 0;
                            }

                            $challan_entries[] = [
                                'challan_id'         => $challan['id'],
                                'invoice_id'         => $invoice->id,
                                'product_id'         => $product_buy_price1['product_id'],
                                'product_entry_id'   => $value1,
                                'pcs'                => $cal_pcs1,
                                'cartoon'            => $cal_cartoon1,
                                'quantity'           => $data['quantity'][$key1],
                                'branch_id'          => $branch_id,
                                'created_by'         => $user_id,
                                'created_at'         => date('Y-m-d H:i:s'),
                            ];

                            $update_challan_item    = ChallanItems::where('invoice_id', $invoice->id)
                                                                    ->where('product_id', $product_buy_price1['product_id'])
                                                                    ->where('product_entry_id', $value1)
                                                                    ->first();

                            $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] + $data['quantity'][$key1];
                            $update_challan_item->save();
                        }

                        DB::table('challan_entries')->insert($challan_entries);
                    }
                }

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }
                    
                    $customer_due   = Customers::find($data['customer_id']);
                    $previous_due   = $customer_due['balance'];
                    
                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                //For SMS Start
                
                $invoice        = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                        ->select('invoices.*',
                                                 'customers.name as contact_name',
                                                 'customers.address as contact_address',
                                                 'customers.phone as contact_phone')
                                        ->find($invoice['id']);

                $entries        = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->where('invoice_entries.invoice_id', $invoice['id'])
                                            ->select('invoice_entries.*',
                                                     'product_entries.product_type as product_type',
                                                     'product_entries.type as type',
                                                     'product_entries.name as product_entry_name',
                                                     'product_entries.product_code as product_code',
                                                     'units.name as unit_name',
                                                     'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                     'sizes.height as height',
                                                     'sizes.width as width',
                                                     'categories.name as brand_name',
                                                     'products.name as product_name')
                                            ->get();  
                         
                $user_info  = Users::find(1);

                if ($invoice->total_discount_type == 1)
                {
                    $total_dis  = $invoice->total_discount_amount;
                }
                else
                {
                    $total_dis  = ($invoice->total_discount_amount*$entries->sum('total_amount'))/100;
                }

                $in_amount  = number_format($invoice->invoice_amount,2,'.',',');
                $pre_due    = number_format($previous_due,2,'.',',');
                $cash       = number_format($invoice->cash_given,2,'.',',');
                $cur_due    = number_format($previous_due + $invoice->invoice_amount - $invoice->cash_given,2,'.',',');
                $dis        = number_format($invoice->total_discount + $total_dis,2,'.',',');

                if (isset($data['send_sms']))
                {
                    $custom_num = Customers::find($data['customer_id']); 
                    $mobile     = $data['customer_phone'] != null ? $data['customer_phone'] : $custom_num['phone'];
                    $message    = 'আসসালামু আলাইকুম।'. "\n" .
                                    'নূর স্যানিটারীতে আপনাকে স্বাগতম।'. "\n" .
                                    'আপনার পণ্যের ক্রয় মূল্য :' . $in_amount . "\n" . 
                                    'পূর্বের বাকী :' . $pre_due . "\n" .
                                    'ছাড় :' . $dis . "\n" .
                                    'নগদ গ্রহন :' . $cash ."\n" .
                                    'বর্তমান বাকী :' . $cur_due . "\n" .
                                    'যেকোন তথ্যের জন্য কল করুন:' . "\n" .
                                    'শোরুম: 01317-349 956'. "\n" .
                                    'এমডি: 01733- 846 666'. "\n" .
                                    'ধন্যবাদ, আবার আসবেন।';

                    $user       = Users::find(1);
                    $response   = Http::get('https://digishop7.com/digishop-billing/send-mim-sms/'.$user['billing_id'].'?mobile='.$mobile.'&message='.$message);
                }
                //For SMS End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    return redirect()->route('invoices_show', $invoice->id);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'customers.contact_type as contact_type',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  
                     
        $user_info  = Users::find(1);

        return view('invoices::show', compact('entries', 'invoice', 'user_info'));
    }

    public function challan($id)
    {
        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')                       
                                    ->select('invoices.*',
                                             'customers.name as contact_name',
                                             'customers.address as contact_address',
                                             'customers.phone as contact_phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.type as type',
                                             'product_entries.name as product_entry_name',
                                             'product_entries.product_code as product_code',
                                             'units.name as unit_name',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'categories.name as brand_name',
                                             'products.name as product_name')
                                    ->get();  

        $user_info  = Users::find(1);

        return view('invoices::challan', compact('entries', 'invoice', 'user_info'));
    }

    public function showPos($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoice    = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone')
                                    ->find($id);

        $entries    = InvoiceEntries::leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                    ->where('invoice_entries.invoice_id', $id)
                                    ->select('invoice_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();   
                     
        $user_info  = Users::find(1);

        return view('invoices::show_pos', compact('entries', 'invoice', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_invoice           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                            ->select('invoices.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'customers.phone as contact_phone')
                                            ->find($id);

        $find_invoice_entries   = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                            ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->where('invoice_entries.invoice_id', $id)
                                            ->select('invoice_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'product_entries.id as item_id',
                                                    'product_entries.type as product_type',
                                                    'units.name as unit_name',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.buy_price as buy_price',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                                    'sizes.height as height',
                                                    'sizes.width as width',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();
        $current_balance        = JournalEntries::whereIn('transaction_head', ['payment-receive'])
                                                ->where('invoice_id', $id)
                                                ->where('debit_credit', 1)
                                                ->selectRaw('journal_entries.*')
                                                ->get();

        $current_balance_count  = $current_balance->count();
        $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();
 
        return view('invoices::edit', compact('find_invoice', 'find_invoice_entries', 'entries_count', 'current_balance', 'current_balance_count', 'paid_accounts', 'accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'          => 'required|date',
            'customer_id'           => 'required|integer',
            'product_entries.*'     => 'required|integer',
            'amount.*'              => 'required|numeric',
            'customer_id'           => 'required|integer',
            'reference_id'          => 'nullable|integer',
            'customer_name'         => 'nullable|string|max:255',
            'customer_phone'        => 'nullable|numeric',
            'invoice_note'          => 'nullable|string',
            'selling_date'          => 'required|date',
            'product_entries.*'     => 'required|integer',
            'rate.*'                => 'required|numeric',
            'quantity.*'            => 'required|numeric',
            'discount_type.*'       => 'required|integer',
            'discount_amount.*'     => 'nullable|numeric',
            'amount.*'              => 'required|numeric',
            'total_amount'          => 'required|numeric',
            'total_discount_type'   => 'nullable|integer',
            'vat_type'              => 'nullable|numeric',
            'vat_amount'            => 'nullable|numeric',
            'total_discount_amount' => 'nullable|numeric',
            'total_discount_note'   => 'nullable|string',
            'cash_given'            => 'nullable|numeric',
            'change_amount'         => 'nullable|numeric',
            'coupon_code'           => 'nullable|numeric',
            'account_information.*' => 'nullable|string',
            'paid_through.*'        => 'nullable|integer',
            'note.*'                => 'nullable|string',
            'amount_paid.*'         => 'nullable|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
            $invoice        = Invoices::find($id);

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            //Calculate Due Amount

                if ($data['total_amount'] > $invoice['invoice_amount']) 
                {
                    $invoice_dues = $invoice['due_amount'] + ($data['total_amount'] - $invoice['invoice_amount']);

                }
                
                if ($data['total_amount'] < $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'] - ($invoice['invoice_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $invoice['invoice_amount'])
                {
                    $invoice_dues = $invoice['due_amount'];
                }
         
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->customer_name             = $data['customer_name'];
            $invoice->customer_phone            = $data['customer_phone'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $invoice_dues;
            $invoice->total_buy_price           = $buy_price;
            $invoice->total_discount            = $discount;
            $invoice->customer_address          = $data['customer_address'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->discount_code             = $data['coupon_code'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->labor_cost                = $data['labor_cost'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->account_id                = $data['account_id'];
            $invoice->updated_by                = $user_id;

            if ($invoice->save())
            {
                $item_id                = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                $item_delete            = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    if ($data['pcs'][$key] != null)
                    {
                        $cal_pcs = $data['pcs'][$key] != 'NaN' ? $data['pcs'][$key] : 0;
                    }
                    else
                    {
                        $cal_pcs = 0;
                    }

                    if ($data['cartoon'][$key] != null)
                    {
                        $cal_cartoon = $data['cartoon'][$key] != 'NaN' ? $data['cartoon'][$key] : 0;
                    }
                    else
                    {
                        $cal_cartoon = 0;
                    }

                    $invoice_entries[] = [
                        'invoice_id'         => $invoice['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'customer_id'        => $invoice['customer_id'],
                        'pcs'                => $cal_pcs,
                        'cartoon'            => $cal_cartoon,
                        'reference_id'       => $data['reference_id'],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['amount'][$key],
                        'discount_type'      => $data['discount_type'][$key],
                        'discount_amount'    => $data['discount'][$key],
                        'is_collected'       => $data['is_collected'][$key],
                        'branch_id'          => $branch_id,
                        'created_by'         => $user_id,
                    ];

                    $old_chalan_item    = ChallanItems::where('invoice_id', $invoice->id)
                                                    ->where('product_entry_id', $value)
                                                    ->first();

                    if ($old_chalan_item != null)
                    {
                        $challan_items[] = [
                            'invoice_id'         => $invoice['id'],
                            'product_id'         => $product_buy_price['product_id'],
                            'product_entry_id'   => $value,
                            'pcs'                => $cal_pcs,
                            'cartoon'            => $cal_cartoon,
                            'invoice_quantity'   => $data['quantity'][$key],
                            'delivered_quantity' => $old_chalan_item['delivered_quantity'],
                            'branch_id'          => $branch_id,
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                    else
                    {
                        $challan_items[] = [
                            'invoice_id'         => $invoice['id'],
                            'product_id'         => $product_buy_price['product_id'],
                            'product_entry_id'   => $value,
                            'pcs'                => $cal_pcs,
                            'cartoon'            => $cal_cartoon,
                            'invoice_quantity'   => $data['quantity'][$key],
                            'delivered_quantity' => 0,
                            'branch_id'          => $branch_id,
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                $old_chalan_item_delete     = ChallanItems::where('invoice_id', $invoice->id)->delete();

                DB::table('invoice_entries')->insert($invoice_entries);
                DB::table('challan_items')->insert($challan_items);

                // stockOut($data, $item_id=$item_id);

                $jour_ent_debit     = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Start
                    debitUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $pay_debit  = JournalEntries::find($data['current_balance_id'][$i]);
                                $pay_credit = JournalEntries::find($data['current_balance_id'][$i] + 1);

                                if ($pay_debit != null)
                                { 
                                    debitUpdate($pay_debit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    debit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }

                                if ($pay_credit != null)
                                { 
                                    creditUpdate($pay_credit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    credit($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                            }
                            else
                            {
                                $pay_debit  = JournalEntries::where('id', $data['current_balance_id'][$i])->delete();
                                $pay_credit = JournalEntries::where('id', $data['current_balance_id'][$i] + 1)->delete();
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Insert into journal_entries End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_index')->with("success","Sales Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('invoices_show', $invoice['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Invoices::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {   
                        $account_transactions[] = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through_id'       => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'sales',
                            'associated_id'         => $data['invoice_id'],
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }

                if (isset($account_transactions))
                {
                    DB::table('account_transactions')->insert($account_transactions);
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json($exception);
        }
    }

    public function productPriceList($id)
    {
        $data   = ProductEntries::leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->selectRaw('product_entries.id as id,
                                             product_entries.type as type,
                                             product_entries.unit_id as unit_id,
                                             units.name as unit_name,
                                             product_entries.sell_price as sell_price,
                                             product_entries.buy_price as buy_price,
                                             product_entries.stock_in_hand as stock_in_hand,
                                             sizes.height as height,
                                             sizes.width as width,
                                             product_entries.pcs_per_cartoon as pcs_per_cartoon
                                            ')
                                            ->find($id);

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.branch_id', $branch_id)
                                    ->orderBy('invoices.id', 'DESC')
                                    ->select('invoices.*',
                                            'sales_return.id as return_id',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('invoices.id')
                                    ->take(200)
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer_type, $memo_number, $name, $phone, $customer)
    {   
        $branch_id              = Auth()->user()->branch_id;
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
     
        $explode_invoice  = explode(' - ', $memo_number);

        if (isset($explode_invoice[1]))
        {
            $invoice_number  = ltrim($explode_invoice[1],"0");;
        }
        else
        {
            $invoice_number  = 0;
        }

        $data           = Invoices::leftjoin('sales_return', 'sales_return.invoice_id', 'invoices.id')
                                        ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                        ->leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                        ->where('invoices.type', 1)
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('invoices.invoice_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($customer_type == 1, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 0);
                                        })
                                        ->when($customer_type == 2, function ($query) use ($customer_type) {
                                            return $query->where('customers.contact_type', 4);
                                        })
                                        ->when($customer_type == 3, function ($query) use ($customer_type) {
                                            return $query->where('invoices.customer_id', 1);
                                        })
                                        ->when($invoice_number != 0, function ($query) use ($invoice_number) {
                                            return $query->where('invoices.invoice_number', $invoice_number);
                                        })
                                        ->when($phone != 0, function ($query) use ($phone) {
                                            return $query->where('invoices.customer_phone', $phone);
                                        })
                                        ->when($name != 0, function ($query) use ($name) {
                                            return $query->where('invoices.customer_name', 'LIKE', "%$name%");
                                        })
                                        ->when($customer != 0, function ($query) use ($customer) {
                                            return $query->where('invoices.customer_id', $customer);
                                        })
                                        ->where('invoices.branch_id', $branch_id)
                                        ->orderBy('invoices.created_at', 'DESC')
                                        ->select('invoices.*',
                                                 'sales_return.id as return_id',
                                                 'customers.name as contact_name',
                                                 'customers.phone as contact_phone')
                                        ->distinct('invoices.id')
                                        ->take(100)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $data           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('invoices.type', 1)
                                    ->where('product_entries.product_code', $id)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                = new Customers;
        $customers->name          = $data['customer_name'];
        $customers->address       = $data['address'];
        $customers->phone         = $data['mobile_number'];
        $customers->contact_type  = $data['contact_type'];
        $customers->branch_id     = $branch_id;
        $customers->created_by    = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        $branch_id = Auth::user()->branch_id;
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data       = array();
        $i          = 0;
        $fetchData  = $fetchData->where('branch_id', $branch_id);
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "address" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "address" =>$value['address'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('invoices.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('invoices.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }
    
    public function customerInfo($customer_id)
    {
        $info  = Customers::select('name as customerName','phone as contactPhone','address as contactAddress')->where('id',$customer_id)->first();

        return Response::json($info);
    }

    public function storeProduct(Request $request)
    {
        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product                  = new ProductEntries;
            $product->product_id      = $data['product_category_id'];
            $product->brand_id        = $data['brand_id'];
            $product->size_id         = $data['size_id'];
            $product->pcs_per_cartoon = $data['per_cartoon_pcs'];
            $product->name            = $data['product_name'];
            $product->unit_id         = $data['unit_id'];
            $product->product_code    = $data['product_code'];
            $product->sell_price      = $data['selling_price'];
            $product->buy_price       = $data['buying_price'];

            // if (($data['stock_quantity'] != null) && ($data['stock_quantity'] > 0))
            // {
            //     $product->stock_in_hand   = $data['stock_quantity'];
            //     $product->opening_stock   = $data['stock_quantity'];
            // }

            $product->status         = 1;
            $product->alert_quantity = $data['alert_quantity'];
            $product->product_type   = $data['variation_type'];
            $product->type           = $data['product_type'];
            $product->created_by     = $user_id;

            if ($product->save())
            {   
                if ($data['variation_type'] == 2)
                {
                    foreach ($data['variation_id'] as $key => $value)
                    {
                        // if (($value != null) && ($data['variation_value'] != null))
                        // {
                            $variation_values[] = [
                                'product_entry_id'      => $product['id'],
                                'variation_id'          => $value,
                                'variation_value_id'    => $data['variation_value'][$key],
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        // }
                    }

                    DB::table('product_variation_entries')->insert($variation_values);
                }

                $return_data['id']          = $product->id;
                $return_data['name']        = $data['product_name'];
                $return_data['sell_price']  = $data['selling_price'];

                DB::commit();
                return Response::json($return_data);
            }
        }catch (\Exception $exception){
            DB::rollback();
            return Response::json(0);
        }
    }

    public function challanList($invoice_id)
    {
        $challans  = Challans::where('invoice_id', $invoice_id)->orderBy('id', 'ASC')->get();

        return view('invoices::challan_list', compact('challans', 'invoice_id'));
    }

    public function generateChallan($invoice_id)
    {
        $challan_items  = ChallanItems::leftjoin('product_entries', 'product_entries.id', 'challan_items.product_entry_id')
                                    ->leftjoin('sizes', 'sizes.id', 'product_entries.size_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->where('challan_items.invoice_id', $invoice_id)
                                    ->select('challan_items.*',
                                             'product_entries.id as item_id',
                                             'product_entries.type as product_type',
                                             'units.name as unit_name',
                                             'product_entries.product_code as product_code',
                                             'product_entries.stock_in_hand as stock_in_hand',
                                             'product_entries.pcs_per_cartoon as pcs_per_cartoon',
                                             'sizes.height as height',
                                             'sizes.width as width',
                                             'product_entries.name as item_name')
                                            ->get();

        $find_invoice   = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->select('invoices.*',
                                         'customers.id as customer_id',
                                         'customers.name as contact_name',
                                         'customers.phone as contact_phone')
                                    ->find($invoice_id);

        return view('invoices::generate_challan', compact('challan_items', 'invoice_id', 'find_invoice'));
    }

    public function generateChallanStore(Request $request, $invoice_id)
    {
        $rules = array(
            'product_entries.*'     => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                          = Challans::orderBy('created_at', 'DESC')->first();
            $challan_number                     = $data_find != null ? $data_find['challan_number'] + 1 : 1;

            $challan                            = new Challans;;
            $challan->challan_number            = $challan_number;
            $challan->date                      = date('Y-m-d', strtotime($data['challan_date']));
            $challan->invoice_id                = $invoice_id;
            $challan->branch_id                 = $branch_id;
            $challan->created_by                = $user_id;

            if ($challan->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $challan_entries[] = [
                        'challan_id'         => $challan['id'],
                        'invoice_id'         => $invoice_id,
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'pcs'                => $data['challan_pcs'][$key] != null ? $data['challan_pcs'][$key] : 0,
                        'cartoon'            => $data['challan_cart'][$key] != null ? $data['challan_cart'][$key] : 0,
                        'quantity'           => $data['challan_quantity'][$key],
                        'branch_id'          => $branch_id,
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $update_challan_item    = ChallanItems::where('invoice_id', $invoice_id)
                                                            ->where('product_id', $product_buy_price['product_id'])
                                                            ->where('product_entry_id', $value)
                                                            ->first();

                    $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] + $data['challan_quantity'][$key];
                    $update_challan_item->save();

                    //update main product stock start
                    $product_buy_price->stock_in_hand     = $product_buy_price['stock_in_hand'] - $data['challan_quantity'][$key];
                    $product_buy_price->total_sold        = $product_buy_price['total_sold'] + $data['challan_quantity'][$key];
                    $product_buy_price->save();
                    //update main product stock end
                }

                DB::table('challan_entries')->insert($challan_entries);

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Sales Created Successfully !!");
                }
                else
                {
                    return redirect()->route('invoices_show_challan', $challan->id);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function showChallan($id)
    {
        $challan    = Challans::find($id);

        $entries    = ChallanEntries::where('challan_entries.challan_id', $id)
                                    ->select('challan_entries.*')
                                    ->get();  
                     
        $user_info  = Users::find(1);

        return view('invoices::challan_show', compact('entries', 'challan', 'user_info'));
    }

    public function deletetChallan($challan_id)
    {
        DB::beginTransaction();

        try{
            $challans     = ChallanEntries::where('challan_id', $challan_id)->get();

            foreach ($challans as $key => $value)
            {
                $update_challan_item                        = ChallanItems::where('invoice_id', $value['invoice_id'])->where('product_entry_id', $value['product_entry_id'])->first();
                $update_challan_item->delivered_quantity    = $update_challan_item['delivered_quantity'] - $value['quantity'];
                $update_challan_item->save();

                $update_product_entries                   = ProductEntries::find($value['product_entry_id']);
                $update_product_entries->stock_in_hand    = $update_product_entries['stock_in_hand'] + $value['quantity'];
                $update_product_entries->save();
            }

            $delete_chalan  = Challans::find($challan_id);

            if ($delete_chalan->delete())
            {   
                DB::commit();
                return back()->with('success', 'Challan deleted successfully');
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function allChallanList()
    {
        $branch_id = Auth::user()->branch_id;
        $challans  = Challans::where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();

        return view('invoices::all_challan_list', compact('challans'));
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Customers::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }
}
