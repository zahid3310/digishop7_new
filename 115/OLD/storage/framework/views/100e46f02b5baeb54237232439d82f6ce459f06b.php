

<?php $__env->startSection('title', 'Edit Users'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Users</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">Edit Users</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('users_update',$user_find['id'])); ?>" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					<?php echo e(csrf_field()); ?>


                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Name *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="<?php echo e($user_find['name']); ?>" name="name" id="name" placeholder="Enter Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">User Name *</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="<?php echo e($user_find['email']); ?>" name="user_name" id="user_name" readonly>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Password</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="" name="password" id="password" placeholder="Enter Password">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Role</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="role">
                                            <?php if(Auth::user()->role == 1 || Auth::user()->id == 1): ?>
                                            <option <?php echo e($user_find['role'] == 1 ? 'selected' : ''); ?> value="1">Super Admin</option>
                                            <?php endif; ?>
                                            <option <?php echo e($user_find['role'] == 2 ? 'selected' : ''); ?> value="2">Admin</option>
                                            <option <?php echo e($user_find['role'] == 3 ? 'selected' : ''); ?> value="3">Employee</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Status</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" name="status">
                                            <option <?php echo e($user_find['status'] == 1 ? 'selected' : ''); ?> value="1">Active</option>
                                            <option <?php echo e($user_find['status'] == 0 ? 'selected' : ''); ?> value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('users_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/39/Modules/Users/Resources/views/edit.blade.php ENDPATH**/ ?>