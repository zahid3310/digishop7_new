<?php
//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\Payments;
use App\Models\Users;
use App\Models\DiscountProducts;
use App\Models\Permissions;
use App\Models\ModulesAccess;
use App\Models\Transactions;
use App\Models\CurrentBalance;

function userAccess($user_id)
{
    $current_route      = Route::currentRouteName();

    $access             = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                ->where('permissions.user_id', $user_id)
                                ->where('urls.url', $current_route)
                                ->first();

    return $access['access_level'];
}

function moduleAccess($user_id)
{
    $access     = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                ->where('modules_access.user_id', $user_id)
                                ->get();

    return $access;
}

function stockOut($data, $item_id)
{
    //Here Item Id is used for update purpose
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {   
            if ($data['is_collected'][$key] != 0)
            {
                $old_item_entry_id[]    = $value['product_entry_id'];
                $old_items_stock[]      = $value['quantity'];
            }
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        {   
            if ($data['is_collected'][$key2] != 0)
            {
                $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
                $quantity_add_to_product_entry->total_sold       = $quantity_add_to_product_entry['total_sold'] - $old_items_stock[$key2];
                $quantity_add_to_product_entry->save();
            }
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        if ($data['is_collected'][$key4] == 0)
        {
            $product_entries                    = ProductEntries::find($value4);
            $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
            $product_entries->total_sold        = $product_entries['total_sold'] + $data['quantity'][$key4];
            $product_entries->save();
        }
    }

    return 0;
}

function stockIn($data, $item_id)
{
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {
            $old_item_entry_id[]    = $value['product_entry_id'];
            $old_items_stock[]      = $value['quantity'];
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        { 
            $quantity_add_to_product_entry                   = ProductEntries::find($value2);
            $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2];
            // $quantity_add_to_product_entry->buy_price        = $data['rate'][$key2];
            $quantity_add_to_product_entry->save();
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4];
        $product_entries->buy_price         = $data['rate'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockInReturn($data, $item_id)
{
    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->total_sold        = $product_entries['total_sold'] - $data['return_quantity'][$key4];
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['return_quantity'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockOutReturn($data, $item_id)
{
    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $product_entries                    = ProductEntries::find($value4);
        $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['return_quantity'][$key4];
        $product_entries->save();
    }

    return 0;
}

function createCustomer($data)
{
    $customer                       = new Customers;
    $customer->name                 = $data['customer_name'];
    $customer->phone                = $data['mobile_number'];
    $customer->address              = $data['address'];
    $customer->nid_number           = $data['nid_number'];
    $customer->alternative_contact  = $data['alternative_mobile_number'];
    $customer->contact_type         = $data['contact_type'];
    $customer->save();

    return $customer['id'];
}

function addPayment($data, $invoice_bill_id, $customer_id ,$type)
{
    $user_id                    = Auth::user()->id;

    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['selling_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 0)
        {
            $payment_entries[] = [
                'invoice_id'        => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 0,  // 0 = In , 1 = Out
                'transaction_head'      => 'sales-return',
                'associated_id'         => $invoice_bill_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_dues                = Invoices::find($invoice_bill_id);
            $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            $update_dues->save();
        }
        else
        {
            $payment_entries[] = [
                'bill_id'           => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 1,  // 0 = In , 1 = Out
                'transaction_head'      => 'purchase-return',
                'associated_id'         => $invoice_bill_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_bill_dues                = Bills::find($invoice_bill_id);
            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'];
            $update_bill_dues->save();
        }

        DB::table('payment_entries')->insert($payment_entries);
        DB::table('account_transactions')->insert($account_transactions);

        return 0;
    }
}

function addPaymentReturn($data, $invoice_bill_id, $customer_id ,$type)
{
    $user_id                    = Auth::user()->id;

    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['return_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 2)
        {
            $payment_entries[] = [
                'sales_return_id'   => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 1,  // 0 = In , 1 = Out
                'transaction_head'      => 'sales-return',
                'associated_id'         => $invoice_bill_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_dues                = SalesReturn::find($invoice_bill_id);
            $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            $update_dues->save();

            if ($data['amount_paid'] > 0)
            {
                $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                $transaction_data['type']           = 0;
                $transaction_data['account_head']   = 'sales-return';
                $transaction_data['transaction_id'] = $invoice_bill_id;
                $transaction_data['customer_id']    = $customer_id;
                $transaction_data['note']           = 'Purchase Return Received';
                $transaction_data['amount']         = $data['amount_paid'];
                $transaction_data['paid_through']   = 1;
                transactions($transaction_data);
            }
        }

        if ($type == 3)
        {
            $payment_entries[] = [
                'purchase_return_id'    => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'amount'                => $data['amount_paid'],
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 0,  // 0 = In , 1 = Out
                'transaction_head'      => 'purchase-return',
                'associated_id'         => $invoice_bill_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_return_dues              = PurchaseReturn::find($invoice_bill_id);
            $update_return_dues->due_amount  = $update_return_dues['due_amount'] - $data['amount_paid'];
            $update_return_dues->save();

            if ($data['amount_paid'] > 0)
            {
                $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                $transaction_data['type']           = 1;
                $transaction_data['account_head']   = 'purchase-return';
                $transaction_data['transaction_id'] = $invoice_bill_id;
                $transaction_data['customer_id']    = $customer_id;
                $transaction_data['note']           = 'Purchase Return Received';
                $transaction_data['amount']         = $data['amount_paid'];
                $transaction_data['paid_through']   = 1;
                transactions($transaction_data);
            }
        }

        DB::table('payment_entries')->insert($payment_entries);
        DB::table('account_transactions')->insert($account_transactions);

        return 0;
    }
}

function userDetails()
{
    $user       = Users::find(1);

    return $user;
}

function stockSellValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['sell_price']);
    }

    return $stock_value;
}

function stockPurchaseValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['buy_price']);
    }

    return $stock_value;
}

function salesReturn($invoice_id)
{
    $returns                = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return.sales_return_date as sales_return_date',
                                                'sales_return.id as id',
                                                'sales_return.invoice_id as invoice_id',
                                                'sales_return.sales_return_number as sales_return_number',
                                                'sales_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'sales_return.return_amount as return_amount',
                                                'sales_return.due_amount as due_amount',
                                                'sales_return.return_note as return_note',
                                                'sales_return.vat_type as vat_type',
                                                'sales_return.total_vat as total_vat',
                                                'sales_return.tax_type as tax_type',
                                                'sales_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                            ->leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                            = $return_entries->where('invoice_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['sales_return_number']  = 'SR - ' . str_pad($value['sales_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['sales_return_date']    = date('d-m-Y', strtotime($value['sales_return_date']));
            $data[$value['id']]['sales_return_id']      = $value['id'];
            $data[$value['id']]['customer_name']        = $value['customer_name'];
            $data[$value['id']]['invoice_id']           = $value['invoice_id'];
            $data[$value['id']]['return_amount']        = $value['return_amount'];
            $data[$value['id']]['paid_amount']          = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']           = $value['due_amount'];
            $data[$value['id']]['return_note']          = $value['return_note'];
            $data[$value['id']]['sub_total']            = $sub_total_value;
            $data[$value['id']]['return_entries']       = $return_entries->where('sales_return_id', $value['id']);

            $total_return_amount                        = $total_return_amount + $value['return_amount'];
            $total_paid_amount                          = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                           = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data                       = [];
        $total_return_amount        = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
        ];
}

function purchaseReturn($bill_id)
{
    $returns                = PurchaseReturn::leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                'purchase_return.id as id',
                                                'purchase_return.bill_id as bill_id',
                                                'purchase_return.purchase_return_number as purchase_return_number',
                                                'purchase_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'purchase_return.return_amount as return_amount',
                                                'purchase_return.due_amount as due_amount',
                                                'purchase_return.return_note as return_note',
                                                'purchase_return.vat_type as vat_type',
                                                'purchase_return.total_vat as total_vat',
                                                'purchase_return.tax_type as tax_type',
                                                'purchase_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                            ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;
    $total_tax_amount           = 0;
    $total_vat_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                                = $return_entries->where('bill_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['purchase_return_number']   = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['purchase_return_date']     = date('d-m-Y', strtotime($value['purchase_return_date']));
            $data[$value['id']]['purchase_return_id']       = $value['id'];
            $data[$value['id']]['customer_name']            = $value['customer_name'];
            $data[$value['id']]['invoice_id']               = $value['invoice_id'];
            $data[$value['id']]['return_amount']            = $value['return_amount'];
            $data[$value['id']]['paid_amount']              = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']               = $value['due_amount'];
            $data[$value['id']]['return_note']              = $value['return_note'];
            $data[$value['id']]['sub_total']                = $sub_total_value;
            $data[$value['id']]['return_entries']           = $return_entries->where('purchase_return_id', $value['id']);

            $total_return_amount                            = $total_return_amount + $value['return_amount'];
            $total_paid_amount                              = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                               = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data = [];
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
            'total_tax_amount'      => $total_tax_amount,
            'total_vat_amount'      => $total_vat_amount,
        ];
}

function discountProducts($discount_id)
{
    $discounts      = DiscountProducts::leftjoin('product_entries', 'product_entries.id', 'discount_products.product_id')
                            ->where('discount_products.discount_id', $discount_id)
                            ->orderBy('discount_products.discount_id', 'DESC')
                            ->select('product_entries.name as product_name')
                            ->get();

    return $discounts;
}

function openingBalanceStore($amount, $customer_id, $contact_type)
{
    if ($contact_type == 0 || $contact_type == 4)
    {
        $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
        $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

        $invoice                    = new Invoices;
        $invoice->invoice_number    = $invoice_number;
        $invoice->customer_id       = $customer_id;
        $invoice->invoice_date      = date('Y-m-d');
        $invoice->invoice_amount    = $amount;
        $invoice->due_amount        = $amount;
        $invoice->total_buy_price   = 0;
        $invoice->total_discount    = 0;
        $invoice->type              = 2;
        $invoice->created_by        = Auth::user()->id;
        $invoice->save();

        $transaction_data['date']           = date('Y-m-d');
        $transaction_data['type']           = 0;
        $transaction_data['account_head']   = 'customer-opening-balance';
        $transaction_data['transaction_id'] = $invoice->id;
        $transaction_data['customer_id']    = $customer_id;
        $transaction_data['note']           = 'Customer Opening Balance';
        $transaction_data['amount']         = $amount;
        $transaction_data['paid_through']   = null;
        transactions($transaction_data);

        customerBalanceUpdate($customer_id);
    }
    else
    {
        $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
        $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

        $bill                       = new Bills;
        $bill->bill_number          = $bill_number;
        $bill->vendor_id            = $customer_id;
        $bill->bill_date            = date('Y-m-d');
        $bill->bill_amount          = $amount;
        $bill->due_amount           = $amount;
        $bill->total_discount       = 0;
        $bill->type                 = 2;
        $bill->created_by           = Auth::user()->id;
        $bill->save();

        $transaction_data['date']           = date('Y-m-d');
        $transaction_data['type']           = 1;
        $transaction_data['account_head']   = 'supplier-opening-balance';
        $transaction_data['transaction_id'] = $bill->id;
        $transaction_data['customer_id']    = $customer_id;
        $transaction_data['note']           = 'Supplier Opening Balance';
        $transaction_data['amount']         = $amount;
        $transaction_data['paid_through']   = null;
        transactions($transaction_data);

        supplierBalanceUpdate($customer_id);
    }

    return 0;
}

function openingBalanceUpdate($amount, $customer_id, $contact_type)
{
    if ($contact_type == 0 || $contact_type == 4)
    {
        $invoice                    = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                                ->where('invoices.customer_id', $customer_id)
                                                ->where('invoices.type', 2)
                                                ->select('invoices.*')
                                                ->first();

        if ($invoice != null)
        {
            $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payment_entries.invoice_id', $invoice['id'])
                                                ->sum('payments.amount');

            $invoice->invoice_amount    = $amount;
            $invoice->due_amount        = $amount - $payment_amount;
            $invoice->updated_by        = Auth::user()->id;
            $invoice->save();
        }
        else
        {
            $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                    = new Invoices;
            $invoice->invoice_number    = $invoice_number;
            $invoice->customer_id       = $customer_id;
            $invoice->invoice_date      = date('Y-m-d');
            $invoice->invoice_amount    = $amount;
            $invoice->due_amount        = $amount;
            $invoice->total_buy_price   = 0;
            $invoice->total_discount    = 0;
            $invoice->type              = 2;
            $invoice->created_by        = Auth::user()->id;
            $invoice->save();
        }

        customerBalanceUpdate($customer_id);
    }
    else
    {
        $bill                       = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                                ->where('bills.vendor_id', $customer_id)
                                                ->where('bills.type', 2)
                                                ->select('bills.*')
                                                ->first();

        if ($bill != null)
        {
            $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
                                                ->where('payments.customer_id', $customer_id)
                                                ->where('payment_entries.bill_id', $bill['id'])
                                                ->sum('payments.amount');

            $bill->bill_amount          = $amount;
            $bill->due_amount           = $amount - $payment_amount;
            $bill->updated_by           = Auth::user()->id;
            $bill->save();
        }
        else
        {
            $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                       = new Bills;
            $bill->bill_number          = $bill_number;
            $bill->vendor_id            = $customer_id;
            $bill->bill_date            = date('Y-m-d');
            $bill->bill_amount          = $amount;
            $bill->due_amount           = $amount;
            $bill->total_discount       = 0;
            $bill->type                 = 2;
            $bill->created_by           = Auth::user()->id;
            $bill->save();
        }

        supplierBalanceUpdate($customer_id);
    }
    
    return 0;
}

function customersTableDetails($id)
{
    $customers  = Customers::find($id);

    return $customers;
}

function ProductVariationName($product_entry_id)
{
    $product            = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations')
                                            ->find($product_entry_id);

    return $product['variations'];
}

function ProductName($product_entry_id)
{
    $product            = ProductEntries::find($product_entry_id);

    return $product['name'];
}

function numberTowords(float $amount)
{
    $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
  $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
  while( $x < $count_length ) {
       $get_divider = ($x == 2) ? 10 : 100;
       $amount = floor($num % $get_divider);
       $num = floor($num / $get_divider);
       $x += $get_divider == 10 ? 1 : 2;
       if ($amount) {
         $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
         $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
         $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
         }else $string[] = null;
       }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paisa' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Taka ' : '') . $get_paise;
}

function previousDues($customer_id)
{
    $data  = Invoices::where('customer_id', $customer_id)->sum('due_amount');

    return $data;
}

function previousDuesBill($supplier_id)
{
    $data  = Bills::where('vendor_id', $supplier_id)->sum('due_amount');

    return $data;
}

function transactions($data)
{
    // $type = 0= debit/paid/receivable, 1= credit/received/receivable
    $branch_id  = Auth::user()->branch_id;  
    $user_id    = Auth::user()->id;  

    $transaction                    = new Transactions;
    $transaction->date              = date('Y-m-d', strtotime($data['date']));
    $transaction->type              = $data['type'];
    $transaction->paid_through      = $data['paid_through'];
    $transaction->account_head      = $data['account_head'];
    $transaction->customer_id       = $data['customer_id'];
    $transaction->note              = $data['note'];
    $transaction->amount            = $data['amount'];
    $transaction->branch_id         = $branch_id;
    $transaction->created_by        = $user_id;

    if ($data['account_head'] == 'sales')
    {
        $transaction->invoice_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'previoue-due-collection')
    {
        $transaction->payment_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'sales-return')
    {
        $transaction->sales_return_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'sales-return-payment')
    {
        $transaction->return_payment_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'purchase')
    {
        $transaction->bill_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'previoue-due-paid')
    {
        $transaction->payment_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'purchase-return')
    {
        $transaction->purchase_return_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'expense')
    {
        $transaction->expense_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'income')
    {
        $transaction->income_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'payment')
    {
        $transaction->payment_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'customer-opening-balance')
    {
        $transaction->invoice_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'supplier-opening-balance')
    {
        $transaction->bill_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'customer-advance-adjustment')
    {
        $transaction->invoice_id = $data['transaction_id'];
    }
    elseif ($data['account_head'] == 'supplier-advance-adjustment')
    {
        $transaction->bill_id = $data['transaction_id'];
    }
    else
    {
    }

    $transaction->save();

    return 0;
}

function customerBalanceUpdate($customer_id)
{
    $data       = Transactions::where('transactions.customer_id', $customer_id)
                                ->select('type', 'amount', 'account_head')
                                ->get();

    $debit1     = $data->where('type', 1)->sum('amount');
    $debit2     = $data->where('type', 1)->where('account_head', 'customer-advance-adjustment')->sum('amount');
    $credit     = $data->where('type', 0)->sum('amount');
    $balance    = $credit - $debit1 + ($debit2*2);

    // dd($balance);

    if ($balance >= 0)
    {
        $customer_update                            = Customers::find($customer_id);
        $customer_update->customer_advance_payment  = 0;
        $customer_update->save();
    }

    if ($balance < 0)
    {
        $customer_update                            = Customers::find($customer_id);
        $customer_update->customer_advance_payment  = abs($balance);
        $customer_update->save();

        $remove_negetive_amount  = "UPDATE `invoices` SET `due_amount`= 0 WHERE `customer_id` = '.$customer_id.'";
    }

    $remove_negetive_amount  = "UPDATE `invoices` SET `due_amount`= 0 WHERE `due_amount` < 0";
    
    return $balance;
}

function supplierBalanceUpdate($customer_id)
{
    $data       = Transactions::where('transactions.customer_id', $customer_id)
                                ->select('type', 'amount', 'account_head')
                                ->get();

    $debit1     = $data->where('type', 1)->sum('amount');
    $debit2     = $data->where('type', 0)->where('account_head', 'supplier-advance-adjustment')->sum('amount');
    $credit     = $data->where('type', 0)->sum('amount');
    $balance    = $debit1 - $credit + ($debit2*2);

    // dd($balance);

    if ($balance >= 0)
    {
        $customer_update                            = Customers::find($customer_id);
        $customer_update->supplier_advance_payment  = 0;
        $customer_update->save();
    }

    if ($balance < 0)
    {
        $customer_update                            = Customers::find($customer_id);
        $customer_update->supplier_advance_payment  = abs($balance);
        $customer_update->save();

        $remove_negetive_amount  = "UPDATE `bills` SET `due_amount`= 0 WHERE `vendor_id` = '.$customer_id.'";
    }

    $remove_negetive_amount  = "UPDATE `bills` SET `due_amount`= 0 WHERE `due_amount` < 0";
    
    return $balance;
}