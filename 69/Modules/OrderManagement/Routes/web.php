<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('ordermanagement')->group(function() {
    Route::get('/quick-order', 'OrderManagementController@index')->name('quick_order');
    Route::get('/order-list', 'OrderManagementController@orderList')->name('order_list');
    Route::post('/store', 'OrderManagementController@store')->name('order_store');
    Route::get('/order/list/load', 'OrderManagementController@orderListLoad')->name('order_list_load');
    Route::get('/cancel-order/{id}', 'OrderManagementController@orderCancel')->name('order_cancel');
    Route::post('/change-order-status', 'OrderManagementController@orderStatusChange')->name('change_order_status');
    Route::get('/edit/{id}', 'OrderManagementController@edit')->name('order_edit');
    Route::post('/update/{id}', 'OrderManagementController@update')->name('order_update');
    Route::get('/customer-make-payment/{id}', 'OrderManagementController@makePayment');
    Route::post('/pay-customer-bill/payments', 'OrderManagementController@storePayment')->name('pay_customer_bill');
});
