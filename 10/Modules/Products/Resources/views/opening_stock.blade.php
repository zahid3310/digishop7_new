@extends('layouts.app')

@section('title', 'Opening Stock')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Opening Stock</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Opening Stock</a></li>
                                    <li class="breadcrumb-item active">Opening Stock</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form action="{{ route('products_opening_stock') }}" method="get">

                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Category </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="category_id" name="category_id" class="form-control select2">
                                                    @if($category_name != null)
                                                    <option value="{{ $category_id }}">{{ $category_name != null ? $category_name->name : '' }}</option>
                                                    @else
                                                    <option value="0">All</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Brand </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="brand_id" name="brand_id" class="form-control select2">
                                                    @if($brand_name != null)
                                                    <option value="{{ $brand_id }}">{{ $brand_name != null ? $brand_name->name : '' }}</option></option>
                                                    @else
                                                    <option value="0">All</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Size </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="size_id" name="size_id" class="form-control select2">
                                                    <option value="0" selected>-- All Sizes --</option>
                                                    @if(!empty($sizes) && ($sizes->count() > 0))
                                                    @foreach($sizes as $key => $value)
                                                        <option {{ isset($_GET['size_id']) && ($_GET['size_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['height'] . ' X ' . $value['width'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Code </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="product_code" name="product_code" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                        @if($product_code_name != null)
                                                        <option value="{{ $product_code }}">{{ $product_code_name != null ? $product_code_name->product_code : '' }}</option>
                                                        @else
                                                        <option value="0">All</option>
                                                        @endif
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>
                                <button style="display: none" type="submit" id="openingStockSubmit">Save</button>
                                </form>

                                <hr>

            					<form id="FormSubmit" action="{{ route('products_opening_stock_store') }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                @if(!empty($product_entries) && ($product_entries->count() > 0))
                                    @foreach($product_entries as $key => $value)
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-8 col-form-label">
                                                {{ $value['product_code'] . ' | ' }}
                                                {{ $value['category_name']. ' | ' }}
                                                {{ $value['size_name']. ' | ' }}
                                                {{ $value['brand_name']. ' | ' }}
                                                {{ $value['name'] }}
                                                <?php
                                                    if ($value['product_type'] == 2)
                                                    {
                                                        echo ' - ' . $value['variations'];
                                                    } 
                                                ?>
                                            </label>
                                            
                                            <div class="col-md-4">
                                                <input class="form-control" type="text" value="" name="stock_in_hand[]" id="stock_in_hand_{{$key}}" placeholder="Enter Opening Stock In SFT" required>
                                                <input class="form-control" type="hidden" value="{{ $value['id'] }}" name="product_entry_id[]">
                                                <input class="form-control" type="hidden" value="{{ $value['product_id'] }}" name="product_id[]">
                                            </div>
                                        </div>
                                    @endforeach

                                    <div class="form-group row">
                                        <div class="col-md-8"></div>
                                        <div class="button-items col-md-2 pull-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">Close</a></button>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                @else
                                <div class="form-group row">
                                    <div style="text-align: center" class="col-md-12">
                                        <h6>No Product Available For Adding Opening Stock.</h6>
                                    </div>
                                </div>
                                @endif
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            var site_url  = $('.site_url').val();

            $("#category_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#brand_id").select2({
                ajax: { 
                url:  site_url + '/reports/item-list/brand-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $("#product_code").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-code-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }

        function searchPayment()
        {
            $('#openingStockSubmit').click();
        }
    </script>
@endsection