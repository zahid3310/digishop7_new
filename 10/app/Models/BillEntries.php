<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BillEntries extends Model
{
    protected $table = "bill_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Models\Customers','vendor_id');
    }

    public function bill()
    {
        return $this->belongsTo('App\Models\Bills','bill_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Units','main_unit_id');
    }

    public function convertedUnit()
    {
        return $this->belongsTo('App\Models\Units','conversion_unit_id');
    }
}
