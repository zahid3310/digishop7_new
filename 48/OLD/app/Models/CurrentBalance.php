<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class CurrentBalance extends Model
{  
    protected $table = "current_balance";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function transferFrom()
    {
        return $this->belongsTo('App\Models\PaidThroughAccounts','transfer_from');
    }

    public function transferTo()
    {
        return $this->belongsTo('App\Models\PaidThroughAccounts','transfer_to');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','associated_id');
    }

    public function salesReturn()
    {
        return $this->belongsTo('App\Models\SalesReturn','associated_id');
    }

    public function bill()
    {
        return $this->belongsTo('App\Models\Bills','associated_id');
    }

    public function purchaseReturn()
    {
        return $this->belongsTo('App\Models\PurchaseReturn','associated_id');
    }

    public function income()
    {
        return $this->belongsTo('App\Models\Incomes','associated_id');
    }

    public function expense()
    {
        return $this->belongsTo('App\Models\Expenses','associated_id');
    }
}
