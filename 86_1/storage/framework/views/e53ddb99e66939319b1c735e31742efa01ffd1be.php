<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="<?php echo e(route('dashboard')); ?>" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>
                
                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Booking Management</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('quick_order')); ?>">Take Booking</a> </li>
                        <li> <a href="<?php echo e(route('order_list')); ?>">List of Booking</a> </li>
                        <li> <a href="<?php echo e(route('community_service_index')); ?>">Community Service</a> </li>
                        <li> <a href="<?php echo e(route('sms_list')); ?>">Booking SMS List</a> </li>
                    </ul>
                </li>

                
                <li class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Purchase</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('bills_index')); ?>">Purchase Item</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('bills_all_bills')); ?>">List of Purchase</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('purchase_return_index')); ?>">Purchase Return</a> </li>
                        <li> <a class="<?php echo e(Request::getQueryString() == 'payment_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('payments_create').'?payment_type=1'); ?>">Bill Wise Payment</a> </li>
                    </ul>
                </li>
                <!--
                <li class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Use Inventory</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li style="display:none;"> <a href="<?php echo e(route('quick_sales')); ?>">Quick Sales</a> </li>
                        <li> <a href="<?php echo e(route('invoices_index')); ?>">Daily Use</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : ''); ?>" href="<?php echo e(route('invoices_all_sales')); ?>">List of Use</a> </li>

                        <li style="display: none;"> <a class="<?php echo e(Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : ''); ?>"  href="<?php echo e(route('sales_return_index')); ?>">Use Return</a> </li>

                        <li style="display: none;"> <a class="<?php echo e(Request::getQueryString() == 'payment_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('payments_create').'?payment_type=0'); ?>">Invoice Wise Collection</a> </li>
                    </ul>
                </li>
                -->
                
                 <li class="">
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Use Inventory</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('daily_use')); ?>">Daily Use</a> </li>
                        <li> <a href="<?php echo e(route('all_use_list')); ?>">List of Use</a> </li>
                    </ul>
                </li>



                <li class="<?php echo e(Route::currentRouteName() == 'damages_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_transfer_to_dp_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'damages_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'damages_transfer_to_dp_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-dumpster"></i><span>Damages</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Direct Damage
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('damages_create')); ?>">New Damage</a> </li>
                                <li> <a href="<?php echo e(route('damages_index')); ?>">List Of Damages</a> </li>
                            </ul>
                        </li>
                    </ul>

                </li>

                <li  class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('expenses_index')); ?>">Expenses</a> </li>

                        <li> <a class="<?php echo e(Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('incomes_index')); ?>">Incomes</a> </li>

                        <li> <a class="<?php echo e(Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('balance_transfer_index')); ?>">Balance Transfer</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('messages_send_index')); ?>">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Booking
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('booking_monthly_report_index')); ?>">Monthly Report</a> </li>
                            </ul>
                        </li>
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Inventory
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('inventory_statement_index')); ?>">Statement of Use.</a> </li>
                                <li> <a href="<?php echo e(route('inventory_summary_index')); ?>">Use Summary</a> </li>
                                <li> <a href="<?php echo e(route('inventory_product_wise_summary_index')); ?>">Product Wise Use</a> </li>
                            </ul>
                        </li>
                        
                        <li class="" style="display: none;">
                            <a class="has-arrow waves-effect">
                                Sales
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('sales_statement_index')); ?>">Statement of Sales</a> </li>
                                <li> <a href="<?php echo e(route('sales_summary_index')); ?>">Sales Summary</a> </li>
                                <li> <a href="<?php echo e(route('product_wise_sales_report_index')); ?>">Product Wise Sales</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Purchase
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('purchase_statement_index')); ?>">Statement of Pur.</a> </li>
                                <li> <a href="<?php echo e(route('purchase_summary_index')); ?>">Purchase Summary</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('expense_report_index')); ?>">List of Expense</a> </li>
                                <li> <a href="<?php echo e(route('income_report_index')); ?>">List of Income</a> </li>
                                <li> <a href="<?php echo e(route('income_report_summary_index')); ?>">Income Report Summary</a> </li>
                                <li> <a href="<?php echo e(route('expense_report_summary_index')); ?>">Expense Report Summary</a> </li>
                                <li> <a href="<?php echo e(route('monthly_profit_Loss_summary_index')); ?>">Monthly Profit/Loss</a> </li>
                                <!--<li> <a href="<?php echo e(route('income_statement_index')); ?>">Income Statement</a> </li>-->
                                <!-- <li> <a href="<?php echo e(route('collection_report_index')); ?>">Daily Report</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('customer_payment_report_index')); ?>">Customer Payments</a> </li>
                                <li> <a href="<?php echo e(route('supplier_payment_report_index')); ?>">Supplier Payments</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                MIS
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('current_balance_index')); ?>">Current Balance</a> </li>
                                <li> <a href="<?php echo e(route('stock_report_index')); ?>">Stock Status</a> </li>
                                <li> <a href="<?php echo e(route('due_report_customer_index')); ?>">Buyer Ledger</a> </li>
                                <li> <a href="<?php echo e(route('due_report_supplier_index')); ?>">Supplier Ledger</a> </li>
                                <li> <a href="<?php echo e(route('due_ledger_index')); ?>">Due Ledger</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Basic Report
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('item_list_index')); ?>">List of Items</a> </li>
                                <li> <a href="<?php echo e(route('emergency_item_list_index')); ?>">Emergency Purchase</a> </li>
                                <li> <a href="<?php echo e(route('product_suppliers_index')); ?>">Item Wise Supplier</a> </li>
                                <li> <a href="<?php echo e(route('product_customers_index')); ?>">Item Wise Customer</a> </li>
                                <li> <a href="<?php echo e(route('register_list_index').'?type=0'); ?>" target="_blank">Buyer List</a> </li>
                                <li> <a href="<?php echo e(route('register_list_index').'?type=1'); ?>" target="_blank">Supplier List</a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="<?php echo e(route('salary_report_index')); ?>">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">

                        <li class="<?php echo e(Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Inventory
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="" href="<?php echo e(route('products_index_all')); ?>">List Of Inventory</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_index')); ?>">Add Inventory</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_category_index')); ?>">Add Categories</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('categories_index')); ?>" href="<?php echo e(route('categories_index')); ?>">Brand/Manufacturer</a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_units_index')); ?>">Add Unit Measure</a> </li>
                                <li style="display: none;"> <a class="<?php echo e(Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_variations_index')); ?>">Product Variations</a> </li>
                                <li style="display: none;"> <a href="<?php echo e(route('products_barcode_print')); ?>">Print Barcode</a> </li>
                                <li style="display: none;"> <a href="<?php echo e(route('products_opening_stock')); ?>">Bulk Opening Stock</a> </li>
                                <li style="display: none;"> <a href="<?php echo e(route('products_bulk_product_list_update')); ?>">Bulk Product Update</a> </li>
                            </ul>
                        </li>

                        <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=0'); ?>">Add Customer</a> </li>
                                <li style="display: none;"> <a class="<?php echo e(Request::getQueryString() == 'contact_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=1'); ?>">Add Supplier</a> </li>
                                <li > <a class="<?php echo e(Request::getQueryString() == 'contact_type=2' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=2'); ?>">Add Employee</a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=3'); ?>">Add Reference</a> </li>
                            </ul>
                        </li>

                        <li  class="<?php echo e(Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'paid_through_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'paid_through_accounts_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('paid_through_accounts_index')); ?>">Paid Through</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('messages_index')); ?>">Create Text Message</a> </li>
                                <li> <a href="<?php echo e(route('messages_phone_book_index')); ?>">Phone Book</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                 <li> <a href="<?php echo e(route('branch_index')); ?>">Add Branch</a> </li>
                                <li> <a href="<?php echo e(route('users_index')); ?>">Add User</a> </li>
                                <li> <a href="<?php echo e(route('users_index_all')); ?>">List of User</a> </li>
                                <li> <a href="<?php echo e(route('set_access_index')); ?>">Set Permission</a> </li>
                            </ul>
                        </li>
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div><?php /**PATH /home/digishop7/public_html/86_1/resources/views/layouts/headers.blade.php ENDPATH**/ ?>