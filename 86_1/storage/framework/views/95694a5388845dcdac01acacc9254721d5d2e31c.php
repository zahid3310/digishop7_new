<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login | Cyberdyne Technology Ltd.</title>
    <meta name="keywords" content="pos software,inventory software,showroom software,Best software company in bangladesh">
    <meta name="Developed By" content="Cyberdyne Technology Ltd." />
    <meta name="Developer" content="Cyberdyne Technology Team | 01715317133" />
    <meta property="fb:pages" content="114553850372821" />
    <meta name="description" content="This is one of the largest online platform for Point Of Sales Software in Bangladesh." />
    <meta property="og:url" content="https://cyberdynetechnologyltd.com" />
    <meta property="og:type" content="WEBSITE" />
    <meta property="og:title" content="Cyberdyne | Point Of Sales Software" />
    <meta property="og:description" content="This is one of the largest online platform for Point Of Sales Software in Bangladesh." />
    <meta property="og:site_name" content="Cyberdyne | Point Of Sales Software" />

    <!-- <meta name="twitter:title" content="Cyberdyne | Point Of Sales Software"> -->
    <!-- <meta name="twitter:card" value="summary_large_image"> -->
    <!-- <meta name="twitter:description" content="This is one of the largest online platform for Point Of Sales Software in Bangladesh."> -->
    <!-- <meta name="twitter:site" content="@Cyberdyne" /> -->
    <!-- <meta name="twitter:creator" content="@Cyberdyne" /> -->
    <!-- <meta name="twitter:url" content="https://Cyberdynetechnologyltd.com" /> -->
    <link rel="canonical" href="https://Cyberdynetechnologyltd.com">
    <!-- <meta property="og:image" content="https://Cyberdynetechnologyltd.com/one_page_images/cover_photo.png" /> -->
    <!-- <meta name="twitter:image" content="https://Cyberdynetechnologyltd.com/one_page_images/cover_photo.png"> -->
    <meta property="og:image:width" content="700" />
    <meta property="og:image:height" content="400" />
    <link rel="icon" type="image/png" href="<?php echo e(url('/public/favicon.png')); ?>" />
    <link rel="stylesheet" href="<?php echo e(url('public/login_form_assets/css/vendor/flaticon.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('public/login_form_assets/css/custom/all.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('public/login_form_assets/css/vendor/bootstrap.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('public/login_form_assets/css/custom/main.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(url('public/login_form_assets/css/custom/signin-up.css')); ?>">
</head>

<body>
    <section class="sign-part">
        <div class="sign-content">
            <div class="content-cover">
                <!-- <a href="signin-up.php"><img src="assets/img/logo/logo.png" alt="logo"></a> -->
                <em>Wellcome To</em>
                <h1>COMMUNITY CENTER</h1>
            </div>
        </div>
        <div class="sign-form">
            <div class="tab-pane active" id="signin">
                <div class="sign-heading">
                    <h2 id="organizationName"></h2>
                    <p>Use credentials to access your account.</p>
                </div>
                <form method="POST" action="<?php echo e(route('login')); ?>">
                <?php echo csrf_field(); ?>
                    <label class="form-label">
                        <input type="text" name="email" placeholder="Phone Number" value="<?php echo e(old('email')); ?>">
                        <!-- <small>Please follow this example - 01715317133</small> -->
                        <?php $__errorArgs = ['email'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </label>

                    <label class="form-label">
                        <input type="password" name="password" placeholder="Password" required autocomplete="current-password">
                        <button type="button"><i class="eye fas fa-eye"></i></button>
                        <!-- <small>Password must be 6 characters</small> -->
                        <?php $__errorArgs = ['password'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?>
                            <span class="invalid-feedback" role="alert">
                                <strong><?php echo e($message); ?></strong>
                            </span>
                        <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>
                    </label>

                    <label class="form-link">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="signin-check">
                            <label class="custom-control-label" for="signin-check">Remember me</label>
                        </div>
                        <a href="#">Forgot password?</a>
                    </label>

                    <label class="form-btn">
                        <button type="submit" class="btn btn-outline">sign in now</button>
                    </label>
                    <input type="hidden" class="site_url" value="<?php echo e(url('/')); ?>" />
                </form>
                <br>
                <div class="form-bottom">
                   <p>Don't have an account? Please contact your super admin.</p>
                </div>

                <div style="bottom: 20px;position: fixed;text-align: center">
                   <?php echo e('@'.date('Y')); ?> Cyberdyne Technology Ltd. | +88 01748 715 715 | <a href="https://cyberdynetechnologyltd.com/" target="_blank">Cyberdyne Technology Ltd.</a>
                </div>
            </div>
        </div>
   </section>

   <script src="<?php echo e(url('public/login_form_assets/js/vendor/jquery-1.12.4.min.js')); ?>"></script>
   <script src="<?php echo e(url('public/login_form_assets/js/vendor/popper.min.js')); ?>"></script>
   <script src="<?php echo e(url('public/login_form_assets/js/vendor/bootstrap.min.js')); ?>"></script>
   <script src="<?php echo e(url('public/login_form_assets/js/custom/main.js')); ?>"></script>

   <script type="text/javascript">
       $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $.get(site_url + '/login-page-info', function(data){

                $("#organizationName").html(data.organization_name);
            });
        });
   </script>
</body>

</html><?php /**PATH /home/digishop7/public_html/86_1/resources/views/auth/login.blade.php ENDPATH**/ ?>