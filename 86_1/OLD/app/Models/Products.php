<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = "products";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Categories','category_id');
    }

    public function subCategory()
    {
        return $this->belongsTo('App\Models\Subcategories','sub_category_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Items','item_id');
    }

    public function productEntries()
    {
        return $this->hasMany(ProductEntries::class, "product_id");
    }
}
