@extends('layouts.app')

@section('title', 'User Access')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">User Access</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">User Access</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                        @if(Session::has('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {!! Session::get('success') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('unsuccess'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! Session::get('unsuccess') !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        @if(Session::has('errors'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            {!! 'Some required fields are missing..!! Please try again..' !!}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        @endif

                        <form method="get" action="{{ route('set_access_index') }}" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-8 form-group">
                                    <label for="productname">User *</label>
                                    <select id="user_id" style="width: 100%;cursor: pointer" class="form-control select2" name="user_id" onchange="findUser()">
                                        <option value="">--Select User--</option>
                                        @if(!empty($users))
                                            @foreach($users as $key => $user)
                                            @if($user['id'] != 1)
                                            <option {{ $user['id'] == $user_id ? 'selected' : '' }} value="{{ $user->id }}">{{ $user->name }}</option>
                                            @endif
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div style="padding-left: 50px" class="col-lg-4 col-md-4 col-sm-4 col-4">
                                    <br>
                                    <br>
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input allPermission" id="all_permission" onchange="setAllPermission()">
                                        <label class="custom-control-label" for="all_permission">Set All Permission</label>
                                    </div>
                                </div>

                                <button id="click_user" style="display: none" type="submit" class="btn btn-success inner"></button>
                            </div>
                        </form>    

                        @if($user_id != 0)
                        
                        <!-- <hr style="margin-top: 0px !important"> -->

                        <form id="FormSubmit" action="{{ route('set_access_update') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                        {{ csrf_field() }}

                            <input type="hidden" name="user_id" value="{{ isset($_GET['user_id']) ? $_GET['user_id'] : '' }}">

                            <div class="row">
                                <div style="display: none" class="col-lg-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 style="text-align: center" class="card-title">All Modules Display Permission</h4>
                                            <hr>
                                            @if((!empty($module_permissions)) && ($module_permissions->count() > 0))
                                            <?php $key421 = 0; ?>
                                            @foreach($module_permissions as $key420 => $value420)
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-12 col-12 form-group">
                                                    @if($key421 == 0)
                                                        <label class="hidden-xs" for="productname">Module Name *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Module Name *</label>
                                                    <select id="module_id_{{$key420}}" style="width: 100%;cursor: pointer" class="form-control" name="module_id[]">
                                                        <option value="{{ $value420['module_id'] }}">{{ $value420['module_name'] }}</option>
                                                    </select>
                                                </div>

                                                <div class="col-lg-4 col-md-4 col-sm-12 col-12 form-group">
                                                    @if($key421 == 0)
                                                        <label class="hidden-xs" for="productname">Access *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Access *</label>
                                                    <select id="module_permission_{{$key420}}" style="width: 100%;cursor: pointer" class="form-control" name="module_permission[]">
                                                        <option {{ $value420['display'] == 1 ? 'selected' : '' }} value="1">Show</option>
                                                        <option {{ $value420['display'] == 0 ? 'selected' : '' }} value="0">Hide</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php $key421++; ?>
                                            @endforeach
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                @if((!empty($modules)) && ($modules->count() > 0))
                                @foreach($modules as $key1 => $value1)
                                    <div class="col-lg-3">
                                        <div class="card">
                                            <div style="height : 350px" class="card-body">
                                                <h4 style="text-align: center" class="card-title">{{ $value1['name'] }}</h4>

                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input moduleId_{{$value1['id']}} allPermission" id="module_id_500{{$value1['id']}}" onchange="selectAllRoutes({{$value1['id']}})">
                                                    <label class="custom-control-label" for="module_id_500{{$value1['id']}}">All</label>
                                                </div>

                                                <hr>

                                                @if((!empty($permissions)) && ($permissions->count() > 0))
                                                <?php $key22 = 0; ?>
                                                @foreach($permissions as $key => $value)
                                                @if($value1['id'] == $value['module_id'])
                                                <div class="row">

                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input moduleId_{{$value['module_id']}} allPermission" name="url_id[]" id="url_id_{{$value['id']}}" value="{{ $value['url_id'] }}" @if($value['access_level'] == 1) checked @endif>
                                                        <label class="custom-control-label" for="url_id_{{$value['id']}}">{{ $value['url_name'] }}</label>
                                                    </div>

                                                </div>
                                                <?php $key22++; ?>
                                                @endif
                                                @endforeach
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                @endif
                            </div>

                            <hr style="margin-top: 0px">

                            <div class="form-group row">
                                <div class="button-items col-md-12">
                                    <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                    <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('expenses_index') }}">Close</a></button>
                                </div>
                            </div>
                        </form>
                        @endif
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        function findUser()
        {
            $("#click_user").click();
        }

        function selectAllRoutes(module_id)
        {
            var chk = $('.moduleId_'+module_id).prop('checked');

            if(chk == true)
            {
                $('.moduleId_'+module_id).prop('checked',true);
            }
            else
            {
                $('.moduleId_'+module_id).prop('checked',false);
            }
        }

        function setAllPermission()
        {
            var chk = $('#all_permission').prop('checked');

            if(chk == true)
            {
                $('.allPermission').prop('checked',true);
            }
            else
            {
                $('.allPermission').prop('checked',false);
            }
        }
    </script>
@endsection