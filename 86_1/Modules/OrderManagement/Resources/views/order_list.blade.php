@extends('layouts.app')

@section('title', 'All Booking List')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Booking List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Booking Managaement</a></li>
                                    <li class="breadcrumb-item active">All Booking List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div style="min-height: 300px" class="card">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <div class="card-body table-responsive">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Booking From Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Booking To Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Customer </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="customer_id" class="form-control select2">
                                                    <option value="0">All</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Program From Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_program_from_data" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Program To Date</label>
                                            <div class="col-md-8">
                                                <input style="cursor: pointer" id="search_program_to_data" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" for="productname" class="col-md-4 col-form-label">Status </label>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <select style="width: 73%" id="status" name="status" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                       <option value="0">All</option>
                                                       <option value="1">Pending</option>
                                                       <option value="2">Confirm</option>
                                                       <option value="3">Cancel</option>
                                                    </select>
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPayment()">
                                                        <i class="bx bx-search font-size-24"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Booking Date</th>
                                            <th>Program From Date</th>
                                            <th>Program To Date</th>
                                            <th>Customer</th>
                                            <th>Amount</th>
                                            <th>Paid</th>
                                            <th>Discount</th>
                                            <th>Due</th>
                                            <th>Booking Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="invoice_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div style="padding: 0px" class="form-group col-md-3 col-3">
                            <label for="example-text-input" class="col-md-12 col-form-label">Receivable</label>
                            <div class="col-md-12">
                                <input id="receivable" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-3 col-3">
                            <label for="example-text-input" class="col-md-12 col-form-label">Received</label>
                            <div class="col-md-12">
                                <input id="received" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-3 col-3">
                            <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                            <div class="col-md-12">
                                <input id="dues" type="text" class="form-control" readonly>
                            </div>
                        </div>
                        
                         <div style="padding: 0px" class="form-group col-md-3 col-3">
                            <label for="example-text-input" class="col-md-12 col-form-label">Discount</label>
                            <div class="col-md-12">
                                <input id="discount"  type="text" class="form-control">
                            </div>
                        </div>
                        
                    </div>

                    <hr>

                    <input id="customer_id_payment" name="customer_id" type="hidden">
                    <input id="find_invoice_id" name="invoice_id" type="hidden">

                    <div class="form-group row">
                        <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                        <div class="col-md-10">
                            <input style="cursor: pointer" id="payment_date" name="payment_date" type="date" value="<?= date("Y-m-d") ?>" class="form-control" required>
                        </div>
                    </div>

                    <div class="inner-repeater">
                        <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                            <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="changeStatus" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Change Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <form action="{{route('change_order_status')}}" method="post">
                    @csrf

                    <div class="row">
                        <div style="padding: 0px" class="form-group col-md-12 col-12">
                            <div class="col-md-12">
                                <input id="invoice_id" type="hidden" class="form-control" readonly="" name="invoice_id">
                            </div>
                        </div>



                        <div style="padding: 0px" class="form-group col-md-12 col-12">
                            <label for="example-text-input" class="col-md-12 col-form-label">Processing Status</label>
                            <div class="col-md-12">
                                <select class="form-control" required="" name="order_status">
                                    <option value="2">Confirm</option>
                                    <option value="3">Cancel</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <br>

                    <div class="modal-footer">
                        <button type="submit"class="btn btn-primary waves-effect waves-light">Make Change</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>


<div id="payslipModal" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Payslip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div id="printMe">
                    <center><h3>Customer Payslip</h3></center>
                    <hr>
                    <div class="row">
                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-12">
                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ Auth::user()->organization_name }}</h2>
                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ Auth::user()->address }}</p>
                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ Auth::user()->contact_number }}</p>
                    </div>
                    </div>

                    

                    <div class="row">
                        
                        
                        <hr>
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th scope="col">Customer Name</th>
                              <th scope="col">Receivable Amount</th>
                              <th scope="col">Total Received</th>
                              <th scope="col">Paid Amount</th>
                              <th scope="col">Discount Amount</th>
                              <th scope="col">Dues Amount</th>
                              <th scope="col">Payment Date</th>
                              <th scope="col">Paid Through</th>
                            </tr>
                          </thead>
                          <tbody id="payData">
                            
                          </tbody>
                        </table>
                        
                    </div>

                    <br><br><br>

                    <div class="row">
                        <div class="col-md-6">
                            <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Organization Sign </span> </h6>
                        </div>
                        <div class="col-md-6">
                            <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign</span> </h6>
                        </div>
                    </div>

                    </div>

                  <br><br>
                        <div class="d-print-none">
                            <div class="float-right">
                                <!-- <button id='btn' class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></button> -->
                                <button onclick="printDiv('printMe')" type="button" class="float-right btn btn-success">Print</button>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#invoice_number").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/invoices-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });

            $.get(site_url + '/ordermanagement/order/list/load', function(data){

                invoiceList(data);   
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var site_url                    = $('.site_url').val();
            var search_from_date            = $('#search_from_date').val();
            var search_to_date              = $('#search_to_date').val();
            var search_program_from_data    = $('#search_program_from_data').val();
            var search_program_to_data      = $('#search_program_to_data').val();
            var search_customer             = $('#customer_id').val();
            var search_status               = $('#status').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }
            
            
             if (search_program_from_data != '')
            {
                var program_from_date = $('#search_program_from_data').val();
            }
            else
            {
                var program_from_date = 0;
            }

            if (search_program_to_data != '')
            {
                var program_to_date = $('#search_program_to_data').val();
            }
            else
            {
                var program_to_date = 0;
            }

            if (search_customer != '')
            {
                var customer = $('#customer_id').val();
            }
            else
            {
                var customer = 0;
            }

            if (search_status != '')
            {
                var status = $('#status').val();
            }
            else
            {
                var status = 0;
            }

            $.get(site_url + '/ordermanagement/order/search/list/' + from_date + '/' + to_date + '/' + program_from_date + '/' + program_to_date + '/' + customer + '/' +status, function(data){

                invoiceList(data);

            });
        }
    </script>

    <script type="text/javascript">
        function changeStatus(invoice_id)
        {
            $("#invoice_id").val(invoice_id);
        }
    </script>

    <script type="text/javascript">
        function findInvoice(invoice_id)
        {
            var site_url        = $('.site_url').val();
            $('.remove_field_payment').click();
            $('#add_field_button_payment').click();

            $.get(site_url + '/ordermanagement/customer-make-payment/' + invoice_id, function(data){

                $("#receivable").empty();
                $("#receivable").val(data.order_amount);
                
                $("#discount").empty();
                $("#discount").val(data.discount);

                $("#received").empty();
                $("#received").val(parseFloat(data.order_amount) - parseFloat(data.due_amount));

                $("#dues").empty();
                $("#dues").val(data.due_amount);

                $("#customer_id_payment").empty();
                $("#customer_id_payment").val(data.customer_id);

                $("#find_invoice_id").empty();
                $("#find_invoice_id").val(invoice_id);
            });
        }

        

        function calculateDues()
        {
            var due_amount  = $("#dues").val();
            var amount      = $("#amount").val();

            if (parseFloat(amount) > parseFloat(due_amount))
            {
                $("#amount").val(due_amount);
            }
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var invoice_id          = $("#find_invoice_id").val();
            var customer_id         = $("#customer_id_payment").val();
            var receivable          = $("#receivable").val();
            var received            = $("#received").val();
            var discount            = $("#discount").val();
            var payment_date        = $("#payment_date").val();
            var note                = $("#note").val();
            var account_information = $("#account_information").val();
            var site_url            = $('.site_url').val();

            console.log(received);

            var amount_paid = [];
            $('input[name^="amount_paid"]').each(function() {
                amount_paid.push(this.value);
            })

            var paid_through = [];
            $('.paidThrough').each(function() {
                paid_through.push(this.value);
            })

            var note = [];
            $('input[name^="note"]').each(function() {
                note.push(this.value);
            })

            var account_information = [];
            $('input[name^="account_information"]').each(function() {
                account_information.push(this.value);
            })

            if (customer_id == '' || invoice_id == '' || receivable == '' || payment_date == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
    
                type:   'post',
                url:    site_url + '/ordermanagement/pay-customer-bill/payments',
                data:   {  invoice_id : invoice_id,customer_id : customer_id, receivable : receivable,discount: discount, payment_date : payment_date, amount_paid : amount_paid, paid_through : paid_through, note : note, account_information : account_information, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {

                        $("#payData").empty();
                        $('#CloseButton').click();
                        $('.remove_field_payment').click();

                        var site_url  = $('.site_url').val();

                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $.get(site_url + '/ordermanagement/customer-pay-slip/' + invoice_id, function(result){

                        if (result.discount == null) 
                        {
                            var discountAmount = '0.00';
                        }else
                        {
                            var discountAmount = result.discount;
                        }

                        $("#payslipModal").modal("toggle");
                        $("#payData").append(`<tr>
                            <td>`+result.customer_name+`</td>
                            <td>`+result.order_amount+`</td>
                            <td>`+result.cash_given+`</td>
                            <td>`+data.amount+`</td>
                            <td>`+discountAmount+`</td>
                            <td>`+result.due_amount+`</td>
                            <td>`+data.payment_date+`</td>
                            <td>`+data.paid_through_name+`</td>
                            </tr>`);
                        });

                        $('#success_message').show();
                        
                    }
                    else
                    {
                        var site_url  = $('.site_url').val();
                        $.get(site_url + '/ordermanagement/order/list/load', function(data){
                            invoiceList(data);
                        });

                        $('#unsuccess_message').show();
                    }
                }
    
            });
            }
        });    
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 50;                           //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields_payment)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var account_info_label  = '<label class="hidden-xs" for="productname">Account Information</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var account_info_label  = '';
                    var note_label          = '';
                    var action_label        = '';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+x+'">' +
                                                    '<div style="" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="number" name="amount_paid[]" class="inner form-control amountPaid" id="amount_paid_'+x+'" value="0" required oninput="checkOverPayment('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+x+'" style="cursor: pointer" name="paid_through[]" class="form-control paidThrough">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Account Information</label>\n' +
                                                        account_info_label +
                                                        '<input type="text" name="account_information[]" class="form-control" id="account_information_'+x+'" placeholder="Account Information"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    '<div style="" class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_payment" data-val="'+x+'">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_payment_'+x).remove(); x--;
        });
    </script>

    <script type="text/javascript">
        function invoiceList(data)
        {
            var invoice_list = '';
            var sl_no        = 1;
            $.each(data, function(i, invoice_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var edit_url            = site_url + '/ordermanagement/edit/' + invoice_data.id;
                var print_url_pos       = site_url + '/ordermanagement/show/' + invoice_data.id;
                var cancel_url          = site_url + '/ordermanagement/cancel-order/' + invoice_data.id;

                if (invoice_data.customer_name != null)
                {
                    var customer  = invoice_data.customer_name;
                }
                else
                {
                    var customer  = invoice_data.contact_name;
                }

                if (invoice_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (invoice_data.order_status == 1)
                {
                    var order_status  = '<span style="color:#ff9800;font-weight:bold">Pending</span>';
                }
                else if(invoice_data.order_status == 2)
                {
                    var order_status  = '<span style="color:#00bcd4;;font-weight:bold">Confirm</span>';
                }
                else if(invoice_data.order_status == 3)
                {
                    var order_status  = '<span style="color:red;font-weight:bold">Cancel</span>';
                }
                
                if (invoice_data.discount != null)
                {
                    var discount_amount  = invoice_data.discount;
                }
                else
                {
                    var discount_amount  = '0.00';
                }

                invoice_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(invoice_data.order_date) +
                                    '</td>' +

                                    '<td>' +
                                       formatDate(invoice_data.program_from_data) +
                                    '</td>' +

                                    '<td>' +
                                       formatDate(invoice_data.program_to_data) +
                                    '</td>' +

                                    '<td>' +
                                        customer + 
                                    '</td>' +
                                    '<td>' +
                                       invoice_data.order_amount +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(invoice_data.order_amount) - parseFloat(invoice_data.due_amount)) +
                                    '</td>' +
                                     '<td>' +
                                       (parseFloat(discount_amount)) +
                                    '</td>' +

                                    '<td>' +
                                       (invoice_data.due_amount - discount_amount) +
                                    '</td>' +

                                    '<td>' + order_status +
                                    '</td>' +

                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                
                                                '<a  class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                
                                                '<a  class="dropdown-item" href="' + print_url_pos +'" target="_blank">' + 'Print' + '</a>' +

                                                '<a class="dropdown-item" href="" data-toggle="modal" data-target="#myModal1" onclick="findInvoice('+ invoice_data.id +')">' + 'Make Payment' + '</a>' +

                                                  '<a class="dropdown-item order-action" href="" data-toggle="modal" data-target="#changeStatus" onclick="changeStatus('+ invoice_data.id+')">' + 'Change Order Status' + '</a>' +

                                                '<a class="dropdown-item" href="' + cancel_url +'">' + 'Cancel' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#invoice_list").empty();
            $("#invoice_list").append(invoice_list);
        }

        function checkOverPayment(x)
        {
            var dAmount           = $("#dues").val();
            var pAmount           = $("#amount_paid_"+x).val();

            var amount_left       = parseFloat(dAmount);
            var paid              = 0;

            if (Math.sign(parseFloat(amount_left)) == -1)
            {
                $("#amount_paid_"+x).val(parseFloat(pAmount));

                $('.amountPaid').each(function()
                {  
                    amount_left     -= parseFloat($(this).val());
                    paid            += parseFloat($(this).val());
                });
            }

            if (Math.sign(parseFloat(amount_left)) == 1)
            {
                $("#amount_paid_"+x).val(parseFloat(pAmount));

                $('.amountPaid').each(function()
                {  
                    amount_left     -= parseFloat($(this).val());
                    paid            += parseFloat($(this).val());
                });
            }

            if (parseFloat(amount_left) < 0)
            {
                $("#amount_paid_"+x).val(0);
            }
        }
    </script>

    <script type="text/javascript">
      function printDiv(divName){
          var printContents = document.getElementById(divName).innerHTML;
          var originalContents = document.body.innerHTML;

          document.body.innerHTML = printContents;

          window.print();

          document.body.innerHTML = originalContents;

      }
  </script>
@endsection