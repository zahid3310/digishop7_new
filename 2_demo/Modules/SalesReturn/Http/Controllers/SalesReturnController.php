<?php

namespace Modules\SalesReturn\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\PaidThroughAccounts;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\AccountTransactions;
use App\Models\UnitConversions;
use App\Models\Transactions;
use App\Models\CurrentBalance;
use App\Models\ReturnPayments;
use Validator;
use Auth;
use Response;
use DB;

class SalesReturnController extends Controller
{
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $invoices       = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name')
                                ->get();

        $products       = Products::orderBy('products.total_sold', 'DESC')->get();
        $paid_accounts  = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('salesreturn::index', compact('invoices', 'products', 'paid_accounts'));
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('salesreturn::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'return_date'     => 'required',
            'customer_id'     => 'required',
            'invoice_id'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat  = $data['vat_amount'];

            $data_find                          = SalesReturn::orderBy('created_at', 'DESC')->first();
            $sales_return_number                = $data_find != null ? $data_find['sales_return_number'] + 1 : 1;

            $sales_return                       = new SalesReturn;
            $sales_return->sales_return_number  = $sales_return_number;
            $sales_return->customer_id          = $data['customer_id'];
            $sales_return->invoice_id           = $data['invoice_id'];
            $sales_return->sales_return_date    = date('Y-m-d', strtotime($data['return_date']));
            $sales_return->return_note          = $data['return_note'];
            $sales_return->sub_total_amount     = $data['total_return_amount'];
            $sales_return->return_amount        = $data['balance_type'] == 1 ? $data['total_return_amount'] : $data['total_return_amount_paid'];
            $sales_return->due_amount           = $data['balance_type'] == 1 ? 0 : $data['total_return_amount'] - $data['total_return_amount_paid'];
            $sales_return->total_vat            = $vat;
            $sales_return->vat_type             = $data['vat_type'];
            $sales_return->total_discount_type  = $data['total_discount_type'];
            $sales_return->total_discount_amount= $data['total_discount_amount'];
            $sales_return->created_by           = $user_id;
            $sales_return->branch_id            = $branch_id;

            if ($sales_return->save())
            {
                $update_invoice                 =  Invoices::find($data['invoice_id']);
                $update_invoice->return_amount  =  $update_invoice['return_amount'] + $data['total_return_amount'];
                $update_invoice->save();

                foreach ($data['product_id'] as $key => $value)
                {
                    if ($data['return_quantity'][$key] != 0)
                    {
                        $product    = ProductEntries::find($data['product_entries'][$key]);
                        $buy_price  = $product['buy_price'];

                        $sales_return_entries[] = [
                            'invoice_id'         => $sales_return['invoice_id'],
                            'sales_return_id'    => $sales_return['id'],
                            'product_id'         => $value,
                            'product_entry_id'   => $data['product_entries'][$key],
                            'customer_id'        => $sales_return['customer_id'],
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'buy_price'          => $buy_price,
                            'rate'               => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'           => $data['return_quantity'][$key],
                            'total_amount'       => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'branch_id'          => $branch_id,
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('sales_return_entries')->insert($sales_return_entries);

                stockInReturn($data, $item_id=null);


                //Account Start
                if ($data['balance_type'] == 1) 
                {
                    //Customer has due, so due invoices will be adjusted
                    $current_dues       = Invoices::find($data['invoice_id']);
                    $due_invoices       = Invoices::where('customer_id', $data['customer_id'])
                                                        ->where('id', '!=', $data['invoice_id'])
                                                        ->where('due_amount', '>', 0)
                                                        ->orderBy('created_at', 'ASC')
                                                        ->get();

                    $adjusted_amount    = $data['total_return_amount'];

                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'sales-return';
                    $transaction_data['transaction_id'] = $sales_return->id;
                    $transaction_data['customer_id']    = $data['customer_id'];
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন বাবদ প্রদান';
                    $transaction_data['amount']         = $data['total_return_amount'];
                    $transaction_data['paid_through']   = $data['paid_through'][0];
                    transactions($transaction_data);


                    if ($current_dues  != null)
                    {
                        if ($current_dues['due_amount'] > 0)
                        {
                            if ($current_dues['due_amount'] <= $adjusted_amount)
                            {
                                $data_find_con_1_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_1   = $data_find_con_1_1 != null ? $data_find_con_1_1['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                                    'amount'                => $current_dues['due_amount'],
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 0,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $data['invoice_id'],
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_1 = [
                                    'payment_number'        => $payment_number_con_1_1,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                                    'amount'                => $current_dues['due_amount'],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 2,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_1 = DB::table('payments')->insertGetId($payments_1);      

                                if ($payment_id_con_1_1)
                                {
                                    $payment_entries_con_1_1 = [
                                            'payment_id'        => $payment_id_con_1_1,
                                            'invoice_id'        => $data['invoice_id'],
                                            'amount'            => $current_dues['due_amount'],
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_1);  
                                }

                                $update_invoice_dues_con_1_1                = Invoices::find($data['invoice_id']);
                                $update_invoice_dues_con_1_1->due_amount    = $update_invoice_dues_con_1_1['due_amount'] - $current_dues['due_amount'];
                                $update_invoice_dues_con_1_1->save();

                                $excess_amount_addable = $current_dues['due_amount'];

                                $invoice_adjustment = [
                                    'sales_return_id'       => $sales_return->id,
                                    'invoice_id'            => $data['invoice_id'],
                                    'adjustment_invoice_id' => $data['invoice_id'],
                                    'customer_id'           => $data['customer_id'],
                                    'payment_id'            => $payment_id_con_1_1,
                                    'amount'                => $current_dues['due_amount'],
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                DB::table('sales_return_adjustments')->insert($invoice_adjustment);
                            }

                            if ($current_dues['due_amount'] > $adjusted_amount)
                            {
                                $data_find_con_1_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_2   = $data_find_con_1_2 != null ? $data_find_con_1_2['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['customer_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                                    'amount'                => $adjusted_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'sales',
                                    'associated_id'         => $data['invoice_id'],
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_1_2 = [
                                    'payment_number'        => $payment_number_con_1_2,
                                    'customer_id'           => $data['customer_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                                    'amount'                => $adjusted_amount,
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 2,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_2 = DB::table('payments')->insertGetId($payments_con_1_2);      
                                if ($payment_id_con_1_2)
                                {
                                    $payment_entries_con_1_2 = [
                                            'payment_id'        => $payment_id_con_1_2,
                                            'invoice_id'        => $data['invoice_id'],
                                            'amount'            => $adjusted_amount,
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_2);  
                                }

                                $update_invoice_dues_con_1_2                = Invoices::find($data['invoice_id']);
                                $update_invoice_dues_con_1_2->due_amount    = $update_invoice_dues_con_1_2['due_amount'] - $adjusted_amount;
                                $update_invoice_dues_con_1_2->save();

                                $excess_amount_addable = $adjusted_amount;

                                $invoice_adjustment = [
                                    'sales_return_id'       => $sales_return->id,
                                    'invoice_id'            => $data['invoice_id'],
                                    'adjustment_invoice_id' => $data['invoice_id'],
                                    'customer_id'           => $data['customer_id'],
                                    'payment_id'            => $payment_id_con_1_2,
                                    'amount'                => $adjusted_amount,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                DB::table('sales_return_adjustments')->insert($invoice_adjustment);
                            }

                            $adjusted_amount = $adjusted_amount - $excess_amount_addable;
                        }

                        if (isset($account_transactions_con_1))
                        {
                            DB::table('account_transactions')->insert($account_transactions_con_1);
                        }
                    }

                    if ($adjusted_amount > 0)
                    {
                        foreach ($due_invoices as $key_con_2 => $value_con_2)
                        {
                            if ($adjusted_amount > 0)
                            {
                                if ($value_con_2->due_amount <= $adjusted_amount)
                                {
                                    $data_find_con_2_1        = Payments::orderBy('id', 'DESC')->first();
                                    $payment_number_con_2_1   = $data_find_con_2_1 != null ? $data_find_con_2_1['payment_number'] + 1 : 1;

                                    $account_transactions_con_2[] = [
                                        'customer_id'           => $data['customer_id'],
                                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                                        'amount'                => $value_con_2->due_amount,
                                        'paid_through_id'       => $data['paid_through'][0],
                                        'note'                  => $data['note'][0],
                                        'type'                  => 1,  // 0 = In , 1 = Out
                                        'transaction_head'      => 'sales-return',
                                        'associated_id'         => $value_con_2->id,
                                        'branch_id'             => $branch_id,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    $payments_con_2_1 = [
                                        'payment_number'        => $payment_number_con_2_1,
                                        'customer_id'           => $data['customer_id'],
                                        'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                                        'amount'                => $value_con_2->due_amount,
                                        'paid_through'          => $data['paid_through'][0],
                                        'note'                  => $data['note'][0],
                                        'type'                  => 0,
                                        'branch_id'             => $branch_id,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    $payment_id_con_2_1 = DB::table('payments')->insertGetId($payments_con_2_1);      

                                    if ($payment_id_con_2_1)
                                    {
                                        $payment_entries_con_2_1 = [
                                                'payment_id'        => $payment_id_con_2_1,
                                                'invoice_id'        => $value_con_2['id'],
                                                'amount'            => $value_con_2->due_amount,
                                                'branch_id'         => $branch_id,
                                                'created_by'        => $user_id,
                                                'created_at'        => date('Y-m-d H:i:s'),
                                        ];

                                        DB::table('payment_entries')->insert($payment_entries_con_2_1);  
                                    }

                                    $update_invoice_dues_con_2_1                = Invoices::find($value_con_2['id']);
                                    $update_invoice_dues_con_2_1->due_amount    = $update_invoice_dues_con_2_1['due_amount'] - $value_con_2->due_amount;
                                    $update_invoice_dues_con_2_1->save();

                                    $excess_cal = $value_con_2->due_amount;

                                    $invoice_adjustment = [
                                        'sales_return_id'       => $sales_return->id,
                                        'invoice_id'            => $data['invoice_id'],
                                        'adjustment_invoice_id' => $value_con_2['id'],
                                        'customer_id'           => $data['customer_id'],
                                        'payment_id'            => $payment_id_con_2_1,
                                        'amount'                => $value_con_2->due_amount,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('sales_return_adjustments')->insert($invoice_adjustment);
                                }

                                if ($value_con_2->due_amount > $adjusted_amount)
                                {
                                    $data_find_con_2_2        = Payments::orderBy('id', 'DESC')->first();
                                    $payment_number_con_2_2   = $data_find_con_2_2 != null ? $data_find_con_2_2['payment_number'] + 1 : 1;

                                    $account_transactions_con_2[] = [
                                        'customer_id'           => $data['customer_id'],
                                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                                        'amount'                => $adjusted_amount,
                                        'paid_through_id'       => $data['paid_through'][0],
                                        'note'                  => $data['note'][0],
                                        'type'                  => 0,  // 0 = In , 1 = Out
                                        'transaction_head'      => 'sales-return',
                                        'associated_id'         => $value_con_2->id,
                                        'branch_id'             => $branch_id,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    $payments_con_2_2 = [
                                        'payment_number'        => $payment_number_con_2_2,
                                        'customer_id'           => $data['customer_id'],
                                        'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                                        'amount'                => $adjusted_amount,
                                        'paid_through'          => $data['paid_through'][0],
                                        'note'                  => $data['note'][0],
                                        'type'                  => 2,
                                        'branch_id'             => $branch_id,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    $payment_id_con_2_2 = DB::table('payments')->insertGetId($payments_con_2_2);      

                                    if ($payment_id_con_2_2)
                                    {
                                        $payment_entries_con_2_2 = [
                                                'payment_id'        => $payment_id_con_2_2,
                                                'invoice_id'        => $value_con_2['id'],
                                                'amount'            => $adjusted_amount,
                                                'branch_id'         => $branch_id,
                                                'created_by'        => $user_id,
                                                'created_at'        => date('Y-m-d H:i:s'),
                                        ];

                                        DB::table('payment_entries')->insert($payment_entries_con_2_2);  
                                    }

                                    $update_invoice_dues_con_2_2                = Invoices::find($value_con_2['id']);
                                    $update_invoice_dues_con_2_2->due_amount    = $update_invoice_dues_con_2_2['due_amount'] - $adjusted_amount;
                                    $update_invoice_dues_con_2_2->save();

                                    $excess_cal     = $adjusted_amount;

                                    $invoice_adjustment = [
                                        'sales_return_id'       => $sales_return->id,
                                        'invoice_id'            => $data['invoice_id'],
                                        'adjustment_invoice_id' => $value_con_2['id'],
                                        'customer_id'           => $data['customer_id'],
                                        'payment_id'            => $payment_id_con_2_2,
                                        'amount'                => $adjusted_amount,
                                        'created_by'            => $user_id,
                                        'created_at'            => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('sales_return_adjustments')->insert($invoice_adjustment);
                                }

                                $adjusted_amount = $adjusted_amount - $excess_cal;
                            }
                        }

                        if (isset($account_transactions_con_2))
                        {
                            DB::table('account_transactions')->insert($account_transactions_con_2);
                        }
                    }

                    if ($adjusted_amount > 0)
                    {
                        //Update advance payment in customer table start
                        $find_customer                              = Customers::find($data['customer_id']);
                        $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] + $adjusted_amount;
                        $find_customer->save();
                        //Update advance payment in customer table end

                        $data_find_adv          = Payments::orderBy('id', 'DESC')->first();
                        $payment_number_adv     = $data_find_adv != null ? $data_find_adv['payment_number'] + 1 : 1;

                        $payments_adv = [
                            'payment_number'        => $payment_number_adv,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $adjusted_amount,
                            'paid_through'          => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 2,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id_adv = DB::table('payments')->insertGetId($payments_adv);      
                        if ($payment_id_adv)
                        {
                            $payment_entries_adv = [
                                    'payment_id'        => $payment_id_adv,
                                    'sales_return_id'   => $sales_return->id,
                                    'amount'            => $adjusted_amount,
                                    'branch_id'         => $branch_id,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries_adv);  
                        }

                        $account_transactions_adv = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $adjusted_amount,
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment_id_adv,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $current_balance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $adjusted_amount,
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment_id_adv,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        if (isset($account_transactions))
                        {
                            DB::table('account_transactions')->insert($account_transactions);
                        }

                        // if (isset($current_balance))
                        // {
                        //     DB::table('current_balance')->insert($current_balance);
                        // }

                        $adjusted_amount  = 0; 
                    }
                }
                else 
                {
                    //Customer has advance, so advance will be changed
                    if ($data['total_return_amount'] > 0)
                    {   
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                        $transaction_data['type']           = 1;
                        $transaction_data['account_head']   = 'customer-advance';
                        $transaction_data['transaction_id'] = $sales_return->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['note']           = 'বিক্রয় রিটার্ন বাবদ প্রদেয়';
                        $transaction_data['amount']         = $data['total_return_amount'];
                        $transaction_data['paid_through']   = $data['paid_through'][0];
                        transactions($transaction_data);

                        //Update advance payment in customer table start
                        $find_customer                              = Customers::find($data['customer_id']);
                        $find_customer->customer_advance_payment    = $find_customer['customer_advance_payment'] + $data['total_return_amount'];
                        $find_customer->save();
                        //Update advance payment in customer table end

                        $data_find_adv          = Payments::orderBy('id', 'DESC')->first();
                        $payment_number_adv     = $data_find_adv != null ? $data_find_adv['payment_number'] + 1 : 1;

                        $payments_adv = [
                            'payment_number'        => $payment_number_adv,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $data['total_return_amount_paid'],
                            'paid_through'          => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 2,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id_adv = DB::table('payments')->insertGetId($payments_adv);      
                        if ($payment_id_adv)
                        {
                            $payment_entries_adv = [
                                    'payment_id'        => $payment_id_adv,
                                    'sales_return_id'   => $sales_return->id,
                                    'amount'            => $data['total_return_amount_paid'],
                                    'branch_id'         => $branch_id,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries_adv);  
                        }

                        $account_transactions_adv = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $data['total_return_amount_paid'],
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment_id_adv,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $current_balance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $data['total_return_amount_paid'],
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 0,  // 0 = In , 1 = Out
                            'transaction_head'      => 'customer-advance',
                            'associated_id'         => $payment_id_adv,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        if (isset($account_transactions))
                        {
                            DB::table('account_transactions')->insert($account_transactions);
                        }

                        // if (isset($current_balance))
                        // {
                        //     DB::table('current_balance')->insert($current_balance);
                        // }
                    }

                    if ($data['total_return_amount_paid'] > 0)
                    {
                        $return_payment                    = new ReturnPayments;
                        $return_payment->customer_id       = $data['customer_id'];
                        $return_payment->date              = date('Y-m-d', strtotime($data['return_date']));
                        $return_payment->amount            = $data['total_return_amount_paid'];
                        $return_payment->paid_through_id   = $data['paid_through'][0];
                        $return_payment->note              = $data['note'][0];
                        $return_payment->type              = 0;
                        $return_payment->branch_id         = $branch_id;
                        $return_payment->created_by        = $user_id;

                        $cus_update                             = Customers::find($data['customer_id']);
                        $cus_update->customer_advance_payment   = $cus_update['customer_advance_payment'] - $data['total_return_amount_paid'];
                        $cus_update->save();

                        $account_transactions = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $data['total_return_amount_paid'],
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 1,  // 0 = In , 1 = Out
                            'transaction_head'      => 'sales-return-payment',
                            'associated_id'         => $return_payment->id,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $current_balance = [
                            'customer_id'           => $data['customer_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                            'amount'                => $data['total_return_amount_paid'],
                            'paid_through_id'       => $data['paid_through'][0],
                            'note'                  => $data['note'][0],
                            'type'                  => 1,  // 0 = In , 1 = Out
                            'transaction_head'      => 'sales-return-payment',
                            'associated_id'         => $return_payment->id,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                        $transaction_data['type']           = 0;
                        $transaction_data['account_head']   = 'sales-return-payment';
                        $transaction_data['transaction_id'] = $return_payment->id;
                        $transaction_data['customer_id']    = $data['customer_id'];
                        $transaction_data['amount']         = $data['total_return_amount_paid'];
                        $transaction_data['note']           = $data['note'][0];
                        $transaction_data['paid_through']   = $data['paid_through'][0];
                        transactions($transaction_data);

                        if (isset($account_transactions))
                        {
                            DB::table('account_transactions')->insert($account_transactions);
                        }

                        // if (isset($current_balance))
                        // {
                        //     DB::table('current_balance')->insert($current_balance);
                        // }
                    }
                }
                //Account End
  

                DB::commit();
                return back()->with("success","Sales Return Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $sales_return       = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                        ->select('sales_return.*',
                                                 'customers.name as customer_name',
                                                 'customers.address as address',
                                                 'customers.phone as phone')
                                        ->find($id);

        $entries            = SalesReturnEntries::leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                        ->where('sales_return_entries.sales_return_id', $id)
                                        ->select('sales_return_entries.*',
                                                 'product_entries.name as product_entry_name',
                                                 'products.name as product_name')
                                        ->get();  
                     
        $user_info  = Users::find(1);

        return view('salesreturn::show', compact('entries', 'sales_return', 'user_info'));
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('salesreturn::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function delete($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $sales_return       = SalesReturn::find($id);
            $entries            = SalesReturnEntries::where('sales_return_id', $id)->get();
            $payment_entries    = PaymentEntries::where('payment_entries.sales_return_id', $id)->get();

            //Invoice Update
            $invoice                    = Invoices::find($sales_return['invoice_id']);
            $invoice->return_amount     = $invoice['return_amount'] - $sales_return['return_amount'];
            $invoice->save();

            //stock Update
            foreach ($entries as $key => $value)
            {   
                $conversion_rate_find             = UnitConversions::where('main_unit_id', $value['main_unit_id'])
                                                            ->where('converted_unit_id', $value['conversion_unit_id'])
                                                            ->where('product_entry_id', $value['product_entry_id'])
                                                            ->first();

                $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $value['quantity']/$conversion_rate_find['conversion_rate'] : $value['quantity'];


                //product entries table update
                $product_entry                    = ProductEntries::find($value['product_entry_id']);
                $product_entry->stock_in_hand     = $product_entry['stock_in_hand'] - $converted_quantity_to_main_unit;
                $product_entry->total_sold        = $product_entry['total_sold'] + $value['quantity'];
                $product_entry->save();
            }

            $sales_return->delete();
            
            $delete_current_balance     = CurrentBalance::where('customer_id', $sales_return['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
            $delete_transaction         = AccountTransactions::where('customer_id', $sales_return['customer_id'])->where('transaction_head', 'sales-return')->where('associated_id', $id)->delete();
            $delete_trans               = Transactions::where('customer_id', $sales_return['customer_id'])->where('account_head', 'sales-return')->where('sales_return_id', $id)->delete();

            foreach ($payment_entries as $key1 => $value1)
            {
                $find_payment    = Payments::find($value1['payment_id']);

                if ($find_payment)
                {
                   $find_payment ->delete();
                }
            }

            DB::commit();
            return back()->with("success","Sales Return Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }
    }

    public function invoiceList($id)
    {
        $data           = Invoices::leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->where('invoices.customer_id', $id)
                                ->orderBy('invoices.created_at', 'DESC')
                                ->select('invoices.*',
                                         'customers.name as customer_name')
                                ->get();
   
        return Response::json($data);
    }

    public function invoiceEntriesList($id)
    {
        $invoice_entry          = InvoiceEntries::leftjoin('customers', 'customers.id', 'invoice_entries.customer_id')
                                        ->leftjoin('products', 'products.id', 'invoice_entries.product_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'invoice_entries.product_entry_id')
                                        ->leftjoin('units', 'units.id', 'invoice_entries.conversion_unit_id')
                                        ->where('invoice_entries.invoice_id', $id)
                                        ->orderBy('invoice_entries.created_at', 'DESC')
                                        ->select('invoice_entries.*',
                                                 'products.name as product_name',
                                                 'product_entries.name as entry_name',
                                                 'product_entries.product_code as product_code',
                                                 'units.name as conversion_unit_name',
                                                 'customers.name as customer_name')
                                        ->get();

        $sales_return_entry     = SalesReturnEntries::where('sales_return_entries.invoice_id', $id)
                                        ->get();

        $invoice                = Invoices::where('invoices.id', $id)
                                        ->select('invoices.*')
                                        ->first();

        if ($invoice_entry->count() > 0)
        {
            foreach ($invoice_entry as $key => $value)
            {
                $sales_return_quantity_sum                          = $sales_return_entry->where('product_entry_id', $value['product_entry_id']) ->sum('quantity');

                $entry_data[$value['id']]['id']                     =  $value['id'];
                $entry_data[$value['id']]['invoice_id']             =  $value['invoice_id'];
                $entry_data[$value['id']]['product_id']             =  $value['product_id'];
                $entry_data[$value['id']]['product_entry_id']       =  $value['product_entry_id'];
                $entry_data[$value['id']]['product_code']           =  $value['product_code'];
                $entry_data[$value['id']]['product_type']           =  $value['product_type'];
                $entry_data[$value['id']]['customer_id']            =  $value['customer_id'];
                $entry_data[$value['id']]['main_unit_id']           =  $value['main_unit_id'];
                $entry_data[$value['id']]['conversion_unit_id']     =  $value['conversion_unit_id'];
                $entry_data[$value['id']]['conversion_unit_name']   =  $value['conversion_unit_name'];
                $entry_data[$value['id']]['buy_price']              =  $value['buy_price'];
                $entry_data[$value['id']]['rate']                   =  $value['rate'];
                $entry_data[$value['id']]['original_quantity']      =  $value['quantity'];
                $entry_data[$value['id']]['quantity']               =  $value['quantity'] - $sales_return_quantity_sum;
                $entry_data[$value['id']]['total_amount']           =  $value['total_amount'];
                $entry_data[$value['id']]['discount_type']          =  $value['discount_type'];
                $entry_data[$value['id']]['discount_amount']        =  $value['discount_amount'];
                $entry_data[$value['id']]['product_name']           =  $value['product_name'];
                $entry_data[$value['id']]['entry_name']             =  $value['entry_name'];
                $entry_data[$value['id']]['customer_name']          =  $value['customer_name'];
            }
        }
        else
        {
            $entry_data = [];
        }

        $data['invoice']            = $invoice;
        $data['invoice_entries']    = $entry_data;
   
        return Response::json($data);
    }

    public function salesReturnListLoad()
    {
        $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function salesReturnListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('sales_return.sales_return_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('sales_return.sales_return_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = SalesReturn::leftjoin('invoices', 'invoices.id', 'sales_return.invoice_id')
                                ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                ->orderBy('sales_return.created_at', 'DESC')
                                ->select('sales_return.*',
                                         'customers.name as customer_name',
                                         'customers.phone as phone',
                                         'invoices.invoice_number as invoice_number')
                                ->take(100)
                                ->get();
        }

        return Response::json($data);
    }

    public function findCustomerName($id)
    {
        $data           = Customers::find($id);

        return Response::json($data);
    }

    public function findInvoiceDetails($id)
    {
        $data           = Invoices::find($id);

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $branch_id  = Auth::user()->branch_id;

        $due                    = Invoices::where('customer_id', $customer_id)
                                          ->where('due_amount', '>', 0)
                                          ->sum('due_amount');
                                          
        $customer_advance       = Customers::find($customer_id);
        $advance                = $customer_advance['customer_advance_payment'];

        if ($due > 0)
        {
            $data['balance']   = $due;
            $data['type']      = 1; //1 = previous dues
        }

        if ($advance > 0)
        {
            $data['balance']   = $advance;
            $data['type']      = 2; //2 = advance amount
        }

        if (($due == 0) && ($advance == 0))
        {
            $data['balance']   = 0;
            $data['type']      = 1; //1 = previous dues
        }

        return Response::json($data);
    }
}
