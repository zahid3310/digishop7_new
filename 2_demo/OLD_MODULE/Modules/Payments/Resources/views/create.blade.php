@extends('layouts.app')

<?php
    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 0))
    {
        $title = 'Due Collection';
    }

    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 1))
    {
        $title = 'Due Payment';
    }

    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 2))
    {
        $title = 'Return Payment';
    }

    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 3))
    {
        $title = 'Return Collection';
    }

    if (!isset($_GET['payment_type']))
    {
        $title = 'Payments';
    }
?>

@section('title', $title)

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Payments</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Payments</a></li>
                                    <li class="breadcrumb-item active">Payments</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">Payment Type *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" onchange="searchContact()">
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 0))
                                                <option value="0">SR/Customers Payment</option>
                                                @endif
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 1))
                                                <option value="1">Supplier Payment</option>
                                                @endif
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 2))
                                                <option value="2">Sales Return</option>
                                                @endif
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 3))
                                                <option value="3">Purchase Return</option>
                                                @endif
                                                @if(!isset($_GET['payment_type']))
                                                <option value="0">SR/Customers Payment</option>
                                                <option value="1">Supplier Payment</option>
                                                <option value="2">Sales Return</option>
                                                <option value="3">Purchase Return</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">Search Contact *</label>
	                                        <select id="contact_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--Select Contact--</option>
												@if(!empty($customers))
                                                    <?php 
                                                        if (isset($_GET['payment_type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['payment_type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													@foreach($customers as $key => $customer)
													<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['id'] ? 'selected' : '' }} @endif value="{{ $customer->id }}">{{ $customer->name }} {{ $customer->phone != null ? ' | ' . $customer->phone : ''}}</option>
													@endforeach
												@endif
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                
            					<div class="media">
                                    <div class="media-body">
                                        <h4 id="customerName" class="mb-0"></h4>
                                        <p id="customerAddress" class="text-muted font-weight-medium"></p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                
            					<div class="media">
                                    <div class="media-body">
                                        <p id="receivablePShow" class="text-muted font-weight-medium">Total Receivable</p>
                                        <p id="payablePShow" class="text-muted font-weight-medium">Total Payable</p>
                                        <h4 id="totalReceivable" class="mb-0">0.00</h4>
                                    </div>

                                    <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                        <span class="avatar-title">
                                            <i class="bx bx-credit-card font-size-24"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                
            					<div class="media">
                                    <div class="media-body">
                                        <p id="receivedPShow" class="text-muted font-weight-medium">Total Received</p>
                                        <p id="paidPShow" class="text-muted font-weight-medium">Total Paid</p>
                                        <h4 id="totalReceived" class="mb-0">0.00</h4>
                                    </div>

                                    <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                        <span class="avatar-title">
                                            <i class="bx bx-credit-card font-size-24"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card">
                            <div class="card-body">
                                
            					<div class="media">
                                    <div class="media-body">
                                        <p class="text-muted font-weight-medium">Total Dues</p>
                                        <h4 id="totalDues" class="mb-0">0.00</h4>
                                    </div>

                                    <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                        <span class="avatar-title">
                                            <i class="bx bx-credit-card font-size-24"></i>
                                        </span>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('payments_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                    <input id="customerIdHidden" name="customer_id" type="hidden" class="form-control">
                                    <input id="customerIdHiddenReload" name="" type="hidden" class="form-control" value="{{Session('find_customer')}}">
                                    <input id="typeHiddenReload" name="" type="hidden" class="form-control" value="{{Session('find_type')}}">
                                    <input id="typeInput" name="type" type="hidden" class="form-control">

            						<div class="row">
					                	<div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="payment_date">Payment Date *</label>
					                            <input id="payment_date" name="payment_date" type="text" value="{{ Session::has('payment_date') ? date('d-m-Y', strtotime(Session::get('payment_date'))) : date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
					                        </div>
					                    </div>

					                    <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="amount">Amount *</label>
					                            <input id="amount" name="amount" type="text" class="form-control" onchange="searchContact()">
					                        </div>
					                    </div>

					                    <div class="col-sm-6 form-group">
				                            <label class="control-label">Paid Through</label>
                                            <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                                @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                @foreach($paid_accounts as $key => $paid_account)
                                                    <option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>
                                                @endforeach
                                                @endif
                                            </select>
				                        </div>

				                        <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="note">Note</label>
					                            <input id="note" name="note" type="text" class="form-control">
					                        </div>
					                    </div>
				                	</div>

                                    <div id="type0" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Order#</th>
                                                    <th>Date</th>
                                                    <th>Receivable</th>
                                                    <th>Received</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                    <th>Discount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="invoice_list">
												
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="type1" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Purchase#</th>
                                                    <th>Date</th>
                                                    <th>Payable</th>
                                                    <th>Paid</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                    <th>Discount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="bill_list">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="type2" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Sales Return#</th>
                                                    <th>Date</th>
                                                    <th>Payable</th>
                                                    <th>Paid</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="sales_return_list">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                    <div id="type3" style="display: none" class="table-responsive">
                                        <table class="table table-centered table-nowrap">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Purchase Return#</th>
                                                    <th>Date</th>
                                                    <th>Payable</th>
                                                    <th>Paid</th>
                                                    <th>Dues</th>
                                                    <th>Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody id="purchase_return_list">
                                                
                                            </tbody>
                                        </table>
                                    </div>

                                    <hr style="margin-top: 0px !important">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Make Payment</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('payments_create') }}">Close</a></button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Payment List</h4>

                                <div style="margin-right: 10px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Payment#</th>
                                            <th>Contact</th>
                                            <th>Type</th>
                                            <th>Paid Through</th>
                                            <th>Note</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="payment_list">
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the payment ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            $('.enableOnInput').prop('disabled', true);

            var customerId      = $('#customerIdHiddenReload').val();
            var typeId          = $('#typeHiddenReload').val();
            var type_input      = $('#type').val();

            $('#contact_id').val(customerId).change();
            $('#typeInput').val(type_input);

            if(typeId != '')
            {    
                $('#type').val(typeId).change();
            }

            var site_url        = $('.site_url').val();
            var contact_id      = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/payment/list/' + contact_id_find, function(data){

                paymentList(data);
                
            });

            searchContact();
        });

        function searchContact()
        {
            var contact_id  = $('#contact_id').val();
            var site_url    = $('.site_url').val();
            var amounts     = $('#amount').val();
            var type_input  = $('#type').val();

            $('#typeInput').val(type_input);

            if (amounts == '')
            {
                var amount = 0;
            }
            else
            {
                var amount = amounts;
            }

            if (type_input == 0)
            {
                $('#type0').show();
                $('#type1').hide();
                $('#type2').hide();
                $('#type3').hide();

                $('#receivablePShow').show();
                $('#receivedPShow').show();
                $('#payablePShow').hide();
                $('#paidPShow').hide();

                $.get(site_url + '/payments/contact/list/' + contact_id, function(data){

                    var invoice_list = '';
                    var amount_left  = parseFloat(amount);
                    var paid         = 0;

                    $.each(data.invoices, function(i, data)
                    {   
                        if (amount_left > data.due_amount)
                        {
                            var amount_paid  = data.due_amount;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                        else
                        {
                            var amount_paid  = amount_left;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }

                        // if (paid != 0)
                        // {
                        //     $('#amount').val(paid);
                        // }

                        invoice_list += '<tr>' +
                                            '<input class="form-control" type="hidden" name="invoice_id[]" value="' +  data.id + '">' +
                                            '<td style="text-align: left">' +
                                               'INV - ' + data.invoice_number.padStart(5, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(data.invoice_date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (data.invoice_amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (parseFloat(data.invoice_amount) - parseFloat(data.due_amount)) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (data.due_amount) +
                                            '</td>' +

                                            '<td style="text-align: left">' +
                                                '<input class="form-control" type="text" name="paid[]" value="' + amount_paid + '">' +
                                            '</td>' +

                                            '<td style="text-align: left">' +
                                                '<input class="form-control" type="text" name="discount[]" value="0">' +
                                            '</td>' +
                                        '</tr>';
                    });

                    var amt     = $('#amount').val();
                    if (parseFloat(amt) > 0)
                    {
                        $('.enableOnInput').prop('disabled', false);
                    }
                    else
                    {
                        $('.enableOnInput').prop('disabled', true);
                    }

                    $("#invoice_list").empty();
                    $("#bill_list").empty();
                    $("#sales_return_list").empty();
                    $("#purchase_return_list").empty();
                    $("#invoice_list").append(invoice_list);

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + data.phone;
                    }
                    else
                    {
                        var address = data.phone;
                    }

                    if (data.receivable != null)
                    {
                        var receivable = data.receivable;
                    }
                    else
                    {
                        var receivable = 0;
                    }

                    if (data.received != null)
                    {
                        var received = data.received;
                    }
                    else
                    {
                        var received = 0;
                    }

                    var dues = parseFloat(receivable) - parseFloat(received);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(receivable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(received);
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);    
                });
            }
            
            if (type_input == 1)
            {
                $('#type1').show();
                $('#type0').hide();
                $('#type2').hide();
                $('#type3').hide();

                $('#receivablePShow').hide();
                $('#receivedPShow').hide();
                $('#payablePShow').show();
                $('#paidPShow').show();

                $.get(site_url + '/payments/contact/list/bill/' + contact_id, function(data){

                    var bill_list       = '';
                    var amount_left     = parseFloat(amount);
                    var paid            = 0;

                    $.each(data.bills, function(i, data)
                    {   
                        if (amount_left > data.due_amount)
                        {
                            var amount_paid  = data.due_amount;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                        else
                        {
                            var amount_paid  = amount_left;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }

                        // if (paid != 0)
                        // {
                        //     $('#amount').val(paid);
                        // }

                        bill_list += '<tr>' +
                                        '<input class="form-control" type="hidden" name="bill_id[]" value="' +  data.id + '">' +
                                        '<td style="text-align: left">' +
                                           'BILL - ' + data.bill_number.padStart(5, '0') +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                            formatDate(data.bill_date) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           data.bill_amount +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           (parseFloat(data.bill_amount) - parseFloat(data.due_amount)) +
                                        '</td>' +
                                        '<td style="text-align: left">' +
                                           data.due_amount +
                                        '</td>' +

                                        '<td style="text-align: left">' +
                                            '<input class="form-control" type="text" name="paid[]" value="' + amount_paid + '">' +
                                        '</td>' +

                                        '<td style="text-align: left">' +
                                            '<input class="form-control" type="text" name="discount[]" value="0">' +
                                        '</td>' +
                                    '</tr>';

                        var amt     = $('#amount').val();
                        if (parseFloat(amt) > 0)
                        {
                            $('.enableOnInput').prop('disabled', false);
                        }
                        else
                        {
                            $('.enableOnInput').prop('disabled', true);
                        }
                    });

                    var amt     = $('#amount').val();
                    if (parseFloat(amt) > 0)
                    {
                        $('.enableOnInput').prop('disabled', false);
                    }
                    else
                    {
                        $('.enableOnInput').prop('disabled', true);
                    }

                    $("#bill_list").empty();
                    $("#sales_return_list").empty();
                    $("#purchase_return_list").empty();
                    $("#invoice_list").empty();
                    $("#bill_list").append(bill_list);

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + data.phone;
                    }
                    else
                    {
                        var address = data.phone;
                    }

                    if (data.payable != null)
                    {
                        var payable = data.payable;
                    }
                    else
                    {
                        var payable = 0;
                    }

                    if (data.paid != null)
                    {
                        var paid = data.paid;
                    }
                    else
                    {
                        var paid = 0;
                    }

                    var dues = parseFloat(payable) - parseFloat(paid);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(payable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(paid);
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);
                });
            }

            if (type_input == 2)
            {
                $('#type2').show();
                $('#type0').hide();
                $('#type1').hide();
                $('#type3').hide();

                $('#receivablePShow').hide();
                $('#receivedPShow').hide();
                $('#payablePShow').show();
                $('#paidPShow').show();

                $.get(site_url + '/payments/contact/list/sales-return/' + contact_id, function(data){

                    var sales_return_list   = '';
                    var amount_left         = parseFloat(amount);
                    var paid                = 0;

                    $.each(data.sale_returns, function(i, data)
                    {   
                        if (amount_left > data.due_amount)
                        {
                            var amount_paid  = data.due_amount;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                        else
                        {
                            var amount_paid  = amount_left;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                            if (paid != 0)
                            {
                                $('#amount').val(paid);
                            }

                            sales_return_list += '<tr>' +
                                                    '<input class="form-control" type="hidden" name="sales_return_id[]" value="' +  data.id + '">' +
                                                    '<td style="text-align: left">' +
                                                       'SR - ' + data.sales_return_number.padStart(5, '0') +
                                                    '</td>' +
                                                    '<td style="text-align: left">' +
                                                        formatDate(data.sales_return_date) +
                                                    '</td>' +
                                                    '<td style="text-align: left">' +
                                                       data.return_amount +
                                                    '</td>' +
                                                    '<td style="text-align: left">' +
                                                       (parseFloat(data.return_amount) - parseFloat(data.due_amount)) +
                                                    '</td>' +
                                                    '<td style="text-align: left">' +
                                                       data.due_amount +
                                                    '</td>' +

                                                    '<td style="text-align: left">' +
                                                        '<input style="width: 150px" class="form-control" type="text" name="paid[]" value="' + amount_paid + '">' +
                                                    '</td>' +
                                                '</tr>';
                    });

                    var amt     = $('#amount').val();
                    if (parseFloat(amt) > 0)
                    {
                        $('.enableOnInput').prop('disabled', false);
                    }
                    else
                    {
                        $('.enableOnInput').prop('disabled', true);
                    }

                    $("#sales_return_list").empty();
                    $("#invoice_list").empty();
                    $("#bill_list").empty();
                    $("#purchase_return_list").empty();
                    $("#sales_return_list").append(sales_return_list);

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + data.phone;
                    }
                    else
                    {
                        var address = data.phone;
                    }

                    if (data.payable != null)
                    {
                        var payable = data.payable;
                    }
                    else
                    {
                        var payable = 0;
                    }

                    if (data.paid != null)
                    {
                        var paid = data.paid;
                    }
                    else
                    {
                        var paid = 0;
                    }

                    var dues = parseFloat(payable) - parseFloat(paid);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(payable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(paid);
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);
                });
            }

            if (type_input == 3)
            {
                $('#type3').show();
                $('#type0').hide();
                $('#type1').hide();
                $('#type2').hide();

                $('#receivablePShow').show();
                $('#receivedPShow').show();
                $('#payablePShow').hide();
                $('#paidPShow').hide();

                $.get(site_url + '/payments/contact/list/purchase-return/' + contact_id, function(data){

                    var purchase_return_list    = '';
                    var amount_left             = parseFloat(amount);
                    var paid                    = 0;

                    $.each(data.purchase_returns, function(i, data)
                    {   
                        if (amount_left > data.due_amount)
                        {
                            var amount_paid  = data.due_amount;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                        else
                        {
                            var amount_paid  = amount_left;
                            amount_left     -= parseFloat(amount_paid);
                            paid            += parseFloat(amount_paid);
                        }
                            if (paid != 0)
                            {
                                $('#amount').val(paid);
                            }

                            purchase_return_list += '<tr>' +
                                                        '<input class="form-control" type="hidden" name="purchase_return_id[]" value="' +  data.id + '">' +
                                                        '<td style="text-align: left">' +
                                                           'PR - ' + data.purchase_return_number.padStart(5, '0') +
                                                        '</td>' +
                                                        '<td style="text-align: left">' +
                                                            formatDate(data.purchase_return_date) +
                                                        '</td>' +
                                                        '<td style="text-align: left">' +
                                                           data.return_amount +
                                                        '</td>' +
                                                        '<td style="text-align: left">' +
                                                           (parseFloat(data.return_amount) - parseFloat(data.due_amount)) +
                                                        '</td>' +
                                                        '<td style="text-align: left">' +
                                                           data.due_amount +
                                                        '</td>' +

                                                        '<td style="text-align: left">' +
                                                            '<input style="width: 150px" class="form-control" type="text" name="paid[]" value="' + amount_paid + '">' +
                                                        '</td>' +
                                                    '</tr>';
                    });

                    var amt     = $('#amount').val();
                    if (parseFloat(amt) > 0)
                    {
                        $('.enableOnInput').prop('disabled', false);
                    }
                    else
                    {
                        $('.enableOnInput').prop('disabled', true);
                    }

                    $("#purchase_return_list").empty();
                    $("#invoice_list").empty();
                    $("#bill_list").empty();
                    $("#sales_return_list").empty();
                    $("#purchase_return_list").append(purchase_return_list);

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + data.phone;
                    }
                    else
                    {
                        var address = data.phone;
                    }

                    if (data.payable != null)
                    {
                        var payable = data.payable;
                    }
                    else
                    {
                        var payable = 0;
                    }

                    if (data.paid != null)
                    {
                        var paid = data.paid;
                    }
                    else
                    {
                        var paid = 0;
                    }

                    var dues = parseFloat(payable) - parseFloat(paid);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(payable.toFixed(2));
                    $("#totalReceived").empty();
                    $("#totalReceived").html(paid.toFixed(2));
                    $("#totalDues").empty();
                    $("#totalDues").html(dues.toFixed(2));
                });
            }
            
            $("#customerIdHidden").val(contact_id);

            //Payment List Show In The Index Section For A Specific Contact ID
            $.get(site_url + '/payments/payment/list/' + contact_id, function(data){

                paymentList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text = $('#searchPayment').val();
            var site_url    = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }
            else
            {
                var search_text = $('#searchPayment').val();
            }

            $.get(site_url + '/payments/payment/list/search/' + search_text, function(data){

                paymentList(data);

            });
        }
    </script>

    <script type="text/javascript">
        $('.delete_btn').click(function () {
            var site_url            = $('.site_url').val();
            var id                  = $('.paymentId').val();
            window.location.href    = site_url + "/payments/delete/"+id;
        })
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function paymentList(data)
        {
            var payment_list    = '';
            var site_url        = $('.site_url').val();
            $.each(data, function(i, payment_data)
            {
                var serial      = parseFloat(i) + 1;

                var locationValue   = (new URL(location.href)).searchParams.get('payment_type');
                if (locationValue   != null)
                {
                    var show_url    = site_url + '/payments/show/' + payment_data.id + '?payment_type=' + locationValue;
                    var delete_url  = site_url + '/payments/delete/' + payment_data.id + '?payment_type=' + locationValue;
                }
                else
                {
                    var show_url    = site_url + '/payments/show/' + payment_data.id;
                    var delete_url  = site_url + '/payments/delete/' + payment_data.id;
                }

                if (payment_data.type == 0)
                {
                    var p_type = "SR/Customers Payment";
                }

                if (payment_data.type == 1)
                {
                    var p_type = "Supplier Payment";
                }

                if (payment_data.type == 2)
                {
                    var p_type = "Sales Return Payment";
                }

                if (payment_data.type == 3)
                {
                    var p_type = "Purchase Return Payment";
                }

                if(payment_data.note != null)
                {
                    var p_note = payment_data.note;
                }
                else
                {
                    var p_note = "";
                }
                
                if (locationValue != null)
                {
                    if (payment_data.type == locationValue)
                    {
                        payment_list += '<tr>' +
                                            '<input class="form-control paymentId" type="hidden" name="payment_id[]" value="' +  payment_data.id + '">' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(payment_data.payment_date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               'PM - ' + payment_data.payment_number.padStart(5, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.paid_through_accounts_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_note +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (payment_data.amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '@if(Auth::user()->role == 1)' + 
                                                        // '<a class="dropdown-item" href="' + delete_url +'">' + 'Delete' + '</a>' +
                                                        '@endif' +
                                                        '<a class="dropdown-item" href="' + show_url +'" target="_blank">' + 'Show' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                    }
                }
                else
                {
                    payment_list += '<tr>' +
                                            '<input class="form-control paymentId" type="hidden" name="payment_id[]" value="' +  payment_data.id + '">' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(payment_data.payment_date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               'PM - ' + payment_data.payment_number.padStart(5, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.paid_through_accounts_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_note +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (payment_data.amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '@if(Auth::user()->role == 1)' + 
                                                        '<a class="dropdown-item" href="' + delete_url +'">' + 'Delete' + '</a>' +
                                                        '@endif' +
                                                        '<a class="dropdown-item" href="' + show_url +'" target="_blank">' + 'Show' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                }
            });

            $("#payment_list").empty();
            $("#payment_list").append(payment_list);
        }
    </script>
@endsection