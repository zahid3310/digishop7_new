<!DOCTYPE html>
<html>

<head>
    <title>Daily Collection Report</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Collection Report</h6>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="text-align: center">DATE FROM</th>
                                            <th style="text-align: center">CUSTOMER NAME</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">
                                        <tr class="gradeC">
                                            <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                            <td style="text-align: center"><?php echo e($find_customer != null ? $find_customer->name : 'All'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 5%">SL</th>
                                            <th style="text-align: center;width: 15%">SR NAME</th>
                                            <th style="text-align: center;width: 10%">TOTAL SALES</th>
                                            <th style="text-align: center;width: 10%">CASH PAYMENT</th>
                                            <th style="text-align: center;width: 10%">BANK PAYMENT</th>
                                            <th style="text-align: center;width: 10%">SR EXPENSES</th>
                                            <th style="text-align: center;width: 10%">SR DUE</th>
                                            <th style="text-align: center;width: 10%">ADVANCE PAYMENT</th>
                                            <th style="text-align: center;width: 10%">GENERAL ESPENSES</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php 
                                            $i = 1; 

                                            if(isset($_GET['from_date']))
                                            {
                                                $f_date = $_GET['from_date'];
                                            }
                                            else
                                            {
                                                $f_date = 0;
                                            }

                                            if(isset($_GET['to_date']))
                                            {
                                                $t_date = $_GET['to_date'];
                                            }
                                            else
                                            {
                                                $t_date = 0;
                                            }

                                            $total_sale             = 0;
                                            $total_cash_payment     = 0;
                                            $total_bank_payment     = 0;
                                            $total_dsr_expense      = 0;
                                            $total_dsr_due          = 0;
                                            $total_advance_payment  = 0;
                                        ?>

                                        <?php if(!empty($data)): ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if(($value['sale']) > 0): ?>
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle" rowspan=""><?php echo e($i); ?></td>
                                            <td style="text-align: left;vertical-align: middle" rowspan=""><?php echo e($value['customer_name']); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['sale'],2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['cash_payment'],2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['bank_payment'],2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['dsr_expense'],2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['due_amount'],2,'.',',')); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['customer_advance'],2,'.',',')); ?></td>
                                            <?php if($i == 1): ?>
                                            <td style="text-align: center;vertical-align: middle" rowspan="<?php echo e(count($data)); ?>"><?php echo e(number_format($genaral_expense,2,'.',',')); ?></td>
                                            <?php endif; ?>
                                        </tr>

                                        <?php 
                                            $i++;

                                            $total_sale             = $total_sale + $value['sale'];
                                            $total_cash_payment     = $total_cash_payment + $value['cash_payment'];
                                            $total_bank_payment     = $total_bank_payment + $value['bank_payment'];
                                            $total_dsr_expense      = $total_dsr_expense + $value['dsr_expense'];
                                            $total_dsr_due          = $total_dsr_due + $value['due_amount'];
                                            $total_advance_payment  = $total_advance_payment + $value['customer_advance']; 
                                        ?>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_sale,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_cash_payment,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_bank_payment,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_dsr_expense,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_dsr_due,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_advance_payment,2,'.',',')); ?></th>
                                            <th colspan="1" style="text-align: center;"><?php echo e(number_format($genaral_expense,2,'.',',')); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-4"></div>

                            <div class="col-md-4">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 25%">ACCOUNT NAME</th>
                                            <th style="text-align: center;width: 10%">OPENING BALANCE</th>
                                            <th style="text-align: center;width: 10%">BALANCE</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $i             = 1;
                                            $total_in      = 0;
                                            $total_out     = 0;
                                            $total_balance = 0;
                                        ?>

                                        <?php $__currentLoopData = $data_current; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="text-align: left">
                                                <?php echo e($value['account_name']); ?>

                                            </td>
                                            <td style="text-align: right"><?php echo e($value['opening_balance']); ?></td>
                                            <td style="text-align: right"><?php echo e($value['balance']); ?></td>
                                        </tr>

                                        <?php
                                            $i++;
                                            $total_in      = $total_in + $value['cash_in_sum'];
                                            $total_out     = $total_out + $value['cash_out_sum'];
                                            $total_balance = $total_balance + $value['balance'];
                                        ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;"><?php echo e($total_balance); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-4"></div>
                        </div>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/2_demo/Modules/Reports/Resources/views/daily_collection_report_print.blade.php ENDPATH**/ ?>