<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p style="font-size: 20px"><?php echo e($user_info['address']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_number']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_email']); ?></p>
                        <p style="font-size: 14px;text-align: right"><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['phone']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Date</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Details</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Transaction#</th>
                                    <th style="text-align: center;width: 9%" colspan="5">Description</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Ship/Cost</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Paid Through</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Payable</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Paid</th>
                                    <th style="text-align: center;width: 9%;vertical-align: bottom" rowspan="2">Balance</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 10%;vertical-align: bottom">Item Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Qty.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">U/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">T/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Discount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="10"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important"><strong><?php echo e($opening_balance); ?></strong></td>
                                    <td style="text-align: center;font-size: 12px !important"></td>
                                    <td style="text-align: right;font-size: 12px !important"><strong><?php echo e($opening_balance); ?></strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                <?php if($data->count() > 0): ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if ($value['type'] == 0)
                                    {
                                        $credit  = $value['amount'];
                                        $debit   = 0;
                                    }
                                    else
                                    {
                                        $debit  = $value['amount'];
                                        $credit = 0;
                                    }

                                    if ($value['account_head'] == 'supplier-advance-adjustment')
                                    {
                                        $sub_total      = $sub_total + $debit + $credit;
                                    }
                                    else
                                    {
                                        $sub_total      = $sub_total + $debit - $credit;
                                    }

                                    if ($value['account_head'] == 'purchase')
                                    {
                                        $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'previoue-due-paid')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'purchase-return')
                                    {
                                        $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'purchase')
                                    {
                                        $trans_number   = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'purchase-return')
                                    {
                                        $trans_number   = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'payment')
                                    {
                                        $trans_number   = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                    }
                                    elseif ($value['account_head'] == 'supplier-opening-balance')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'supplier-advance-adjustment')
                                    {
                                        $trans_number   = '';
                                    }
                                    elseif ($value['account_head'] == 'supplier-settlement')
                                    {
                                        $trans_number   = '';
                                        $value['note']  = 'সাপ্লাইয়ার সেটেলমেন্ট';
                                    }
                                    else 
                                    {
                                        $trans_number   = '';
                                    }

                                    if ($value['bill_id'] != 0)
                                    {
                                        $entries = \App\Models\BillEntries::where('bill_id', $value['bill_id'])->get();
                                    }
                                    else
                                    {
                                        $entries = collect();
                                    }
                                    
                                    if (($value['note'] == 'ক্রয় বাবদ প্রদেয়') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়'))
                                    {
                                        if ($value['bill_id'] != 0)
                                        {   
                                            $entries = \App\Models\BillEntries::where('bill_id', $value['bill_id'])->get();
                                        }
                                        else
                                        {
                                            $entries = collect();
                                        }
                                    }
                                    elseif ($value['note'] == 'ক্রয় রিটার্ন বাবদ প্রদান')
                                    {   
                                        if ($value['purchase_return_id'] != 0)
                                        {
                                            $entries = \App\Models\PurchaseReturnEntries::where('purchase_return_id', $value['purchase_return_id'])->get();
                                        }
                                        else
                                        {
                                            $entries = collect();
                                        }
                                    }
                                    else
                                    {
                                        $entries = collect();
                                    } 
                                ?>

                                <?php if($value['account_head'] != 'supplier-advance-adjustment'): ?>
                                    <tr>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e(date('d-m-Y', strtotime($value['date']))); ?></a>
                                        </td>

                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e($value['note']); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e($trans_number); ?></td>


                                        <?php if(($value['note'] == 'ক্রয় বাবদ প্রদেয়') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়')): ?> 
                                        <?php if($value['bill_id'] != 0): ?>
                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $detail_data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php if($key1 == 0): ?>
                                        <td style="text-align: left;font-size: 12px !important;"><?php echo e($detail_data1->productEntries->name); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data1->quantity); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data1->rate); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data1->total_amount); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data1->discount_type == 1 ? $detail_data1->discount_amount : (($detail_data1->discount_amount*$detail_data1->quantity*$detail_data1->rate)/100)); ?></td>
                                        <?php endif; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php else: ?>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <?php endif; ?>

                                        <?php if($value['bill_id'] != null && $entries->count() == 0): ?>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <td style="text-align: center;font-size: 12px !important;"></td>
                                        <?php endif; ?>
                                        <?php else: ?>
                                        <td colspan="5"></td>
                                        <?php endif; ?>


                                        <?php if(($value['note'] == 'ক্রয় বাবদ প্রদেয়') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়')): ?>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e(isset($value->bill->shipping_cost) ? $value->bill->shipping_cost : ''); ?></td>
                                        <?php else: ?>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>></td>
                                        <?php endif; ?>
                                        <td style="text-align: center;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>>
                                            <?php
                                                if ($value['note'] != 'ক্রয় বাবদ প্রদেয়') 
                                                {
                                                    if($value->paid_through != null)
                                                    {
                                                        echo $value->paidThroughAccounts->name;
                                                    }
                                                    else
                                                    {
                                                        $pMethode = supplierPaymentMethode($value->customer_id, $value->bill_id, $value->created_at);

                                                        echo $pMethode['name'];
                                                    }
                                                }
                                            ?>
                                        </td>
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e(number_format($debit,2,'.',',')); ?></td>
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e(number_format($credit,2,'.',',')); ?></td>
                                        <td style="text-align: right;font-size: 12px !important;vertical-align: middle" <?php if($value['bill_id'] != 0): ?> rowspan="<?php echo e($entries->count() > 0 ? $entries->count() : 1); ?>" <?php else: ?> rowspan="1" <?php endif; ?>><?php echo e(number_format($sub_total,2,'.',',')); ?></td>
                                    </tr>

                                    <?php if(($value['note'] == 'ক্রয় বাবদ প্রদেয়') || ($value['note'] == 'পূর্বের অগ্রিম থেকে সমন্বয়')): ?>
                                    <?php if($value['bill_id'] != 0): ?>
                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $detail_data2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key2 != 0): ?>
                                    <tr>
                                        <td style="text-align: left;font-size: 12px !important;"><?php echo e($detail_data2->productEntries->name); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->quantity); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->rate); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->total_amount); ?></td>
                                        <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->discount_type == 1 ? $detail_data2->discount_amount : (($detail_data2->discount_amount*$detail_data2->quantity*$detail_data2->rate)/100)); ?></td>
                                    </tr>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                    <?php endif; ?>
                                 
                                <?php $i++; ?>

                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="12" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important"><?php echo e(number_format($sub_total,2,'.',',')); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/2_demo/Modules/Reports/Resources/views/supplier_due_report_details.blade.php ENDPATH**/ ?>