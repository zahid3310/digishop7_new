<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MajorCategories extends Model
{
    protected $table = "major_categories";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function product()
    {
        return $this->hasMany(Products::class, "major_category_id")->where('status', 1);
    }

    public function productEntries()
    {
        return $this->hasMany(ProductEntries::class, "major_category_id")->where('status', 1);
    }
}
