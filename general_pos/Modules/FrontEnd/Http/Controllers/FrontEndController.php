<?php

namespace Modules\FrontEnd\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
//Models
use App\Models\Sliders;
use App\Models\MajorCategories;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\ProductImages;
use App\Models\Customers;
use App\Models\CustomersLogin;
use App\Models\Orders;
use App\Models\OrderEntries;
use Response;
use DB;
use Hash;
use PDF;

class FrontEndController extends Controller
{

    public function search(Request $request)
    {
       $name =  $request->search;
       $category_id =  $request->category;

       $products =          ProductEntries::query()
                            ->where('name', 'LIKE', "%{$name}%")
                            ->paginate(15);
                            // ->Where('major_category_id', 'LIKE', "%{$category_id}%") 
                            

                            // dd($products);
        return view('frontend::layouts.product_list', compact('products'));
    }

    public function index()
    {
    	$sliders 			= Sliders::selectRaw("sliders.image")
                            			->where('status', 1)
                            			->get();

        $major_categories   = MajorCategories::where('status', 1)
                                                ->selectRaw('id, name')
                                                ->with('productEntries')
                                                ->get();

        return view('frontend::index', compact('sliders', 'major_categories'));
    }

    public function majorCategoryList()
    {
    	$majorCategories     = MajorCategories::where('status', 1)
                                                ->selectRaw('id, name')
                                                ->with('product')
                                                ->get();

        return Response::json($majorCategories);
    }

    public function productDetails($id)
    {   
        $find_product   = ProductEntries::where('status', 1)
                                        ->selectRaw('product_entries.*')
                                        ->find($id);

        $product_images = ProductImages::where('product_entry_id', $id)->get();

        $products       = ProductEntries::where('status', 1)
                                        ->where('id', '!=', $id)
                                        ->selectRaw('product_entries.*')
                                        ->get()
                                        ->take(5);

        return view('frontend::product_details', compact('find_product', 'product_images', 'products'));
    }

    public function productList()
    {
        $major_categories   = MajorCategories::where('status', 1)
                                                ->selectRaw('id, name')
                                                ->with('productEntries')
                                                ->get();

        $products           = ProductEntries::where('status', 1)
                                        ->selectRaw('product_entries.*')
                                        ->paginate(15);



        return view('frontend::layouts.product_list', compact('products'));
    }

    public function about()
    {
        return view('frontend::layouts.about');
    }

    public function contact()
    {
        return view('frontend::layouts.contact');
    }

    public function checkout()
    {
        return view('frontend::layouts.checkout');
    }

    
    public function cartPage()
    {
        return view('frontend::layouts.cart_page');
    }

    public function placeOrder(Request $request)
    {
        $user_id    = session('customer_id');
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $data_find                      = Orders::orderBy('created_at', 'DESC')->first();
            $order_number                   = $data_find != null ? $data_find['order_number'] + 1 : 1;

            $order                          = new Orders;;
            $order->order_number            = $order_number;
            $order->customer_id             = $user_id;
            $order->first_name              = $data['first_name'];
            $order->last_name               = $data['last_name'];
            $order->phone                   = $data['phone'];
            $order->email                   = $data['email'];
            $order->address                 = $data['address'];
            // $order->order_date              = date('Y-m-d', strtotime($data['selling_date']));
            $order->order_date              = date('Y-m-d H:i:s');
            $order->order_amount            = $data['totalCartPrice'];
            $order->order_note              = $data['order_note'];
            $order->created_by              = $user_id;

            if ($order->save())
            {
                foreach ($data['productId'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $order_entries[] = [
                        'order_id'          => $order['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value,
                        'main_unit_id'      => $product_buy_price['unit_id'],
                        'conversion_unit_id'=> $product_buy_price['unit_id'],
                        'customer_id'       => $user_id,
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $order['order_amount'],
                        'discount_type'     => 0,
                        'discount_amount'   => 0,
                        'created_by'        => 1,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('order_entries')->insert($order_entries);
                DB::commit();
                return redirect()->route('customer_order_list')->with("success","Order Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function customerLogin()
    {
        return view('frontend::layouts.customer_login');
    }

    public function loginSubmit(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

        $customer_loginInfo = CustomersLogin::where('username', '=', $username)->first();

        if($customer_loginInfo == null)
        {
           return back()->with('error', 'User dose not exit'); 
        }elseif($customer_loginInfo->username != $username){
            return back()->with('error', 'These credentials do not match our records !');
        }elseif(!Hash::check($password,$customer_loginInfo->password)){
            return back()->with('error', 'These credentials do not match our records !');
        }else{
            $id             = $customer_loginInfo->id;
            $customer_id    = $customer_loginInfo->customer_id;
            $username       = $customer_loginInfo->username;
            session()->flash('success', 'Successully Logged in !');
            $data           =  $request->input();
            $request->session()->put('id',$id);
            $request->session()->put('customer_id',$customer_id);
            $request->session()->put('username',$username);

            return redirect()->route('cart_page');
        }
    }

    public function customerLogout(Request $request)
    {
          $session_data = $request->session()->flush();
          return redirect()->route('home');
    }

    public function customerRegistration(Request $request)
    {

            $customer_login_info = CustomersLogin::all();

            if(CustomersLogin::where('username', $request->username)->exists())
            {
                return back()->with("username_check","This Username not acceptable.");
            }else{
                $request->validate([
                'name'      => 'required',
                'username'      => 'required',
                'phone'      => 'required',
                'password'      => 'required',
                
            ], [
                'name.required'     => 'Fullname is required',
                'username.required'     => 'Username is required',
                'phone.required'     => 'Phone Number is required',
                'password.required'     => 'Password Number is required',
            ]);
            }


            $data = $request->all();
            $customer                          = new Customers;
            $customer->name                    = $data['name'];
            $customer->contact_type            = 1;
            $customer->joining_date            = date('Y-m-d');
            $customer->phone                   = $data['phone'];
            $customer->email                   = $data['email'];
            $customer->save();

            $registration                          = new CustomersLogin;
            $registration->customer_id             = $customer['id'];
            $registration->username                = $data['username'];
            $registration->password                = Hash::make($data['password']);
            $registration->save();
            
            return back()->with("registration_success","Registration successfully.");
    }

    public function customerDashboard()
    {
        $session_customer_id = session('customer_id');

        $customer_info =
        DB::table('customer_login_info')
        ->select('customer_login_info.*', 'customers.*','customers.id as customers_primary_id')
        ->leftjoin('customers','customers.id','=','customer_login_info.customer_id')
        ->where('customer_login_info.id', '=', $session_customer_id)
        ->get();

        return view('frontend::layouts.customer_dashboard')->with("customer_info", $customer_info);
    }

    public function customerOrderList()
    {
        $session_customer_id = session('customer_id');
        $orderList = 
        DB::table('orders')
        ->select('orders.*', 'customers.name')
        ->leftjoin('customers','customers.id','=','orders.customer_id')
        ->where('orders.customer_id', '=' , $session_customer_id)
        ->get();
        return view('frontend::layouts.customer_order_list')->with("orderList", $orderList);
    }

    public function orderInformation(Request $request,$id)
    {
        $order_id = decrypt($id);
        $orderDetails = 
        DB::table('orders')
        ->select('orders.*','customers.*','customers.id as customer_id')
        ->leftjoin('customers','customers.id','=','orders.customer_id')
        ->where('orders.id', '=' , $order_id)
        ->get();

        $productDetails = 
        DB::table('order_entries')
        ->join('product_entries','product_entries.id','=','order_entries.product_entry_id')
        ->select('order_entries.*','product_entries.name','product_entries.product_code')
        ->where('order_entries.order_id',$order_id)
        ->get();


        return view('frontend::layouts.order_information_pdf',compact('orderDetails','productDetails')); 

        /** For Pdf system
        $pdf = PDF::loadView('frontend::layouts.order_information_pdf',array('orderDetails' => $orderDetails, 'productDetails' => $productDetails ));
        return $pdf->stream('orderInformation.pdf');
        return $pdf->download('orderInformation.pdf');
        **/     
    }
}
