@extends('frontend::layouts.app')

@section('main_body')
<!-- End Header -->
<main class="main checkout">
<!-- <div class="page-header bg-dark"
style="background-image: url('images/shop/page-header-back.jpg'); background-color: #3C63A4;">
<h1 class="page-title">Checkout</h1>
<ul class="breadcrumb">
<li><a href="demo1.html"><i class="d-icon-home"></i></a></li>
<li>Checkout</li>
</ul>
</div> -->
<!-- End PageHeader -->
<div class="page-content pt-10 pb-10">
	<div class="step-by pt-2 pb-2 pr-4 pl-4">
		<h3 class="title title-simple title-step visited"><a href="{{route('cart_page')}}">1. Shopping Cart</a></h3>
		<h3 class="title title-simple title-step active"><a href="{{route('checkout')}}">2. Checkout</a></h3>
		<!-- <h3 class="title title-simple title-step"><a href="order.html">3. Order Complete</a></h3> -->
	</div>
	<div class="container mt-8">
		<form action="{{route('place_order')}}" method="POST" class="form">
			@csrf
			<div class="row gutter-lg">
				<div class="col-lg-7 mb-6">
					<h3 class="title title-simple text-left">Billing Details</h3>
					<div class="row">
						<div class="col-xs-6">
							<label>First Name *</label>
							<input type="text" class="form-control" name="first_name"/ required="">
						</div>
						<div class="col-xs-6">
							<label>Last Name *</label>
							<input type="text" class="form-control" name="last_name"/ required="">
						</div>

						<div class="col-xs-6">
							<label>Email *</label>
							<input type="email" class="form-control" name="email"/>
						</div>

						<div class="col-xs-6">
							<label>Phone *</label>
							<input type="text" class="form-control" name="phone"/ required="">
						</div>


					</div>

					<label>Address *</label>
					<textarea class="form-control" name="address" placeholder="Enter your full address"></textarea>


					<h3 class="title title-simple text-left mb-3">Additional information</h3>
					<label>Order Notes (optional)</label>
					<textarea name="order_note" class="form-control" cols="30" rows="6"
					placeholder="Notes about your order, e.g. special notes for delivery"></textarea>
				</div>
				<aside class="col-lg-5 sticky-sidebar-wrapper">
					<div class="sticky-sidebar" data-sticky-options="{'bottom': 50}">
						<h3 class="title title-simple text-left">Your Order</h3>
						<div class="summary mb-4">
							<table class="order-table">
								{{ Session::get('productId')}}
								<tbody>
									<tr>
										<h5>Product</h5>
										<ul class="list-group checkout_details"></ul>
									</tr>

									<tr class="summary-subtotal">
										<td>
											<h4 class="summary-subtitle">Subtotal</h4>
										</td>
										<td class="summary-subtotal-price totalCartPrice">
										</td>												
									</tr>
									<tr class="sumnary-shipping shipping-row-last">
										<td colspan="2">
											<h4 class="summary-subtitle">Shipping</h4>
											<ul>
												<li>
													<div class="custom-radio">
														<input type="radio" id="free-shipping" name="shipping" class="custom-control-input" value="1" checked>
														<label class="custom-control-label" for="free-shipping">Free
														Shipping</label>
													</div>
												</li>

											</ul>
										</td>
									</tr>
									<tr class="summary-subtotal">
										<td>
											<h4 class="summary-subtitle">Total</h4>
										</td>
										<td>
											<div class="click-cart">
												<input class="form-control totalCartPrice2"  name="totalCartPrice" type="hidden" value="">
												<p class="totalCartPrice"></p>
											</td>												
										</tr>
									</tbody>
								</table>

								<p class="checkout-info">Your personal data will used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.</p>

							 @if(session('customer_id'))
                               <button type="submit" class="btn btn-dark btn-order">Place Order</button>
                             @else
                             <a href="{{ route('customer_login') }}" class="btn btn-dark"><span>Place Order</span></a>
                             @endif
								
							</div>
						</div>
					</aside>
				</div>
			</form>
		</div>
	</div>
</main>
<!-- End Main -->
@endsection