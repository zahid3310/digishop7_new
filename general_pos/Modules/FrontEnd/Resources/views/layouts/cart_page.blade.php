@extends('frontend::layouts.app')

@section('main_body')
<main class="main cart">
    <div class="page-content pt-10 pb-10">
        <div class="step-by pt-2 pb-2 pr-4 pl-4">
            <h3 class="title title-simple title-step visited"><a href="{{route('cart_page')}}">1. Shopping Cart</a></h3>
        <h3 class="title title-simple title-step active"><a href="{{route('checkout')}}">2. Checkout</a></h3>
        </div>
        <div class="container mt-8 mb-4">
            <div class="row gutter-lg">
                <div class="col-lg-8 col-md-12">
                    <table class="shop-table cart-table mt-2">
                        <thead>
                            <tr>
                                <th><span>Product</span></th>
                                <th>Name</th>
                                <th><span>Price</span></th>
                                <th><span>quantity</span></th>
                                <th>Subtotal</th>
                            </tr>
                        </thead>
                        <tbody class="getCartDetails"></tbody>
                    </table>
                    <div class="cart-actions mb-6 pt-6">
                        <!-- <div class="coupon">
                            <input type="text" name="coupon_code" class="input-text form-control" id="coupon_code" value="" placeholder="Coupon code">
                            <button type="submit" class="btn btn-md">Apply Coupon</button>
                        </div> -->
                        <!-- <button type="submit" class="btn btn-md btn-icon-left"><i
                            class="d-icon-refresh"></i>Update Cart</button> -->

                           <!--  <a href="{{ url()->previous() }}" class="btn btn-md btn-icon-left"><i
                            class="d-icon-refresh"></i>Update Cart</a>  -->

                            <button class="btn btn-md btn-icon-left" onclick="update_cart()"><i
                            class="d-icon-refresh"></i>Update Cart</button>

                        </div>
                    </div>
                    <aside class="col-lg-4 sticky-sidebar-wrapper">
                        <div class="sticky-sidebar" data-sticky-options="{'bottom': 20}">
                            <div class="summary mb-4">
                                <h3 class="summary-title text-left">Cart Totals</h3>
                                <table class="shipping">
                                    <tr class="summary-subtotal">
                                        <td>
                                            <h4 class="summary-subtitle">Subtotal</h4>
                                        </td>
                                        <td>
                                            <p class="summary-subtotal-price totalCartPrice"></p>
                                        </td>                                               
                                    </tr>
                                    <!-- <tr class="sumnary-shipping shipping-row-last">
                                        <td colspan="2">
                                            <h4 class="summary-subtitle">Shipping</h4>
                                            <ul>
                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="free-shipping" name="shipping" class="custom-control-input" checked>
                                                        <label class="custom-control-label" for="free-shipping">Free
                                                        Shipping</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="standard_shipping" name="shipping" class="custom-control-input">
                                                        <label class="custom-control-label" for="standard_shipping">Standard</label>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="express_shipping" name="shipping" class="custom-control-input">
                                                        <label class="custom-control-label" for="express_shipping">Express</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr> -->
                                </table>
                                <!-- <div class="shipping-address pb-4">
                                    <label>Shipping to CA.</label>
                                    <div class="select-box">
                                        <select name="country" class="form-control">
                                            <option value="us" selected>United States</option>
                                            <option value="uk">United Kingdom</option>
                                            <option value="fr">France</option>
                                            <option value="aus">Austria</option>
                                        </select>
                                    </div>
                                    <div class="select-box">
                                        <select name="country" class="form-control">
                                            <option value="default" selected="selected">California</option>
                                        </select>
                                    </div>
                                    <input type="text" class="form-control" name="code" placeholder="Town / city" />
                                    <input type="text" class="form-control" name="code" placeholder="zip" />
                                    <a href="#" class="btn btn-md">Update totals</a>
                                </div> -->
                                <table>
                                    <tr class="summary-subtotal">
                                        <td>
                                            <h4 class="summary-subtitle">Total</h4>
                                        </td>
                                        <td>
                                            <p class="summary-total-price totalCartPrice total_update_price"></p>
                                        </td>                                               
                                    </tr>
                                </table>
                                <a href="{{ route('checkout') }}" class="btn btn-dark btn-checkout">Proceed to checkout</a>
                            </div>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </main>
    @endsection