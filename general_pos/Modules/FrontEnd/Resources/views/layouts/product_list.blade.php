@extends('frontend::layouts.app')

@section('main_body')
<main class="main mt-lg-4">
    <div class="page-content">
        <div class="container">
            <div class="row main-content-wrap gutter-lg">
                <aside class="col-lg-3 sidebar sidebar-fixed shop-sidebar sticky-sidebar-wrapper">
                    <div class="sidebar-overlay">
                        <a class="sidebar-close" href="#"><i class="d-icon-times"></i></a>
                    </div>
                    <div class="sidebar-content">
                        <div class="sticky-sidebar">
                            <div class="widget widget-collapsible">
                                <h3 class="widget-title">All Top Categories</h3>
                                <ul class="widget-body filter-items search-ul">
                                    @if($majorCategories->count() > 0)
                                    @foreach($majorCategories as $key => $major_category)
                                    <li class="with-ul">
                                        <a href="{{route('product_list')}}">{{ $major_category['name'] }}</a>
                                        <ul>
                                            @if($major_category->product->count() > 0)
                                            @foreach($major_category->product as $key1 => $category)
                                            <li><a href="{{route('product_list')}}">{{ $category['name'] }}</a></li>
                                            @endforeach
                                            @endif
                                        </ul>
                                    </li>
                                    @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </aside>

                <div class="col-xl-9 col-lg-8">
                    {!! $products->links() !!}
                    <br>
                    <div class="product-lists product-wrapper">
                        @if($products->count() > 0)
                        @foreach($products as $key => $product)
                        <div class="product product-list">
                            <figure class="product-media">
                                <a href="{{ route('product_details', $product->id) }}">
                                    @if(!empty(productImage($product['id'])))
                                    <img src="{{ url('public/images/products/'.productImage($product['id'])->image) }}" alt="product"
                                        width="280" height="315">
                                    @else
                                    <img src="public/default-product-image.jpeg" alt="product"
                                        width="280" height="315">
                                    @endif
                                </a>
                            </figure>
                            <div class="product-details">
                                <div class="product-cat">
                                    <a href="{{ route('product_details', $product->id) }}">{{ $product->major_category_id != null ? $product->product->name : '' }}</a>
                                </div>
                                <h3 class="product-name">
                                    <a href="{{ route('product_details', $product->id) }}">{{ $product->name }}</a>
                                </h3>
                                <div class="product-price">
                                    <span class="price">{{ 'BDT '. $product->sell_price }}</span>
                                </div>
                                <!-- <p class="product-short-desc">
                                    Pellentesque habitant morbi tristique senectus et netus et malesuada fames
                                    ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
                                    tempor sit amet, ante. Donec eu libero sit amet…
                                </p> -->
                                <div class="product-action">
                                    <a class="btn-product btn-cart addItemToCart"
                                        data-id="{{$product->id}}" data-thumb="{{ !empty(productImage($product['id'])) ? url('public/images/products/'.productImage($product['id'])->image) : url('public/default-product-image.jpeg') }}" data-title="{{$product->name}}" data-price="{{$product->sell_price}}" data-url="{{ route('product_details', $product->id) }}" data-quantity="1" data-toggle="modal" href="javascript:void(0)" title="Add to cart">ADD TO CART</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div>

                    {!! $products->links() !!}

                    <br>

                    <!-- <nav class="toolbox toolbox-pagination mt-6">
                        <p class="show-info">Showing <span>6 of 56</span> Products</p>
                        <ul class="pagination">
                            <li class="page-item disabled">
                                <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1"
                                    aria-disabled="true">
                                    <i class="d-icon-arrow-left"></i>Prev
                                </a>
                            </li>
                            <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item page-item-dots"><a class="page-link" href="#">6</a></li>
                            <li class="page-item">
                                <a class="page-link page-link-next" href="#" aria-label="Next">
                                    Next<i class="d-icon-arrow-right"></i>
                                </a>
                            </li>
                        </ul>
                    </nav> -->
                </div>
            </div>
        </div>
    </div>
</main>
@endsection