@extends('layouts.app')

@section('title', 'Upload Product Images')

@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-18">Upload Product Images</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">Ecommerce Content</a></li>
                                <li class="breadcrumb-item active">Upload Product Images</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <form action="{{ route('ecommerce_product_images_edit') }}" method="get">

                                <div class="form-group row">
                                    <label for="productname" class="col-md-2 col-form-label">Product</label>

                                    <div class="col-md-4">
                                        <select id="product_entry_id" name="product_entry_id" class="form-control select2">
                                           <option value="{{ $product_name != null ? $product_name->id : '' }}">{{ $product_name != null ? $product_name->name : '--Select Product--' }}</option>
                                        </select>
                                    </div>

                                   

                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success" onclick="addImagesForProduct()">Search</button>
                                    </div>
                                </div>

                                <button id="saveBtn" class="btn btn-primary waves-effect waves-light" style="display: none"></button>
                            </form>

                            <form id="FormSubmit" action="{{ route('ecommerce_product_images_update') }}" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <input name="product_entries_id" type="hidden" class="form-control" value="{{ $product_name != null ? $product_name->id : '' }}">

                                 <div class="col-md-12">
                                    <label>Product Description</label>
                                    <textarea name="description" class="form-control"></textarea>
                                 </div>
                                 <br>

                                <div id="imageDiv" style="display: none" class="row">
                                    <div class="col-md-12">
                                        <hr style="margin-top: 0px !important">

                                        <div data-repeater-list="inner-group" class="inner form-group mb-0 row">
                                            <div class="inner col-lg-12 ml-md-auto">

                                                <h5>Add Images</h5>

                                                <div class="col-md-12">
                                                @if($product_images->count() > 0)
                                                @foreach($product_images as $key2 => $value2)
                                                    <div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_old_img_{{$value2->id}}">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                                            <img style="height: 150px;width: 150px" src="{{ url('public/images/products/'.$value2->image) }}">
                                                        </div>

                                                        <div class="col-lg-1 col-md-2 col-sm-6 col-6 form-group" onclick="deleteImg({{$value2->id}})">
                                                            <i class="btn btn-success btn-block bx bx-trash-alt font-size-20"></i>
                                                        </div>
                                                    </div>
                                                @endforeach
                                                @endif
                                            </div>

                                            <div class="col-md-12 input_fields_wrap_img getMultipleRowImg">
                                            </div>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 col-sm-6"></div>
                                            <div class="col-lg-1 col-md-2 col-sm-6 col-12 form-group">
                                                <div id="add_field_button_img" class="add_field_button_img">
                                                    <i class="btn btn-success btn-block bx bx-plus font-size-20"></i>
                                                </div>
                                            </div>
                                        </div>

                                        <hr style="margin-top: 0px !important">
                                    </div>
                                </div>

                                <div id="saveDiv" style="display: none" class="form-group row">
                                    <div class="button-items col-md-3">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('home') }}">Close</a></button>
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url  = $('.site_url').val();

        $(".productCategory").select2({
            ajax: { 
                url:  site_url + '/products/product-category-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                cache: true
            }
        });

        $("#product_entry_id").select2({
            ajax: { 
            url:  site_url + '/reports/sales-statement/product-list',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                if (result['id'] != 0)
                {
                    return result['text'];
                }
            },
        });

        var productEntryId = (new URL(location.href)).searchParams.get('product_entry_id'); 

        if (productEntryId != null)
        {
            $("#saveDiv").show();
            $("#imageDiv").show();
        }
    });
</script>

<script type="text/javascript">
        var max_fields_img       = 50;                           //maximum input boxes allowed
        var wrapper_img          = $(".input_fields_wrap_img");      //Fields wrapper
        var add_button_img       = $(".add_field_button_img");       //Add button ID
        var index_no_img         = 1;

        //For apending another rows start
        var y       = {{$images_count}};
        $(add_button_img).click(function(e)
        {   
            e.preventDefault();

            if(y < max_fields_img)
            {
                y++;

                if (y == 1)
                {
                    var add_btn                 =   '<div style="margin-bottom: 15px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button_img">\n' + 
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButtonImage()"></i>' +
                                                    '</div>\n';
                }
                else
                {
                    var add_btn                 =   '<div style="margin-bottom: 15px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field_img" data-val="'+y+'">\n' + 
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                    '</div>\n';
                }

                $('.getMultipleRowImg').append(' ' + '<div style="margin-bottom: 0px !important" class="mb-3 row align-items-center di_img_'+y+'">' +
                                                        '<div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">\n' +
                                                            '<input name="images[]" type="file" class="form-control" required>\n' +

                                                        '</div>\n' +

                                                        add_btn + 
                                                    '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }
        });
        //For apending another rows end

        $(wrapper_img).on("click",".remove_field_img", function(e)
        {
            e.preventDefault();

            var y = $(this).attr("data-val");

            $('.di_img_'+y).remove(); y--;
        });
    </script>

<script type="text/javascript">
    function preventDoubleClick()
    {
        $('.enableOnInput').prop('disabled', true)
        $('#FormSubmit').submit();
    }

    function addImagesForProduct()
    {
        $("#saveBtn").click();
    }
</script>

<script type="text/javascript">
    function deleteImg(product_image_id)
    {
        var site_url = $(".site_url").val();
        $.get(site_url + '/ecommerce/productImages/delete-product-image/' + product_image_id, function(data){

            if (data == 1)
            {
                $(".di_old_img_"+product_image_id).remove();
            }
        });
    }
</script>
@endsection