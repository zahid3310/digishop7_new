<!DOCTYPE html>
<html>

<head>
    <title>Balance Sheet</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

    .table td, .table th {
        font-size: 12px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Balance Sheet</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">As at {{ date('d-m-Y') }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="3">DEBIT</th>

                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="3">CREDIT</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="font-size: 10px;text-align: center;width: 2%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">DESCRIPTION</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">TOTAL</th>

                                    <th style="font-size: 10px;text-align: center;width: 2%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">DESCRIPTION</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">TOTAL</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                = 1;
                                    $total_debit      = 0;
                                    $total_credit     = 0;
                                ?>
                                
                                <tr>
                                    <td style="text-align: center;">1</td>
                                    <td style="text-align: left;">Capital of Business</td>
                                    <td style="text-align: right">{{ number_format($data['debit']['capital'],0,'.',',') }}</td>

                                    <td style="text-align: center;">1</td>
                                    <td style="text-align: left;">Fixed Asstes</td>
                                    <td style="text-align: right">{{ number_format($data['credit']['fixed_asset'],0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">2</td>
                                    <td style="text-align: left;">Loan Liability</td>
                                    <td style="text-align: right">{{ number_format($data['debit']['loan_liability'],0,'.',',') }}</td>

                                    <td style="text-align: center;">2</td>
                                    <td style="text-align: left;">Loan Pay Advance</td>
                                    <td style="text-align: right">{{ number_format(abs($data['credit']['loan_advance']),0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">3</td>
                                    <td style="text-align: left;">Profit & Loss (Total)</td>
                                    <td style="text-align: right">{{ number_format($data['debit']['profit_loss'],0,'.',',') }}</td>

                                    <td style="text-align: center;">3</td>
                                    <td style="text-align: left;">Party Advance</td>
                                    <td style="text-align: right">{{ number_format(abs($data['credit']['party_advance']),0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">4</td>
                                    <td style="text-align: left;">Party Liability</td>
                                    <td style="text-align: right">{{ number_format($data['debit']['party_liability'],0,'.',',') }}</td>

                                    <td style="text-align: center;">4</td>
                                    <td style="text-align: left;">Customer Due</td>
                                    <td style="text-align: right">{{ number_format($data['credit']['customer_due'],0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">5</td>
                                    <td style="text-align: left;"></td>
                                    <td style="text-align: right"></td>

                                    <td style="text-align: center;">5</td>
                                    <td style="text-align: left;">Stock Value</td>
                                    <td style="text-align: right">{{ number_format($data['credit']['stock_value'],0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">6</td>
                                    <td style="text-align: left;"></td>
                                    <td style="text-align: right"></td>

                                    <td style="text-align: center;">6</td>
                                    <td style="text-align: left;">Cash (TK)</td>
                                    <td style="text-align: right">{{ number_format($data['credit']['cash_tk'],0,'.',',') }}</td>
                                </tr>

                                <tr>
                                    <td style="text-align: center;">7</td>
                                    <td style="text-align: left;"></td>
                                    <td style="text-align: right"></td>

                                    <td style="text-align: center;">7</td>
                                    <td style="text-align: left;">Bank (TK)</td>
                                    <td style="text-align: right">{{ number_format($data['credit']['bank_tk'],0,'.',',') }}</td>
                                </tr>

                                <?php
                                    $total_debit      = $total_debit + $data['debit']['party_liability'] + $data['debit']['loan_liability'] + $data['debit']['profit_loss'] + $data['debit']['capital'];

                                    $total_credit     = $total_credit + $data['credit']['fixed_asset'] + abs($data['credit']['party_advance']) + $data['credit']['customer_due'] + $data['credit']['stock_value'] + $data['credit']['cash_tk'] + $data['credit']['bank_tk'] + abs($data['credit']['loan_advance']);
                                ?>
                                </tr>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="2" style="text-align: right;">TOTAL DEBIT</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($total_debit,0,'.',',') }}</th>
                                    <th colspan="2" style="text-align: right;">TOTAL CREDIT</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($total_credit,0,'.',',') }}</th>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>