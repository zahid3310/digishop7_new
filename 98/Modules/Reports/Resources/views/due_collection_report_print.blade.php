<!DOCTYPE html>
<html>

<head>
    <title>Due Collection Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }

   /* .table td, .table th {
        font-size: 12px !important;
    }*/
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Due Collection Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">From Date : {{ date('d-m-Y', strtotime($from_date)) }}  To Date : {{ date('d-m-Y', strtotime($to_date)) }}</th>
                                </tr>
                            </thead>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 20%">Customer</th>
                                    <th style="text-align: center;width: 10%">Payment Date</th>
                                    <th style="text-align: center;width: 10%">Payment#</th>
                                    <th style="text-align: center;width: 10%">Paid Through</th>
                                    <th style="text-align: center;width: 10%">Amount</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php
                                    $total_amount = 0;
                                ?>

                                @foreach($collections as $key=> $value)
                                <tr>
                                    <td style="text-align: center;vertical-align: middle" rowspan="{{count($value)}}">{{ $loop->index + 1 }}</td>
                                    <td style="text-align: left;vertical-align: middle" rowspan="{{count($value)}}">{{ $value[0]->customer->name }}</td>
                                    <td style="text-align: center;vertical-align: middle">{{ date('d-m-Y', strtotime($value[0]->date)) }}</td>
                                    <td style="text-align: center;vertical-align: middle">{{ 'PM - ' . str_pad($value[0]->payment->payment_number, 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;vertical-align: middle">{{ $value[0]->account->account_name }}</td>
                                    <td style="text-align: right;vertical-align: middle">{{ number_format($value[0]->amount,2,'.',',') }}</td>
                                </tr>

                                <?php
                                    $sub_total_amount = 0;
                                ?>

                                @foreach($value as $key1=> $value1)

                                <?php 
                                    $sub_total_amount  = $sub_total_amount + $value1->amount;
                                ?>
                                @if(($key1 != 0))
                                <tr>
                                    <td style="text-align: center;vertical-align: middle">{{ date('d-m-Y', strtotime($value1->date)) }}</td>
                                    <td style="text-align: center;vertical-align: middle">{{ 'PM - ' . str_pad($value1->payment->payment_number, 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;vertical-align: middle">{{ $value1->account->account_name }}</td>
                                    <td style="text-align: right;vertical-align: middle">{{ number_format($value1->amount,2,'.',',') }}</td>
                                </tr>
                                @endif
                                @endforeach

                                <?php
                                    $total_amount = $total_amount + $sub_total_amount;
                                ?>

                                @endforeach
                                
                                <tr>
                                    <th colspan="5" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($total_amount,2,'.',',') }}</th>
                                </tr>
                                 
                            </tbody>

                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>