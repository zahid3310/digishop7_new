<?php

namespace Modules\Customers\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\CustomerTypes;
use Response;
use DB;

class CustomersController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        // $customers  = Customers::orderBy('created_at', 'ASC')->get();
        
        // foreach($customers as $key => $val)
        // {
        //     if ($val['contact_type'] == 0)
        //     {
        //         $invoice                    = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
        //                                                 ->where('invoices.customer_id', $val['id'])
        //                                                 ->where('invoices.type', 2)
        //                                                 ->select('invoices.*')
        //                                                 ->first();
        
        //         if ($invoice != null)
        //         {
        //             $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
        //                                                 ->where('payments.customer_id', $val['id'])
        //                                                 ->where('payment_entries.invoice_id', $invoice['id'])
        //                                                 ->sum('payments.amount');
        
        //             $invoice->invoice_amount    = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //             $invoice->due_amount        = $val['opening_balance'] != null ? $val['opening_balance'] : 0 - $payment_amount;
        //             $invoice->updated_by        = Auth::user()->id;
        //             $invoice->save();
        
        //             $find_transaction   = Transactions::where('invoice_id', $invoice['id'])
        //                                               ->where('type', 0)
        //                                               ->delete();
        
        //             $transaction_data['date']           = date('Y-m-d');
        //             $transaction_data['type']           = 0;
        //             $transaction_data['account_head']   = 'customer-opening-balance';
        //             $transaction_data['transaction_id'] = $invoice->id;
        //             $transaction_data['customer_id']    = $val['id'];
        //             $transaction_data['note']           = 'কাস্টমার ওপেনিং ব্যালেন্স';
        //             $transaction_data['amount']         = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //             $transaction_data['paid_through']   = null;
        //             transactions($transaction_data);
        //         }
        //         else
        //         {   
        //             if($val['opening_balance'] > 0)
        //             {
        //                 $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
        //                 $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;
            
        //                 $invoice                    = new Invoices;
        //                 $invoice->invoice_number    = $invoice_number;
        //                 $invoice->customer_id       = $val['id'];
        //                 $invoice->invoice_date      = date('Y-m-d');
        //                 $invoice->invoice_amount    = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $invoice->due_amount        = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $invoice->total_buy_price   = 0;
        //                 $invoice->total_discount    = 0;
        //                 $invoice->type              = 2;
        //                 $invoice->branch_id         = $val['branch_id'];
        //                 $invoice->created_by        = Auth::user()->id;
        //                 $invoice->save();
            
        //                 $transaction_data['date']           = date('Y-m-d');
        //                 $transaction_data['type']           = 0;
        //                 $transaction_data['account_head']   = 'customer-opening-balance';
        //                 $transaction_data['transaction_id'] = $invoice->id;
        //                 $transaction_data['customer_id']    = $val['id'];
        //                 $transaction_data['note']           = 'কাস্টমার ওপেনিং ব্যালেন্স';
        //                 $transaction_data['amount']         = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $transaction_data['paid_through']   = null;
        //                 transactions($transaction_data);
        //             }
        //         }
        //     }
        //     else
        //     {
        //         $bill                       = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
        //                                                 ->where('bills.vendor_id', $val['id'])
        //                                                 ->where('bills.type', 2)
        //                                                 ->select('bills.*')
        //                                                 ->first();
        
        //         if ($bill != null)
        //         {
        //             $payment_amount             = Payments::leftjoin('payment_entries', 'payment_entries.payment_id', 'payments.id')
        //                                                 ->where('payments.customer_id', $val['id'])
        //                                                 ->where('payment_entries.bill_id', $bill['id'])
        //                                                 ->sum('payments.amount');
        
        //             $bill->bill_amount          = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //             $bill->due_amount           = $val['opening_balance'] != null ? $val['opening_balance'] : 0 - $payment_amount;
        //             $bill->updated_by           = Auth::user()->id;
        //             $bill->save();
        
        //             $find_transaction   = Transactions::where('bill_id', $bill['id'])
        //                                               ->where('type', 1)
        //                                               ->delete();
        
        //             $transaction_data['date']           = date('Y-m-d');
        //             $transaction_data['type']           = 1;
        //             $transaction_data['account_head']   = 'supplier-opening-balance';
        //             $transaction_data['transaction_id'] = $bill->id;
        //             $transaction_data['customer_id']    = $val['id'];
        //             $transaction_data['note']           = 'সাপ্লাইয়ার ওপেনিং ব্যালেন্স';
        //             $transaction_data['amount']         = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //             $transaction_data['paid_through']   = null;
        //             transactions($transaction_data);
        //         }
        //         else
        //         {   
        //             if($val['opening_balance'] > 0)
        //             {
        //                 $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
        //                 $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;
            
        //                 $bill                       = new Bills;
        //                 $bill->bill_number          = $bill_number;
        //                 $bill->vendor_id            = $val['id'];
        //                 $bill->bill_date            = date('Y-m-d');
        //                 $bill->bill_amount          = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $bill->due_amount           = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $bill->total_discount       = 0;
        //                 $bill->type                 = 2;
        //                 $bill->branch_id            = $val['branch_id'];
        //                 $bill->created_by           = Auth::user()->id;
        //                 $bill->save();
            
        //                 $transaction_data['date']           = date('Y-m-d');
        //                 $transaction_data['type']           = 1;
        //                 $transaction_data['account_head']   = 'supplier-opening-balance';
        //                 $transaction_data['transaction_id'] = $bill->id;
        //                 $transaction_data['customer_id']    = $val['id'];
        //                 $transaction_data['note']           = 'সাপ্লাইয়ার ওপেনিং ব্যালেন্স';
        //                 $transaction_data['amount']         = $val['opening_balance'] != null ? $val['opening_balance'] : 0;
        //                 $transaction_data['paid_through']   = null;
        //                 transactions($transaction_data);
        //             }
        //         }
        //     }
        // }

        $customers  = Customers::orderBy('created_at', 'DESC')->get();
        $users      = Users::orderBy('created_at', 'DESC')->get();
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::index', compact('customers', 'users', 'types'));
    }

    public function create()
    {
        return view('customers::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();
        
   

        DB::beginTransaction();

        try{
            $customers                              = new Customers;
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            if(isset($data['customer_type'])){
            $customers->customer_type               = $data['customer_type'];
            }
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {
                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])) && ($data['opening_balance'] != null) && ($data['opening_balance'] > 0))
                {   
                    openingBalanceStore($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();
                
                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Added Successfully !!");
                }
                else
                {
                    return back()->with("success","Contact Added Successfully !!");
                }
            }

        }catch (\Exception $exception){
            
            dd($exception);
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('customers::show');
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $find_customer  = Customers::find($id);
        $customers      = Customers::orderBy('created_at', 'DESC')->get();
        $users          = Users::orderBy('created_at', 'DESC')->get();
        $types          = CustomerTypes::orderBy('created_at', 'DESC')->get();
        $find_type      = CustomerTypes::get();

        return view('customers::edit', compact('customers', 'find_customer', 'users', 'types', 'find_type'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_name'     => 'required',
            'contact_type'      => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = Customers::find($id);
            $customers->name                        = $data['customer_name'];
            $customers->address                     = $data['address'];
            $customers->phone                       = $data['mobile_number'];
            $customers->nid_number                  = $data['nid_number'];
            $customers->alternative_contact         = $data['alternative_mobile_number'];
            $customers->contact_type                = $data['contact_type'];
            $customers->joining_date                = date('Y-m-d', strtotime($data['joining_date']));
            $customers->designation                 = $data['designation'];
            $customers->salary                      = $data['salary'];
            if(isset($data['customer_type'])){
            $customers->customer_type               = $data['customer_type'];
            }
            $customers->branch_id                   = $branch_id;
            $customers->user_id                     = $data['user_id'];

            if($request->hasFile('image'))
            {   
                if ($customers->image != null)
                {
                    unlink('public/'.$customers->image);
                }

                $companyLogo            = $request->file('image');
                $logoName               = time().".".$companyLogo->getClientOriginalExtension();
                $directory              = 'images/customers/';
                $companyLogo->move(public_path($directory), $logoName);
                $logoUrl                = $directory.$logoName;
                $customers->image       = $logoUrl;
            }

            if ((isset($data['opening_balance'])))
            {
                $customers->opening_balance         = $data['opening_balance'];
            }

            $customers->updated_by                  = $user_id;

            if ($customers->save())
            {   
                if ((isset($data['opening_balance'])))
                {   
                    openingBalanceUpdate($data['opening_balance'], $customers['id'], $data['contact_type']);
                }

                DB::commit();

                if ((isset($data['contact_type_reference'])))
                {
                    return redirect()->route('customers_index','contact_type='.$data['contact_type_reference'])->with("success","Contact Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('customers_index')->with("success","Contact Updated Successfully !!");
                }
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function customerListLoad()
    {
        $branch_id      = Auth()->user()->branch_id;
        $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                ->orderBy('customers.created_at', 'DESC')
                                ->when($branch_id != 1, function ($query) use ($branch_id) {
                                    return $query->where('customers.branch_id', $branch_id);
                                })
                                ->whereNotIn('customers.id', [1,2])
                                ->select('customers.*',
                                         'users.name as user_name')
                                // ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function customerListSearch($id)
    {
        $branch_id      = Auth()->user()->branch_id;
        if ($id != 'No_Text')
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('customers.phone', 'LIKE', "%$id%")
                                        ->orWhere('customers.alternative_contact', 'LIKE', "%$id%")
                                        ->orWhere('customers.address', 'LIKE', "%$id%")
                                        ->orWhere('customers.nid_number', 'LIKE', "%$id%")
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }
        else
        {
            $data           = Customers::leftjoin('users', 'users.id', 'customers.user_id')
                                        ->orderBy('customers.created_at', 'DESC')
                                        ->when($branch_id != 1, function ($query) use ($branch_id) {
                                            return $query->where('customers.branch_id', $branch_id);
                                        })
                                        ->select('customers.*',
                                                 'users.name as user_name')
                                        // ->take(100)
                                        ->get();
        }

        return Response::json($data);
    }

    //Customer Types
    public function indexType()
    {
        $types  = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.index', compact('types'));
    }

    public function storeType(Request $request)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = new CustomerTypes;
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->created_by                  = $user_id;

            if ($customers->save())
            {   
                DB::commit();
                return back()->with("success","Customer Type Added Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function editType($id)
    {
        $find_type  = CustomerTypes::find($id);
        $types      = CustomerTypes::orderBy('created_at', 'DESC')->get();

        return view('customers::types.edit', compact('types', 'find_type'));
    }

    public function updateType(Request $request, $id)
    {
        $rules = array(
            'name'     => 'required',
            'status'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth()->user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $customers                              = CustomerTypes::find($id);
            $customers->name                        = $data['name'];
            $customers->status                      = $data['status'];
            $customers->updated_by                  = Auth::user()->id;

            if ($customers->save())
            {   
                 DB::commit();
                return redirect()->route('customers_type_index')->with("success","Customer Type Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }
}
