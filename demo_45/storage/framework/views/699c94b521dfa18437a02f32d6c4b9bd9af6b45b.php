

<?php $__env->startSection('title', 'Expenses'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Expenses</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Expenses</a></li>
                                    <li class="breadcrumb-item active">Expenses</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="height: 350px;overflow-y: auto;overflow-x: auto" class="col-lg-4 col-md-4 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div style="padding-left: 0px" class="col-lg-10 col-md-10 col-sm-6 col-6">
                                            <h4 class="card-title">Categories</h4>
                                        </div>
                                        <div class="col-lg-2 col-md-2 col-sm-6 col-6">
                                            <?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>
                                            <button style="padding: 3px;border-radius: 4px" type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#myModal">Add</button>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                
                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>name</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if(!empty($expense_categories) && ($expense_categories->count() > 0)): ?>
                                        <?php $__currentLoopData = $expense_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $expense_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($expense_category['name']); ?></td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>
                                                            <a class="dropdown-item" href="<?php echo e(route('expenses_categories_edit', $expense_category['id'])); ?>">Edit</a>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-lg-8 col-md-8 col-sm-12 col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('expenses_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="expense_date">Expense Date *</label>
                                                <input id="expense_date" name="expense_date" type="text" value="<?php echo e(date('d-m-Y')); ?>" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Expense To </label>
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="">--Select Contact--</option>
                                                    
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div id="contact_id_reload" class="form-group">
                                                <label class="control-label">Expense Category *</label>
                                                <select style="width: 100" class="form-control select2" name="expense_category_id" required>
                                                    <option value="">--Select Category--</option>
                                                    <?php if(!empty($expense_categories)): ?>
                                                        <?php $__currentLoopData = $expense_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $expense_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php if(isset($find_expense_category)): ?> <?php echo e($find_expense_category['id'] == $expense_category['id'] ? 'selected' : ''); ?> <?php endif; ?> value="<?php echo e($expense_category->id); ?>"><?php echo e($expense_category->name); ?></option>
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="amount">Amount *</label>
                                                <input id="amount" name="amount" type="text" class="form-control" oninput="searchContact()" required>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="note">Paid Through *</label>
                                                <select id="paid_through_id" style="cursor: pointer" name="paid_through" class="form-control select2">
                                                <?php if(!empty($paid_accounts) && ($paid_accounts->count() > 0)): ?>
                                                <?php $__currentLoopData = $paid_accounts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $paid_account): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($paid_account['id']); ?>"><?php echo e($paid_account['name']); ?></option>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="note">Account Information</label>
                                                <input type="text" name="account_information" class="form-control" id="account_information" placeholder="Account Information"/>
                                            </div>
                                        </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="note">Note</label>
                                                <input id="note" name="note" type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <hr style="margin-top: 0px">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('expenses_index')); ?>">Close</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <hr>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">Expense List</h4>
                                
                                <div style="margin-right: 0px" class="row">
                                    <div class="col-lg-9 col-md-6 col-sm-4 col-4"></div>
                                    <div class="col-lg-1 col-md-2 col-sm-4 col-4">Search : </div>
                                    <div class="col-lg-2 col-md-4 col-sm-4 col-4">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Expense Date</th>
                                            <th>Expense Number</th>
                                            <th>Category</th>
                                            <th>Amount</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="expense_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Create New Expense Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="FormSubmit1" action="<?php echo e(route('expenses_categories_store')); ?>" method="post" files="true" enctype="multipart/form-data">
                    <?php echo e(csrf_field()); ?>

                    <div style="padding-top: 10px;padding-bottom: 0px" class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="expense_category_name">Category Name *</label>
                                    <input id="expense_category_name" name="expense_category_name" type="text" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url        = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $.get(site_url + '/expenses/expense/list/load', function(data){

                var expense_list = '';
                $.each(data, function(i, expense_data)
                {   
                    var serial      = parseFloat(i) + 1;

                    if (expense_data.expense_category_id != 2)
                    {
                        var edit_url    = site_url + '/expenses/edit/' + expense_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/expenses/';
                    }

                    expense_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(expense_data.expense_date) +
                                        '</td>' +
                                        '<td>' +
                                           'EXP - ' + expense_data.expense_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            expense_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#expense_list").empty();
                $("#expense_list").append(expense_list);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/expenses/expense/search/list/' + search_text, function(data){

                var expense_list = '';
                $.each(data, function(i, expense_data)
                {   
                    var serial      = parseFloat(i) + 1;
                    
                    if (expense_data.expense_category_id != 2)
                    {
                        var edit_url    = site_url + '/expenses/edit/' + expense_data.id;
                    }
                    else
                    {
                        var edit_url    = site_url + '/expenses/';
                    }

                    expense_list += '<tr>' +
                                        '<td>' +
                                            serial +
                                        '</td>' +
                                        '<td>' +
                                           formatDate(expense_data.expense_date) +
                                        '</td>' +
                                        '<td>' +
                                           'EXP - ' + expense_data.expense_number.padStart(6, '0') +
                                        '</td>' +
                                        '<td>' +
                                            expense_data.category_name +
                                        '</td>' +
                                        '<td>' +
                                           expense_data.amount +
                                        '</td>' +
                                        '<td>' +
                                            '<div class="dropdown">' +
                                                '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                    '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                '</a>' +
                                                '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                    '<?php if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3): ?>' +
                                                    '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                    '<?php endif; ?>' +
                                                '</div>' +
                                            '</div>' +
                                        '</td>' +
                                    '</tr>';
                });

                $("#expense_list").empty();
                $("#expense_list").append(expense_list);
            });
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp\htdocs\cyberdyne-technology\pos\45\Modules/Expenses\Resources/views/index.blade.php ENDPATH**/ ?>