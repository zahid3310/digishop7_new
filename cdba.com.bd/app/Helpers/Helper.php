<?php
//Model
use App\Models\Invoices;
use App\Models\Users;
use App\Models\URLS;
use App\Models\Permissions;
use App\Models\Modules;
use App\Models\ModulesAccess;
use App\Models\JournalEntries;
use App\Models\AccountHead;
use App\Models\VoucherSummary;
use App\Models\AccountTransaction;
use App\Models\ChequeTransactions;

function userAccess($user_id)
{
    $current_route      = Route::currentRouteName();

    $access             = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                ->where('permissions.user_id', $user_id)
                                ->where('urls.url', $current_route)
                                ->first();

    return $access['access_level'];
}

function moduleAccess($user_id)
{
    $access     = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                ->where('modules_access.user_id', $user_id)
                                ->get();

    return $access;
}

function userDetails()
{
    $user_id    = Auth::user()->id;
    $user       = Users::find(1);

    return $user;
}

function accessLevel($route)
{
    //All Routes start

    //products Module Start
        $products_create        = 'products_create';
        $products_store         = 'products_store';
        $products_edit          = 'products_edit';
        $products_update        = 'products_update';
    //products Module End

    //Invoice Module Start
        $invoices_edit       = 'invoices_edit';
        $invoices_update     = 'invoices_update';
    //Invoice Module End

    //Bill Module Start
       $bills_index    = 'bills_index';
       $bills_create   = 'bills_create';
       $bills_store    = 'bills_store';
       $bills_edit     = 'bills_edit';
       $bills_update   = 'bills_update';
       $bills_show     = 'bills_show';
    //Bill Module End

    //Payments Module Start
        $payments_edit              = 'payments_edit';
        $payments_update            = 'payments_update';
    //Payments Module End

    //expenses Module Start
        $expenses_edit             = 'expenses_edit';
        $expenses_update           = 'expenses_update';
    //expenses Module End

    //Users Module Start
        $users_index             = 'users_index';
        $users_create            = 'users_create';
        $users_store             = 'users_store';
        $users_edit              = 'users_edit';
        $users_update            = 'users_update';
    //Users Module End

    //Customers Module Start
      $customers_edit            = 'customers_edit';
      $customers_update          = 'customers_update';
    //Customers Module End

    //All Routes End

    if (Auth::user()->role == 0)
    {
        if ( ($route == $products_create) || 
            ($route == $products_store) || 
            ($route == $products_edit) || 
            ($route == $products_update) ||
            ($route == $invoices_edit) || 
            ($route == $invoices_update) || 
            ($route == $bills_index) || 
            ($route == $bills_create) || 
            ($route == $bills_store) || 
            ($route == $bills_edit) ||
            ($route == $bills_update) || 
            ($route == $bills_show) ||  
            ($route == $expenses_edit) ||  
            ($route == $expenses_update) ||  
            ($route == $payments_edit) || 
            ($route == $users_index) || 
            ($route == $users_create) || 
            ($route == $users_store) || 
            ($route == $users_edit) || 
            ($route == $users_update) )
        {
            return "No";
        }
    else{
      return "accepted";
    }
    }   
    else
    {
            return "accepted";
    }
}

function numberTowords(float $amount)
{
    $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
  $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
  while( $x < $count_length ) {
       $get_divider = ($x == 2) ? 10 : 100;
       $amount = floor($num % $get_divider);
       $num = floor($num / $get_divider);
       $x += $get_divider == 10 ? 1 : 2;
       if ($amount) {
         $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
         $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
         $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
         }else $string[] = null;
       }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paisa' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Taka ' : '') . $get_paise;
}

//Voucher store
function voucherSummaryCreate($VoucherNumber, $VoucherDate, $ProjectID, $RegisterID, $Type, $BankAccountNumber, $Status, $TotalAmount, $Narration, $MoneyReceiptNo, $CompanyID, $created_by)
{
    $voucher_summary                        = new VoucherSummary;
    $voucher_summary->VoucherNumber         = $VoucherNumber;
    $voucher_summary->VoucherDate           = date('Y-m-d', strtotime($VoucherDate));
    $voucher_summary->ProjectID             = $ProjectID;
    $voucher_summary->RegisterID            = $RegisterID;
    $voucher_summary->Type                  = $Type;
    $voucher_summary->BankAccountNumber     = $BankAccountNumber;
    $voucher_summary->Status                = $Status;
    $voucher_summary->TotalAmount           = $TotalAmount;
    $voucher_summary->Narration             = $Narration;
    $voucher_summary->MoneyReceiptNo        = $MoneyReceiptNo;
    $voucher_summary->CompanyID             = $CompanyID;
    $voucher_summary->created_by            = $created_by;

    if ($voucher_summary->save())
    {
        return $voucher_summary->id;
    }
    else
    {
        return 0;
    }
}

//Voucher update
function voucherSummaryUpdate($id, $VoucherNumber, $VoucherDate, $ProjectID, $RegisterID, $Type, $BankAccountNumber, $Status, $TotalAmount, $Narration, $MoneyReceiptNo, $CompanyID, $created_by)
{
    $voucher_summary                        = VoucherSummary::find($id);
    $voucher_summary->VoucherNumber         = $VoucherNumber;
    $voucher_summary->VoucherDate           = date('Y-m-d', strtotime($VoucherDate));
    $voucher_summary->ProjectID             = $ProjectID;
    $voucher_summary->RegisterID            = $RegisterID;
    $voucher_summary->Type                  = $Type;
    $voucher_summary->BankAccountNumber     = $BankAccountNumber;
    $voucher_summary->Status                = $Status;
    $voucher_summary->TotalAmount           = $TotalAmount;
    $voucher_summary->Narration             = $Narration;
    $voucher_summary->MoneyReceiptNo        = $MoneyReceiptNo;
    $voucher_summary->CompanyID             = $CompanyID;
    $voucher_summary->updated_by            = $created_by;

    if ($voucher_summary->save())
    {
        return $voucher_summary->id;
    }
    else
    {
        return 0;
    }
}

//Debit store
function accountTransactionsDebitStore($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by)
{
    $account_transactions                       = new AccountTransaction;
    $account_transactions->VoucherId            = $VoucherId;
    $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $account_transactions->VoucherNumber        = $VoucherNumber;
    $account_transactions->ProjectID            = $ProjectID;
    $account_transactions->RegisterID           = $RegisterID;
    $account_transactions->HeadID               = $HeadID;
    $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
    $account_transactions->Particulars          = $Particulars;
    $account_transactions->DebitAmount          = $DebitAmount;
    $account_transactions->CreditAmount         = $CreditAmount;
    $account_transactions->AccountNumber        = $AccountNumber;
    $account_transactions->VoucherType          = $VoucherType;
    $account_transactions->CBAccount            = $CBAccount;
    $account_transactions->IsPosted             = $IsPosted;
    $account_transactions->CompanyID            = $CompanyID;
    $account_transactions->Status               = $Status;
    $account_transactions->created_by           = $created_by;
    $account_transactions->save();

    return 1;
}

//Credit store
function accountTransactionsCreditStore($VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $created_by)
{
    $account_transactions                       = new AccountTransaction;
    $account_transactions->VoucherId            = $VoucherId;
    $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $account_transactions->VoucherNumber        = $VoucherNumber;
    $account_transactions->ProjectID            = $ProjectID;
    $account_transactions->RegisterID           = $RegisterID;
    $account_transactions->HeadID               = $HeadID;
    $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
    $account_transactions->Particulars          = $Particulars;
    $account_transactions->DebitAmount          = $DebitAmount;
    $account_transactions->CreditAmount         = $CreditAmount;
    $account_transactions->AccountNumber        = $AccountNumber;
    $account_transactions->VoucherType          = $VoucherType;
    $account_transactions->CBAccount            = $CBAccount;
    $account_transactions->IsPosted             = $IsPosted;
    $account_transactions->CompanyID            = $CompanyID;
    $account_transactions->Status               = $Status;
    $account_transactions->created_by           = $created_by;
    $account_transactions->save();

    return 1;
}

//Debit update
function accountTransactionsDebitUpdate($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by)
{
    $account_transactions                       = AccountTransaction::find($id);
    $account_transactions->VoucherId            = $VoucherId;
    $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $account_transactions->VoucherNumber        = $VoucherNumber;
    $account_transactions->ProjectID            = $ProjectID;
    $account_transactions->RegisterID           = $RegisterID;
    $account_transactions->HeadID               = $HeadID;
    $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
    $account_transactions->Particulars          = $Particulars;
    $account_transactions->DebitAmount          = $DebitAmount;
    $account_transactions->CreditAmount         = $CreditAmount;
    $account_transactions->AccountNumber        = $AccountNumber;
    $account_transactions->VoucherType          = $VoucherType;
    $account_transactions->CBAccount            = $CBAccount;
    $account_transactions->CompanyID            = $CompanyID;
    $account_transactions->updated_by           = $updated_by;
    $account_transactions->save();

    return 1;
}

//Credit update
function accountTransactionsCreditUpdate($id, $VoucherId, $VoucherDate, $VoucherNumber, $ProjectID, $RegisterID, $HeadID, $PurposeAccHeadName, $Particulars, $DebitAmount, $CreditAmount, $AccountNumber, $VoucherType, $CBAccount, $IsPosted, $CompanyID, $Status, $updated_by)
{
    $account_transactions                       = AccountTransaction::find($id);
    $account_transactions->VoucherId            = $VoucherId;
    $account_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $account_transactions->VoucherNumber        = $VoucherNumber;
    $account_transactions->ProjectID            = $ProjectID;
    $account_transactions->RegisterID           = $RegisterID;
    $account_transactions->HeadID               = $HeadID;
    $account_transactions->PurposeAccHeadName   = $PurposeAccHeadName;
    $account_transactions->Particulars          = $Particulars;
    $account_transactions->DebitAmount          = $DebitAmount;
    $account_transactions->CreditAmount         = $CreditAmount;
    $account_transactions->AccountNumber        = $AccountNumber;
    $account_transactions->VoucherType          = $VoucherType;
    $account_transactions->CBAccount            = $CBAccount;
    $account_transactions->CompanyID            = $CompanyID;
    $account_transactions->updated_by           = $updated_by;
    $account_transactions->save();

    return 1;
}

//cheque Store
function chequeStore($VoucherDate, $VoucherNumber, $CompanyID, $ProjectID, $RegisterID, $HeadID, $ChequeState, $Type, $StaffID, $ChequeNumber, $ChequeType, $ChequeDate, $EnCashDate, $Amount, $created_by)
{
    $cheque_transactions                       = new ChequeTransactions;
    $cheque_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $cheque_transactions->VoucherNumber        = $VoucherNumber;
    $cheque_transactions->CompanyID            = $CompanyID;
    $cheque_transactions->ProjectID            = $ProjectID;
    $cheque_transactions->RegisterID           = $RegisterID;
    $cheque_transactions->HeadID               = $HeadID;
    $cheque_transactions->ChequeState          = $ChequeState;
    $cheque_transactions->Type                 = $Type;
    $cheque_transactions->StaffID              = $StaffID;
    $cheque_transactions->ChequeNumber         = $ChequeNumber;
    $cheque_transactions->ChequeType           = $ChequeType;
    $cheque_transactions->ChequeDate           = date('Y-m-d', strtotime($ChequeDate));
    $cheque_transactions->EnCashDate           = date('Y-m-d', strtotime($EnCashDate));
    $cheque_transactions->Amount               = $Amount;
    $cheque_transactions->created_by           = $created_by;
    $cheque_transactions->save();

    return 1;
}

//cheque Update
function chequeUpdate($id, $VoucherDate, $VoucherNumber, $CompanyID, $ProjectID, $RegisterID, $HeadID, $ChequeState, $Type, $StaffID, $ChequeNumber, $ChequeType, $ChequeDate, $EnCashDate, $Amount, $updated_by)
{
    $cheque_transactions                       = ChequeTransactions::find($id);
    $cheque_transactions->VoucherDate          = date('Y-m-d', strtotime($VoucherDate));
    $cheque_transactions->VoucherNumber        = $VoucherNumber;
    $cheque_transactions->CompanyID            = $CompanyID;
    $cheque_transactions->ProjectID            = $ProjectID;
    $cheque_transactions->RegisterID           = $RegisterID;
    $cheque_transactions->HeadID               = $HeadID;
    $cheque_transactions->ChequeState          = $ChequeState;
    $cheque_transactions->Type                 = $Type;
    $cheque_transactions->StaffID              = $StaffID;
    $cheque_transactions->ChequeNumber         = $ChequeNumber;
    $cheque_transactions->ChequeType           = $ChequeType;
    $cheque_transactions->ChequeDate           = date('Y-m-d', strtotime($ChequeDate));
    $cheque_transactions->EnCashDate           = date('Y-m-d', strtotime($EnCashDate));
    $cheque_transactions->Amount               = $Amount;
    $cheque_transactions->updated_by           = $updated_by;
    $cheque_transactions->save();

    return 1;
}

function cachInHand()
{
    $branch_id      = Auth()->user()->branch_id;
    $data_list      = AccountTransaction::where('IsPosted', 1)
                                        ->where('CompanyID', $branch_id)
                                        ->where('PurposeAccHeadName', 'cash_ac')
                                        ->whereIn('VoucherType', ['CP', 'CR', 'JR', 'CN'])
                                        ->get();

    $cash_in_hand_debit     = $data_list->sum('DebitAmount');
    $cash_in_hand_credit    = $data_list->sum('CreditAmount');
    $cash_in_hand           = $cash_in_hand_debit - $cash_in_hand_credit;

    return $cash_in_hand;
}

function cachInBank($bank_id)
{
    $branch_id      = Auth()->user()->branch_id;
    $data_list      = AccountTransaction::where('IsPosted', 1)
                                ->where('CompanyID', $branch_id)
                                ->where('PurposeAccHeadName', 'bank_ac')
                                ->where('HeadID', $bank_id)
                                ->whereIn('VoucherType', ['BP', 'BR', 'JR', 'CN'])
                                ->get();

    $bank_debit     = $data_list->sum('DebitAmount');
    $bank_credit    = $data_list->sum('CreditAmount');
    $balance        = $bank_debit - $bank_credit;

    return $balance;
}
