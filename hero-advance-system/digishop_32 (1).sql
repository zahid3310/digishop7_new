-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 11, 2021 at 02:54 AM
-- Server version: 5.7.33
-- PHP Version: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `digishop_32`
--

-- --------------------------------------------------------

--
-- Table structure for table `backup`
--

CREATE TABLE `backup` (
  `id` bigint(20) NOT NULL,
  `file_url` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `bill_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `bill_date` date NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `bill_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `return_amount` double DEFAULT NULL,
  `total_discount` double NOT NULL,
  `adjustment_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_adjustment` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `adjustment_note` text COLLATE utf8mb4_unicode_ci,
  `bill_note` text COLLATE utf8mb4_unicode_ci,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `total_discount_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cash_given` double DEFAULT NULL,
  `change_amount` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `bill_entries`
--

CREATE TABLE `bill_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `vendor_id` bigint(20) UNSIGNED NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci,
  `email` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `nid_number` text COLLATE utf8mb4_unicode_ci,
  `image` text COLLATE utf8mb4_unicode_ci,
  `alternative_contact` text COLLATE utf8mb4_unicode_ci,
  `contact_type` tinyint(4) NOT NULL COMMENT '0= customer 1= Supplier 2= Employee 3= Reference',
  `joining_date` date DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `opening_balance` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `phone`, `email`, `address`, `nid_number`, `image`, `alternative_contact`, `contact_type`, `joining_date`, `designation`, `salary`, `user_id`, `branch_id`, `opening_balance`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Walk-In Customer', NULL, NULL, NULL, NULL, NULL, NULL, 0, '2021-01-03', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2021-01-03 07:38:36', '2021-01-03 07:38:36'),
(2, 'Walk-In Supplier', NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-01-03', NULL, NULL, NULL, NULL, NULL, 1, 5, '2021-01-03 07:38:49', '2021-04-11 08:34:20'),
(150, 'Md. Moktar', '01799114498', NULL, 'Chapai', NULL, NULL, NULL, 0, '2021-04-11', NULL, NULL, NULL, NULL, NULL, 5, NULL, '2021-04-11 22:32:16', '2021-04-11 22:32:16'),
(151, 'Md. Shoriful', '01799114498', NULL, 'Chapai', NULL, NULL, NULL, 0, '2021-04-11', NULL, NULL, NULL, NULL, NULL, 5, NULL, '2021-04-11 22:32:36', '2021-04-11 22:32:36'),
(152, 'Md. Shahin', '01799114498', NULL, 'Chapai', NULL, NULL, NULL, 0, '2021-04-11', NULL, NULL, NULL, NULL, NULL, 5, NULL, '2021-04-11 22:33:07', '2021-04-11 22:33:07');

-- --------------------------------------------------------

--
-- Table structure for table `discounts`
--

CREATE TABLE `discounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0=coupons 1=membership',
  `coupon_code` bigint(20) DEFAULT NULL,
  `card_number` double DEFAULT NULL,
  `phone_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `issue_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL COMMENT '0=inactive 1=active',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `discount_products`
--

CREATE TABLE `discount_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `discount_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expenses`
--

CREATE TABLE `expenses` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `expense_category_id` bigint(20) UNSIGNED NOT NULL,
  `expense_date` date NOT NULL,
  `expense_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `salary_type` tinyint(4) DEFAULT NULL COMMENT '0=Salary 1=Service Charge',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `paid_through_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_information` text COLLATE utf8mb4_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `expense_categories`
--

CREATE TABLE `expense_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `expense_categories`
--

INSERT INTO `expense_categories` (`id`, `name`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Employee Salary', NULL, 1, NULL, '2021-01-03 14:43:18', '2021-01-03 14:43:18'),
(2, 'Product Purchase', NULL, 1, NULL, '2021-01-03 14:43:30', '2021-01-03 14:43:30');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `incomes`
--

CREATE TABLE `incomes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `income_category_id` bigint(20) UNSIGNED NOT NULL,
  `income_date` date NOT NULL,
  `income_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `note` text COLLATE utf8_unicode_ci,
  `paid_through_id` bigint(20) UNSIGNED DEFAULT NULL,
  `account_information` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `income_categories`
--

CREATE TABLE `income_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '1',
  `invoice_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `invoice_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `reference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `customer_name` text COLLATE utf8mb4_unicode_ci,
  `customer_phone` text COLLATE utf8mb4_unicode_ci,
  `invoice_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `return_amount` double DEFAULT NULL,
  `total_buy_price` double NOT NULL,
  `total_discount` double NOT NULL,
  `total_adjustment` double DEFAULT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `adjustment_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `adjustment_note` text COLLATE utf8mb4_unicode_ci,
  `invoice_note` text COLLATE utf8mb4_unicode_ci,
  `discount_code` double DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `total_discount_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cash_given` double DEFAULT NULL,
  `change_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invoice_entries`
--

CREATE TABLE `invoice_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `reference_id` bigint(20) UNSIGNED DEFAULT NULL,
  `buy_price` double NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `discount_type` tinyint(4) NOT NULL COMMENT '0= % 1= BDT',
  `discount_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sub_category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_deliveries`
--

CREATE TABLE `message_deliveries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `message_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `phone_book_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `message_lists`
--

CREATE TABLE `message_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Product Categories', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Products', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Orders', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Purchases', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Sales Return', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Purchase Return', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Expenses', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 'Payments', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 'Contacts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(10, 'Paid Through Accounts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 'Units', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 'Employee Salary', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 'Bar Code Print', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(14, 'Discounts', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 'Reports', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 'Users', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 'Access Level', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `modules_access`
--

CREATE TABLE `modules_access` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `display` tinyint(4) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `modules_access`
--

INSERT INTO `modules_access` (`id`, `module_id`, `user_id`, `display`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(2, 2, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(3, 3, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(4, 4, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(5, 5, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(6, 6, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(7, 7, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(8, 8, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(9, 9, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(10, 10, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(11, 11, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(12, 12, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(13, 13, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(14, 14, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(15, 15, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(16, 16, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(17, 17, 1, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(18, 1, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(19, 2, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(20, 3, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(21, 4, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(22, 5, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(23, 6, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(24, 7, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(25, 8, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(26, 9, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(27, 10, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(28, 11, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(29, 12, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(30, 13, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(31, 14, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(32, 15, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(33, 16, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(34, 17, 2, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(52, 1, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(53, 2, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(54, 3, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(55, 4, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(56, 5, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(57, 6, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(58, 7, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(59, 8, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(60, 9, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(61, 10, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(62, 11, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(63, 12, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(64, 13, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(65, 14, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(66, 15, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(67, 16, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(68, 17, 5, 1, 1, NULL, '2021-01-31 10:39:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `paid_through_accounts`
--

CREATE TABLE `paid_through_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `paid_through_accounts`
--

INSERT INTO `paid_through_accounts` (`id`, `name`, `description`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'CASH', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_date` date NOT NULL,
  `amount` double NOT NULL,
  `paid_through` bigint(20) UNSIGNED NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `account_information` text COLLATE utf8mb4_unicode_ci,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '0= invoice 1= bill 2= sales return 3= purchase return',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_entries`
--

CREATE TABLE `payment_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `payment_id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `bill_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sales_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `url_id` bigint(20) UNSIGNED NOT NULL,
  `access_level` tinyint(4) NOT NULL COMMENT '0= Not Allowed 1= Allowed',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `user_id`, `url_id`, `access_level`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 1, 85, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(2, 1, 86, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(3, 1, 87, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(4, 1, 88, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(5, 1, 89, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(6, 1, 90, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(7, 1, 91, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(8, 1, 92, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(9, 1, 93, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(10, 1, 94, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(11, 1, 95, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(12, 1, 96, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(13, 1, 97, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(14, 1, 98, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(15, 1, 99, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(16, 1, 100, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(17, 1, 101, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(18, 1, 102, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(19, 1, 103, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(20, 1, 104, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(21, 1, 105, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(22, 1, 106, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(23, 1, 107, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(24, 1, 108, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(25, 1, 109, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(26, 1, 110, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(27, 1, 111, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(28, 1, 112, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(29, 1, 113, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(30, 1, 114, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(31, 1, 115, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(32, 1, 116, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(33, 1, 117, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(34, 1, 118, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(35, 1, 119, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(36, 1, 120, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(37, 1, 121, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(38, 1, 122, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(39, 1, 123, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(40, 1, 124, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(41, 1, 125, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(42, 1, 126, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(43, 1, 127, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(44, 1, 128, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(45, 1, 129, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(46, 1, 130, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(47, 1, 131, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(48, 1, 132, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(49, 1, 133, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(50, 1, 134, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(51, 1, 135, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(52, 1, 136, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(53, 1, 137, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(54, 1, 138, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(55, 1, 139, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(56, 1, 140, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(57, 1, 141, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(58, 1, 142, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(59, 1, 143, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(60, 1, 144, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(61, 1, 145, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(62, 1, 146, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(63, 1, 147, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(64, 1, 148, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(65, 1, 149, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(66, 1, 150, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(67, 1, 151, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(68, 1, 152, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(69, 1, 153, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(70, 1, 154, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(71, 1, 155, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(72, 1, 156, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(73, 1, 157, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(74, 1, 158, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(75, 1, 159, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(76, 1, 160, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(77, 1, 161, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(78, 1, 162, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(79, 1, 163, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(80, 1, 164, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(81, 1, 165, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(82, 1, 166, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(83, 1, 167, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(84, 1, 168, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(85, 1, 169, 1, 1, NULL, '2021-01-03 07:38:07', NULL),
(86, 2, 85, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(87, 2, 86, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(88, 2, 87, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(89, 2, 88, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(90, 2, 89, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(91, 2, 90, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(92, 2, 91, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(93, 2, 92, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(94, 2, 93, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(95, 2, 94, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(96, 2, 95, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(97, 2, 96, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(98, 2, 97, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(99, 2, 98, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(100, 2, 99, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(101, 2, 100, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(102, 2, 101, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(103, 2, 102, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(104, 2, 103, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(105, 2, 104, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(106, 2, 105, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(107, 2, 106, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(108, 2, 107, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(109, 2, 108, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(110, 2, 109, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(111, 2, 110, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(112, 2, 111, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(113, 2, 112, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(114, 2, 113, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(115, 2, 114, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(116, 2, 115, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(117, 2, 116, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(118, 2, 117, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(119, 2, 118, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(120, 2, 119, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(121, 2, 120, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(122, 2, 121, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(123, 2, 122, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(124, 2, 123, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(125, 2, 124, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(126, 2, 125, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(127, 2, 126, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(128, 2, 127, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(129, 2, 128, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(130, 2, 129, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(131, 2, 130, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(132, 2, 131, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(133, 2, 132, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(134, 2, 133, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(135, 2, 134, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(136, 2, 135, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(137, 2, 136, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(138, 2, 137, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(139, 2, 138, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(140, 2, 139, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(141, 2, 140, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(142, 2, 141, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(143, 2, 142, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(144, 2, 143, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(145, 2, 144, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(146, 2, 145, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(147, 2, 146, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(148, 2, 147, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(149, 2, 148, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(150, 2, 149, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(151, 2, 150, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(152, 2, 151, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(153, 2, 152, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(154, 2, 153, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(155, 2, 154, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(156, 2, 155, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(157, 2, 156, 0, 1, NULL, '2021-01-20 10:59:35', '2021-01-20 11:03:20'),
(158, 2, 157, 0, 1, NULL, '2021-01-20 10:59:35', '2021-01-20 11:03:20'),
(159, 2, 158, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(160, 2, 159, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(161, 2, 160, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(162, 2, 161, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(163, 2, 162, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(164, 2, 163, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(165, 2, 164, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(166, 2, 165, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(167, 2, 166, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(168, 2, 167, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(169, 2, 168, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(170, 2, 169, 1, 1, NULL, '2021-01-20 10:59:35', NULL),
(256, 5, 85, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(257, 5, 86, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(258, 5, 87, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(259, 5, 88, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(260, 5, 89, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(261, 5, 90, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(262, 5, 91, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(263, 5, 92, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(264, 5, 93, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(265, 5, 94, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(266, 5, 95, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(267, 5, 96, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(268, 5, 97, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(269, 5, 98, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(270, 5, 99, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(271, 5, 100, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(272, 5, 101, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(273, 5, 102, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(274, 5, 103, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(275, 5, 104, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(276, 5, 105, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(277, 5, 106, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(278, 5, 107, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(279, 5, 108, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(280, 5, 109, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(281, 5, 110, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(282, 5, 111, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(283, 5, 112, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(284, 5, 113, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(285, 5, 114, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(286, 5, 115, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(287, 5, 116, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(288, 5, 117, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(289, 5, 118, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(290, 5, 119, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(291, 5, 120, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(292, 5, 121, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(293, 5, 122, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(294, 5, 123, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(295, 5, 124, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(296, 5, 125, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(297, 5, 126, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(298, 5, 127, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(299, 5, 128, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(300, 5, 129, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(301, 5, 130, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(302, 5, 131, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(303, 5, 132, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(304, 5, 133, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(305, 5, 134, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(306, 5, 135, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(307, 5, 136, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(308, 5, 137, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(309, 5, 138, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(310, 5, 139, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(311, 5, 140, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(312, 5, 141, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(313, 5, 142, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(314, 5, 143, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(315, 5, 144, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(316, 5, 145, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(317, 5, 146, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(318, 5, 147, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(319, 5, 148, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(320, 5, 149, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(321, 5, 150, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(322, 5, 151, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(323, 5, 152, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(324, 5, 153, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(325, 5, 154, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(326, 5, 155, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(327, 5, 156, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(328, 5, 157, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(329, 5, 158, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(330, 5, 159, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(331, 5, 160, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(332, 5, 161, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(333, 5, 162, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(334, 5, 163, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(335, 5, 164, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(336, 5, 165, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(337, 5, 166, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(338, 5, 167, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(339, 5, 168, 1, 1, NULL, '2021-01-31 10:39:48', NULL),
(340, 5, 169, 1, 1, NULL, '2021-01-31 10:39:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `phone_book`
--

CREATE TABLE `phone_book` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sub_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `item_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` text COLLATE utf8mb4_unicode_ci,
  `stock_in_hand` double DEFAULT NULL,
  `total_sold` double DEFAULT NULL,
  `unit` bigint(20) UNSIGNED DEFAULT NULL,
  `buy_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `total_purchase_return` double DEFAULT NULL,
  `total_sales_return` double DEFAULT NULL,
  `total_damage` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `sub_category_id`, `item_id`, `name`, `product_code`, `stock_in_hand`, `total_sold`, `unit`, `buy_price`, `sell_price`, `total_purchase_return`, `total_sales_return`, `total_damage`, `image`, `status`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, 'Semai', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:14:23', '2021-04-11 22:14:23'),
(2, NULL, NULL, NULL, 'Chanachur', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:14:35', '2021-04-11 22:14:35'),
(3, NULL, NULL, NULL, 'Cake', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:14:42', '2021-04-11 22:14:42'),
(4, NULL, NULL, NULL, 'Bread', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:14:49', '2021-04-11 22:14:49'),
(5, NULL, NULL, NULL, 'Biscuit', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:14:57', '2021-04-11 22:14:57'),
(6, NULL, NULL, NULL, 'Muri', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, 5, NULL, '2021-04-11 22:15:06', '2021-04-11 22:15:06');

-- --------------------------------------------------------

--
-- Table structure for table `product_customers`
--

CREATE TABLE `product_customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `whole_sale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_entries`
--

CREATE TABLE `product_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `sub_category_id` bigint(20) UNSIGNED DEFAULT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `supplier_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_code` text COLLATE utf8mb4_unicode_ci,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `stock_in_hand` double DEFAULT NULL,
  `opening_stock` double DEFAULT NULL,
  `total_sold` double DEFAULT '0',
  `buy_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `wholesale_price` double DEFAULT NULL,
  `total_purchase_return` double DEFAULT NULL,
  `total_sales_return` double DEFAULT NULL,
  `total_damage` double DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `alert_quantity` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '1=Inclusive 2=Exclusive',
  `selling_price_exclusive_tax` double DEFAULT NULL,
  `vat_percentage` double DEFAULT NULL,
  `service_charge` double DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `product_type` tinyint(4) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_entries`
--

INSERT INTO `product_entries` (`id`, `product_id`, `sub_category_id`, `brand_id`, `supplier_id`, `product_code`, `name`, `stock_in_hand`, `opening_stock`, `total_sold`, `buy_price`, `sell_price`, `wholesale_price`, `total_purchase_return`, `total_sales_return`, `total_damage`, `image`, `unit_id`, `alert_quantity`, `tax_type`, `selling_price_exclusive_tax`, `vat_percentage`, `service_charge`, `status`, `branch_id`, `product_type`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 6, NULL, NULL, NULL, '000001', 'Muri-200g', 400, 400, 0, 0, 450, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:22:42', '2021-04-11 22:22:42'),
(2, 6, NULL, NULL, NULL, '000002', 'Muri-400g', 410, 410, 0, 0, 600, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:23:29', '2021-04-11 22:23:29'),
(3, 6, NULL, NULL, NULL, '000003', 'Muri-500g', 440, 440, 0, 0, 690, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, 5, '2021-04-11 22:24:03', '2021-04-11 22:27:39'),
(4, 5, NULL, NULL, NULL, '000004', 'Bag Biscuit', 445, 445, 0, 0, 135, NULL, NULL, NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:24:59', '2021-04-11 22:24:59'),
(5, 5, NULL, NULL, NULL, '000005', '400 g Biscuit', 445, 445, 0, 0, 44, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:25:36', '2021-04-11 22:25:36'),
(6, 5, NULL, NULL, NULL, '000006', '500 g Biscuit', 935, 935, 0, 0, 55, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:26:09', '2021-04-11 22:26:09'),
(7, 5, NULL, NULL, NULL, '000007', '200 g Biscuit', 925, 925, 0, 0, 22, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:26:45', '2021-04-11 22:26:45'),
(8, 5, NULL, NULL, NULL, '000008', 'SP Full Bread', 410, 410, 0, 0, 30, NULL, NULL, NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:27:15', '2021-04-11 22:27:15'),
(9, 4, NULL, NULL, NULL, '000009', 'Full Bread', 440, 440, 0, 0, 20, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, NULL, 1, 5, NULL, '2021-04-11 22:28:10', '2021-04-11 22:28:10');

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers`
--

CREATE TABLE `product_suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `whole_sale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variations`
--

CREATE TABLE `product_variations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variation_entries`
--

CREATE TABLE `product_variation_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `variation_value_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variation_values`
--

CREATE TABLE `product_variation_values` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_variation_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return`
--

CREATE TABLE `purchase_return` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `purchase_return_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `purchase_return_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `sub_total_amount` double NOT NULL,
  `return_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `return_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_return_entries`
--

CREATE TABLE `purchase_return_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `purchase_return_id` bigint(20) UNSIGNED NOT NULL,
  `bill_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_return`
--

CREATE TABLE `sales_return` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `sales_return_number` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_return_date` date NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `sub_total_amount` double NOT NULL,
  `return_amount` double NOT NULL,
  `due_amount` double NOT NULL,
  `total_tax` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `vat_type` tinyint(4) DEFAULT NULL COMMENT '0= % 1= BDT',
  `total_vat` double DEFAULT NULL,
  `return_note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `total_discount_type` tinyint(4) DEFAULT NULL,
  `total_discount_amount` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales_return_entries`
--

CREATE TABLE `sales_return_entries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sales_return_id` bigint(20) UNSIGNED NOT NULL,
  `invoice_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_unit_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `buy_price` double NOT NULL,
  `rate` double NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `total_amount` double NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=active 0=inactive',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transaction_date` date NOT NULL,
  `invoice_id` bigint(20) UNSIGNED DEFAULT NULL,
  `bill_id` bigint(20) UNSIGNED DEFAULT NULL,
  `sales_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_return_id` bigint(20) UNSIGNED DEFAULT NULL,
  `expense_id` bigint(20) UNSIGNED DEFAULT NULL,
  `income_id` bigint(20) UNSIGNED DEFAULT NULL,
  `payment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `amount` double NOT NULL,
  `paid_through` bigint(20) UNSIGNED NOT NULL,
  `type` tinyint(4) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `branch_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Pcs', NULL, 1, 1, '2021-01-03 07:40:36', '2021-02-18 08:08:51'),
(2, 'Gram', NULL, 1, NULL, '2021-02-18 08:08:41', '2021-02-18 08:08:41'),
(3, 'Bag', NULL, 1, NULL, '2021-02-18 08:08:57', '2021-02-18 08:08:57'),
(4, 'Pack', NULL, 1, NULL, '2021-02-18 08:09:13', '2021-02-18 08:09:13');

-- --------------------------------------------------------

--
-- Table structure for table `unit_conversions`
--

CREATE TABLE `unit_conversions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_entry_id` bigint(20) UNSIGNED NOT NULL,
  `main_unit_id` bigint(20) UNSIGNED NOT NULL,
  `converted_unit_id` bigint(20) UNSIGNED NOT NULL,
  `conversion_rate` double NOT NULL,
  `purchase_price` double DEFAULT NULL,
  `sell_price` double DEFAULT NULL,
  `wholesale_price` double DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated-at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `urls`
--

CREATE TABLE `urls` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `module_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `urls`
--

INSERT INTO `urls` (`id`, `name`, `url`, `module_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(85, 'Product Categories Create', 'products_category_index', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(86, 'Product Categories Store', 'products_category_store', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(87, 'Product Categories Edit', 'products_category_edit', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(88, 'Product Categories Update', 'products_category_update', 1, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(89, 'Product Create', 'products_index', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(90, 'Product Store', 'products_store', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(91, 'Product Edit', 'products_edit', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(92, 'Product Update', 'products_update', 2, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(93, 'All Orders', 'invoices_all_sales', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(94, 'Order Create', 'invoices_index', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(95, 'Order Store', 'invoices_store', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(96, 'Order Edit', 'invoices_edit', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(97, 'Order Update', 'invoices_update', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(98, 'Order Show A4 Size', 'invoices_show', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(99, 'Order Show Pos Printer', 'invoices_show_pos', 3, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(100, 'All Purchases', 'bills_all_bills', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(101, 'Purchase Create', 'bills_index', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(102, 'Purchase Store', 'bills_edit', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(103, 'Purchase Edit', 'bills_store', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(104, 'Purchase Update', 'bills_update', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(105, 'Purchase Show', 'bills_show', 4, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(106, 'Sales Return Create', 'sales_return_index', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(107, 'Sales Return Store', 'sales_return_store', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(108, 'Sales Return Show', 'sales_return_show', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(109, 'Sales Return Delete', 'sales_return_delete', 5, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(110, 'Purchase Return Create', 'purchase_return_index', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(111, 'Purchase Return Store', 'purchase_return_store', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(112, 'Purchase Return Show', 'purchase_return_show', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(113, 'Purchase Return Delete', 'purchase_return_delete', 6, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(114, 'Expense Categories Store', 'expenses_categories_store', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(115, 'Expense Categories Edit', 'expenses_categories_edit', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(116, 'Expense Categories Update', 'expenses_categories_update', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(117, 'Expenses Create', 'expenses_index', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(118, 'Expenses Store', 'expenses_store', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(119, 'Expenses Edit', 'expenses_edit', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(120, 'Expenses Update', 'expenses_update', 7, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(121, 'Payments Create', 'payments_create', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(122, 'Payments Store', 'payments_store', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(123, 'Payments Show', 'payments_print', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(124, 'Payments Delete', 'payments_delete', 8, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(125, 'Contacts Create', 'customers_index', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(126, 'Contacts Store', 'customers_store', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(127, 'Contacts Edit', 'customers_edit', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(128, 'Contacts Update', 'customers_update', 9, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(129, 'Paid Through Accounts Create', 'paid_through_accounts_index', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(130, 'Paid Through Accounts Store', 'paid_through_accounts_store', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(131, 'Paid Through Accounts Edit', 'paid_through_accounts_edit', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(132, 'Paid Through Accounts Update', 'paid_through_accounts_update', 10, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(133, 'Units Create', 'products_units_index', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(134, 'Units Store', 'products_units_store', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(135, 'Units Edit', 'products_units_edit', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(136, 'Units Update', 'products_units_update', 11, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(137, 'Employee Salary Create', 'expenses_employee_salary_index', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(138, 'Employee Salary Store', 'expenses_employee_salary_store', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(139, 'Employee Salary Edit', 'expenses_employee_salary_edit', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(140, 'Employee Salary Update', 'expenses_employee_salary_update', 12, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(141, 'Bar Code Generate', 'products_barcode_print', 13, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(142, 'Bar Code Print', 'products_barcode_print_print', 13, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(143, 'Discounts Create', 'discounts_index', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(144, 'Discounts Store', 'discounts_store', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(145, 'Discounts Edit', 'discounts_edit', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(146, 'Discounts Update', 'discounts_update', 14, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(147, 'Users Create', 'users_index', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(148, 'Users Store', 'users_store', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(149, 'Users Edit', 'users_edit', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(150, 'Users Update', 'users_update', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(151, 'Users Delete', 'users_destroy', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(152, 'Users Profile Edit', 'users_edit_profile', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(153, 'Users Profile Update', 'users_update_profile', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(154, 'Users Settings Edit', 'users_edit_settings', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(155, 'Users Settings Update', 'users_update_settings', 16, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(156, 'Access Level Create', 'set_access_index', 17, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(157, 'Access Level Update', 'set_access_update', 17, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(158, 'Stock Report', 'stock_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(159, 'Profit Loss Report', 'profit_loss_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(160, 'Sales Report', 'sales_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(161, 'Sales Summary Report', 'sales_summary_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(162, 'Purchase Report', 'purchase_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(163, 'Purchase Summary Report', 'purchase_summary_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(164, 'Customer Due Report', 'due_report_customer_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(165, 'Supplier Due Report', 'due_report_supplier_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(166, 'Payment Report', 'payment_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(167, 'Expense Rerport', 'expense_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(168, 'Collection Report', 'collection_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00'),
(169, 'SalaryReport', 'salary_report_index', 15, 1, 0, '2020-11-05 01:49:29', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci,
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` tinyint(4) NOT NULL COMMENT '1=Super Admin 2=Admin 3=Employee 4=cutomer',
  `branch_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` tinyint(4) NOT NULL COMMENT '1=Active\r\n0=Inactive',
  `organization_name` text COLLATE utf8mb4_unicode_ci,
  `address` text COLLATE utf8mb4_unicode_ci,
  `contact_number` text COLLATE utf8mb4_unicode_ci,
  `contact_email` text COLLATE utf8mb4_unicode_ci,
  `website` text COLLATE utf8mb4_unicode_ci,
  `header_image` text COLLATE utf8mb4_unicode_ci,
  `footer_image` text COLLATE utf8mb4_unicode_ci,
  `sales_show` double DEFAULT NULL,
  `vat_reg_number` text COLLATE utf8mb4_unicode_ci,
  `mushak` text COLLATE utf8mb4_unicode_ci,
  `vat_type` tinyint(4) DEFAULT NULL,
  `vat_amount` double DEFAULT NULL,
  `tax_type` tinyint(4) DEFAULT NULL,
  `tax_amount` double DEFAULT NULL,
  `pos_printer` tinyint(4) DEFAULT '1' COMMENT '0= 58 mm Label Size 1 = 80 mm Label Size 2 = A4 3 = letter',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `photo`, `logo`, `remember_token`, `role`, `branch_id`, `status`, `organization_name`, `address`, `contact_number`, `contact_email`, `website`, `header_image`, `footer_image`, `sales_show`, `vat_reg_number`, `mushak`, `vat_type`, `vat_amount`, `tax_type`, `tax_amount`, `pos_printer`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Cyberdyne Technology Ltd.', 'superadmin', '0000-00-00 00:00:00', '$2y$10$e4t28BJ6.DSK1gmQ/IO3qOB0WqszhEzUvr0HGEypWGuHLuBY9zC3S', NULL, 'company-profile-images/1618061964.png', '', 1, 1, 1, 'মেসার্স সজ্জাদ এন্টারপ্রাইজ', 'বাতেন খাঁর মোড় চাঁপাইনবাবগঞ্জ সদর, চাঁপাইনবাবগঞ্জ', '01712171524', NULL, 'www.sylhetibazzar.com', '', '', 0, NULL, NULL, 0, NULL, 0, 0, 1, 1, 1, '2020-04-23 15:06:17', '2021-04-11 22:07:27'),
(5, 'Super Admin', '01715262667', NULL, '$2y$10$As6SORM3/p3Km5XZeDh0fO01.HMyWNIIXrs2sYhf8seYwpuzUXXN.', NULL, NULL, NULL, 1, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2021-01-31 10:39:24', '2021-04-11 22:04:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `backup`
--
ALTER TABLE `backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bills_78_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bills_78_created_by_foreign` (`created_by`),
  ADD KEY `bills_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `bill_entries`
--
ALTER TABLE `bill_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `bill_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `bill_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `bill_entries_78_vendor_id_foreign` (`vendor_id`),
  ADD KEY `bill_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `bill_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `categories_78_created_by_foreign` (`created_by`),
  ADD KEY `categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `discounts`
--
ALTER TABLE `discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discounts_78_created_by_foreign` (`created_by`),
  ADD KEY `discounts_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `discount_products`
--
ALTER TABLE `discount_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discount_products_78_product_id_foreign` (`product_id`),
  ADD KEY `discount_products_78_discount_id_foreign` (`discount_id`),
  ADD KEY `discount_products_78_created_by_foreign` (`created_by`),
  ADD KEY `discount_products_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_78_expense_category_id_foreign` (`expense_category_id`),
  ADD KEY `expenses_78_user_id_foreign` (`user_id`),
  ADD KEY `expenses_78_created_by_foreign` (`created_by`),
  ADD KEY `expenses_78_updated_by_foreign` (`updated_by`),
  ADD KEY `paid_through_id` (`paid_through_id`);

--
-- Indexes for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expense_categories_78_created_by_foreign` (`created_by`),
  ADD KEY `expense_categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `incomes`
--
ALTER TABLE `incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `income_category_id` (`income_category_id`),
  ADD KEY `paid_through_id` (`paid_through_id`);

--
-- Indexes for table `income_categories`
--
ALTER TABLE `income_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_78_customer_id_foreign` (`customer_id`),
  ADD KEY `invoices_78_created_by_foreign` (`created_by`),
  ADD KEY `invoices_78_updated_by_foreign` (`updated_by`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `invoice_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `invoice_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `invoice_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `invoice_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `items_78_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `items_78_created_by_foreign` (`created_by`),
  ADD KEY `items_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `phone_book_id` (`phone_book_id`),
  ADD KEY `message_id` (`message_id`);

--
-- Indexes for table `message_lists`
--
ALTER TABLE `message_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_78_created_by_foreign` (`created_by`),
  ADD KEY `modules_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `modules_access`
--
ALTER TABLE `modules_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `modules_access_78_module_id_foreign` (`module_id`),
  ADD KEY `modules_access_78_user_id_foreign` (`user_id`),
  ADD KEY `modules_access_78_created_by_foreign` (`created_by`),
  ADD KEY `modules_access_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paid_through_accounts_78_created_by_foreign` (`created_by`),
  ADD KEY `paid_through_accounts_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`(191));

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_78_customer_id_foreign` (`customer_id`),
  ADD KEY `payments_78_paid_through_foreign` (`paid_through`),
  ADD KEY `payments_78_created_by_foreign` (`created_by`),
  ADD KEY `payments_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `payment_entries`
--
ALTER TABLE `payment_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_entries_78_payment_id_foreign` (`payment_id`),
  ADD KEY `payment_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `payment_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `payment_entries_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `payment_entries_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `payment_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `payment_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_78_url_id_foreign` (`url_id`),
  ADD KEY `permissions_78_user_id_foreign` (`user_id`),
  ADD KEY `permissions_78_created_by_foreign` (`created_by`),
  ADD KEY `permissions_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `phone_book`
--
ALTER TABLE `phone_book`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_78_category_id_foreign` (`category_id`),
  ADD KEY `products_78_sub_category_id_foreign` (`sub_category_id`),
  ADD KEY `products_78_item_id_foreign` (`item_id`),
  ADD KEY `products_78_unit_foreign` (`unit`),
  ADD KEY `products_78_created_by_foreign` (`created_by`),
  ADD KEY `products_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `product_customers`
--
ALTER TABLE `product_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `supplier_id` (`customer_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `product_entries`
--
ALTER TABLE `product_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `product_entries_78_unit_id_foreign` (`unit_id`),
  ADD KEY `product_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `product_entries_78_updated_by_foreign` (`updated_by`),
  ADD KEY `supplier_id` (`supplier_id`);

--
-- Indexes for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `supplier_id` (`supplier_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `product_entry_id` (`product_entry_id`),
  ADD KEY `variation_id` (`variation_id`),
  ADD KEY `variation_value_id` (`variation_value_id`);

--
-- Indexes for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `product_variation_id` (`product_variation_id`);

--
-- Indexes for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_return_78_bill_id_foreign` (`bill_id`),
  ADD KEY `purchase_return_78_customer_id_foreign` (`customer_id`),
  ADD KEY `purchase_return_78_created_by_foreign` (`created_by`),
  ADD KEY `purchase_return_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_return_entries_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `purchase_return_entries_78_bill_id_foreign` (`bill_id`),
  ADD KEY `purchase_return_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `purchase_return_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `purchase_return_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `purchase_return_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `purchase_return_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sales_return`
--
ALTER TABLE `sales_return`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_return_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `sales_return_78_customer_id_foreign` (`customer_id`),
  ADD KEY `sales_return_78_created_by_foreign` (`created_by`),
  ADD KEY `sales_return_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sales_return_entries_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `sales_return_entries_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `sales_return_entries_78_product_id_foreign` (`product_id`),
  ADD KEY `sales_return_entries_78_product_entry_id_foreign` (`product_entry_id`),
  ADD KEY `sales_return_entries_78_customer_id_foreign` (`customer_id`),
  ADD KEY `sales_return_entries_78_created_by_foreign` (`created_by`),
  ADD KEY `sales_return_entries_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_78_category_id_foreign` (`category_id`),
  ADD KEY `sub_categories_78_created_by_foreign` (`created_by`),
  ADD KEY `sub_categories_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_78_invoice_id_foreign` (`invoice_id`),
  ADD KEY `transactions_78_bill_id_foreign` (`bill_id`),
  ADD KEY `transactions_78_sales_return_id_foreign` (`sales_return_id`),
  ADD KEY `transactions_78_purchase_return_id_foreign` (`purchase_return_id`),
  ADD KEY `transactions_78_expense_id_foreign` (`expense_id`),
  ADD KEY `transactions_78_payment_id_foreign` (`payment_id`),
  ADD KEY `transactions_78_customer_id_foreign` (`customer_id`),
  ADD KEY `transactions_78_paid_through_foreign` (`paid_through`),
  ADD KEY `transactions_78_created_by_foreign` (`created_by`),
  ADD KEY `transactions_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `units_78_created_by_foreign` (`created_by`),
  ADD KEY `units_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `main_unit_id` (`main_unit_id`),
  ADD KEY `converted_unit_id` (`converted_unit_id`),
  ADD KEY `product_entry_id` (`product_entry_id`);

--
-- Indexes for table `urls`
--
ALTER TABLE `urls`
  ADD PRIMARY KEY (`id`),
  ADD KEY `urls_78_module_id_foreign` (`module_id`),
  ADD KEY `urls_78_created_by_foreign` (`created_by`),
  ADD KEY `urls_78_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `branch_id` (`branch_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `backup`
--
ALTER TABLE `backup`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT for table `bill_entries`
--
ALTER TABLE `bill_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=345;

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `discounts`
--
ALTER TABLE `discounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `discount_products`
--
ALTER TABLE `discount_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `expense_categories`
--
ALTER TABLE `expense_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incomes`
--
ALTER TABLE `incomes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `income_categories`
--
ALTER TABLE `income_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `message_lists`
--
ALTER TABLE `message_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `modules_access`
--
ALTER TABLE `modules_access`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_entries`
--
ALTER TABLE `payment_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=426;

--
-- AUTO_INCREMENT for table `phone_book`
--
ALTER TABLE `phone_book`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_customers`
--
ALTER TABLE `product_customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_entries`
--
ALTER TABLE `product_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `product_variations`
--
ALTER TABLE `product_variations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=507;

--
-- AUTO_INCREMENT for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `purchase_return`
--
ALTER TABLE `purchase_return`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `sales_return`
--
ALTER TABLE `sales_return`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `urls`
--
ALTER TABLE `urls`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=170;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `backup`
--
ALTER TABLE `backup`
  ADD CONSTRAINT `backup_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `backup_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bills`
--
ALTER TABLE `bills`
  ADD CONSTRAINT `bills_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bills_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bills_78_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bill_entries`
--
ALTER TABLE `bill_entries`
  ADD CONSTRAINT `bill_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `bill_entries_78_vendor_id_foreign` FOREIGN KEY (`vendor_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `branches`
--
ALTER TABLE `branches`
  ADD CONSTRAINT `branches_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `branches_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `customers_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discounts`
--
ALTER TABLE `discounts`
  ADD CONSTRAINT `discounts_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discounts_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `discount_products`
--
ALTER TABLE `discount_products`
  ADD CONSTRAINT `discount_products_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_discount_id_foreign` FOREIGN KEY (`discount_id`) REFERENCES `discounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `discount_products_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expenses`
--
ALTER TABLE `expenses`
  ADD CONSTRAINT `expenses_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_expense_category_id_foreign` FOREIGN KEY (`expense_category_id`) REFERENCES `expense_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expenses_ibfk_1` FOREIGN KEY (`paid_through_id`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `expense_categories`
--
ALTER TABLE `expense_categories`
  ADD CONSTRAINT `expense_categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `expense_categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `incomes`
--
ALTER TABLE `incomes`
  ADD CONSTRAINT `incomes_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_3` FOREIGN KEY (`income_category_id`) REFERENCES `income_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `incomes_ibfk_4` FOREIGN KEY (`paid_through_id`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `income_categories`
--
ALTER TABLE `income_categories`
  ADD CONSTRAINT `income_categories_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `income_categories_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoices_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `invoice_entries`
--
ALTER TABLE `invoice_entries`
  ADD CONSTRAINT `invoice_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `invoice_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `items`
--
ALTER TABLE `items`
  ADD CONSTRAINT `items_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_78_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `items_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `message_deliveries`
--
ALTER TABLE `message_deliveries`
  ADD CONSTRAINT `message_deliveries_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_deliveries_ibfk_2` FOREIGN KEY (`phone_book_id`) REFERENCES `phone_book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_deliveries_ibfk_3` FOREIGN KEY (`message_id`) REFERENCES `message_lists` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `message_lists`
--
ALTER TABLE `message_lists`
  ADD CONSTRAINT `message_lists_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `message_lists_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `modules_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `modules_access`
--
ALTER TABLE `modules_access`
  ADD CONSTRAINT `modules_access_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_access_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `paid_through_accounts`
--
ALTER TABLE `paid_through_accounts`
  ADD CONSTRAINT `paid_through_accounts_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `paid_through_accounts_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_paid_through_foreign` FOREIGN KEY (`paid_through`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payments_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payment_entries`
--
ALTER TABLE `payment_entries`
  ADD CONSTRAINT `payment_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `payment_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
  ADD CONSTRAINT `permissions_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_url_id_foreign` FOREIGN KEY (`url_id`) REFERENCES `urls` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permissions_78_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_78_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_item_id_foreign` FOREIGN KEY (`item_id`) REFERENCES `items` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_sub_category_id_foreign` FOREIGN KEY (`sub_category_id`) REFERENCES `sub_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_unit_foreign` FOREIGN KEY (`unit`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `products_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_entries`
--
ALTER TABLE `product_entries`
  ADD CONSTRAINT `product_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_entries_ibfk_1` FOREIGN KEY (`supplier_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD CONSTRAINT `product_suppliers_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_3` FOREIGN KEY (`supplier_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_suppliers_ibfk_4` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variations`
--
ALTER TABLE `product_variations`
  ADD CONSTRAINT `product_variations_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variations_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variation_entries`
--
ALTER TABLE `product_variation_entries`
  ADD CONSTRAINT `product_variation_entries_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_3` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_4` FOREIGN KEY (`variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_entries_ibfk_5` FOREIGN KEY (`variation_value_id`) REFERENCES `product_variation_values` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `product_variation_values`
--
ALTER TABLE `product_variation_values`
  ADD CONSTRAINT `product_variation_values_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_values_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `product_variation_values_ibfk_3` FOREIGN KEY (`product_variation_id`) REFERENCES `product_variations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_return`
--
ALTER TABLE `purchase_return`
  ADD CONSTRAINT `purchase_return_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `purchase_return_entries`
--
ALTER TABLE `purchase_return_entries`
  ADD CONSTRAINT `purchase_return_entries_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `purchase_return_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_return`
--
ALTER TABLE `sales_return`
  ADD CONSTRAINT `sales_return_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sales_return_entries`
--
ALTER TABLE `sales_return_entries`
  ADD CONSTRAINT `sales_return_entries_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_product_entry_id_foreign` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sales_return_entries_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_78_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_categories_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `sub_categories_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_78_bill_id_foreign` FOREIGN KEY (`bill_id`) REFERENCES `bills` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_expense_id_foreign` FOREIGN KEY (`expense_id`) REFERENCES `expenses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_paid_through_foreign` FOREIGN KEY (`paid_through`) REFERENCES `paid_through_accounts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_payment_id_foreign` FOREIGN KEY (`payment_id`) REFERENCES `payments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_purchase_return_id_foreign` FOREIGN KEY (`purchase_return_id`) REFERENCES `purchase_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_sales_return_id_foreign` FOREIGN KEY (`sales_return_id`) REFERENCES `sales_return` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transactions_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `units`
--
ALTER TABLE `units`
  ADD CONSTRAINT `units_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `units_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `unit_conversions`
--
ALTER TABLE `unit_conversions`
  ADD CONSTRAINT `unit_conversions_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_3` FOREIGN KEY (`main_unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_4` FOREIGN KEY (`converted_unit_id`) REFERENCES `units` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `unit_conversions_ibfk_5` FOREIGN KEY (`product_entry_id`) REFERENCES `product_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `urls`
--
ALTER TABLE `urls`
  ADD CONSTRAINT `urls_78_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `urls_78_module_id_foreign` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `urls_78_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branches` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
