<!DOCTYPE html>
<html>

<head>
    <title>Income Statement Print</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">

        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
               
                
                    <div class="row">
                        @if($user_info['header_image'] == null)
                        <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                            <img style="width: 100px;height: 100px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                        </div>
                        <div class="col-md-8 col-xs-8 col-sm-8">
                            <i><h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 40px;font-weight: bold;">{{ $user_info['organization_name'] }}</h2></i>
                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                        </div>
                        <!-- <div class="col-md-4 col-xs-12 col-sm-12"></div> -->
                        @else
                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                        @endif
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Income Statement Print</h6>
                    </div>

                    <div class="ibox-content">

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 20%">Category</th>
                                    <th style="text-align: center;width: 15%">DATE</th>
                                    <th style="text-align: center;width: 15%">Income#</th>
                                    <th style="text-align: center;width: 25%">Note </th>
                                    <th style="text-align: center;width: 20%">Amount </th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $i                  = 1;
                                    $total_quantity     = 0;
                                ?>
                                @foreach($data as $key => $value)
                               
                                <tr>
                                    <td style="text-align: center;vertical-align: middle;" rowspan="{{ count($value) }}">{{ $i }}</td>
                                    <td style="text-align: left;vertical-align: middle;" rowspan="{{ count($value) }}">{{$value[0]['income_categories_name']}}</td>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value[0]['income_date'])) }}</td>
                                    <td style="text-align: left;">INC- {{ str_pad($value[0]['income_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{$value[0]['note']}}</td>
                                    <td style="text-align: center;">{{$value[0]['amount']}}</td>
                                </tr>

                                @foreach($value as $key1 => $value1)
                                @if(($key1 != 0))
                                <tr>
                                    <td style="text-align: center;">{{ date('d-m-Y', strtotime($value1['income_date'])) }}</td>
                                    <td style="text-align: left;">INC- {{ str_pad($value1['income_number'], 6, "0", STR_PAD_LEFT) }}</td>
                                    <td style="text-align: center;">{{$value1['note']}}</td>
                                    <td style="text-align: center;">{{$value1['amount']}}</td>
                                </tr>
                                @endif
                                @endforeach
                                 
                                <?php
                                    $total_quantity      = 0;
                                    $i++;
                                ?>
                                @endforeach
                            </tbody>

                            <!-- <tfoot class="tfheight">
                                <tr>
                                    <th colspan="8" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"></th>
                                <tr>
                            </tfoot> -->
                        </table>
                        
                        <br>
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-lg-1"></div>
                            <div class="col-lg-5">
                                <span style="font-size: 18px;font-weight: bold;border-top: 2px dashed; black;">Authorization signature</span>
                            </div>
                            <div class="col-lg-5" style="text-align: right;">
                                <span style="font-size: 18px;font-weight: bold;border-top: 2px dashed; black;">Manufacturer's signature</span>
                            </div>
                            <div class="col-lg-1"></div>
                        </div>

                        <br>
                        <br>
                        <br>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>

            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>