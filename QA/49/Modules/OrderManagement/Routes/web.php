<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::prefix('ordermanagement/services')->group(function() {
    Route::get('/', 'OrderManagementController@serviceIndex')->name('community_service_index');
    Route::post('/store', 'OrderManagementController@serviceStore')->name('community_service_store');
    Route::get('/edit/{id}', 'OrderManagementController@serviceEdit')->name('community_service_edit');
    Route::post('/update/{id}', 'OrderManagementController@serviceUpdate')->name('community_service_update');
    Route::get('/sms-list', 'OrderManagementController@smsList')->name('sms_list');
    
    
    Route::get('/program-type', 'OrderManagementController@programTypeIndex')->name('program_type_index');
    Route::post('/program-type-store', 'OrderManagementController@programTypeStore')->name('program_type_store');
    Route::get('/program-type-edit/{id}', 'OrderManagementController@programTypeEdit')->name('program_type_edit');
    Route::post('/program-type-update/{id}', 'OrderManagementController@programTypeUpdate')->name('program_type_update');
    
    
    Route::get('/service-man-cost', 'OrderManagementController@serviceManCostIndex')->name('service_man_cost_index');
    Route::post('/service-man-cost-store', 'OrderManagementController@serviceManCostStore')->name('service_man_cost_store');
    Route::get('/service-man-cost-edit/{id}', 'OrderManagementController@serviceManCostEdit')->name('service_man_cost_edit');
    Route::post('/service-man-cost-update/{id}', 'OrderManagementController@serviceManCostUpdate')->name('service_man_cost_update');
});

Route::prefix('ordermanagement')->group(function() {
    Route::get('/quick-order', 'OrderManagementController@index')->name('quick_order');
    Route::get('/order-list', 'OrderManagementController@orderList')->name('order_list');
    Route::get('/order/list/load', 'OrderManagementController@orderListLoad')->name('order_list_load');
     Route::post('/order-cancel-confirm/{id}', 'OrderManagementController@orderCancelConfirm')->name('order_cancel_confirm');

    Route::post('/store', 'OrderManagementController@store')->name('order_store');
    Route::get('/edit/{id}', 'OrderManagementController@edit')->name('order_edit');
    Route::post('/update/{id}', 'OrderManagementController@update')->name('order_update');
    Route::get('/show/{id}', 'OrderManagementController@show')->name('order_show');
    
    Route::get('/cancel-order/{id}', 'OrderManagementController@orderCancel')->name('order_cancel');
    Route::get('/final-voucher/{id}', 'OrderManagementController@finalVoucher')->name('final_voucher');
    Route::post('/change-order-status', 'OrderManagementController@orderStatusChange')->name('change_order_status');

    Route::get('/customer-make-payment/{id}', 'OrderManagementController@makePayment');
    Route::post('/pay-customer-bill/payments', 'OrderManagementController@storePayment')->name('pay_customer_bill');
    
    
    Route::get('/order/search/list/{from_date}/{to_date}/{program_from_date}/{program_to_date}/{customer}/{status}', 'OrderManagementController@orderListSearch')->name('order_list_search');

    Route::get('/customer-pay-slip/{invoice_id}', 'OrderManagementController@customerPaySlip')->name('customer_pay_slip');
    Route::get('/service-man-cost/{service_man_cost}', 'OrderManagementController@serviceManCost');
    Route::get('/payslip/{id}', 'OrderManagementController@payslip')->name('ordermanagement_payslip');
});

Route::prefix('setmenu')->group(function() {
    Route::get('/add-set-menu', 'OrderManagementController@createSetMenu')->name('set_menu_index');
    Route::post('/store-set-menu', 'OrderManagementController@storeSetMenu')->name('set_menu_store');
    Route::get('/edit-set-menu/{id}', 'OrderManagementController@editSetMenu')->name('set_menu_edit');
    Route::post('/update-set-menu/{id}', 'OrderManagementController@updateSetMenu')->name('set_menu_update');
    
    Route::get('/add-set-menu-item', 'OrderManagementController@createSetMenuItem')->name('set_menu_item_index');
    Route::post('/store-set-menu-item', 'OrderManagementController@storeSetMenuItem')->name('set_menu_item_store');
    Route::get('/edit-set-menu-item/{id}', 'OrderManagementController@editSetMenuItem')->name('set_menu_item_edit');
    Route::post('/update-set-menu-item/{id}', 'OrderManagementController@updateSetMenuItem')->name('set_menu_item_update');
    Route::get('/setmenuItem/{setMenuId}', 'OrderManagementController@setmenuItem');
    Route::get('/price/{setMenuId}', 'OrderManagementController@setmenuPrice');
    Route::get('/collect-setmenuItem/{orderId}', 'OrderManagementController@collectSetmenu');
});
