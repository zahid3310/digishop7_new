@extends('layouts.app')

@section('title', 'Show')

<style type="text/css">
    .jol {
    position: absolute;
    left: 35%;
    height: 173px;
    top: 243px;
    opacity: 0.08;
}
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Booking</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Booking</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <!-- <div class="col-md-4 col-xs-12 col-sm-12"></div> -->
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            {{ $invoice['customer_name'] }}
                                            @if($invoice['address'] != null)
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            @if($invoice['address'] == null)
                                                <br>
                                            @endif
                                            {{ $invoice['phone'] }}

                                            <br>

                                            <strong>Program Time:</strong><br>
                                            <strong>Start-Time : </strong> 
                                            <?php $start_time = new DateTime($invoice['start_time'], new DateTimezone('Asia/Dhaka')); echo $start_time->format('g:i a'); ?> <strong> End-Time : </strong><?php $end_time = new DateTime($invoice['end_time'], new DateTimezone('Asia/Dhaka')); echo $end_time->format('g:i a'); ?><br>
                                        </address>
                                    </div>


                                    <div style="font-size: 16px" class="col-sm-6 col-6 text-sm-right">
                                        <address>
                                            <strong>Booking Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['order_date'])) }}<br>

                                            <strong>Booking From Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['program_from_data'])) }}<br>

                                            <strong>Booking To Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['program_to_data'])) }}<br>

                                           

                                            <br>
                                        </address>
                                    </div>
                                </div>

                                <img class="jol" src="{{ url('public/'.userDetails()->logo) }}" alt="Logo">

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Booking summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Booking # {{ 'Ord - ' . str_pad($invoice['order_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th>Floar</th>
                                                <th class="text-right">Floar Rant</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{$value->service_name}}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="2" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>


                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="2" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="2" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($invoice['invoice_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $invoice['invoice_note'] }}</h6>
                                    </div>
                                </div>
                                @endif
                                
                                <br><br><br>

                    <div class="row">
                        <div class="col-md-6">
                            <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Organization Sign </span> </h6>
                        </div>
                        <div class="col-md-6">
                            <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign</span> </h6>
                        </div>
                    </div>

                                

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    @if($user_info['header_image'] == null)
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                        <div class="col-md-8 col-xs-12 col-sm-12">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                        </div>
                                        <!-- <div class="col-md-4 col-xs-12 col-sm-12"></div> -->
                                    @else
                                        <img class="float-left" src="{{ url('public/images/customer_header_'.Auth::user()->associative_contact_id.'_'.$user_info['header_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address>
                                            <strong>Billed To:</strong><br>
                                            {{ $invoice['customer_name'] }}
                                            @if($invoice['address'] != null)
                                               <br> <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            @if($invoice['address'] == null)
                                                <br>
                                            @endif
                                            {{ $invoice['phone'] }}

                                            <br>

                                            <strong>Program Time:</strong><br>
                                            <strong>Start-Time : </strong> 
                                            <?php $start_time = new DateTime($invoice['start_time'], new DateTimezone('Asia/Dhaka')); echo $start_time->format('g:i a'); ?> <strong> End-Time : </strong><?php $end_time = new DateTime($invoice['end_time'], new DateTimezone('Asia/Dhaka')); echo $end_time->format('g:i a'); ?><br>
                                        </address>
                                    </div>


                                    <div style="font-size: 16px" class="col-sm-6 col-6 text-sm-right">
                                        <address>
                                            <strong>Booking Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['order_date'])) }}<br>

                                            <strong>Booking From Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['program_from_data'])) }}<br>

                                            <strong>Booking To Date:</strong><br>
                                            {{ date('d-m-Y', strtotime($invoice['program_to_data'])) }}<br>

                                           

                                            <br>
                                        </address>
                                    </div>
                                </div>

                                <img class="jol" src="{{ url('public/'.userDetails()->logo) }}" alt="Logo">

                                <div class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6 hidden-xs">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Booking summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Booking # {{ 'Ord - ' . str_pad($invoice['order_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <div class="table-responsive">
                                    <table class="table table-nowrap">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th>Floar</th>
                                                <th class="text-right">Floar Rant</th>
                                                <th class="text-right">Total</th>
                                            </tr>
                                        </thead>
                                        <tbody style="font-size: 16px">

                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{$value->service_name}}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                            
                                            <tr>
                                                <td style="font-size: 16px" colspan="2" class="text-right">Sub Total</td>
                                                <td style="font-size: 16px" class="text-right">{{ number_format($sub_total,2,'.',',') }}</td>
                                            </tr>


                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="2" class="border-0 text-right">Paid</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['cash_given'],2,'.',',') }}
                                                </td>
                                            </tr>
                                            
                                            <tr style="line-height: 0px">
                                                <td style="font-size: 16px" colspan="2" class="border-0 text-right">Dues</td>
                                                <td style="font-size: 16px" class="border-0 text-right">
                                                    {{ number_format($invoice['due_amount'],2,'.',',') }}
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                                @if($invoice['invoice_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $invoice['invoice_note'] }}</h6>
                                    </div>
                                </div>
                                @endif
                                
                                <br><br><br>

                    <div class="row">
                        <div class="col-md-6">
                            <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Organization Sign </span> </h6>
                        </div>
                        <div class="col-md-6">
                            <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign</span> </h6>
                        </div>
                    </div>

            

                                <div style="position: fixed;bottom: 0">
                                    <!-- <h4 class="float-right font-size-16">Order # 12345</h4> -->
                                    <!-- <div class="col-md-4">
                                        <img class="float-left" src="{{ url('public/az-ai.png') }}" alt="logo" height="20"/>
                                    </div>

                                    <div class="col-md-4">
                                        <h2 style="text-align: center">{{ $user_info['organization_name'] }}</h2>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['address'] }}</p>
                                        <p style="line-height: 0px;text-align: center">{{ $user_info['contact_number'] }}</p>
                                    </div>

                                    <div class="col-md-4">
                                        <p style="line-height: 18px;text-align: right;padding: 0px">Phone - 01718937082<br>01711418731<br>01711418731</p>
                                    </div> -->
                                    @if($user_info['footer_image'] != null)
                                        <img class="float-left" src="{{ url('public/images/customer_footer_'.Auth::user()->associative_contact_id.'_'.$user_info['footer_image']) }}" alt="logo" style="width: 100%" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                    <div class="col-12">
                    <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                                </div>
                </div> <br><br>
                
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection