@extends('layouts.app')

@section('title', 'Show')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold">{{ userDetails()->organization_name }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> {{ userDetails()->address }} </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 0{{ userDetails()->contact_number }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: {{ userDetails()->email }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("string") }}
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">SR/Customer :   </span>{{ $invoice['customer_name'] }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> {{ $invoice->customer->phone != null ? $invoice->customer->phone : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%">SL</th>
                                            <th style="font-size: 18px;width: 60%">Item Name & Code</th>
                                            <th style="font-size: 18px;width: 10%">Quantity</th>
                                            <th style="font-size: 18px;width: 15%">Rate</th>
                                            <th style="font-size: 18px;width: 10%">Taka</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            if ($value->productEntries->warrenty_type == 2)
                                            {
                                                $serial_number  = isset($value->warrentyProducts->serial_number) ? ' (Serial# '.$value->warrentyProducts->serial_number . ')' : '';
                                            }
                                            else
                                            {
                                                $serial_number  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center">{{ $key + 1 }}</td>
                                            <td style="padding-left: 30px">{{ $value['product_entry_name'] . $productCode . $serial_number }}</td>
                                            <td style="text-align: center">{{ $value['quantity'] }}</td>
                                            <td style="text-align: center">{{ $value['rate'] . $unit }}</td>
                                            <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong>Total</strong></th>
                                            <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Discount</strong></th>
                                            <th style="text-align: center">{{ round($discount_on_total_amount + $invoice['total_discount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Payable</strong></th>
                                            <th style="text-align: center">{{ round($invoice['invoice_amount']) }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                                            <th style="text-align: center">{{ $pre_dues  != 0 ? round($pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Net Due</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                                            <th style="text-align: center">{{ $paid != 0 ? round($paid, 2) : '' }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>
                                            <th style="text-align: center">{{ $invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : '' }}</th>
                                        </tr>  
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Authorized Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
