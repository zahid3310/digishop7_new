<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('services')->group(function() {
    Route::get('/', 'ServicesController@create')->name('service_index');
    Route::post('/store-services', 'ServicesController@store')->name('service_store');
    Route::get('/all-services', 'ServicesController@index')->name('service_all');
    Route::get('/edit-services/{id}', 'ServicesController@edit')->name('service_edit');
    Route::post('/update-services/{id}', 'ServicesController@update')->name('service_update');
});
