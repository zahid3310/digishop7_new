<!DOCTYPE html>
<html>

<head>
    <title>Daily Collection Report</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Daily Collection Report</h6>
                    </div>

                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                                    <thead class="theight">
                                        <tr>
                                            <th style="text-align: center">DATE FROM</th>
                                            <th style="text-align: center">CUSTOMER NAME</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">
                                        <tr class="gradeC">
                                            <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                            <td style="text-align: center">{{ $find_customer != null ? $find_customer->name : 'All' }}</td>
                                        </tr>
                                    </tbody>
                                </table>

                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 5%">SL</th>
                                            <th style="text-align: center;width: 15%">SR/Customer</th>
                                            <th style="text-align: center;width: 10%">TOTAL SALES</th>
                                            <th style="text-align: center;width: 10%">CASH PAYMENT</th>
                                            <th style="text-align: center;width: 10%">BANK PAYMENT</th>
                                            <th style="text-align: center;width: 10%">SR/Customer EXPENSES</th>
                                            <th style="text-align: center;width: 10%">SR/Customer</th>
                                            <th style="text-align: center;width: 10%">ADVANCE PAYMENT</th>
                                            <th style="text-align: center;width: 10%">GENERAL ESPENSES</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php 
                                            $i = 1; 

                                            if(isset($_GET['from_date']))
                                            {
                                                $f_date = $_GET['from_date'];
                                            }
                                            else
                                            {
                                                $f_date = 0;
                                            }

                                            if(isset($_GET['to_date']))
                                            {
                                                $t_date = $_GET['to_date'];
                                            }
                                            else
                                            {
                                                $t_date = 0;
                                            }

                                            $total_sale             = 0;
                                            $total_cash_payment     = 0;
                                            $total_bank_payment     = 0;
                                            $total_dsr_expense      = 0;
                                            $total_dsr_due          = 0;
                                            $total_advance_payment  = 0;
                                        ?>

                                        @if(!empty($data))
                                        @foreach($data as $key => $value)
                                        @if(($value['sale']) > 0)
                                        <tr>
                                            <td style="text-align: center;vertical-align: middle" rowspan="">{{ $i }}</td>
                                            <td style="text-align: left;vertical-align: middle" rowspan="">{{ $value['customer_name'] }}</td>
                                            <td style="text-align: right;">{{ number_format($value['sale'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['cash_payment'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['bank_payment'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['dsr_expense'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['due_amount'],2,'.',',') }}</td>
                                            <td style="text-align: right;">{{ number_format($value['customer_advance'],2,'.',',') }}</td>
                                            @if($i == 1)
                                            <td style="text-align: center;vertical-align: middle" rowspan="{{ count($data) }}">{{ number_format($genaral_expense,2,'.',',') }}</td>
                                            @endif
                                        </tr>

                                        <?php 
                                            $i++;

                                            $total_sale             = $total_sale + $value['sale'];
                                            $total_cash_payment     = $total_cash_payment + $value['cash_payment'];
                                            $total_bank_payment     = $total_bank_payment + $value['bank_payment'];
                                            $total_dsr_expense      = $total_dsr_expense + $value['dsr_expense'];
                                            $total_dsr_due          = $total_dsr_due + $value['due_amount'];
                                            $total_advance_payment  = $total_advance_payment + $value['customer_advance']; 
                                        ?>
                                        @endif
                                        @endforeach
                                        @endif
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_sale,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_cash_payment,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_bank_payment,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_dsr_expense,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_dsr_due,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: right;">{{ number_format($total_advance_payment,2,'.',',') }}</th>
                                            <th colspan="1" style="text-align: center;">{{ number_format($genaral_expense,2,'.',',') }}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-4"></div>

                            <div class="col-md-4">
                                <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                                    <thead class="theight">
                                        <tr style="background:#ddd;">
                                            <th style="text-align: center;width: 25%">ACCOUNT NAME</th>
                                            <th style="text-align: center;width: 10%">OPENING BALANCE</th>
                                            <th style="text-align: center;width: 10%">BALANCE</th>
                                        </tr>
                                    </thead>

                                    <tbody class="theight">

                                        <?php
                                            $i             = 1;
                                            $total_in      = 0;
                                            $total_out     = 0;
                                            $total_balance = 0;
                                        ?>

                                        @foreach($data_current as $key => $value)
                                        <tr>
                                            <td style="text-align: left">
                                                {{ $value['account_name'] }}
                                            </td>
                                            <td style="text-align: right">{{ $value['opening_balance'] }}</td>
                                            <td style="text-align: right">{{ $value['balance'] }}</td>
                                        </tr>

                                        <?php
                                            $i++;
                                            $total_in      = $total_in + $value['cash_in_sum'];
                                            $total_out     = $total_out + $value['cash_out_sum'];
                                            $total_balance = $total_balance + $value['balance'];
                                        ?>
                                        @endforeach
                                    </tbody>

                                    <tfoot class="tfheight">
                                        <tr>
                                            <th colspan="2" style="text-align: right;">TOTAL</th>
                                            <th colspan="1" style="text-align: right;">{{ $total_balance }}</th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>

                            <div class="col-md-4"></div>
                        </div>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>