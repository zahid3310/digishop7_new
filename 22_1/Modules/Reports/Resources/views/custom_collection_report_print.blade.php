<!DOCTYPE html>
<html>

<head>
    <title>Summary Report</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">
</head>

<style type="text/css" media="print">        
    @page {
        size: auto;   
        margin: 15mm 5mm 5mm 10mm;
    }

    #print-footer {
        display: none;
        position: fixed;
        bottom: 0;
        left:0;
    }

    #hide-print {
        display: none;
    }

    #print-footer {
        display: none;
    }

    body {
        margin-bottom:10mm;
    }

    .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
        background-color: #ddd;
    }
    
    fontSizeC {
        font-size: 10px !important;
    }
</style>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">CASH BOOK</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">From Date</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <!--<th style="font-size: 10px;text-align: left;font-size: 15px" colspan="9">Opening Balance  :  66709/-</th>-->
                                    <th style="font-size: 10px;text-align: left;font-size: 15px" colspan="9">Opening Balance  :  {{ number_format($opening_credit_sum - $opening_debit_sum,2,'.',',') }}</th>
                                </tr>
                                
                                <tr style="background:#ddd;">
                                    
                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="4">DEBIT</th>
                                    <th style="font-size: 10px;text-align: center;font-size: 15px" colspan="5">CREDIT</th>
                                    
                                </tr>

                                <tr>
                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 15%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Amount</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Total</th>


                                    <th style="font-size: 10px;text-align: center;width: 5%">SL</th>
                                    <th style="font-size: 10px;text-align: center;width: 15%">Description</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Amount</th>
                                    <th style="font-size: 10px;text-align: center;width: 10%">Total</th>


                                    <th style="font-size: 10px;text-align: center;width: 15%;">Cash In Hand</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                @if(isset($result))
                                <?php 
                                    $check                  = 0; 
                                    $check1                 = 0; 
                                    $total_invoice_credit   = 0; 
                                    $total_invoice_debit    = 0; 
                                ?>
                                @for($i =0; $i < $count_sum; $i++)
                                <tr>
                                    @if(isset($result['credit'][$i]))
                                        @if(isset($result['credit'][$i]['check1']))
                                            <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                            <td style="font-size: 10px;text-align: left;">{{ $result['credit'][$i]['description'] }}</td>
                                            <td style="font-size: 10px;text-align: right;">{{ $result['credit'][$i]['amount'] != 0 ? number_format($result['credit'][$i]['amount'],2,'.',',') : '' }}</td>
                                            @if($check1 == 0)
                                            <td style="font-size: 10px;text-align: right;vertical-align: middle;" rowspan="{{ $invoice_count }}">{{ number_format($total_invoice_amount,2,'.',',') }}</td>
                                            @endif
    
                                            @if(isset($result['credit'][$i]['check1']) &&  ($result['credit'][$i]['check1'] == 1))
                                                <?php 
                                                    $check1++; 
                                                    $total_invoice_credit   = $total_invoice_credit + $result['credit'][$i]['amount'] != 0 ? $result['credit'][$i]['amount'] : 0;
                                                ?>
                                            @endif
                                        @else
                                            <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                            <td style="font-size: 10px;text-align: left;">{{ $result['credit'][$i]['description'] }}</td>
                                            <td style="font-size: 10px;text-align: right;"></td>
                                            <td style="font-size: 10px;text-align: right;">{{ $result['credit'][$i]['amount'] != 0 ? number_format($result['credit'][$i]['amount'],2,'.',',') : '' }}</td>
                                        @endif
                                    @else
                                        <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                    @endif



                                    @if(isset($result['debit'][$i]))
                                        @if(isset($result['debit'][$i]['check']))
                                            <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                            <td style="font-size: 10px;text-align: left;">{{ $result['debit'][$i]['description'] }}</td>
                                            <td style="font-size: 10px;text-align: right;">{{ number_format($result['debit'][$i]['amount'],2,'.',',') }}</td>
    
                                            @if($check == 0)
                                            <td style="font-size: 10px;text-align: right;vertical-align: middle;" rowspan="{{ $ch }}">{{ number_format($total_invoice_due_amount,2,'.',',') }}</td>
                                            @endif
    
                                            @if(isset($result['debit'][$i]['check']) &&  ($result['debit'][$i]['check'] == 1))
                                                <?php 
                                                    $check++; 
                                                    $total_invoice_debit   = $total_invoice_debit + $result['debit'][$i]['amount'] != 0 ? $result['debit'][$i]['amount'] : 0;
                                                ?>
                                            @endif
                                        @else
                                            <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                            <td style="font-size: 10px;text-align: left;">{{ $result['debit'][$i]['description'] }}</td>
                                            <td style="font-size: 10px;text-align: right;"></td>
                                            <td style="font-size: 10px;text-align: right;">{{ number_format($result['debit'][$i]['amount'],2,'.',',') }}</td>
                                        @endif
                                    @else
                                        <td style="font-size: 10px;text-align: center;">{{ $i + 1 }}</td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: left;"></td>
                                        <td style="font-size: 10px;text-align: right;"></td>
                                    @endif


                                    @if($i == 0)
                                    <!--<td style="font-size: 10px;text-align: center;vertical-align: middle;" rowspan="{{ $count_sum }}">{{ number_format(($credit_sum) - $debit_sum,2,'.',',') }}</td>-->
                                    <td style="font-size: 10px;text-align: center;vertical-align: middle;" rowspan="{{ $count_sum }}">{{ number_format($debit_sum,2,'.',',') }}</td>
                                    @endif
                                </tr>
                                @endfor
                                @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="3" style="text-align: right;">Total Debit </th>

                                    <th colspan="1" style="text-align: right;">{{ number_format($credit_sum + ($opening_credit_sum - $opening_debit_sum)   ,2,'.',',') }}</th>
                                    <th colspan="3" style="text-align: right;"></th>
                                    <th colspan="1" style="text-align: right;">{{ number_format($credit_sum + ($opening_credit_sum - $opening_debit_sum)   ,2,'.',',') }}</th>
                                    <th colspan="1" style="text-align: center;">{{ number_format($opening_credit_sum - $opening_debit_sum + $credit_sum - $debit_sum,2,'.',',') }}</th>
                                </tr>
                            </tfoot>

                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>