<!DOCTYPE html>
<html>

<head>
    <title>Current Balance Print</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p><?php echo e($user_info['address']); ?></p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Current Balance</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">Account Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    <td style="text-align: center">
                                        <?php if($paid_through_name != null): ?>
                                            <?php echo e($paid_through_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">DATE</th>
                                    <th style="text-align: center;width: 10%">IN</th>
                                    <th style="text-align: center;width: 10%">OUT</th>
                                    <th style="text-align: center;width: 10%">BALANCE</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                <?php
                                    $sub_total_in      = 0;
                                    $sub_total_out     = 0;
                                    $sub_total_balance = 0;
                                ?>

                                <?php $__currentLoopData = $data['date_wise_data']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php
                                    $in_1   = collect($value1)->where('type', 0)->sum('amount');
                                    $in_2   = collect($value1)->where('type', 3)->sum('amount');
                                    $out_1  = collect($value1)->where('type', 1)->sum('amount');
                                    $out_2  = collect($value1)->where('type', 2)->sum('amount');
                                    $in     = $in_1 + $in_2;
                                    $out    = $out_1 + $out_2;
                                    $balanc = $in - $out;

                                    $sub_total_in      = $sub_total_in + $in;
                                    $sub_total_out     = $sub_total_out + $out;
                                    $sub_total_balance = $sub_total_balance + $balanc;
                                ?>

                                <tr>
                                    <td style="text-align: center"><?php echo e($key1); ?></td>
                                    <td style="text-align: right"><?php echo e($in); ?></td>
                                    <td style="text-align: right"><?php echo e($out); ?></td>
                                    <td style="text-align: right"><?php echo e($balanc); ?></td>
                                </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="1" style="text-align: right;">TOTAL</th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($sub_total_in); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($sub_total_out); ?></th>
                                    <th colspan="1" style="text-align: right;"><?php echo e($sub_total_balance); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/22_1/Modules/Reports/Resources/views/current_balance_details.blade.php ENDPATH**/ ?>