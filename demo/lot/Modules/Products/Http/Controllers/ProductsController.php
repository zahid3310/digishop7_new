<?php

namespace Modules\Products\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Units;
use App\Models\BillEntries;
use App\Models\Branches;
use DB;
use Response;

class ProductsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->select('product_entries.*', 
                                             'units.name as unit_name',
                                             'customers.name as supplier_name',
                                             'categories.name as brand_name',
                                             'products.name as category_name')
                                    ->orderBy('product_entries.id', 'ASC')
                                    ->get();

        $product_id         = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $units              = Units::orderBy('id', 'ASC')
                                    ->get();

        return view('products::index', compact('products', 'product_id', 'units'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')
                                                        ->first();

            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            $product->supplier_id       = $data['supplier_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            if (($data['stock_quantity'] != null) && ($data['stock_quantity'] > 0))
            {
                $product->stock_in_hand   = $data['stock_quantity'];
                $product->opening_stock   = $data['stock_quantity'];
            }

            $product->tax_type                      = $data['tax_type'];
            $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            $product->vat_percentage                = $data['vat_percentage'];
            $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return back()->with("success","Product Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function edit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->select('product_entries.*',
                                             'units.name as unit_name',
                                             'customers.name as supplier_name',
                                             'categories.name as brand_name',
                                             'products.name as category_name')
                                    ->orderBy('product_entries.product_id', 'DESC')
                                    ->get();

        $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                    ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->select('product_entries.*',
                                             'customers.name as supplier_name',
                                             'categories.name as brand_name',
                                             'products.name as category_name')
                                    ->find($id);

        $entry_count        = $product_entries->count();
        $product_id         = ProductEntries::select('product_entries.*')
                                    ->orderBy('product_entries.product_code', 'DESC')
                                    ->first();

        $units              = Units::orderBy('id', 'ASC')
                                    ->get();

        return view('products::edit', compact('products', 'product_entries', 'entry_count', 'product_id', 'units'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_category_id'   => 'required',
            'buying_price'          => 'required',
            'selling_price'         => 'required',
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product                    = ProductEntries::find($id);
            $product->product_id        = $data['product_category_id'];
            $product->brand_id          = $data['brand_id'];
            $product->supplier_id       = $data['supplier_id'];
            $product->name              = $data['product_name'];
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->stock_in_hand     = $data['stock_quantity'];

            $product->tax_type                      = $data['tax_type'];
            $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            $product->vat_percentage                = $data['vat_percentage'];
            $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return redirect()->route('products_index')->with("success","Product Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function barcodePrint()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill_id            = isset($_GET['bill_id']) ? $_GET['bill_id'] : 0;

        $product_entries    = ProductEntries::orderBy('product_entries.total_sold', 'DESC')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->select('product_entries.*')
                                    ->get();

        $product_entries->sortBy('name');

        $bill_entries       = BillEntries::where('bill_entries.bill_id', $bill_id)
                                    ->select('bill_entries.*')
                                    ->get();

        $entry_count        = $bill_entries->count();

        $user_info          = userDetails();

        return view('products::barcodes', compact('product_entries', 'user_info', 'bill_entries', 'bill_id', 'entry_count'));
    }

    public function barcodePrintPrint(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_entry_id.*'  => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $product_entry_id   = $request->product_entry_id;
        $quantity           = $request->quantity;
        $print_type         = $request->type_id;
        $label_size         = $request->label_size;
        $margin             = 'margin-left: '.$request->margin.'px'.';margin-right: '.$request->margin.'px';
        $user_info          = userDetails();

        foreach ($request->product_entry_id as $key => $value)
        {
            $product_entries    = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->where('product_entries.id', $value)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*', 'products.name as product_name')
                                    ->first();

            $data[$product_entries['id']]['product_name_show']      = isset($request->product_name) ? 1 : 0;
            $data[$product_entries['id']]['organization_name_show'] = isset($request->organization_name) ? 1 : 0;
            $data[$product_entries['id']]['sell_price_show']        = isset($request->product_price) ? 1 : 0;
            $data[$product_entries['id']]['name']                   = $product_entries['name'];
            $data[$product_entries['id']]['organization_name']      = $user_info['organization_name'];
            $data[$product_entries['id']]['product_code']           = $product_entries['product_code'];
            $data[$product_entries['id']]['sell_price']             = $product_entries['sell_price'];
            $data[$product_entries['id']]['quantity']               = $quantity[$key];
        }

        return view('products::barcodes_print', compact('product_entries', 'data', 'print_type', 'user_info', 'label_size', 'margin'));
    }

    public function barcodePrintAjax($product_id, $product_entry_id)
    {
        $tables             = TableNameByUsers();
        $table_id           = Auth::user()->associative_contact_id;

        $product_entries    = $tables['product_entries']
                                    ->where('product_entries_'.$table_id.'.product_id', $product_id)
                                    ->orderBy('product_entries_'.$table_id.'.total_sold', 'DESC')
                                    ->select('product_entries_'.$table_id.'.*')
                                    ->get();

        $product_entries->sortBy('name');


        return Response::json($product_entries);
    }

    public function productListAjax()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $product_entries    = ProductEntries::where('product_entries.product_id', '!=', 1)->get();
        $product_entries->sortBy('name');

        return Response::json($product_entries);
    }

    public function producCategorytList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Products::orderBy('products.total_sold', 'DESC')
                                    ->where('products.id', '!=', 1)
                                    ->select('products.*')
                                    ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Products::where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Products::where('products.name', 'LIKE', "%$search%")
                                    ->where('products.id', '!=', 1)
                                    ->orderBy('products.total_sold', 'DESC')
                                    ->take(50)
                                    ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productBrandList()
    {
        $user_id            = Auth::user()->id;
        $user_branch_id     = Auth::user()->branch_id;
        $user_role          = Auth::user()->role;

        $data               = Categories::orderBy('categories.created_at', 'DESC')
                                            ->select('categories.*')
                                            ->get();

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = Categories::orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];   
            $fetchData      = Categories::where('categories.name', 'LIKE', "%$search%")
                                            ->orderBy('categories.created_at', 'DESC')
                                            ->take(50)
                                            ->get();
        }

        $data = array();
        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);
        }

        return Response::json($data);
    }

    public function productStoreProduct(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $products               = new Products;
        $products->name         = $data['product_name'];
        $products->status       = 1;
        $products->created_by   = $user_id;

        if ($products->save())
        {   
            return Response::json($products);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function productStoreBrand(Request $request)
    {
        $user_id                = Auth::user()->id;
        $data                   = $request->all();

        $brand                  = new Categories;
        $brand->name            = $data['brand_name'];
        $brand->status          = 1;
        $brand->created_by      = $user_id;

        if ($brand->save())
        {   
            return Response::json($brand);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function openingStock()
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $product_entries    = ProductEntries::whereNull('product_entries.stock_in_hand')
                                    ->where('product_entries.product_id', '!=', 1)
                                    ->select('product_entries.*')
                                    ->orderBy('product_entries.id', 'DESC')
                                    ->get();

        return view('products::opening_stock', compact('product_entries'));
    }

    public function storeOpeningStock(Request $request)
    {
        //Users Access Level Start
        // $access_check  = userAccess(Auth::user()->id);
        // if ($access_check == 0)
        // {
        //     return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        // }
        //Users Access Level End

        $rules = array(
            'product_entry_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        try{
            foreach ($data['stock_in_hand'] as $key => $value)
            {   
                if ($value != null)
                {
                    $product_entry                       = ProductEntries::find($data['product_entry_id'][$key]);
                    $product_entry->stock_in_hand        = $value;
                    $product_entry->opening_stock        = $value;
                    $product_entry->updated_by           = $user_id;
                    $product_entry->save();
                }
            }

            return back()->with("success","Stock Updated Successfully !!");

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $user_role          = Auth::user()->role;
        $products           = Products::where('products.id', '!=', 1)->orderBy('products.id', 'DESC')->get();

        $units              = Units::orderBy('id', 'ASC')->get();

        return view('products::category_index', compact('products', 'units'));
    }

    public function categoryStore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = new Products;
            $product->name              = $data['product_name'];
            $product->status            = 1;
            $product->created_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Created Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function categoryEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = Products::where('products.id', '!=', 1)
                                        ->orderBy('products.id', 'DESC')
                                        ->get();

        $find_product       = Products::find($id);
        $units              = Units::orderBy('id', 'ASC')
                                        ->get();

        return view('products::category_edit', compact('products', 'find_product', 'units'));
    }

    public function categoryUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'product_name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Products::find($id);
            $product->name              = $data['product_name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_category_index')->with("success","Product Category Updated Successfully !!");
            }

        }catch (\Exception $exception){
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsIndex()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        return view('products::units_index', compact('units'));
    }

    public function unitsstore(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $table                    = new Units;
            $table->name              = $data['name'];
            $table->created_by        = $user_id;
            
            if ($table->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Created Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }

    public function unitsEdit($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $units              = Units::select('units.*')
                                    ->orderBy('units.id', 'DESC')
                                    ->get();

        $find_unit          = Units::find($id);

        return view('products::units_edit', compact('find_unit', 'units'));
    }

    public function unitsUpdate(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'name'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        try{
            $product                    = Units::find($id);
            $product->name              = $data['name'];
            $product->updated_by        = $user_id;
            
            if ($product->save())
            {
                return redirect()->route('products_units_index')->with("success","Unit Updated Successfully !!");
            }

        }catch (\Exception $exception){
            return back()->with("unsuccess","Not Added");
        }
    }
}
