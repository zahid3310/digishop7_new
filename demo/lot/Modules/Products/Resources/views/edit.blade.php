@extends('layouts.app')

@section('title', 'Edit Product')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Edit Product</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Products</a></li>
                                    <li class="breadcrumb-item active">Edit Product</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('products_update', $product_entries['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-form-label">Category *</label>
                                        <select style="width: 83%" id="product_category_id" name="product_category_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                           <option value="{{ $product_entries['product_id'] }}" selected>{{ $product_entries['category_name'] }}</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Code</label>
                                        <input type="text" name="code" class="inner form-control barCode" id="bar_code_0" value="{{  str_pad($product_entries['product_code'], 6, "0", STR_PAD_LEFT) }}" readonly />
                                    </div>

                                    <div class="col-lg-12 col-md-12 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Product Name *</label>
                                        <input type="text" name="product_name" class="inner form-control" id="product_name" value="{{ $product_entries['name'] }}" required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Brand </label>
                                        <select style="width: 83%" id="brand_id" name="brand_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                                            @if($product_entries['brand_id'] != null)
                                            <option value="{{ $product_entries['brand_id'] }}" selected>{{ $product_entries['brand_name'] }}</option>
                                            @else
                                            <option value="">--Select Brand--</option>
                                            @endif
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal1">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Supplier </label>
                                        <select style="width: 83%" id="supplier_id" name="supplier_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10">
                                            @if($product_entries['supplier_id'] != null)
                                            <option value="{{ $product_entries['supplier_id'] }}" selected>{{ $product_entries['supplier_name'] }}</option>
                                            @else
                                            <option value="">--Select Supplier--</option>
                                            @endif
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal2">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Unit</label>
                                        <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                            <option value="">--Select Unit--</option>
                                            @if(!empty($units))
                                                @foreach($units as $key => $unit)
                                                <option {{ $product_entries['unit_id'] == $unit['id'] ? 'selected' : '' }} value="{{ $unit->id }}">{{ $unit->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Purchase Price *</label>
                                        <input type="text" name="buying_price" class="inner form-control" id="buying_price_0" value="{{ $product_entries['buy_price'] }}" required />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Selling Price Tax Type *</label>
                                        <select id="tax_type" style="width: 100%" class="form-control select2 unit" name="tax_type" onchange="sellingPriceTaxType()">
                                            <option {{ $product_entries['tax_type'] == 1 ? 'selected' : '' }} value="1">Inclusive</option>
                                            <option {{ $product_entries['tax_type'] == 2 ? 'selected' : '' }} value="2">Exclusive</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Sell Price *</label>
                                        <input type="text" name="selling_price" class="inner form-control" id="selling_price_0" value="{{ $product_entries['sell_price'] }}" oninput="CalculateTotalSellPrice()" required />
                                    </div>

                                    <div id="vatPercentage" style="display: none" class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Vat (%)</label>
                                        <input type="text" name="vat_percentage" class="inner form-control" id="vat_percentage_0" value="{{ $product_entries['vat_percentage'] }}" oninput="CalculateTotalSellPrice()" />
                                    </div>

                                    <div id="serviceCharge" style="display: none" class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Service Charge (%)</label>
                                        <input type="text" name="service_charge" class="inner form-control" id="service_charge_0" value="{{ $product_entries['service_charge'] }}" oninput="CalculateTotalSellPrice()" />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Sell Price Exclusive Vat *</label>
                                        <input type="text" name="selling_price_exclusive_tax" class="inner form-control" id="selling_price_exclusive_tax_0" value="{{ $product_entries['selling_price_exclusive_tax'] }}" required  readonly />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Stock Quantity</label>
                                        <input type="number" name="stock_quantity" class="inner form-control" id="stock_quantity" value="{{ $product_entries['stock_in_hand'] }}" />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Alert Quantity</label>
                                        <input type="text" name="alert_quantity" class="inner form-control" id="alert_quantity" value="{{ $product_entries['alert_quantity'] }}" />
                                    </div>

                                    <div class="col-lg-6 col-md-6 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label padding-left-0-xs">Status</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <select style="cursor: pointer" class="form-control" name="status">
                                                <option value="1" {{ $product_entries['status'] == 1 ? 'selected' : '' }}>Active</option>
                                                <option value="0" {{ $product_entries['status'] == 0 ? 'selected' : '' }}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Products</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <!-- <th>SL</th> -->
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Brand</th>
                                            <th>Supplier</th>
                                            <th style="text-align: center">Purchase Price</th>
                                            <th style="text-align: center">Sell Price</th>
                                            <th style="text-align: center">Stock</th>
                                            <th style="text-align: center">Sold</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($products) && ($products->count() > 0))
                                        @foreach($products as $key => $product)
                                            <tr>
                                                <!-- <td>{{ $key + 1 }}</td> -->
                                                <td>{{ str_pad($product['product_code'], 6, "0", STR_PAD_LEFT) }}</td>
                                                <td>
                                                    {{ $product['name'] }}
                                                </td>
                                                <td>{{ $product['category_name'] }}</td>
                                                <td>{{ $product['brand_name'] }}</td>
                                                <td>{{ $product['supplier_name'] }}</td>
                                                <td style="text-align: center">{{ number_format($product['buy_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">{{ number_format($product['sell_price'],2,'.',',') }}</td>
                                                <td style="text-align: center">
                                                    {{ number_format($product['stock_in_hand'],2,'.',',') }} 
                                                    @if($product['stock_in_hand'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                <td style="text-align: center">
                                                    {{ number_format($product['total_sold'],2,'.',',') }} 
                                                    @if($product['total_sold'] != null)
                                                       <?php echo $product['unit_name'] ?> 
                                                    @endif
                                                </td>
                                                <td>{{ $product['status'] == 1 ? 'Active' : 'Inactive' }}</td>
                                                <td>
                                                    @if(Auth::user()->role == 1 || Auth::user()->role == 2 || Auth::user()->role == 3)
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('products_edit', $product['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Category Name *</label>
                        <div class="col-md-12">
                            <input id="category_name" name="category_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Brand</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-bottom: 0px !important;padding-top: 0px !important" class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Brand Name *</label>
                        <div class="col-md-12">
                            <input id="brand_name" name="brand_name" type="text" class="form-control" required>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal2" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Supplier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn2" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton2" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#product_category_id").select2({
                ajax: { 
                    url:  site_url + '/products/product-category-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#brand_id").select2({
                ajax: { 
                    url:  site_url + '/products/brand-brand-list',
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        var type = 'Customer';
                    }

                    if (result['contact_type'] == 1)
                    {
                        var type = 'Supplier';
                    }

                    if (result['contact_type'] == 1)
                    {
                        return result['text'] + ' | ' + type;
                    }
                },
            });

            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
            }
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var product_name  = $("#category_name").val();
            var site_url      = $('.site_url').val();

            if (product_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
    
                type:   'get',
                url:    site_url + '/products/add-new-category/',
                data:   {  product_name : product_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                    }
                    
                    $("#product_category_id").empty();
                    $('#product_category_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#category_name').val('');
                }
    
            });
        });

        $('#submitBtn1').click(function() {
            
            var brand_name      = $("#brand_name").val();
            var site_url        = $('.site_url').val();

            if (brand_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }

            $.ajax({
                type:   'get',
                url:    site_url + '/products/add-new-brand/',
                data:   {  brand_name : brand_name, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    if(data != 0)
                    {
                        $('#CloseButton1').click();
                    }
                    
                    $("#brand_id").empty();
                    $('#brand_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    $('#brand_name').val('');
                }
            });
        });

        $('#submitBtn2').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 1;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton2').click();
                        }
                        
                        $("#supplier_id").empty();
                        $('#supplier_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        function sellingPriceTaxType()
        {
            var tax_type  = $("#tax_type").val();

            if (tax_type == 2)
            {
                $("#serviceCharge").hide();
                $("#vatPercentage").hide();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
            else
            {
                $("#serviceCharge").show();
                $("#vatPercentage").show();
                $("#service_charge_0").val('');
                $("#vat_percentage_0").val('');

                CalculateTotalSellPrice();
            }
        }

        function CalculateTotalSellPrice()
        {
            var sell_price_call                 = $("#selling_price_0").val();
            var vat_percentage_call             = $("#vat_percentage_0").val();
            var service_charge_call             = $("#service_charge_0").val();

            if (sell_price_call == '')
            {
                var sell_price                  = 0;
            }
            else
            {
                var sell_price                  = $("#selling_price_0").val();
            }

            if (vat_percentage_call == '')
            {
                var vat_percentage          = 0;
            }
            else
            {
                var vat_percentage          = $("#vat_percentage_0").val();
            }

            if (service_charge_call == '')
            {
                var service_charge          = 0;
            }
            else
            {
                var service_charge          = $("#service_charge_0").val();
            }

            var vat_amount                  = (parseFloat(sell_price)*parseFloat(vat_percentage))/100;
            var service_charge_amount       = (parseFloat(sell_price)*parseFloat(service_charge))/100;
            var selling_price_exclusive_tax = parseFloat(sell_price) - parseFloat(vat_amount) - parseFloat(service_charge_amount);

            $("#selling_price_exclusive_tax_0").val(selling_price_exclusive_tax);
        }
    </script>
@endsection