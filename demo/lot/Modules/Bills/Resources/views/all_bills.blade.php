@extends('layouts.app')

@section('title', 'All Purchases')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">All Purchases</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Purchases</a></li>
                                    <li class="breadcrumb-item active">All Purchases</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif
                                
                            <div class="card-body table-responsive">
                                
                                <div style="margin-right: 0px" class="row">
                                    <div class="col-md-9"></div>
                                    <div class="col-md-1">Search : </div>
                                    <div class="col-md-2">
                                        <input type="text" id="searchPayment" oninput="searchPayment()">
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th>Purchase#</th>
                                            <th>Supplier</th>
                                            <th>Amount</th>
                                            <th>Paid</th>
                                            <th>Due</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="bill_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Set Expire Date For Product Lots</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form id="FormSubmit" action="{{ route('bills_update_expire_date') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                {{ csrf_field() }}

                <div id="lot_list" class="modal-body">
                </div>

                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $.get(site_url + '/bills/bill/list/load', function(data){

                billList(data);
                
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPayment()
        {
            var search_text     = $('#searchPayment').val();
            var site_url        = $('.site_url').val();

            if (search_text == '')
            {
                var search_text = 'No_Text';
            }

            $.get(site_url + '/bills/bill/search/list/' + search_text, function(data){

                billList(data);

            });
        }

        function billList(data)
        {
            var bill_list = '';
            var site_url  = $('.site_url').val();
            $.each(data, function(i, bill_data)
            {   
                var serial              = parseFloat(i) + 1;
                var edit_url            = site_url + '/bills/edit/' + bill_data.id;
                var print_url           = site_url + '/bills/show/' + bill_data.id;
                var purchase_return     = site_url + '/purchasereturn?supplier_id=' + bill_data.vendor_id + '&bill_id=' + bill_data.id;
                var barcode_print       = site_url + '/products/barcode-print?bill_id=' + bill_data.id;

                if (bill_data.return_id != null)
                {
                    var return_sign  = '&nbsp;' + '&nbsp;' + '<i class="bx bx-repost font-size-15">' + '</i>';
                }
                else
                {
                    var return_sign  = '';
                }

                if (bill_data.contact_type == 0)
                {
                    var type = 'Customer';
                }

                if (bill_data.contact_type == 1)
                {
                    var type = 'Supplier';
                }

                if (bill_data.contact_type == 2)
                {
                    var type = 'Employee';
                }

                bill_list += '<tr>' +
                                    '<td>' +
                                        serial +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(bill_data.bill_date) +
                                    '</td>' +
                                    '<td>' +
                                       'BILL - ' + bill_data.bill_number.padStart(6, '0') + return_sign +
                                    '</td>' +
                                    '<td>' +
                                        bill_data.customer_name + ' | ' + type +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.bill_amount +
                                    '</td>' +
                                    '<td>' +
                                       (parseFloat(bill_data.bill_amount) - parseFloat(bill_data.due_amount)) +
                                    '</td>' +
                                    '<td>' +
                                       bill_data.due_amount +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                '@if(Auth::user()->role == 1)' +
                                                '<a style="cursor: pointer" class="dropdown-item" data-toggle="modal" data-target="#myModal" onclick="findLots('+ bill_data.id +')">' + 'Set Expire Date' + '</a>' +
                                                '<a class="dropdown-item" href="' + edit_url +'">' + 'Edit' + '</a>' +
                                                '@endif' +
                                                '<a class="dropdown-item" href="' + purchase_return +'" target="_blank">' + 'Purchase Return' + '</a>' +
                                                '<a class="dropdown-item" href="' + print_url +'" target="_blank">' + 'Show' + '</a>' +
                                                '<a class="dropdown-item" href="' + barcode_print +'" target="_blank">' + 'Print Bar Code' + '</a>' +
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';
            });

            $("#bill_list").empty();
            $("#bill_list").append(bill_list);
        }

        function findLots(bill_id)
        {
            var site_url    = $('.site_url').val();
            var billId      = bill_id;

            $.get(site_url + '/bills/get-lot-details/' + billId, function(data){

                var lot_list = '';
                $.each(data, function(i, lot_data)
                { 
                    if (i == 0)
                    {
                        var productName     = '<label for="productname">' + 'Product Name' + '</label>\n';
                        var productCode     = '<label for="productname">' + 'Code' + '</label>\n';
                        var lotNumber       = '<label for="productname">' + 'Lot Number' + '</label>\n';
                        var quantity        = '<label for="productname">' + 'Quantity' + '</label>\n';
                        var manufactureDate = '<label for="productname">' + 'Manufacture Date' + '</label>\n';
                        var expireDate      = '<label for="productname">' + 'Expire Date' + '</label>\n';
                    }
                    else
                    {
                        var productName     = '';
                        var productCode     = '';
                        var lotNumber       = '';
                        var quantity        = '';
                        var manufactureDate = '';
                        var expireDate      = '';
                    }

                    lot_list += '<div class="row">\n' +
                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        productName +
                                        '<input type="text" value="'+ lot_data.product_name +'" class="inner form-control" readonly />\n' +
                                    '</div>\n' +

                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        productCode +
                                        '<input type="text" value="'+ pad(lot_data.product_code, 6) +'" class="inner form-control" readonly />\n' +
                                    '</div>\n' + 

                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        lotNumber +
                                        '<input type="text" value="'+ lot_data.lot_number +'" name="lot_number[]" class="inner form-control lotNumber" readonly />\n' +
                                    '</div>\n' +

                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        quantity +
                                        '<input type="text" value="'+ lot_data.quantity +'" class="inner form-control" readonly />\n' +
                                    '</div>\n' +

                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        manufactureDate +
                                        '<input style="cursor: pointer" value="'+ lot_data.manufacture_date +'" name="manufacture_date[]" type="date" class="form-control manufacturingDate" >\n' +
                                    '</div>\n' +

                                    '<div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">\n' +
                                        expireDate +
                                        '<input style="cursor: pointer" value="'+ lot_data.expire_date +'" name="expire_date[]" type="date" class="form-control expireDate" >\n' +
                                    '</div>\n' +

                                    '<input value="'+ lot_data.product_entry_id +'" name="product_entry_id[]" type="hidden" class="productEntryId">' +
                                    '<input value="'+ lot_data.id +'" name="lot_id[]" type="hidden" class="lotId">' +

                                '</div>\n';
                });

                lot_list += '<div class="modal-footer">\n' +
                                '<button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">' + 'Save' + '</button>\n' +
                                '<button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">' + 'Close' + '</button>\n' +
                            '</div>\n';

                $("#lot_list").empty();
                $("#lot_list").append(lot_list);

            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>
@endsection