@extends('layouts.app')

@section('title', 'Update Settings')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Update Settings</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">Update Settings</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif
                                
                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('users_update_settings',$user['id']) }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Organization Name</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['organization_name'] }}" name="organization_name" id="organization_name" placeholder="Enter Your Organization Name">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Address</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['address'] }}" name="address" id="address" placeholder="Enter Your Organization Address">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Contact Number</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['contact_number'] }}" name="contact_number" id="contact_number" placeholder="Enter Contact Number">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Email</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['contact_email'] }}" name="contact_email" id="contact_email" placeholder="Enter Email">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Website</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['website'] }}" name="website" id="website" placeholder="Enter Your Organization Website">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Sales Deduction</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['sales_show'] }}" name="sales_show" id="sales_show">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Vat Reg. Number</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['vat_reg_number'] }}" name="vat_reg_number" id="vat_reg_number">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Mushak</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['mushak'] }}" name="mushak" id="mushak">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Default Vat Amount</label>
                                    <div style="padding-right: 0px" class="col-md-1">
                                        <select style="cursor: pointer" class="form-control" id="vat_type" name="vat_type">
                                            <option {{ $user['vat_type'] == 0 ? 'selected' : '' }} value="0">%</option>
                                            <option {{ $user['vat_type'] == 1 ? 'selected' : '' }} value="1">BDT</option>
                                        </select>
                                    </div>

                                    <div class="col-md-2">
                                        <input class="form-control" type="text" value="{{ $user['vat_amount'] }}" name="vat_amount" id="vat_amount">
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Default Vat Amount</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['vat_amount'] }}" name="vat_amount" id="vat_amount">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Default Vat Amount</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['vat_amount'] }}" name="vat_amount" id="vat_amount">
                                    </div>
                                </div> -->

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Pos Printer</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" id="pos_printer" name="pos_printer">
                                            <option {{ $user['pos_printer'] == 0 ? 'selected' : '' }} value="0">58 mm Label Size</option>
                                            <option {{ $user['pos_printer'] == 1 ? 'selected' : '' }} value="1">80 mm Label Size</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">sale Expire products</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" id="sale_expire_products" name="sale_expire_products">
                                            <option {{ $user['sale_expire_products'] == 0 ? 'selected' : '' }} value="0">No</option>
                                            <option {{ $user['sale_expire_products'] == 1 ? 'selected' : '' }} value="1">Yes</option>
                                        </select>
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Default Tax Amount</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="{{ $user['tax_amount'] }}" name="tax_amount" id="tax_amount">
                                    </div>
                                </div> -->

                                <!-- <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Theme Style</label>
                                    <div class="col-md-10">
                                        <select style="cursor: pointer" class="form-control" id="theme_type" name="theme_type">
                                            <option {{ $user['theme_type'] == 0 ? 'selected' : '' }} value="0">Side Bar View</option>
                                            <option {{ $user['theme_type'] == 1 ? 'selected' : '' }} value="1">Top Bar View</option>
                                        </select>
                                    </div>
                                </div> -->

                                <div class="form-group row orIn">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Old Header Image</label>
                                    <div class="col-md-10">
                                        @if($user['header_image'] != null)
                                            <img src="{{ url('public/images/'.$user['header_image']) }}" style="height: 100px;width: 200px" alt="No Image Found">
                                        @else
                                            <img src="{{ url('public/default-header-footer.jpg') }}" style="height: 100px;width: 200px" alt="No Image Found">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row orIn">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Header Image (1000px X 150px)</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="file" name="header_image" id="header_image">
                                    </div>
                                </div>

                                <div class="form-group row orIn">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Old Footer Image</label>
                                    <div class="col-md-10">
                                        @if($user['footer_image'] != null)
                                            <img src="{{ url('public/images/'.$user['footer_image']) }}" style="height: 100px;width: 200px" alt="No Image Found">
                                        @else
                                            <img src="{{ url('public/default-header-footer.jpg') }}" style="height: 100px;width: 200px" alt="No Image Found">
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row orIn">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Footer Image (1000px X 50px)</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="file" name="footer_image" id="footer_image">
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('users_edit_profile') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection