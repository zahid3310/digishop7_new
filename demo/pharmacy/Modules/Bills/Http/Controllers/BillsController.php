<?php

namespace Modules\Bills\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\PaidThroughAccounts;
use App\Models\ProductEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Expenses;
use App\Models\LotNumbers;
use App\Models\UnitConversions;
use App\Models\AccountTransactions;
use App\Models\Transactions;
use App\Models\CurrentBalance;
use App\Models\BillAdjustment;
use Response;
use DB;
use App\Models\Units;

class BillsController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::where('product_entries.product_id', '!=', 1)
                                            ->orderBy('product_code', 'DESC')
                                            ->get();

        $product_id         = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $units              = Units::orderBy('id', 'ASC')->get();

        return view('bills::index', compact('products', 'product_id', 'paid_accounts', 'units'));
    }

    public function allBills()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End
        
        return view('bills::all_bills');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $data_find                      = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                    = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                           = new Bills;
            $bill->bill_number              = $bill_number;
            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            // $bill->adjustment_type          = $data['adjustment_type'];
            // $bill->total_adjustment         = $adjustment;
            // $bill->adjustment_note          = $data['adjustment_note'];
            $bill->bill_note                = $data['bill_note'];
            // $bill->total_tax                = $tax;
            $bill->total_vat                = $vat;
            // $bill->tax_type                 = $data['tax_type'];
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->previous_due             = $data['previous_due'];
            $bill->previous_due_type        = $data['balance_type'];
            $bill->adjusted_amount          = $data['adjustment'];
            $bill->branch_id                = $branch_id;
            $bill->created_by               = $user_id;

            if ($bill->save())
            {   
                $find_lot_number            = LotNumbers::orderBy('id', 'DESC')->first();
                $lot_number                 = $find_lot_number != null ? $find_lot_number['lot_number'] + 1 : 1;

                foreach ($data['product_entries'] as $key1 => $value1)
                {
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'               => $bill['id'],
                        'product_id'            => $product_buy_price['product_id'],
                        'product_entry_id'      => $value1,
                        'main_unit_id'          => $data['main_unit_id'][$key1],
                        'conversion_unit_id'    => $data['unit_id'][$key1],
                        'vendor_id'             => $bill['vendor_id'],
                        'rate'                  => $data['rate'][$key1],
                        'quantity'              => $data['quantity'][$key1],
                        'total_amount'          => $data['amount'][$key1],
                        'discount_type'         => $data['discount_type'][$key1],
                        'discount_amount'       => $data['discount'][$key1],
                        'expire_date'           => date('Y-m-d', strtotime($data['expire_date'][$key1])),
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key1])
                                                                ->where('converted_unit_id', $data['unit_id'][$key1])
                                                                ->where('product_entry_id', $value1)
                                                                ->first();

                    $converted_quantity     = $conversion_rate_find != null ? $data['quantity'][$key1]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key1];

                    $lot_entries[] = [
                        'bill_id'           => $bill['id'],
                        'lot_number'        => $lot_number,
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value1,
                        'unit_id'           => $data['main_unit_id'][$key1],
                        'quantity'          => $converted_quantity,
                        'expire_date'       => date('Y-m-d', strtotime($data['expire_date'][$key1])),
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];

                    $lot_number++;
                }

                DB::table('bill_entries')->insert($bill_entries);
                DB::table('lot_numbers')->insert($lot_entries);

                stockIn($data, $item_id=null);

                //Condition 1
                if (isset($data['amount_paid']))
                {
                    $data_find_payment  = Payments::orderBy('id', 'DESC')->first();
                    $payment_number     = $data_find_payment != null ? $data_find_payment['payment_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {
                        if ($data['amount_paid'][$i] > 0)
                        {   
                            $account_transactions[] = [
                                'customer_id'           => $data['vendor_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i] . '(ক্রয় বাবদ ব্যয়)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase',
                                'associated_id'         => $bill->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['vendor_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'bill_id'           => $bill['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'initial_payment'   => 1,
                                        'branch_id'         => $branch_id,
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_bill_dues                = Bills::find($bill['id']);
                            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_bill_dues->save();

                            $payment_number++;
                        }
                    }

                    if (isset($account_transactions))
                    {
                        DB::table('account_transactions')->insert($account_transactions);
                    }
                }

                if ($data['balance_type'] == 1)
                {
                    $op_bal  = $data['previous_due'] + $data['total_amount'];
                }
                else
                {
                    $op_bal  = $data['total_amount'];
                }

                $paidAmount  = $data['cash_given'] + $data['adjustment'];

                //Condition 2
                if (($paidAmount > $data['total_amount']) && ($paidAmount <= $op_bal))
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_bills_con_1   = Bills::where('vendor_id', $data['vendor_id'])
                                                        ->where('due_amount', '>', 0)
                                                        ->orderBy('created_at', 'ASC')
                                                        ->get();

                    $excess_amount_con_1      = $paidAmount - $data['total_amount'];

                    foreach ($old_due_bills_con_1 as $key_con_1 => $value_con_1)
                    {
                        if ($excess_amount_con_1 > 0)
                        {
                            if ($value_con_1->due_amount <= $excess_amount_con_1)
                            {
                                $data_find_con_1_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_1   = $data_find_con_1_1 != null ? $data_find_con_1_1['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['vendor_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(পূর্বের ক্রয় থেকে সমন্বয়)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_1 = [
                                    'payment_number'        => $payment_number_con_1_1,
                                    'customer_id'           => $data['vendor_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_1->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 1,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_1 = DB::table('payments')->insertGetId($payments_1);      

                                if ($payment_id_con_1_1)
                                {
                                    $payment_entries_con_1_1 = [
                                            'payment_id'        => $payment_id_con_1_1,
                                            'bill_id'           => $value_con_1->id,
                                            'amount'            => $value_con_1->due_amount,
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_1);  
                                }

                                $update_bills_dues_con_1_1                = Bills::find($value_con_1['id']);
                                $update_bills_dues_con_1_1->due_amount    = $update_bills_dues_con_1_1['due_amount'] - $value_con_1->due_amount;
                                $update_bills_dues_con_1_1->save();

                                $excess_amount_addable = $value_con_1->due_amount;

                                $bill_adjustment = [
                                    'current_bill_id'   => $bill->id,
                                    'bill_id'           => $value_con_1->id,
                                    'customer_id'       => $bill->vendor_id,
                                    'payment_id'        => $payment_id_con_1_1,
                                    'amount'            => $value_con_1->due_amount,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('bill_adjustments')->insert($bill_adjustment);
                            }

                            if ($value_con_1->due_amount > $excess_amount_con_1)
                            {
                                $data_find_con_1_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_1_2   = $data_find_con_1_2 != null ? $data_find_con_1_2['payment_number'] + 1 : 1;

                                $account_transactions_con_1[] = [
                                    'customer_id'           => $data['vendor_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(পূর্বের ক্রয় থেকে সমন্বয়)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value_con_1->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_1_2 = [
                                    'payment_number'        => $payment_number_con_1_2,
                                    'customer_id'           => $data['vendor_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_1,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 1,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_1_2 = DB::table('payments')->insertGetId($payments_con_1_2);      

                                if ($payment_id_con_1_2)
                                {
                                    $payment_entries_con_1_2 = [
                                            'payment_id'        => $payment_id_con_1_2,
                                            'bill_id'           => $value_con_1->id,
                                            'amount'            => $excess_amount_con_1,
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_1_2);  
                                }

                                $update_bills_dues_con_1_2                = Bills::find($value_con_1['id']);
                                $update_bills_dues_con_1_2->due_amount    = $update_bills_dues_con_1_2['due_amount'] - $excess_amount_con_1;
                                $update_bills_dues_con_1_2->save();

                                $excess_amount_addable = $excess_amount_con_1;

                                $bill_adjustment = [
                                    'current_bill_id'   => $bill->id,
                                    'bill_id'           => $value_con_1->id,
                                    'customer_id'       => $bill->vendor_id,
                                    'payment_id'        => $payment_id_con_1_2,
                                    'amount'            => $excess_amount_con_1,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('bill_adjustments')->insert($bill_adjustment);
                            }

                            $excess_amount_con_1 = $excess_amount_con_1 - $excess_amount_addable;
                        }
                    }

                    if (isset($account_transactions_con_1))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_1);
                    }
                }

                //Condition 3
                if ($paidAmount > $op_bal)
                {
                    //This Code is for invoice wise Payment Start
                    $old_due_bills_con_2        = Bills::where('vendor_id', $data['vendor_id'])
                                                            ->where('due_amount', '>', 0)
                                                            ->orderBy('created_at', 'ASC')
                                                            ->get();

                    $excess_amount_con_2        = $data['previous_due'];
                    foreach ($old_due_bills_con_2 as $key_con_2 => $value_con_2)
                    {
                        if ($excess_amount_con_2 > 0)
                        {
                            if ($value_con_2->due_amount <= $excess_amount_con_2)
                            {
                                $data_find_con_2_1        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_1   = $data_find_con_2_1 != null ? $data_find_con_2_1['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['vendor_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(পূর্বের ক্রয় থেকে সমন্বয়)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_1 = [
                                    'payment_number'        => $payment_number_con_2_1,
                                    'customer_id'           => $data['vendor_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $value_con_2->due_amount,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 1,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_1 = DB::table('payments')->insertGetId($payments_con_2_1);      

                                if ($payment_id_con_2_1)
                                {
                                    $payment_entries_con_2_1 = [
                                            'payment_id'        => $payment_id_con_2_1,
                                            'bill_id'           => $value_con_2['id'],
                                            'amount'            => $value_con_2->due_amount,
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_1);  
                                }

                                $update_bills_dues_con_2_1                = Bills::find($value_con_2['id']);
                                $update_bills_dues_con_2_1->due_amount    = $update_bills_dues_con_2_1['due_amount'] - $value_con_2->due_amount;
                                $update_bills_dues_con_2_1->save();

                                $excess_cal = $value_con_2->due_amount;

                                $bill_adjustment = [
                                    'current_bill_id'   => $bill->id,
                                    'bill_id'           => $value_con_2->id,
                                    'customer_id'       => $bill->vendor_id,
                                    'payment_id'        => $payment_id_con_2_1,
                                    'amount'            => $value_con_2->due_amount,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('bill_adjustments')->insert($bill_adjustment);
                            }

                            if ($value_con_2->due_amount > $excess_amount_con_2)
                            {
                                $data_find_con_2_2        = Payments::orderBy('id', 'DESC')->first();
                                $payment_number_con_2_2   = $data_find_con_2_2 != null ? $data_find_con_2_2['payment_number'] + 1 : 1;

                                $account_transactions_con_2[] = [
                                    'customer_id'           => $data['vendor_id'],
                                    'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'paid_through_id'       => $data['paid_through'][0],
                                    'account_information'   => $data['account_information'][0],
                                    'note'                  => $data['note'][0] . '(পূর্বের ক্রয় থেকে সমন্বয়)',
                                    'type'                  => 1,  // 0 = In , 1 = Out
                                    'transaction_head'      => 'purchase',
                                    'associated_id'         => $value_con_2->id,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payments_con_2_2 = [
                                    'payment_number'        => $payment_number_con_2_2,
                                    'customer_id'           => $data['vendor_id'],
                                    'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                    'amount'                => $excess_amount_con_2,
                                    'account_information'   => $data['account_information'][0],
                                    'paid_through'          => $data['paid_through'][0],
                                    'note'                  => $data['note'][0],
                                    'type'                  => 1,
                                    'branch_id'             => $branch_id,
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];

                                $payment_id_con_2_2 = DB::table('payments')->insertGetId($payments_con_2_2);      

                                if ($payment_id_con_2_2)
                                {
                                    $payment_entries_con_2_2 = [
                                            'payment_id'        => $payment_id_con_2_2,
                                            'bill_id'           => $value_con_2['id'],
                                            'amount'            => $excess_amount_con_2,
                                            'branch_id'         => $branch_id,
                                            'created_by'        => $user_id,
                                            'created_at'        => date('Y-m-d H:i:s'),
                                    ];

                                    DB::table('payment_entries')->insert($payment_entries_con_2_2);  
                                }

                                $update_bills_dues_con_2_2                = Bills::find($value_con_2['id']);
                                $update_bills_dues_con_2_2->due_amount    = $update_bills_dues_con_2_2['due_amount'] - $excess_amount_con_2;
                                $update_bills_dues_con_2_2->save();

                                $excess_cal     = $excess_amount_con_2;

                                $bill_adjustment = [
                                    'current_bill_id'   => $bill->id,
                                    'bill_id'           => $value_con_2->id,
                                    'customer_id'       => $bill->vendor_id,
                                    'payment_id'        => $payment_id_con_2_2,
                                    'amount'            => $excess_amount_con_2,
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('bill_adjustments')->insert($bill_adjustment);
                            }

                            $excess_amount_con_2 = $excess_amount_con_2 - $excess_cal;
                        }
                    }

                    if (isset($account_transactions_con_2))
                    {
                        DB::table('account_transactions')->insert($account_transactions_con_2);
                    }
                    //This Code is for invoice wise Payment End
                }

                //For Advance Paymenr Adjustment
                if ($data['adjustment'] > 0)
                {
                    $extra_paid         = $paidAmount - $data['adjustment'];
                    $adjusted_amount    = $data['adjustment'];

                    $account_transactions_customer_advance_adjust = [
                        'customer_id'           => $data['vendor_id'],
                        'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                        'amount'                => abs($adjusted_amount),
                        'paid_through_id'       => 1,
                        'note'                  => '(অগ্রীম থেকে সমন্বয়)',
                        'type'                  => 0,  // 0 = In , 1 = Out
                        'transaction_head'      => 'supplier-advance-adjustment',
                        'associated_id'         => $bill['id'],
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    DB::table('account_transactions')->insert($account_transactions_customer_advance_adjust);

                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['vendor_id']);
                    $find_customer->supplier_advance_payment    = $find_customer['supplier_advance_payment'] - $data['adjustment'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }
                else
                {
                    if ($data['change_amount'] > 0)
                    {
                        $advance_amount_advance_amount_con_2  = $data['change_amount'];

                        $account_transactions_advance_amount_con_2 = [
                            'customer_id'           => $data['vendor_id'],
                            'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                            'amount'                => $advance_amount_advance_amount_con_2,
                            'associated_id'         => $bill->id,
                            'paid_through_id'       => 1,
                            'note'                  => '(অগ্রীম বাবদ প্রদান)',
                            'type'                  => 1,  // 0 = In , 1 = Out
                            'transaction_head'      => 'supplier-advance',
                            'associated_id'         => $bill->id,
                            'branch_id'             => $branch_id,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        DB::table('account_transactions')->insert($account_transactions_advance_amount_con_2);
                    }
                }

                if ($data['change_amount'] > 0)
                {
                    //Update advance payment in customer table start
                    $find_customer                              = Customers::find($data['vendor_id']);
                    $find_customer->supplier_advance_payment    = $find_customer['supplier_advance_payment'] + $data['change_amount'];
                    $find_customer->save();
                    //Update advance payment in customer table end
                }

                $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                $transaction_data['type']           = 1;
                $transaction_data['account_head']   = 'purchase';
                $transaction_data['transaction_id'] = $bill->id;
                $transaction_data['customer_id']    = $data['vendor_id'];
                $transaction_data['note']           = 'ক্রয় বাবদ প্রদেয়';
                $transaction_data['amount']         = $data['total_amount'];
                $transaction_data['paid_through']   = null;
                transactions($transaction_data);

                if ($paidAmount > 0)
                {
                    if ($data['adjustment'] > 0)
                    {
                        $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                        $transaction_data['type']           = 0;
                        $transaction_data['account_head']   = 'supplier-advance-adjustment';
                        $transaction_data['transaction_id'] = $bill->id;
                        $transaction_data['customer_id']    = $data['vendor_id'];
                        $transaction_data['note']           = 'পূর্বের অগ্রিম থেকে সমন্বয়';
                        $transaction_data['amount']         = $data['adjustment'];
                        $transaction_data['paid_through']   = null;
                        transactions($transaction_data);
                    }

                    $transaction_data['date']           = date('Y-m-d', strtotime($data['selling_date']));
                    $transaction_data['type']           = 0;
                    $transaction_data['account_head']   = 'purchase';
                    $transaction_data['transaction_id'] = $bill->id;
                    $transaction_data['customer_id']    = $data['vendor_id'];
                    $transaction_data['note']           = 'ক্রয় বাবদ ব্যায়';
                    $transaction_data['amount']         = $paidAmount;
                    $transaction_data['paid_through']   = null;
                    transactions($transaction_data);
                }

                //Current balance 
                if (isset($data['current_balance_amount_paid']))
                {
                    for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                    {
                        if ($data['current_balance_amount_paid'][$i] > 0)
                        {
                            $current_balance[] = [
                                'customer_id'           => $data['vendor_id'],
                                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['current_balance_amount_paid'][$i],
                                'paid_through_id'       => $data['current_balance_paid_through'][$i],
                                'account_information'   => $data['current_balance_account_information'][$i],
                                'note'                  => $data['current_balance_note'][$i] . '(বিক্রয় বাবদ আদায়)',
                                'type'                  => 1,  // 0 = In , 1 = Out
                                'transaction_head'      => 'purchase',
                                'associated_id'         => $bill->id,
                                'branch_id'             => $branch_id,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];
                        }
                    }

                    if (isset($current_balance))
                    {
                        DB::table('current_balance')->insert($current_balance);
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Created Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
                
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $tables     = TableNameByUsers();
        $table_id   = Auth::user()->associative_contact_id;

        $bill       = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->select('bills.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = BillEntries::leftjoin('products', 'products.id', 'bill_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                ->where('bill_entries.bill_id', $id)
                                ->select('bill_entries.*',
                                         'product_entries.name as product_entry_name',
                                         'products.name as product_name')
                                ->get();  
                     
        $user_info  = userDetails();

        return view('bills::show', compact('entries', 'bill', 'user_info'));
    }

    public function edit($id)
    {
        return back();

        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $tables                 = TableNameByUsers();
        $table_id               = Auth::user()->associative_contact_id;

        $bills                  = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'customers.name as vendor_name')
                                        ->get();

        $products               = ProductEntries::orderBy('created_at', 'DESC')->get();
        $find_bill              = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name')
                                        ->find($id);

        $find_bill_entries      = BillEntries::leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                        ->where('bill_entries.bill_id', $id)
                                        ->select('bill_entries.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name',
                                                 'product_entries.id as item_id',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.stock_in_hand as stock_in_hand',
                                                 'product_entries.sell_price as sell_price',
                                                 'product_entries.name as item_name')
                                        ->get();

        $entries_count          = $find_bill_entries->count();
        $products               = ProductEntries::orderBy('product_code', 'DESC')->get();
        $product_id             = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $units                  = Units::orderBy('id', 'ASC')->get();

        $payment_entries        = PaymentEntries::leftjoin('payments', 'payments.id', 'payment_entries.payment_id')
                                        ->where('payment_entries.bill_id', $id)
                                        ->selectRaw('payment_entries.*, 
                                                     payments.paid_through as paid_through_id,
                                                     payments.account_information as account_information,
                                                     payments.payment_date as payment_date,
                                                     payments.note as note')
                                        ->get();

        $payment_entries_count  = $payment_entries->count();
        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('bills::edit', compact('bills', 'products', 'find_bill', 'find_bill_entries', 'entries_count', 'product_id', 'units', 'payment_entries', 'payment_entries_count', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            // $adjustment     = $data['adjustment_amount'];
            $vat            = $data['vat_amount'];
            // $tax            = $data['tax_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $bill           = Bills::find($id);

            //Calculate Due Amount

                if ($data['total_amount'] > $bill['bill_amount']) 
                {
                    $bill_dues = $bill['due_amount'] + ($data['total_amount'] - $bill['bill_amount']);

                }
                
                if ($data['total_amount'] < $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'] - ($bill['bill_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'];
                }

            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $bill_dues;
            $bill->total_discount           = $discount;
            // $bill->adjustment_type      = $data['adjustment_type'];
            // $bill->total_adjustment     = $adjustment;
            // $bill->adjustment_note      = $data['adjustment_note'];
            $bill->bill_note                = $data['bill_note'];
            // $bill->total_tax                = $tax;
            $bill->total_vat                = $vat;
            // $bill->tax_type                 = $data['tax_type'];
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->updated_by               = $user_id;

            if ($bill->save())
            {
                $item_id         = BillEntries::where('bill_id', $bill['id'])->get();
                $item_delete     = BillEntries::where('bill_id', $bill['id'])->delete();
                
                $find_lot_number = LotNumbers::orderBy('id', 'DESC')->first();
                $lot_number      = $find_lot_number != null ? $find_lot_number['lot_number'] + 1 : 1;

                if (isset($data['payment_id']))
                {
                    foreach ($data['payment_id'] as $key_p => $value_p)
                    {
                        $payment_delete  = Payments::where('id', $value_p)->delete();
                    }

                    $update_bill                = Bills::find($bill->id);
                    $update_bill->due_amount    = $update_bill['bill_amount'];
                    $update_bill->updated_by    = $user_id;
                    $update_bill->save();
                }

                foreach ($data['product_entries'] as $key1 => $value1)
                {   
                    $product_buy_price  = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'           => $bill['id'],
                        'product_id'        => $product_buy_price['product_id'],
                        'product_entry_id'  => $value1,
                        'vendor_id'         => $bill['vendor_id'],
                        'rate'              => $data['rate'][$key1],
                        'quantity'          => $data['quantity'][$key1],
                        'total_amount'      => $data['amount'][$key1],
                        'discount_type'     => $data['discount_type'][$key1],
                        'discount_amount'   => $data['discount'][$key1],
                        'expire_date'       => date('Y-m-d', strtotime($data['expire_date'][$key1])),
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];

                    //If product Id exist then update otherwise insert new row with new product Id start
                    $find_lot           = LotNumbers::where('product_entry_id', $value1)
                                                    ->where('bill_id', $bill['id'])
                                                    ->first();

                    if($find_lot != null)
                    {
                        if ($data['quantity'][$key1] > $find_lot['stock_in_hand'])
                        {
                            $qty                    = $data['quantity'][$key1] - $find_lot['quantity'];
                            $find_lot->quantity     = $find_lot['quantity'] + $qty;
                        }

                        if ($data['quantity'][$key1] < $find_lot['stock_in_hand'])
                        {
                            $qty                    = $find_lot['quantity'] - $data['quantity'][$key1];
                            $find_lot->quantity     = $find_lot['quantity'] - $qty;
                        }
                        $find_lot->expire_date      = date('Y-m-d', strtotime($data['expire_date'][$key1]));
                        $find_lot->save();
                    }
                    else
                    {
                        $lot_entries[] = [
                            'bill_id'           => $bill['id'],
                            'lot_number'        => $lot_number,
                            'product_id'        => $product_buy_price['product_id'],
                            'product_entry_id'  => $value1,
                            'quantity'          => $data['quantity'][$key1],
                            'expire_date'       => date('Y-m-d', strtotime($data['expire_date'][$key1])),
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];

                        $lot_number++;
                    }
                    //If product Id exist then update otherwise insert new row with new product Id start
                }

                DB::table('bill_entries')->insert($bill_entries);

                if (isset($lot_entries))
                {
                    DB::table('lot_numbers')->insert($lot_entries);
                }

                stockIn($data, $item_id);

                if (isset($data['amount_paid']))
                {   
                    $data_find_payment  = Payments::orderBy('id', 'DESC')->first();
                    $payment_number     = $data_find_payment != null ? $data_find_payment['payment_number'] + 1 : 1;

                    $data_find_expense  = Expenses::orderBy('id', 'DESC')->first();
                    $expense_number     = $data_find_expense != null ? $data_find_expense['expense_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $payments = [
                                'payment_number'        => $payment_number,
                                'customer_id'           => $data['vendor_id'],
                                'payment_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through'          => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'type'                  => 1,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            $payment_id = DB::table('payments')->insertGetId($payments);      

                            if ($payment_id)
                            {   
                                $payment_entries = [
                                        'payment_id'        => $payment_id,
                                        'bill_id'           => $bill['id'],
                                        'amount'            => $data['amount_paid'][$i],
                                        'created_by'        => $user_id,
                                        'created_at'        => date('Y-m-d H:i:s'),
                                ];

                                DB::table('payment_entries')->insert($payment_entries);  
                            }

                            $update_bill_dues                = Bills::find($bill['id']);
                            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'][$i];
                            $update_bill_dues->save();

                            //Expense against purchase
                            $expenses = [
                                'expense_number'        => $expense_number,
                                'expense_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'expense_category_id'   => 2,
                                'note'                  => $data['note'][$i],
                                'amount'                => $data['amount_paid'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'created_by'            => $user_id,
                            ];

                            DB::table('expenses')->insert($expenses);  

                            $payment_number++;
                            $expense_number++;
                        }
                    }
                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function ProductEntriesList($id)
    {
        $data       = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->find($id);

        return Response::json($data);
    }

    public function ProductEntriesListInvoice()
    {
        $data       = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.created_at', 'DESC')
                                    ->get();

        return Response::json($data);
    }

    public function billListLoad()
    {
        $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bill_entries.product_id', '!=', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function billListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bill_entries.product_id', '!=', 1)
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('bills.bill_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('bills.bill_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bill_entries.product_id', '!=', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }

    public function storeProduct(Request $request)
    {
        $user_id                        = Auth::user()->id;
        $data                           = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')->first();
            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->tax_type                      = $data['tax_type'];
            $product->selling_price_exclusive_tax   = $data['selling_price_exclusive_tax'];
            $product->vat_percentage                = $data['vat_percentage'];
            $product->service_charge                = $data['service_charge'];
            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return Response::json($product);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }

    public function billProductList()
    {
        $data       = Products::where('products.id', '!=', 1)
                                    ->orderBy('products.name', 'ASC')
                                    ->select('products.*')
                                    ->get();

        return Response::json($data);
    }

    public function posSearchProductBill($id)
    {
        $data       = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.product_code', $id)
                                    ->first();

        return Response::json($data);
    }

    public function ProductEntriesListBill($id)
    {
        $data       = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.product_id', $id)
                                    ->orderBy('product_entries.created_at', 'DESC')
                                    ->get();

        return Response::json($data);
    }

    public function getLotDetails($bill_id)
    {
        $data       = LotNumbers::leftjoin('product_entries', 'product_entries.id', 'lot_numbers.product_entry_id')
                                    ->where('lot_numbers.bill_id', $bill_id)
                                    ->where('lot_numbers.quantity', '>', 0)
                                    ->orderBy('lot_numbers.id', 'ASC')
                                    ->selectRaw('lot_numbers.*, product_entries.name as product_name, product_entries.product_code as product_code')
                                    ->get();

        return Response::json($data);
    }

    public function updateExpireDate(Request $request)
    {
        $user_id = Auth::user()->id;
        $data    = $request->all();

        DB::beginTransaction();

        try{
            
            foreach ($data['lot_id'] as $key => $value)
            {
                $find_lot                       = LotNumbers::find($value);
                $find_lot->manufacture_date     = $data['manufacture_date'][$key] != null ? date('Y-m-d', strtotime($data['manufacture_date'][$key])) : null;
                $find_lot->expire_date          = $data['expire_date'][$key] != null ? date('Y-m-d', strtotime($data['expire_date'][$key])) : null;
                $find_lot->save();

                $find_bill                  = BillEntries::where('bill_id', $find_lot['bill_id'])->where('product_entry_id', $find_lot['product_entry_id'])->first();
                $find_bill->expire_date     = $data['expire_date'][$key] != null ? date('Y-m-d', strtotime($data['expire_date'][$key])) : null;
                $find_bill->save();
            }
            
            DB::commit();
            return back()->with("success","Date Updated Successfully !!");
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
        }
    }

    public function productListLoadBill()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->orderBy('product_entries.created_at', 'DESC')
                                    ->take(10)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntries::where('product_entries.product_id', '!=', 1)
                                    ->where('product_entries.name', 'LIKE', "$search%")
                                    ->orWhere('product_entries.generic_name', 'LIKE', "$search%")
                                    ->orderBy('product_entries.name', 'ASC')
                                    ->get();
        }


        foreach ($fetchData as $key => $value)
        {   
            $name   = $value['name'] . ' ' . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )'.$value['generic_name'];

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }
    
    public function getConversionParam($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.converted_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*')
                                    ->first();

        return Response::json($data);
    }
}
