@extends('layouts.app')

@section('title', 'Update Settings')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Update Settings</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Settings</a></li>
                                    <li class="breadcrumb-item active">Update Settings</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif
                                
                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('users_update_settings',$user['id']) }}" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Organization Name</label>
                                        <input class="form-control" type="text" value="{{ $user['organization_name'] }}" name="organization_name" id="organization_name" placeholder="Enter Your Organization Name">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Address</label>
                                        <input class="form-control" type="text" value="{{ $user['address'] }}" name="address" id="address" placeholder="Enter Your Organization Address">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Contact Number</label>
                                        <input class="form-control" type="text" value="{{ $user['contact_number'] }}" name="contact_number" id="contact_number" placeholder="Enter Contact Number">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Email</label>
                                        <input class="form-control" type="text" value="{{ $user['contact_email'] }}" name="contact_email" id="contact_email" placeholder="Enter Email">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Website</label>
                                        <input class="form-control" type="text" value="{{ $user['website'] }}" name="website" id="website" placeholder="Enter Your Organization Website">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Sales Deduction</label>
                                        <input class="form-control" type="text" value="{{ $user['sales_show'] }}" name="sales_show" id="sales_show">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Vat Reg. Number</label>
                                        <input class="form-control" type="text" value="{{ $user['vat_reg_number'] }}" name="vat_reg_number" id="vat_reg_number">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Vat Mushak</label>
                                        <input class="form-control" type="text" value="{{ $user['mushak'] }}" name="mushak" id="mushak">
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Default Vat Amount</label>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <select style="cursor: pointer" class="form-control" id="vat_type" name="vat_type">
                                                    <option {{ $user['vat_type'] == 0 ? 'selected' : '' }} value="0">%</option>
                                                    <option {{ $user['vat_type'] == 1 ? 'selected' : '' }} value="1">BDT</option>
                                                </select>
                                            </div>

                                            <div class="col-md-6">
                                                <input class="form-control" type="text" value="{{ $user['vat_amount'] }}" name="vat_amount" id="vat_amount">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Printer Type</label>
                                        <select style="cursor: pointer" class="form-control" id="pos_printer" name="pos_printer">
                                            <option {{ $user['pos_printer'] == 0 ? 'selected' : '' }} value="0">58 mm Thermal Receipt</option>
                                            <option {{ $user['pos_printer'] == 1 ? 'selected' : '' }} value="1">80 mm Thermal Receipt</option>
                                            <option {{ $user['pos_printer'] == 2 ? 'selected' : '' }} value="2">A4 Size Paper</option>
                                            <option {{ $user['pos_printer'] == 3 ? 'selected' : '' }} value="3">Letter Size Paper</option>
                                        </select>
                                    </div> 

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Logo</label>
                                        <input class="form-control" type="file" name="logo" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Logo</label>
                                    <div class="col-md-10">
                                        @if($user['logo'] != null)
                                        <img style="height: 60px;width: 200px" src="{{ url('public/company-profile-images/'.$user['logo']) }}">
                                        @endif
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('users_edit_profile') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection