

<?php $__env->startSection('title', 'Print Check'); ?>

<style>
    input[type="text" i] {
        padding: 1px 2px;
        border: none; 
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Accounts</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Print Checks</a></li>
                                    <li class="breadcrumb-item active">Print</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 0px" class="row">
                    <div style="padding-bottom: 10px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <div style="margin-top: 20px" class="row checkSize">
                                    <div style="font-size: 15px;margin-bottom: 10px" class="col-md-7">
                                        <address style="margin-bottom: 0px !important">
                                            <strong></strong>
                                        </address>
                                    </div>
                                    <div style="font-size: 17px;margin-top: 1.3cm;margin-right: -5cm" class="col-md-5 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['d1']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['d2']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['m1']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['m2']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['y1']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['y2']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['y3']); ?>">
                                            <input type="text" style="width: .70cm;height: .6cm;padding-left: 10px" value="<?php echo e($check['y4']); ?>">
                                        </address>
                                    </div>

                                    <div style="font-size: 20px;margin-top: 1.4cm" class="col-md-10">
                                        <address style="margin-bottom: 0px !important;padding-left: 5cm">
                                            <?php echo e($check['pay_to']); ?>

                                        </address>
                                    </div>
                                    <div style="font-size: 20px;margin-top: 1cm" class="col-md-2 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong></strong>
                                        </address>
                                    </div>

                                    <div style="font-size: 20px;margin-top: .40cm;" class="col-md-8">
                                        <address style="margin-bottom: 0px !important;padding-left: 6.5cm">
                                            <?php echo e(numberTowords($check['amount'])); ?>

                                        </address>
                                    </div>
                                    <div style="font-size: 25px;margin-top: .90cm;" class="col-md-4 text-sm-left">
                                        <address style="margin-bottom: 0px !important;padding-left: 3.5cm">
                                            <span style="height: 1.2cm;width: 5.3cm"> <?php echo e($check['amount']); ?></span>
                                        </address>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/demo/rice_mill/Modules/BalanceTransfer/Resources/views/checks/show.blade.php ENDPATH**/ ?>