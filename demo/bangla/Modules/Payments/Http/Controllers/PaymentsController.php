<?php

namespace Modules\Payments\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\Bills;
use App\Models\InvoiceEntries;
use App\Models\Payments;
use App\Models\JournalEntries;
use App\Models\Accounts;
use Response;
use DB;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('payments::index');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $branch_id      = Auth::user()->branch_id;
        $customers      = Customers::where('contact_type', 0)
                                    ->orWhere('contact_type', 1)
                                    ->orWhere('contact_type', 2)
                                    ->when($branch_id != 1, function ($query) use ($branch_id) {
                                        return $query->where('customers.branch_id', $branch_id);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->get();

        $customer_id    = isset($_GET['customer_id']) ? $_GET['customer_id'] : 0;
        $find_customer  = Customers::where('id', $customer_id)->first();
        $paid_accounts  = Accounts::where('account_type_id', 3)->where('status', 1)->get();

        return view('payments::create', compact('customers', 'customer_id', 'find_customer', 'paid_accounts'));
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'customer_id'   => 'required',
            'type'          => 'required',
            'payment_date'  => 'required',
            'amount'        => 'required',
            'paid.*'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        if ($data['amount'] == 0 || $data['amount'] == null || $data['payment_date'] == null)
        {
            return redirect()->back()->withInput();
        }

        DB::beginTransaction();

        try{
            $data_find                  = Payments::orderBy('id', 'DESC')->first();
            $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
            
            $payment                    = new Payments;
            $payment->payment_number    = $payment_number;
            $payment->customer_id       = $data['customer_id'];
            $payment->payment_date      = date('Y-m-d', strtotime($data['payment_date']));
            $payment->amount            = $data['amount'];
            $payment->paid_through      = $data['paid_through'];
            $payment->note              = $data['note'];
            $payment->type              = $data['type'];
            $payment->branch_id         = $branch_id;
            $payment->created_by        = $user_id;

            $find_customer              = $data['customer_id'];
            $find_type                  = $data['type'];

            if ($payment->save())
            {   
                if ($data['type'] == 0)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=95, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    customerBalanceUpdate($data['customer_id']);
                }
                
                if ($data['type'] == 1)
                {
                    //Financial Accounting Start
                        $journal_entry_debit                      = new JournalEntries;
                        $journal_entry_debit->date                = date('Y-m-d', strtotime($data['payment_date']));
                        $journal_entry_debit->account_id          = $data['paid_through'];
                        $journal_entry_debit->debit_credit        = 1; //Debit
                        $journal_entry_debit->customer_id         = $data['customer_id'];
                        $journal_entry_debit->amount              = $data['amount'];
                        $journal_entry_debit->transaction_head    = 'payment-made';
                        $journal_entry_debit->payment_id          = $payment->id;
                        $journal_entry_debit->note                = $data['note'];
                        $journal_entry_debit->branch_id           = $branch_id;
                        $journal_entry_debit->created_by          = $user_id;
                        $journal_entry_debit->save();

                        $journal_entry_debit                      = new JournalEntries;
                        $journal_entry_debit->date                = date('Y-m-d', strtotime($data['payment_date']));
                        $journal_entry_debit->account_id          = 97;
                        $journal_entry_debit->debit_credit        = 0; //Credit
                        $journal_entry_debit->customer_id         = $data['customer_id'];
                        $journal_entry_debit->amount              = $data['amount'];
                        $journal_entry_debit->transaction_head    = 'payment-made';
                        $journal_entry_debit->payment_id          = $payment->id;
                        $journal_entry_debit->note                = $data['note'];
                        $journal_entry_debit->branch_id           = $branch_id;
                        $journal_entry_debit->created_by          = $user_id;
                        $journal_entry_debit->save();
                    //Financial Accounting End

                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=97, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    supplierBalanceUpdate($data['customer_id']);
                }

                if ($data['type'] == 2)
                {
                    //Financial Accounting Start
                        debit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=$data['paid_through'], $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                        credit($customer_id=$data['customer_id'], $date=$data['payment_date'], $account_id=97, $amount=$data['amount'], $note=$data['note'], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=$payment->id, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    //Financial Accounting End

                    employeeBalanceUpdate($data['customer_id']);
                }

                if (isset($payment_entries))
                {
                    DB::table('payment_entries')->insert($payment_entries);
                }

                if (isset($account_transactions))
                {
                    DB::table('account_transactions')->insert($account_transactions);
                }

                if (isset($current_balance))
                {
                    DB::table('current_balance')->insert($current_balance);
                }

                DB::commit();
                return back()->with("success","Payment Created Successfully !!")->with("find_customer", $find_customer)->with('find_type', $find_type)->with('payment_date', $data['payment_date']);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return back()->with("unsuccess","Not Added")->with('find_customer', $find_customer)->with('find_type', $find_type);
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $payment        = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                ->select('payments.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        // if ($payment['type'] == 0)
        // {
        //     $entries    = PaymentEntries::leftjoin('invoices', 'invoices.id', 'payment_entries.invoice_id')
        //                         ->where('payment_entries.payment_id', $id)
        //                         ->select('payment_entries.*', 'invoices.invoice_number as number')
        //                         ->get();
        // }
        // else
        // {
        //     $entries    = PaymentEntries::leftjoin('bills', 'bills.id', 'payment_entries.bill_id')
        //                         ->where('payment_entries.payment_id', $id)
        //                         ->select('payment_entries.*', 'bills.bill_number as number')
        //                         ->get();
        // }

        $user_info  = userDetails();

        return view('payments::show', compact('payment', 'user_info'));
    }

    public function edit($id)
    {
        dd($entries);
        return view('payments::edit');
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        DB::beginTransaction();

        try{
            $payment  = Payments::find($id);

            if ($payment->type == 0)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-receive'])->delete();

                customerBalanceUpdate($payment['customer_id']);
            }

            if ($payment->type == 1)
            {
                $payment_delete         = Payments::where('id', $id)->delete();
                $delete_transactions    = JournalEntries::where('payment_id', $id)->whereIn('transaction_head', ['payment-made'])->delete();
                
                supplierBalanceUpdate($payment['customer_id']);
            }

            DB::commit();
            return back()->with("success","Payment Deleted Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Deleted");
        }      
    }
    
    public function paymentListCustomer()
    {

        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->select('customers.*')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentListAjax()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Payments::orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Payments::where('payments.payment_number', 'LIKE', "%$search%")
                                    ->orderBy('payments.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $paymentNumber  = 'PM - '.str_pad($value['payment_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$paymentNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function paymentSearchList($from_date, $to_date, $customer, $paymentNumber)
    {
        $branch_id                      = Auth()->user()->branch_id;
        $search_by_from_date            = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date              = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer             = $customer != 0 ? $customer : 0;
        $search_by_payment_number       = $paymentNumber != 0 ? $paymentNumber : 0;


        if ($branch_id == 1)
        {

             $data            = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('payments.payment_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                            return $query->where('payments.payment_number', $search_by_payment_number);
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                 'accounts.account_name as accounts_name',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('payments.id')
                                        ->get();

        }else{
             $data            = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->when($search_by_from_date != 0 && $search_by_to_date != 0, function ($query) use ($search_by_from_date, $search_by_to_date) {
                                            return $query->whereBetween('payments.payment_date', [$search_by_from_date, $search_by_to_date]);
                                        })
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                            return $query->where('payments.payment_number', $search_by_payment_number);
                                        })
            
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                 'accounts.account_name as accounts_name',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('payments.id')
                                        ->get();
        }

        return Response::json($data);
    }

    public function contactList($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $customers  = Customers::find($id);
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['receivable']     = $debit;
        $data['received']       = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListBill($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $customers  = Customers::find($id);
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers['phone'];
        $data['address']        = $customers['address'];
        $data['payable']        = $debit;
        $data['paid']           = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function contactListEmployee($id)
    {
        $branch_id  = Auth()->user()->branch_id;
        $debit      = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['employee-salary'])
                                ->where('debit_credit', 1)
                                ->where('branch_id', $branch_id)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $id)
                                ->whereIn('transaction_head', ['payment-made'])
                                ->where('debit_credit', 0)
                                ->where('branch_id', $branch_id)
                                ->sum('amount');

        $data['name']           = $customers['name'];
        $data['phone']          = $customers->department_id != null ? $customers->department->name : '';
        $data['address']        = $customers['designation'];
        $data['payable']        = $debit;
        $data['paid']           = $credit;
        $data['dues']           = $customers['balance'];

        return Response::json($data);
    }

    public function paymentList($id)
    {
        $branch_id  = Auth::user()->branch_id;

         if ($id == 0)
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->orderBy('payments.id', 'DESC')
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('payments.branch_id', $branch_id)
                                        ->where('payments.customer_id', $id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*',
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        
                                        ->get();
            }
        }
        else
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                               
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('payments.customer_id', $id)
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                      
                                        ->get();
            }
        }

        return Response::json($data);
    }

    public function paymentListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            if ($branch_id == 1)
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
            else
            {
                $data   = Payments::leftjoin('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('customers.name', 'LIKE', "%$id%")
                                        ->orWhere('payments.payment_date', 'LIKE', "%$search_by_date%")
                                        ->where('payments.branch_id', $branch_id)
                                        ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                            return $query->orWhere('payments.payment_number', 'LIKE', "%$search_by_payment_number%");
                                        })
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
        }
        else
        {
            if ($branch_id == 1)
            {

                $data   = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
            else
            {
                $data   = Payments::join('customers', 'customers.id', 'payments.customer_id')
                                        ->leftjoin('accounts', 'accounts.id', 'payments.paid_through')
                                        ->where('payments.branch_id', $branch_id)
                                        ->orderBy('payments.created_at', 'DESC')
                                        ->select('payments.*', 
                                                  'accounts.account_name as accounts_name', 
                                                  'customers.name as customer_name')
                                        ->take(20)
                                        ->get();
            }
        }

        return Response::json($data);
    }
}