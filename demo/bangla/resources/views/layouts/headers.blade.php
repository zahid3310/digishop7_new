<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="{{ route('home') }}" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'purchase_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'purchase_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Direct Purchase</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('purchase_index') }}">Purchase</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'purchase_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_show' ? 'mm-active' : '' }}" href="{{ route('purchase_all_purchase') }}">List of Purchase</a> </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : ''  || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span>Direct Sales</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('invoices_index') }}">Daily Sales</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' }}" href="{{ route('invoices_all_sales') }}">List of Sales</a> </li>
                        <!-- <li> <a class="{{ Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : '' }}"  href="{{ route('sales_return_index') }}">Sales Return</a> </li> -->
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span>Rec. From Customer</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('bills_index') }}">Receive</a> </li>
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' }}" href="{{ route('bills_all_bills') }}">List of Receive</a> </li>
                        
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' ||  Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' ||  Route::currentRouteName() == 'productions_delivery_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' ||  Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' ||Route::currentRouteName() == 'productions_delivery_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-product-hunt"></i><span>{{ __('messages.production')}}</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="{{ Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'productions_transfer_to_production_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                {{ __('messages.Send_Production')}}
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('productions_transfer_to_production_create') }}">{{ __('messages.new_send')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'productions_transfer_to_production_edit' ? 'mm-active' : '' }}" href="{{ route('productions_transfer_to_production_index') }}">{{ __('messages.list_of_send')}}</a> </li>
                                <!-- <li> <a href="{{ route('productions_return_to_warehouse_index') }}">{{ __('messages.back_to_warehouse')}}</a> </li> -->
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_show' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'productions_received_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Rec. Finished Goods
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('productions_received_finished_goods_create') }}">{{ __('messages.new_receive')}}</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'productions_received_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_received_finished_goods_show' ? 'mm-active' : '' }}" href="{{ route('productions_received_finished_goods_index') }}">{{ __('messages.list_of_receive')}}</a> </li>
                                <!-- <li> <a class="{{ Route::currentRouteName() == 'productions_damaged_finished_goods_index' ? 'mm-active' : '' }}" href="{{ route('productions_damaged_finished_goods_index') }}">Damage Finished Goods</a> </li> -->
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'productions_delivery_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_show' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'productions_delivery_finished_goods_index' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_create' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_show' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Delivery
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('productions_delivery_finished_goods_create') }}">New Delivery</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'productions_delivery_finished_goods_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'productions_delivery_finished_goods_show' ? 'mm-active' : '' }}" href="{{ route('productions_delivery_finished_goods_index') }}">List of Delivery</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="{{ 
                Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || 
                Request::getQueryString() == 'payment_type=2' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=2' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span>Accounts</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' }}" href="{{ route('expenses_index') }}">Expenses</a> </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' }}" href="{{ route('incomes_index') }}">Incomes</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=0' }}">Receive From Customer</a> </li>
                        <li> <a class="{{ Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=1' }}">Pay To Supplier</a> </li>
                        <!-- <li> <a class="{{ Request::getQueryString() == 'payment_type=2' ? 'mm-active' : '' }}" href="{{ route('payments_create').'?payment_type=2' }}">Pay Salary</a> </li> -->
                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_index') }}">Balance Transfer</a> 
                        </li>

                        <li> <a class="{{ 
                            Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_print_check_show' ? 'mm-active' : '' }}" href="{{ route('balance_transfer_print_check_index') }}">Print Check</a> 
                        </li>
                    </ul>
                </li>

                <li class="{{ Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }}">
                    <a class="{{ Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                       <i class="fas fa-strikethrough"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('process_monthly_salary_index') }}">List of Salary Sheet</a> </li>
                        <li> <a href="{{ route('process_monthly_salary_create') }}">Process Salary Sheet</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span>Messaging</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('messages_send_index') }}">Send Message</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span>Reports</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="{{ route('monthly_alary_report_index') }}">Monthly Salary Sheet</a> </li>
                        <li> <a href="{{ route('attendance_report_index') }}">Attendance Report</a> </li>
                        
                        <li class="">
                            <a class="has-arrow waves-effect">
                                Production
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('production_report_index') }}">Production Report</a> </li>
                                <li> <a href="{{ route('finished_goods_report_index') }}">Finished Goods Report</a> </li>
                                <li> <a href="{{ route('production_finished_report_print') }}" target="_blank">Production Summary</a> </li>
                            </ul>
                        </li>

                        <!-- <li class="">
                            <a class="has-arrow waves-effect">
                                Receive
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('purchase_statement_index') }}">Statement of Rec.</a> </li>
                                <li> <a href="{{ route('purchase_summary_index') }}">Receive Summary</a> </li>
                            </ul>
                        </li> -->

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('expense_report_index') }}">List of Expense</a> </li>
                                <li> <a href="{{ route('income_report_index') }}">List of Income</a> </li>
                                <li> <a href="{{ route('income_expense_ledger_index') }}">Cash Book</a> </li>
                                <li> <a href="{{ route('income_statement_index') }}">Bank Book</a> </li>
                                <li> <a href="{{ route('daily_collection_report_index') }}">Daily Report</a> </li>
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                MIS
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('stock_report_index') }}">Stock Status</a> </li>
                                <li> <a href="{{ route('current_balance_print') }}" target="_blank">Current Balance</a> </li>
                                <li> <a href="{{ route('due_report_supplier_index') }}">Supplier Ledger</a> </li>
                                <li> <a href="{{ route('due_report_customer_index') }}">Customer Ledger</a> </li>
                                <!-- <li> <a href="{{ route('due_report_customer_print_due') }}">Due Customer List</a> </li> -->
                                <!-- <li> <a href="{{ route('due_report_supplier_index_due') }}">Due Supplier List</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                Basic Report
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('item_list_index') }}">List of Items</a> </li>
                                <li> <a href="{{ route('emergency_item_list_index') }}">Emergency Purchase</a> </li>
                                <li> <a href="{{ route('product_suppliers_index') }}">Item Wise Supplier</a> </li>
                                <li> <a href="{{ route('product_customers_index') }}">Item Wise Customer</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=0' }}" target="_blank">Customer List</a> </li>
                                <li> <a href="{{ route('register_list_index').'?type=1' }}" target="_blank">Supplier List</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="{{ 
                Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'stores_edit' ? 'mm-active' : '' }}">
                    <a class="{{ 
                    Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_index' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span>Basic Settings</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_index' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_index' ? 'mm-active' : '' || Route::currentRouteName() == 'stores_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Product
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' }}" href="{{ route('products_index') }}">Add Product</a> </li>
                                <li> <a class="" href="{{ route('products_index_all') }}">List of Products</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' }}" href="{{ route('products_category_index') }}">Add Categories</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' }}" href="{{ route('categories_index') }}" href="{{ route('categories_index') }}">Add Major Categories</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'stores_edit' ? 'mm-active' : '' }}" href="{{ route('stores_index') }}">Stores</a> </li>
                                <li> <a href="{{ route('products_opening_stock') }}">Bulk Opening Stock</a> </li>
                                <li> <a href="{{ route('products_bulk_product_list_update') }}">Bulk Product Update</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}">
                            <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Registers
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=0' }}">Customer</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=1' }}">Add Supplier</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=2' }}">Add Employee</a> </li>
                                <li> <a class="{{ Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' }}" href="{{ route('customers_index').'?contact_type=3' }}">Add Reference</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'department_edit' ? 'mm-active' : '' }}" href="{{ route('department_index') }}">Add Department</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                        Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                        Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                            Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Accounts
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="{{ Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' }}" href="{{ route('account_types_index') }}">Account Types</a> </li>
                                <li> <a class="{{ Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' }}" href="{{ route('chart_of_accounts_index') }}">Chart of Accounts</a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Messaging
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="{{ route('messages_index') }}">Create Text Message</a> </li>
                                <li> <a href="{{ route('messages_phone_book_index') }}">Phone Book</a> </li>
                            </ul>
                        </li>

                        <li class="{{ Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}">
                            <a class="{{ Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : ''|| Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                Payroll Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Grades
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' }}" href="{{ route('salary_grades_index') }}">List of Grades</a> </li>
                                        <li> <a href="{{ route('salary_grades_create') }}">New Grade</a> </li>
                                    </ul>
                                </li>

                                <li class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Salary Statements
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' }}" href="{{ route('salary_statements_index') }}">List of Statements</a> </li>
                                        <li> <a href="{{ route('salary_statements_create') }}">New Statement</a> </li>
                                    </ul>
                                </li>

                                <!-- <li class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}">
                                    <a class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }} has-arrow waves-effect">
                                        Increaments
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="{{ Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : '' }}" href="{{ route('salary_increaments_index') }}">List of Increaments</a> </li>
                                        <li> <a href="{{ route('salary_increaments_create') }}">Add Increament</a> </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                Security System
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a href="{{ route('branch_index') }}">Add Branch</a> </li> -->
                                <li> <a href="{{ route('users_index') }}">Add User</a> </li>
                                <li> <a href="{{ route('users_index_all') }}">List of User</a> </li>
                                <li> <a href="{{ route('set_access_index') }}">Set Permission</a> </li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>