<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentEntries extends Model
{
    protected $table = "payment_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function payment()
    {
        return $this->belongsTo('App\Models\Payments','payment_id');
    }
}
