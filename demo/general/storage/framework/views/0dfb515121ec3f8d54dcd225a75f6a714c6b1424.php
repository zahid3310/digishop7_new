<!DOCTYPE html>
<html>

<head>
    <title>Customer Ledger Details</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-top:5mm;
            margin-bottom:5mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p style="font-size: 20px"><?php echo e($user_info['address']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_number']); ?></p>
                        <p style="font-size: 20px"><?php echo e($user_info['contact_email']); ?></p>
                        <p style="font-size: 14px;text-align: right"><?php echo e('Print Date : ' . date('d-m-Y')); ?></p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Customer Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Customer Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Customer Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important"><?php echo e($from_date); ?> <strong>To</strong> <?php echo e($to_date); ?></td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['name']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        <?php if($customer_name != null): ?>
                                            <?php echo e($customer_name['phone']); ?>

                                        <?php else: ?>
                                            ALL
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >

                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 7%;vertical-align: bottom" rowspan="2">Date</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Details</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Trans#</th>
                                    <th style="text-align: center;width: 8%" colspan="5">Description</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Total Amount Dis.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Ship/Cost</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Paid Through</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Debit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Credit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Balance</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Item Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Qty.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">U/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">T/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Dis.</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="11"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;" colspan="3"><strong><?php echo e(number_format($opening_balance,0,'.',',')); ?></strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                <?php if($data->count() > 0): ?>
                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <?php
                                    if ($value['debit_credit'] == 0)
                                    {
                                        $debit  = $value['amount'];
                                        $credit = 0;
                                    }
                                    else
                                    {
                                        $credit = $value['amount'];
                                        $debit  = 0;
                                    }

                                    $sub_total = $sub_total + $debit -$credit;

                                    if ($value['transaction_head'] == 'customer-opening-balance')
                                    {
                                        $trans_number     = '';
                                        $transaction_head = 'Opening Balance';
                                        $transaction_note = $value->note;
                                        $register         = $value->customer->name;
                                        $payment_methode  = '';
                                        $text             = 'Opening Balance ';
                                        $entries          = collect();
                                        $rowspan          = 1;
                                    }
                                    elseif ($value['transaction_head'] == 'sales')
                                    {
                                        $trans_number       = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Sales of Items';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Item Sales To ';
                                        $entries            = $value->invoice->invoiceEntries;
                                        $rowspan            = $entries->count();
                                    }
                                    elseif ($value['transaction_head'] == 'payment-receive')
                                    {
                                        if ($value->invoice_id != null)
                                        {
                                            $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Payment Received';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        elseif ($value->purchase_return_id != null)
                                        {
                                            $trans_number     = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Payment Received';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        else
                                        {
                                            $trans_number     = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = $value->purchase_return_id != null ? 'Purchase Return of Items' : 'Previous Due Collection';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'sales-return')
                                    {
                                        $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Sales Return of Items';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Return Back to ';
                                        $entries            = $value->salesReturn->salesReturnEntries;
                                        $rowspan            = $entries->count();
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        if ($value->bill_id != null)
                                        {
                                            $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Payment Made';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        elseif ($value->sales_return_id != null)
                                        {
                                            $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Payment Made';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        else
                                        {
                                            $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = $value->sales_return_id != null ? 'Sales Return of items' : 'Previous Due Paid';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'customer-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Customer Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->name;
                                        $text               = 'Return Customer Advance To';
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                    } 
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(date('d-m-Y', strtotime($value['date']))); ?></a>
                                    </td>

                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e($transaction_head); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e($trans_number); ?></td>
                                    <?php if(($value['transaction_head'] == 'sales') || ($value['transaction_head'] == 'sales-return')): ?>
                                    <?php if($entries->count() > 0): ?>
                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $detail_data1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if($key1 == 0): ?>
                                    <td style="text-align: left;font-size: 12px !important;"><?php echo e($detail_data1->productEntries->name); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e(number_format($detail_data1->quantity,2,'.',',')); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e(number_format($detail_data1->rate,0,'.',',')); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e(number_format($detail_data1->total_amount,0,'.',',')); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data1->discount_type == 1 ? number_format($detail_data1->discount_amount,0,'.',',') : number_format((($detail_data1->discount_amount*$detail_data1->quantity*$detail_data1->rate)/100),0,'.',',')); ?></td>
                                    <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php else: ?>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <?php endif; ?>

                                    <?php if($entries->count() > 0 && $entries->count() == 0): ?>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <?php endif; ?>
                                    <?php else: ?>
                                    <td colspan="5"></td>
                                    <?php endif; ?>
                                    
                                    <?php if(($value['transaction_head'] == 'sales') || ($value['transaction_head'] == 'sales-return')): ?>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value->invoice->total_discount_amount) ? number_format($value->invoice->total_discount_amount,0,'.',',') : ''); ?></td>
                                    <?php else: ?>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"></td>
                                    <?php endif; ?>
                                    
                                    <?php if(($value['transaction_head'] == 'sales') || ($value['transaction_head'] == 'sales-return')): ?>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(isset($value->invoice->shipping_cost) ? number_format($value->invoice->shipping_cost,0,'.',',') : ''); ?></td>
                                    <?php else: ?>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"></td>
                                    <?php endif; ?>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>">
                                        <?php echo e($payment_methode); ?>

                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(number_format($debit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(number_format($credit,0,'.',',')); ?></td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="<?php echo e($rowspan); ?>"><?php echo e(number_format($sub_total,0,'.',',')); ?></td>
                                </tr>

                                <?php if(($value['transaction_head'] == 'sales') || ($value['transaction_head'] == 'sales-return')): ?>
                                <?php if($entries->count() > 0): ?>
                                <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key2 => $detail_data2): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php if($key2 != 0): ?>
                                <tr>
                                    <td style="text-align: left;font-size: 12px !important;"><?php echo e($detail_data2->productEntries->name); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->quantity); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->rate); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->total_amount); ?></td>
                                    <td style="text-align: center;font-size: 12px !important;"><?php echo e($detail_data2->discount_type == 1 ? number_format($detail_data2->discount_amount,0,'.',',') : number_format((($detail_data2->discount_amount*$detail_data2->quantity*$detail_data2->rate)/100),0,'.',',')); ?></td>
                                </tr>
                                <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>
                                <?php endif; ?>
                             
                            <?php $i++; ?>
                                
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <?php endif; ?>
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="13" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important"><?php echo e(number_format($sub_total,0,'.',',')); ?></th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/demo/general/Modules/Reports/Resources/views/due_report_customer_details.blade.php ENDPATH**/ ?>