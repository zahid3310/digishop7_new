

<?php $__env->startSection('title', 'Invoice Print'); ?>

<?php $__env->startSection('styles'); ?>
<style>
table, td, th {  
  border: 1px solid #ddd;
  text-align: center;
}

table {
  border-collapse: collapse;
  width: 100%;
}
.myWidth{
    width: 95%!important;
}

.number-center { 
    text-align: center; 
}

.amount-bold { 
    font-weight: bold;
}


</style>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">

                 <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">INVOICE</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($find_order['order_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Order No : <?php echo e('OR - ' . str_pad($find_order['order_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>
                     
                        <div class="card">
                            
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('tailors_order_update', $find_order['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>


                                 

                                <div style="background-color: #F4F4F7;padding-top: 10px;border-top: 2px solid #563d7c;border-radius: 6px;" class="row">
                                   

                                    <div class="col-md-6">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-2 col-form-label"><strong>নাম *</strong></label>
                                            <div class="col-md-10">
                                                <select id="customer_id" name="customer_id" class="customerID form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" disabled="">
                                                  <option value="<?php echo e($find_order['customer_id']); ?>" selected><?php echo e($find_order['contact_name']); ?>(<?php echo e($find_order['mobile_number']); ?>)</option>
                                                </select>
                                               <!--  <span style="float:right;cursor:pointer;background-color:#556EE6;color:white!important;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span> -->
                                            </div>
                                        </div>

                                      
                                    </div>

                                   

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>অর্ডার তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="order_date" name="order_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_order['order_date']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" disabled="">
                                            </div>
                                        </div>
                                        
                                    </div>

                                      <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label"><strong>অগ্রাধিকার:</strong> </label>
                                            <div class="col-md-7">
                                                <select id="priority_id" name="priority_id" class="priority_id form-control select2" disabled="">
                                                    <option <?php echo e($find_order['priority_id'] == 1 ? 'selected' : ''); ?> value="1">Normal</option>
                                                   <option <?php echo e($find_order['priority_id'] == 2 ? 'selected' : ''); ?> value="2">Urgent</option>
                                                   <option <?php echo e($find_order['priority_id'] == 3 ? 'selected' : ''); ?> value="3">Very Urgent</option>
                                                </select>
                                            </div>
                                        </div>
     
                                    </div>

                        
                                     <div class="col-md-3">

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label"><strong>ঘুন্ডি :</strong> </label>
                                            <div class="col-md-8">
                                                <input  type="text" name="knob" class="form-control" width="100%" value="<?php echo e($find_order['knob']); ?>" disabled="">
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-3">
                                      
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-6 col-form-label"><strong>ডেলিভারি তারিখ:</strong></label>
                                            <div class="col-md-6">
                                                <input id="delivery_date" name="delivery_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_order['delivery_date']))); ?>" class="asdasd form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" disabled="">
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>

                                <hr>

                                <div class="row" style="background-color: #F4F4F7;padding-bottom: 5px; padding-top: 5px;">

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাঞ্জাবীর  মাপ:</strong></div>
                                    <div class="col-md-6">
                                        <label class="mt-2" style="font-weight: 500;"><strong>পাঞ্জাবীর বিবরণ:</strong></label>
                                        <textarea class="form-control" name="punjabi_description" rows="1" disabled=""><?php echo e($find_order['punjabi_description']); ?></textarea>
                                    </div>

                                    <div class="col-md-6">
                                        <label class="mt-2"><strong>এমব্রয়ডারি বিবরণ:</strong></label>
                                        <textarea class="form-control" name="embroidery" rows="1" disabled=""><?php echo e($find_order['embroidery']); ?></textarea>
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_hight']); ?>" name="punjabi_hight" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বডি</strong></label> 
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_body']); ?>" name="punjabi_body" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কাঁধ</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_shoulders']); ?>" name="punjabi_shoulders" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতা</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_hand']); ?>" name="punjabi_hand" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>হাতের মুহুরী</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_hand_muhuri']); ?>" name="punjabi_hand_muhuri" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>গলা</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_Throat']); ?>" name="punjabi_Throat" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>ঘের</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_gher']); ?>" name="punjabi_gher" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>আর্মহোল</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_armhole']); ?>" name="punjabi_armhole" disabled="">
                                    </div>

                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কলার প্লেট</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_collar_plate']); ?>" name="punjabi_collar_plate" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বুক ফারা</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['punjabi_buk_fara']); ?>" name="punjabi_buk_fara" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>বোতাম</strong></label>
                                        <input type="number" class="form-control" name="punjabi_button" value="<?php echo e($find_order['punjabi_button']); ?>" disabled="">
                                    </div>
                                    <div class="col-md-2">
                                        <label class="mt-2"><strong>কলার</strong></label>
                                        <select class="form-control" name="punjabi_kolar" disabled="">
                                            <option <?php echo e($find_order['punjabi_kolar'] == 1 ? 'selected' : ''); ?> value="1">গোল গলা</option>
                                            <option <?php echo e($find_order['punjabi_kolar'] == 2 ? 'selected' : ''); ?> value="2">শেরওয়ানি</option>
                                            <option <?php echo e($find_order['punjabi_kolar'] == 3 ? 'selected' : ''); ?> value="3">হাফ বেল্ট</option>
                                            <option <?php echo e($find_order['punjabi_kolar'] == 4 ? 'selected' : ''); ?> value="4">ফুল বেল্ট</option>
                                        </select>
                                    </div>
                                </div>


                                <hr>
                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;">

                                   

                                    <div class="col-md-12" style="padding-top:5px;padding-bottom:5px;text-align: center;font-size: 17px;background-color: #9e9e9e36;border-top: 2px solid #563d7c;border-radius: 6px;color: #000;"><strong>পাজামার মাপ:</strong></div>

                                    <div class="col-md-6">
                                        <label class="mt-2"><strong>পাজামার  বিবরণ:</strong></label>
                                        <textarea class="form-control" rows="1" name="payjama_description" disabled=""><?php echo e($find_order['payjama_description']); ?></textarea>
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>লম্বা</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['payjama_hight']); ?>" name="payjama_hight" disabled="">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>মুহুরী</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['payjama_muhuri']); ?>" name="payjama_muhuri" disabled="">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>লুস</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['payjama_lus']); ?>" name="payjama_lus" disabled="">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>হাই</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['payjama_high']); ?>" name="payjama_high" disabled="">
                                    </div>

                                    <div class="col-md-3">
                                        <label class="mt-2"><strong>কোমর</strong></label>
                                        <input type="text" class="form-control" value="<?php echo e($find_order['payjama_komor']); ?>" name="payjama_komor" disabled="">
                                    </div>

                                </div>

                                <?php if(Auth::user()->role == 1): ?>

                                <hr>

                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;">

                                    <div class="col-md-6" style="border-right: 2px dotted #000;">
                                        <center><strong>পাঞ্জাবীর হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                            <div class="col-md-6">
                                                <label class="mt-2"><strong>পাঞ্জাবীর স্টাইল :</strong></label>
                                                <select name="services_id" id="tessrter" class="ServiceId form-control select2" disabled="">
                                                    <?php $__currentLoopData = $services; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $serv): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option <?php if($find_order_entries[0]['service_id'] == $serv->id): ?> selected  <?php endif; ?> value="<?php echo e($serv->id); ?>"><?php echo e($serv->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>

                                             <div class="col-md-4">
                                                <label class="mt-2"><strong>মজুরি :</strong></label>
                                                <input type="number" id="panjabirMujuri" class="servicePrice number-center form-control" name="panjabi_rate"  value="<?php echo e($find_order_entries[0]['panjabi_rate']); ?>" disabled="">
                                            </div> 

                                            <div class="col-md-2">
                                                <label class="mt-2"><strong>সংখ্যা:</strong></label>
                                                <input type="number" name="number_of_punjabi" id="numberPunjabi" class="number-center form-control" value="<?php echo e($find_order_entries[0]['panjabi_quantity']); ?>" disabled="">
                                            </div> 


                                            <?php if($find_order['cp_cf'] == 1): ?>
                                            <div class="col-md-12">
                                                <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                <!-- <input type="text" class="form-control" name="panjabi_cloth_name"> -->
                                                 <textarea name="panjabi_cloth_name" class="form-control" rows="1" disabled=""><?php echo e($find_order_entries[0]['panjabi_cloth_name']); ?></textarea>
                                            </div>

                                   

                                            <div class="col-md-4">
                                                <label class="mt-2"><strong>কাপড়ের পরিমান</strong></label>
                                                <input type="number" step="0.01" id="panjabirKaporerPorimap" class="number-center form-control" name="amount_panjabi_cloth" value="<?php echo e($find_order_entries[0]['amount_panjabi_cloth']); ?>" disabled="">
                                            </div>

                                            


                                            <div class="col-md-4">
                                                <label class="mt-2"><strong>কাপড়ের দর</strong></label>
                                                <input type="number" id="panjabirKaporerDor" class="number-center form-control" name="price_panjabi_cloth"  value="<?php echo e($find_order_entries[0]['price_panjabi_cloth']); ?>" disabled="">
                                            </div>
                                            <?php endif; ?>

                                             <div class="col-md-4">
                                                <label class="mt-2"><strong>এমব্রয়ডারি</strong></label>
                                                <input type="number" class="number-center form-control" name="ambortari_price" id="panjabirAmbortari" value="<?php echo e($find_order_entries[0]['ambortari_price']); ?>" disabled="">
                                            </div>

                                        </div>
                                    </div>

                                    

                                    <div class="col-md-6" style="border-right: 2px dotted #000;">

                                        <center><strong>পাজামার হিসাব :</strong></center>
                                        <hr>
                                        <div class="row">

                                         <div class="col-md-6">
                                        <label class="mt-2"><strong>পাজামার সংখ্যা:</strong></label>
                                        <input type="number" name="number_of_payjama" id="numberPayjama" class="number-center form-control" value="<?php echo e($find_order_entries[0]['payjama_quantity']); ?>" disabled="">
                                    </div>

                                             

                                              <div class="col-md-6">
                                        <label class="mt-2"><strong> মজুরি :</strong></label>
                                        <input type="number" id="payjamarMujuri" class="number-center form-control" name="payjama_rate"  value="<?php echo e($find_order_entries[0]['payjama_rate']); ?>" disabled="">
                                    </div>

                                     <?php if($find_order['cp_cf'] == 1): ?>
                                    <div class="col-md-12">
                                                <label class="mt-2"><strong>কাপড়ের নাম</strong></label>
                                                <!-- <input type="text" class="form-control" name="payjama_cloth_name"> -->
                                                <textarea name="payjama_cloth_name" class="form-control" rows="1" disabled=""><?php echo e($find_order_entries[0]['payjama_cloth_name']); ?></textarea>
                                            </div>

                                            <div class="col-md-6">
                                                <label class="mt-2"><strong>পরিমান</strong></label>
                                                <input type="number" step="0.01"  class="number-center form-control" name="amount_payjama_cloth" id="payjamaKaporerPorimap" value="<?php echo e($find_order_entries[0]['amount_payjama_cloth']); ?>" disabled="">
                                            </div>
                                        <div class="col-md-6">
                                        <label class="mt-2"><strong> দর</strong></label>
                                        <input type="number" id="payjamaKaporerDor" class="number-center form-control" name="price_payjama_cloth"  value="<?php echo e($find_order_entries[0]['price_payjama_cloth']); ?>" disabled="">
                                    </div>
                                    <?php endif; ?>

                                            

                                        </div>
                                    </div>

                                </div>

                                <div class="row" style="background-color: #F4F4F7;padding-top: 5px;">
                                   

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>মোট টাকা :</strong></label>
                                        <input type="number" id="sum"  class="servicePrice amount-bold number-center form-control" name="total_amount" value="<?php echo e($find_order_entries[0]['total_amount']); ?>" disabled="">


                                    </div>

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>জমা টাকা :</strong></label>
                                        <input type="number" id="Joma" class="amount-bold number-center form-control" name="submission_amount" value="<?php echo e($find_order_entries[0]['submission_amount']); ?>" disabled="">

                                        
                                    </div>

                                     <div class="col-md-4">
                                        <label class="mt-2"><strong>বাকি টাকা :</strong></label>
                                        <input type="number" id="payable" class="servicePrice amount-bold number-center form-control" name="due_amount" value="<?php echo e($find_order_entries[0]['due_amount']); ?>" disabled="">
                                    </div>
                                </div>
                           

                                <hr>

                                
                            


                              

                                <?php endif; ?>


                                </form>

                                <!-- <div class="container">
         <div class="row">
            <div class="col-sm-6 col-sm-offset-3 well">
               <h4 class="text-center">Live Sum of values using jQuery</h4> <hr/>
               <form class="form-horizontal">
                  <div class="form-group">
                     <label class="control-label col-sm-2" for="value1">Value 1</label>
                     <div class="col-sm-10">
                        <input type="number" name="value1" id="value1" class="form-control" min="0" placeholder="Enter first value" required />
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-sm-2" for="value2">Value 2</label>
                     <div class="col-sm-10">
                        <input type="number" name="value2" id="value2" class="form-control" min="0" placeholder="Enter second value" required />
                     </div>
                  </div>
                  <div class="form-group">
                     <label class="control-label col-sm-2" for="sum">Sum</label>
                     <div class="col-sm-10">
                        <input type="number" name="sum" id="sum" class="form-control" readonly />
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div> -->

                            </div>
                        </div>
                     
                </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number*</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Email</label>
                        <div class="col-md-12">
                            <input id="email" name="email" type="text" class="form-control">
                        </div>
                    </div>

                     <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Date of birth</label>
                        <div class="col-md-12">
                            <input type="date" id="db" name="db" class="form-control">
                        </div>
                    </div>

                     <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Marriage date</label>
                        <div class="col-md-12">
                            <input id="md" name="md" type="date" class="form-control">
                        </div>
                    </div>


                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

  
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/tailors/orders/local-customer-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };

                    console.log(response);
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

       
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var email                       = $("#email").val();
            var date_of_birth               = $("#db").val();
            var marriage_date               = $("#md").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();


            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/tailors/orders/local-customer-add',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number,email : email ,date_of_birth : date_of_birth,marriage_date : marriage_date, contact_type : contact_type, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        console.log(data.id)
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });
    
    </script>

 


    <script type="text/javascript">
      
        $(document).ready(function(){
            $("select.customerID").change(function(){
                var customerID = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/localCustomerList1',
                    data:   {customerID : customerID, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (Serviceprice) {
                       console.log(Serviceprice)
                    }
                });
            });


        });


        $(document).ready(function(){
            $("select.ServiceId").change(function(){
                var ServiceId = $(this).children("option:selected").val();
                var site_url                    = $('.site_url').val();
                $.ajax({
                    type:   'get',
                    url:    site_url + '/tailors/orders/get-service-price',
                    data:   {ServiceId : ServiceId, _token: '<?php echo e(csrf_token()); ?>' },
        
                    success: function (data) {
                        console.log(data);
                        $(".servicePrice").val(data[0]['price']);
                    }
                });
            });


        });


        $(document).ready(function(){
            $("select.priority_id").change(function(){
                var priority_id = $(this).children("option:selected").val();

                if (priority_id == 1) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("7 days"))); ?>;
                     $(".asdasd").val(priorityDate);
                }
                if (priority_id == 2) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("4 days"))); ?>;
                    $(".asdasd").val(priorityDate);
                }

                if (priority_id == 3) {
                    var priorityDate = <?php echo json_encode(date('d-m-Y',strtotime("2 days"))); ?>;
                    $(".asdasd").val(priorityDate);
                }
            });
        });




        // function myAmmount() {
        //     var panjabirMujuri              = document.getElementById("panjabirMujuri").value;
        //     var kaporerDor                  = document.getElementById("kaporerDor").value;
        //     var payjamarMujuri              = document.getElementById("payjamarMujuri").value;

        //     var Surviceprice                = $(".servicePrice").val();
           

        //     var numberPunjabi                = document.getElementById("numberPunjabi").value;
        //     var numberPayjama                = document.getElementById("numberPayjama").value;

        //     var sum = parseInt(panjabirMujuri)+parseInt(kaporerDor)+parseInt(payjamarMujuri)+parseInt(Surviceprice);



        //     $("#total").val(sum);
        // }

        // function jomataka() {
        //     var jomataka                = document.getElementById("Joma").value;
        //     var totaltaka                = document.getElementById("total").value;
        //     var Subtraction = parseInt(totaltaka)-parseInt(jomataka);
        //     $("#payable").val(Subtraction);
        // }


        
    </script>

    <script type="text/javascript">
          var myFunction = function() {

                var numberPunjabi          =   parseFloat($('#numberPunjabi').val()) || 1;
                var panjabirMujuri         =   parseFloat($('#panjabirMujuri').val()) || 0;
                var panjabirKaporerPorimap =   parseFloat($('#panjabirKaporerPorimap').val()) || 0;
                var panjabirKaporerDor     =   parseFloat($('#panjabirKaporerDor').val()) || 0;
                var panjabirAmbortari      =   parseFloat($('#panjabirAmbortari').val()) || 0;
               

                var panjabikaporerTotalAmount = panjabirKaporerPorimap * panjabirKaporerDor;

                var panjabirTotalCost = (numberPunjabi * panjabirMujuri) + (panjabikaporerTotalAmount + panjabirAmbortari);
             

                // /////////////////////////////////////Payjama Hisab///////////////////////////////////////////////////////

                var numberPayjama          =   parseFloat($('#numberPayjama').val()) || 1;
                var payjamarMujuri         =   parseFloat($('#payjamarMujuri').val()) || 0;
                var payjamaKaporerPorimap  =   parseFloat($('#payjamaKaporerPorimap').val()) || 0;
                var payjamaKaporerDor      =   parseFloat($('#payjamaKaporerDor').val()) || 0;


                var payjamakaporerTotalAmount = payjamaKaporerPorimap * payjamaKaporerDor;

                var payjamarTotalCost = (numberPayjama * payjamarMujuri) + payjamakaporerTotalAmount;


                // var value1 = parseFloat($('#panjabirMujuri').val()) || 0;
                // var value2 = parseFloat($('#kaporerDor').val()) || 0;
                $('#sum').val(panjabirTotalCost+payjamarTotalCost);


                var Joma                   =   parseFloat($('#Joma').val()) || 0;
                var sum                    =   parseFloat($('#sum').val()) || 0;

                $('#payable').val(sum-Joma);


        };
        setInterval(myFunction, 300);
    </script>

    <script type="text/javascript">
        $(".answer").hide();
        $(".coupon_question1").click(function() {
            if($(this).is(":checked")) {
                $(".answer").show();
                $("#cf").prop("checked", false);
            } else {
                $(".answer").hide();
                $("#cf").prop("checked", true);
            }
        });

         $(".coupon_question2").click(function() {
            if($(this).is(":checked")) {
                $(".answer").hide();
                $("#cp").prop("checked", false);

            } else {
                 $(".answer").show();
                $("#cp").prop("checked", true);
            }
        });

    </script>

    <script type="text/javascript">
          $( document ).ready(function() {
        javascript:window.print();
    });
    </script>

   

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/51/Modules/Tailors/Resources/views/print_invoice.blade.php ENDPATH**/ ?>