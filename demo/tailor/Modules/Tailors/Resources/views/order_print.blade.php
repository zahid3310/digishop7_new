@extends('layouts.app')

@section('title', 'Print Pos')

<style type="text/css">
.col-form-label {
     font-weight: bold!important;
    font-size: 15px!important;
}
</style>

@if($user_info['pos_printer'] == 1)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }


    }
</style>
@endif

@if($user_info['pos_printer'] == 0)
<style type="text/css">
    @media print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            /*width: 60mm;*/
            padding: 0px;
            padding-left: 50px;
            padding-right: 50px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -80px !important;
        }

        
    }
</style>
@endif

@section('content')
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Orders</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Orders</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                @if($user_info['pos_printer'] == 1)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">INVOICE</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : {{ date('d/m/Y', strtotime($order['order_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : {{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Order No : {{ 'OR - ' . str_pad($order['order_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row"></div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                @if(!empty($entries) && ($entries->count() > 0))

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    @foreach($entries as $key => $value)

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            {{ $value['quantity'] }}
                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            {{ $value['service_name'] }}
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            {{ number_format($value['rate'],2,'.',',') }}
                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3">{{ number_format($value['total_amount'],2,'.',',') }}</div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    @endforeach
                                @endif

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($sub_total,2,'.',',') }}</strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_discount_amount       = $order['total_discount_type'] == 0 ? (($sub_total*$order['total_discount_amount'])/100) : $order['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong>{{ number_format($total_discount + $total_discount_amount,2,'.',',') }}</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong>{{ number_format($sub_total - $total_discount_amount,2,'.',',') }}</strong></div>
                                </div>

                                <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6">{{ number_format($order['order_amount'] - $order['due_amount'],2,'.',',') }}</div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6">{{ number_format($order['due_amount'],2,'.',',') }}</div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif

                @if($user_info['pos_printer'] == 0)
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                               

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 10px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                       
                                          <p style="font-size: 14px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">
                                            <span style="font-size: 50px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">লেবাস</span><br>

                                          দারুননাজাত মাদরাসা রোড, পূর্ব বক্সনগর (শুকরসী কবরস্থানের পশ্ছিম পাশে) ডেমরা, ঢাকা-১৩৬১। মোবাইলঃ </p>

                                        
                                    </div>
                                </div>


                              <!--    <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['organization_name'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_number'] }}</p>
                                    </div>
                                </div> -->

                               


                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : {{ date('d/m/Y', strtotime($order['order_date'])) }}</span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px">{{ date('h:i a', strtotime(now())) }}</span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Order No : {{ 'OR - ' . str_pad($order['order_number'], 6, "0", STR_PAD_LEFT) }}</span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                          
                                @if(!empty($entries) && ($entries->count() > 0))


                                    <div class="row">
                                        <div class="col-md-6" style="border-right: 3px dotted;">
                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর সংখ্যা</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->panjabi_quantity}}">
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর কাপড়ের নাম</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->panjabi_cloth_name}}">
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর কাপড়ের পরিমান</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->amount_panjabi_cloth}}">
                                                </div>
                                            </div>

                                        

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর কাপড়ের দর</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->price_panjabi_cloth}}">
                                                </div>
                                            </div>

                                           

                                            <div class="form-group">
                                               <center><img style="height: 100px!important;" src="{{asset('public/admin_panel_assets/order-image/'.$order->panjabi_photo)}}" class="img-thumbnail"></center>
                                            </div>



                                        </div>

                                        <div class="col-md-6">

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার  সংখ্যা</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->payjama_quantity}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার কাপড়ের নাম</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->payjama_cloth_name}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার কাপড়ের পরিমান</label>
                                                <div class="col-sm-4">
                                                    
                                                    <input type="text" class="form-control" value="{{$entries[0]->amount_payjama_cloth}} goj">
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার কাপড়ের দর</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->price_payjama_cloth}}">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                               <center><img style="height: 100px!important;" src="{{asset('public/admin_panel_assets/order-image/'.$order->payjama_photo)}}" class="img-thumbnail"></center>
                                               

                                               
                                            </div>

                                             
                                        </div>

                                        <div class="col-md-12" style="border-left: 3px dotted;">
                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">ডেলিভারি তারিখ </label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$order->delivery_date}}">
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর কাপড়ের মূল্য</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{ $entries[0]->amount_panjabi_cloth * $entries[0]->price_panjabi_cloth }}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার কাপড়ের মূল্য</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{ $entries[0]->amount_payjama_cloth * $entries[0]->price_payjama_cloth }}">
                                                </div>
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাঞ্জাবীর মজুরি </label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->panjabi_rate}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">পাজামার মজুরি </label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->payjama_rate}}">
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-sm-8 col-form-label">এমব্রয়ডারি</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" value="{{$entries[0]->ambortari_price}}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     <div class="row" style="padding-top: 5px;">
                                   

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>মোট টাকা :</strong></label>
                                        <input style="text-align:center;font-weight: bold;" type="number" class="amount-bold number-center form-control" value="{{ number_format($entries[0]->total_amount, 2, '.', '') }}" readonly="">

                                    </div>

                                    <div class="col-md-4">
                                        <label class="mt-2"><strong>জমা টাকা :</strong></label>
                                        <input style="text-align:center;font-weight: bold;" type="number" class="amount-bold number-center form-control" value="{{ number_format($entries[0]->submission_amount, 2, '.', '') }}" readonly="">

                                        
                                    </div>


                                     <div class="col-md-4">
                                        <label class="mt-2"><strong>বাকি টাকা :</strong></label>
                                        <input style="text-align:center;font-weight: bold;" type="number" class="amount-bold number-center form-control" value="{{ number_format($entries[0]->due_amount, 2, '.', '') }}" readonly="">
                                    </div>
                                </div>
                                @endif

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

    $( document ).ready(function() {
        javascript:window.print();
    });
  
    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/tailors/orders/create');
    };
</script>
@endsection