@extends('layouts.app')

@section('title', 'All Deliveries')
@section('styles')
<style type="text/css">
    .order-action{
        border-bottom: 1px solid #232b4f54;
    }
</style>
@endsection
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Deliveries List</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order Management</a></li>
                                    <li class="breadcrumb-item active">List of Deliveries</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div style="min-height: 300px" class="card">
                            @if(Session::has('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {!! Session::get('success') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('unsuccess'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! Session::get('unsuccess') !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            @if(Session::has('errors'))
                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                {!! 'Some required fields are missing..!! Please try again..' !!}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            @endif

                            <div class="card-body table-responsive">
                            
                                <div class="row">
                                    
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Order From Date</label>
                                            <input style="cursor: pointer" id="order_from_date" type="date" class="form-control" value="">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>To Date</label>
                                            <input style="cursor: pointer" id="order_to_date" type="date" class="form-control" value="">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label>Delivery From Date</label>
                                           <input style="cursor: pointer" id="delivery_from__date" type="date" class="form-control" value="">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label>To Date</label>
                                           <input style="cursor: pointer" id="delivery_to_date" type="date" class="form-control" value="">
                                        </div>
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group">
                                           <label>Order Number</label>
                                           <input style="cursor: pointer" id="order_number" type="text" class="form-control">
                                        </div>
                                    </div>


                    
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label >Search </label>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control" onclick="searchPayment()">
                                                <i class="bx bx-search font-size-24"></i>
                                            </span>
                                        </div> 
                                    </div>

                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label >Reload </label>
                                            <span style="float: right;cursor: pointer;background-color: #556EE6;color: white!important;text-align: center;font-weight:bold!important;" class="form-control" id="reload">
                                               <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
  <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
</svg>
                                            </span>
                                        </div> 
                                    </div>
                                </div>

                      

                                <br>


                                
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Order No.</th>
                                            <th>Customer</th>
                                            <th>Order Date</th>
                                            <th>Delivery Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody id="order_list">
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div id="myModal1" class="modal fade bs-example-modal-x" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Make New Payment</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                    <div class="row">
                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Receivable</label>
                            <div class="col-md-12">
                                <input id="receivable" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Received</label>
                            <div class="col-md-12">
                                <input id="received" type="text" class="form-control" readonly>
                            </div>
                        </div>

                        <div style="padding: 0px" class="form-group col-md-4 col-4">
                            <label for="example-text-input" class="col-md-12 col-form-label">Dues</label>
                            <div class="col-md-12">
                                <input id="dues" type="text" class="form-control" readonly>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <input id="customer_id" name="customer_id" type="hidden">
                    <input id="find_order_id" name="order_id" type="hidden">

                    <div class="form-group row">
                        <label for="productname" class="col-md-2 col-form-label">Payment Date *</label>
                        <div class="col-md-10">
                            <input style="cursor: pointer" value="{{date('Y-m-d')}}" id="payment_date" name="payment_date" type="date" class="form-control" required>
                        </div>
                    </div>

                    <div class="inner-repeater">
                        <div style="margin-bottom: 0px !important" data-repeater-list="inner-group" class="inner form-group row">
                            <div class="inner col-lg-12 input_fields_wrap_payment getMultipleRowPayment">
                                <div style="margin-bottom: 0px !important;" class="row di_payment_0">
                                </div>
                            </div>
                        </div>

                        <div class="row justify-content-end">
                            <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                            </div>
                        </div>
                    </div>

                    <br>

                    <div class="modal-footer">
                        <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Make Payment</button>
                        <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

     <div id="changeStatus" class="modal fade bs-example-" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Change Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">

                  <form action="{{route('tailors_make_order_status')}}" method="post">
                    @csrf
                        <input id="order_id" name="order_id" type="hidden">

                    <div class="form-group">
                        <label for="productname">Processing Date*</label>
                            <input style="cursor: pointer" name="processing_date" type="date" class="form-control"  value="{{date('Y-m-d')}}" required>
                    </div>

                    <div class="form-group">
                        <label for="productname">Processing Status*</label>
                        <select class="form-control" required="" name="order_status">
                            <option value="1" hidden="">Select Status</option>
                            <option value="2">Transferd To Factory</option>
                            <option value="3">Sewing completed</option>
                            <option value="4">Received From Factory</option>
                            <option value="5">Delivered completed</option>
                            <option value="5">Order Canceled</option>
                        </select>
                        
                    </div>

                 

                    <br>

                    <div class="modal-footer">
                        <button type="submit"class="btn btn-primary waves-effect waves-light">Make Change</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">

        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $.get(site_url + '/tailors/orders/deliveries-load-list', function(data){
                orderList(data);
            });
        });

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

       function searchPayment()
        {

            // var search_text     = $('#searchPayment').val();    
            // var site_url        = $('.site_url').val();

            // if (search_text == '')
            // {
            //     var search_text = 'No_Text';
            // }

            var  site_url                = $('.site_url').val();
            var  order_from_date        = $('#order_from_date').val();
            var  order_to_date          = $('#order_to_date').val();
            var  delivery_from__date    = $('#delivery_from__date').val();
            var  delivery_to_date       = $('#delivery_to_date').val();
            var  order_number           = $('#order_number').val();





            if (order_from_date != '')
            {
                var order_from_date = $('#order_from_date').val();
            }
            else
            {
                var order_from_date = 0;
            }



            if (order_to_date != '')
            {
                var order_to_date = $('#order_to_date').val();
            }
            else
            {
                var order_to_date = 0;
            }

            if (delivery_from__date != '')
            {
                var delivery_from__date = $('#delivery_from__date').val();
            }
            else
            {
                var delivery_from__date = 0;
            }


            if (delivery_to_date != '')
            {
                var delivery_to_date = $('#delivery_to_date').val();
            }
            else
            {
                var delivery_to_date = 0;
            }



            if (order_number != '')
            {
                var order_number = $('#order_number').val();
            }
            else
            {
                var order_number = 0;
            }

         


            $.get(site_url + '/tailors/orders/delivered-search-list/' + order_from_date + '/' + order_to_date + '/' + delivery_from__date + '/' +delivery_to_date + '/' + order_number, function(data){

                 orderList(data);

                // console.log(data);

                if (data == '') {
                    $("#order_list").append( "<span style='font-size: 25px;'>No data found</span>");
                  }else 
                  {
                    
                    $("#order_list").append(data);
                  }




             
            });
        }
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function orderList(data)
        {
            var order_list = '';
            var sl_no        = 1;
            $.each(data, function(i, order_data)
            {
                var serial              = parseFloat(i) + 1;
                var site_url            = $('.site_url').val();
                var edit_url            = site_url + '/tailors/orders/edit/' + order_data.id;
                var print_url           = site_url + '/tailors/orders/print/' + order_data.id;
                var view_url            = site_url + '/tailors/orders/singleview/' + order_data.id;
                var print_invoice       = site_url + '/tailors/orders/printinvoice/' + order_data.id;

                if (order_data.status == undefined)
                {
                    var status = '';
                }

                if (order_data.status == 1)
                {
                    var status = 'Order Received';
                }

                if (order_data.status == 2)
                {
                    var status = 'Transferred to factory';
                }

                if (order_data.status == 3)
                {
                    var status = 'Sewing completed';
                }

                 if (order_data.status == 4)
                {
                    var status = 'Received from factory';
                }  

                if (order_data.status == 5)
                {
                    var status = 'Delivered completed';
                }

                if (order_data.status == 6)
                {
                    var status = 'Order Canceled';
                }

                if (order_data.priority_id == undefined)
                {
                    var priority = '';
                }

                if (order_data.priority_id == 1)
                {
                    var priority = 'Normal';
                }

                if (order_data.priority_id == 2)
                {
                    var priority = 'High';
                }

                if (order_data.priority_id == 3)
                {
                    var priority = 'Argent';
                }

                if (order_data.assign_to_name != null)
                {
                    var assignedPerson = order_data.assign_to_name;
                }
                else
                {
                    var assignedPerson = '';
                }

                if (order_data.mobile_number != null)
                {
                    var mobile_number = order_data.mobile_number;
                }
                else
                {
                    var mobile_number = '0';
                }



                if (order_data.submission_amount != null ) {
                   var submission_amount =  parseFloat(order_data.submission_amount)
                }else
                {
                    var submission_amount = '0';
                }

                if (order_data.total_amount != null ) {
                   var total_amount =  parseFloat(order_data.total_amount)
                }else
                {
                    var total_amount = '0';
                }


                if (order_data.due_amount != null ) {
                   var due_amount =  parseFloat(order_data.due_amount)
                }else
                {
                    var due_amount = '0';
                }

                order_list += '<tr>' +
                                    '<td>' +
                                        sl_no +
                                    '</td>' +
                                    '<td>' +
                                        'OR - ' + order_data.order_number.padStart(6, '0') +
                                    '</td>' +
                                    '<td>' +
                                        order_data.name + 
                                    '</td>' +
                                    '<td>' +
                                       formatDate(order_data.order_date) +
                                    '</td>' +
                                    '<td>' +
                                       formatDate(order_data.delivery_date) +
                                    '</td>' +

                                    '<td>' +
                                       status +
                                    '</td>' +
                                    '<td>' +
                                        '<div class="dropdown">' +
                                            '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                            '</a>' +
                                            '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                               
                                                '<a class="dropdown-item order-action" href="' + view_url +'">' + '<i class="fa fa-bullseye" aria-hidden="true"></i> Order View' + '</a>' +

                                                 '<a class="dropdown-item order-action" href="' + print_invoice +'"target="_blank">' + '<i class="fa fa-print" aria-hidden="true"></i> Order Print' + '</a>' +

                                                 '<a class="dropdown-item order-action" href="' + print_url +'" target="_blank">' + '<i class="fa fa-book" aria-hidden="true"></i> Print Invoice' + '</a>' +
                                               
                                                
                                            '</div>' +
                                        '</div>' +
                                    '</td>' +
                                '</tr>';

                                sl_no++;
            });

            $("#order_list").empty();
            $("#order_list").append(order_list);
        }
    </script>

    <script type="text/javascript">
        function findOrder(order_id)
        {
            var site_url        = $('.site_url').val();
            $('.remove_field_payment').click();
            $('#add_field_button_payment').click();

            $.get(site_url + '/tailors/orders/make-order-payment/' + order_id, function(data){

                $("#receivable").empty();
                $("#receivable").val(data.total_amount);

                $("#received").empty();
                $("#received").val(parseFloat(data.total_amount) - parseFloat(data.due_amount));

                $("#dues").empty();
                $("#dues").val(data.due_amount);

                $("#customer_id").empty();
                $("#customer_id").val(data.customer_id);

                $("#find_order_id").empty();
                $("#find_order_id").val(order_id);

            });
        }

         function changeStatus(order_id)
        {
            console.log(order_id);
            $("#order_id").val(order_id);
        }




        function calculateDues()
        {
            var due_amount  = $("#dues").val();
            var amount      = $("#amount").val();

            if (parseFloat(amount) > parseFloat(due_amount))
            {
                $("#amount").val(due_amount);
            }
        }
    </script>

    <script type="text/javascript">
        var max_fields_payment       = 50;                           //maximum input boxes allowed
        var wrapper_payment          = $(".input_fields_wrap_payment");      //Fields wrapper
        var add_button_payment       = $(".add_field_button_payment");       //Add button ID
        var index_no_payment         = 1;

        //For apending another rows start
        var x = -1;
        $(add_button_payment).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields_payment)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var amount_paid_label   = '<label class="hidden-xs" for="productname">Amount Paid *</label>\n';
                    var paid_through_label  = '<label class="hidden-xs" for="productname">Paid Through *</label>\n';
                    var note_label          = '<label class="hidden-xs" for="productname">Note</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';
                }
                else
                {
                    var amount_paid_label   = '';
                    var paid_through_label  = '';
                    var note_label          = '';
                    var action_label        = '';
                }

                $('.getMultipleRowPayment').append(' ' + '<div style="margin-bottom: 0px !important" class="row di_payment_'+x+'">' +
                                                    '<div style="" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Amount Paid *</label>\n' +
                                                        amount_paid_label +
                                                        '<input type="text" name="amount_paid[]" class="inner form-control" id="amount_paid_'+x+'" value="0" required />\n' + 
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-4 col-md-14col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Paid Through *</label>\n' +
                                                        paid_through_label  +
                                                        '<select id="paid_through_'+x+'" style="cursor: pointer" name="paid_through[]" class="form-control paidThrough">\n' +
                                                            '@if(!empty($paid_accounts) && ($paid_accounts->count() > 0))\n' +
                                                            '@foreach($paid_accounts as $key => $paid_account)\n' +
                                                                '<option value="{{ $paid_account['id'] }}">{{ $paid_account['name'] }}</option>\n' +
                                                            '@endforeach\n' +
                                                            '@endif\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="" class="col-lg-4 col-md-4 col-sm-6 col-6 form-group">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Note</label>\n' +
                                                        note_label +
                                                        '<input type="text" name="note[]" class="inner form-control" id="note_'+x+'" placeholder="note"/>\n' + 
                                                    '</div>\n' + 
                                                    
                                                    '<div style="" class="col-lg-1 col-md-2 col-sm-6 col-6 form-group remove_field_payment" data-val="'+x+'">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<input type="button" class="btn btn-primary btn-block inner" value="Delete"/>\n' + 
                                                    '</div>\n' + 
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
                }                                    
        });
        //For apending another rows end

        $(wrapper_payment).on("click",".remove_field_payment", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_payment_'+x).remove(); x--;
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var order_id            = $("#find_order_id").val();
            var customer_id         = $("#customer_id").val();
            var receivable          = $("#receivable").val();
            var payment_date        = $("#payment_date").val();
            var note                = $("#note").val();
            var site_url            = $('.site_url').val();

            var amount_paid = [];
            $('input[name^="amount_paid"]').each(function() {
                amount_paid.push(this.value);
            })

            var paid_through = [];
            $('.paidThrough').each(function() {
                paid_through.push(this.value);
            })

            var note = [];
            $('input[name^="note"]').each(function() {
                note.push(this.value);
            })

            if (customer_id == '' || order_id == '' || receivable == '' || payment_date == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
    
                type:   'post',
                url:    site_url + '/tailors/orders/store-order-payment',
                data:   {  order_id : order_id,customer_id : customer_id, receivable : receivable, payment_date : payment_date, amount_paid : amount_paid, paid_through : paid_through, note : note, _token: '{{csrf_token()}}' },
    
                success: function (data) {
                    console.log(data);
                    if(data != 0)
                    {
                        $('#CloseButton').click();
                        $('.remove_field_payment').click();

                        var site_url  = $('.site_url').val();

                        $.get(site_url + '/tailors/orders/load-list', function(data){

                            orderList(data);

                        });

                        $('#success_message').show();
                    }
                    else
                    {
                        var site_url  = $('.site_url').val();

                        $.get(site_url + '/tailors/orders/load-list', function(data){

                            orderList(data);
                            location.reload();

                        });

                        $('#unsuccess_message').show();
                    }
                }
    
            });
            }
        });    
    </script>

    <script>
    $(document).ready(function(){
        $("#reload").click(function(){
            location.reload(true);
        });
    });
</script>
@endsection