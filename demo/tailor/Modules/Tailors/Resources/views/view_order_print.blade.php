 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<title></title>
 	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
  <style type="text/css">
    .logo{
    font-size: 24px;
    border: 1px solid #9e9e9e3d;
    padding: 5px;
    background-color: #f44336;
    border-radius: 10%;
    color: #fff;
    }
  </style>
 </head>
 <body>

 	<div class="container">

    <div id="printMe" class="mb-2">

    <div class="row mt-3">
      <div class="col-md-6">
        <span class="logo">লেবাস</span>
      </div>
      <div class="col-md-6" style="text-align: right;">
        
        <span>Tolarbag (Panir Tank Goli), Mirpur-1, Dhaka-1216</span><br>
        <span>Print Date : {{ date('d-m-Y') }}</span>
      </div>
    </div>
      <hr>

 		<div class="card">
      <div class="card-header" style="text-align: center;">
        <strong>Statement of Orders</strong>
      </div>
      <div class="card-body">
        <table class="table table-bordered table-hover">
  <thead class="thead-light">
    <tr>
      <th scope="col">Order Date</th>
      <th scope="col">Deli. Date</th>
      <th scope="col">Order No.</th>
      <th scope="col">Customer Name</th>
      <th scope="col">Payable Amt</th>
      <th scope="col">Paid Amt</th>
      <th scope="col">Due Amt</th>
      <th scope="col">Priority</th>
    </tr>
  </thead>
  <tbody>
    @foreach($data as $item)
    <tr>
      <td>{{$item->order_date}}</td>
      <td>{{$item->delivery_date}}</td>
      <td>{{$item->order_date}}</td>
      <td>{{$item->name}}</td>
   
      <td>{{$item->submission_amount}}</td>
      <td>{{$item->total_amount}}</td>
      <td>{{$item->due_amount}}</td>
      <td>
        @if($item->priority_id == 1)
        Normal
        @elseif($item->priority_id == 2)
        Urgent
        @elseif($item->priority_id == 3)
        Very Urgent
        @endif
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
      </div> 
    </div>
    </div>

    <button onclick="printDiv('printMe')" type="button" class="float-right btn btn-success">Print PDF</button>
 	</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

<script type="text/javascript">
  function printDiv(divName){
      var printContents = document.getElementById(divName).innerHTML;
      var originalContents = document.body.innerHTML;

      document.body.innerHTML = printContents;

      window.print();

      document.body.innerHTML = originalContents;

    }
</script>
 
 </body>
 </html>