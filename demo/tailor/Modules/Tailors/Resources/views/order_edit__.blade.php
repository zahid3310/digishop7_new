@extends('layouts.app')

@section('title', 'Edit Order')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('tailors_order_update', $find_order['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                {{ csrf_field() }}

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Customer *</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="customer_id" name="customer_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9" required>
                                                   <option value="{{ $find_order['customer_id'] }}" selected>{{ $find_order['contact_name'] }}</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Reference</label>
                                            <div class="col-md-8">
                                                <select style="width: 75%" id="reference_id" name="reference_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option value="{{ customersTableDetails($find_order['reference_id'])['id'] }}" selected>{{ customersTableDetails($find_order['reference_id'])['name'] }}</option>
                                                </select>
                                                <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" data-toggle="modal" data-target="#myModal1">
                                                    <i class="bx bx-plus font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Assign To</label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="assign_to_id" name="assign_to_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <option value="{{ customersTableDetails($find_order['assign_to'])['id'] }}" selected>{{ customersTableDetails($find_order['assign_to'])['name'] }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Priority </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="priority_id" name="priority_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option {{ $find_order['priority_id'] == 1 ? 'selected' : '' }} value="1">Normal</option>
                                                   <option {{ $find_order['priority_id'] == 2 ? 'selected' : '' }} value="2">High</option>
                                                   <option {{ $find_order['priority_id'] == 3 ? 'selected' : '' }} value="3">Argent</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Order Date *</label>
                                            <div class="col-md-7">
                                                <input id="order_date" name="order_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['order_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-5 col-form-label">Delivery Date *</label>
                                            <div class="col-md-7">
                                                <input id="delivery_date" name="delivery_date" type="text" value="{{ date('d-m-Y', strtotime($find_order['delivery_date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Status </label>
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="status" name="status" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                   <option {{ $find_order['status'] == 1 ? 'selected' : '' }} value="1">Preparing</option>
                                                   <option {{ $find_order['status'] == 1 ? 'selected' : '' }} value="2">Ready</option>
                                                   <option {{ $find_order['status'] == 1 ? 'selected' : '' }} value="3">Completed</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 415px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-9 input_fields_wrap getMultipleRow">
                                        @foreach($find_order_entries as $key => $value)
                                            <div style="margin-bottom: 0px !important" class="row di_{{$key}}">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Service *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Service *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                        @if(!empty($services) && ($services->count() > 0))
                                                        @foreach($services as $key1 => $service)
                                                            <option value="{{ $service['id'] }}" {{ $value['service_id'] == $service['id'] ? 'selected' : '' }}>{{ $service['name'] }}</option>
                                                        @endforeach
                                                        @endif
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Qty *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_{{$key}}" value="{{$value['quantity']}}" placeholder="Quantity" oninput="calculateActualAmount({{$key}})" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Rate *</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control" id="rate_{{$key}}" value="{{$value['rate']}}" placeholder="Rate" oninput="calculateActualAmount({{$key}})" />
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">
                                                    
                                                    <div class="row">
                                                        <div  style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" style="padding-top: 15px" for="productname"></label>
                                                            @endif
                                                            <label class="show-xs" style="display: none;padding-top: 17px" for="productname"></label>
                                                            <select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_{{$key}}" oninput="calculateActualAmount({{$key}})">
                                                                <option value="1" {{ $value['discount_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                                <option value="0" {{ $value['discount_type'] == 0 ? 'selected' : '' }}>%</option>
                                                            </select>
                                                        </div>

                                                        <div  style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">
                                                            @if($key == 0)
                                                                <label class="hidden-xs" style="padding-bottom: 0px" for="productname">Discount</label>
                                                            @endif
                                                            <label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>
                                                            <input type="text" name="discount[]" class="inner form-control" id="discount_{{$key}}" value="{{$value['discount_amount']}}" placeholder="Discount" oninput="calculateActualAmount({{$key}})"/>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Total</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Total</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_{{$key}}" value="{{$value['total_amount']}}" placeholder="Total"/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Description</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Description</label>
                                                    <textarea name="description[]" class="inner form-control" id="description_{{$key}}"> {{ $value['description'] }} </textarea>
                                                </div>
                                                
                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    @if($key == 0)
                                                        <label class="hidden-xs" for="productname">Action</label>
                                                    @endif
                                                    <label style="display: none" class="show-xs" for="productname">Action *</label>
                                                    @if($key == 0)
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner add_field_button"></i>
                                                    @else
                                                        <i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner remove_field" data-val="{{$key}}"></i>
                                                    @endif 
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                    <div style="display: none">
                                        <select style="padding: 6px;border-radius: 4px;cursor: pointer" class="taxType" id="tax_type_0" name="tax_type" oninput="calculateActualAmount(0)">
                                            <option style="padding: 10px" value="1" {{ $find_order['tax_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                            <option style="padding: 10px" value="0" {{ $find_order['tax_type'] == 0 ? 'selected' : '' }}>%</option>
                                        </select>
                                        <input id="tax_amount_0" type="text" class="form-control width-xs taxAmount" name="tax_amount" value="{{ $find_order['total_tax'] }}" oninput="calculateActualAmount(0)">
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 415px;padding-top: 20px" class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                            <div class="col-md-7">
                                                <input type="text" id="subTotalBdt" class="form-control">
                                                <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">VAT</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control" id="vat_type_0" name="vat_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" {{ $find_order['vat_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                    <option style="padding: 10px" value="0" {{ $find_order['vat_type'] == 0 ? 'selected' : '' }}>%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="vat_amount_0" type="text" class="form-control width-xs" name="vat_amount" value="{{ $find_order['total_vat'] }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Discount</label>

                                            <div style="padding-right: 0px" class="col-md-3">
                                                <select style="border-radius: 4px;cursor: pointer" class="form-control totalDiscountType" id="total_discount_type_0" name="total_discount_type" oninput="calculateActualAmount(0)">
                                                    <option style="padding: 10px" value="1" {{ $find_order['total_discount_type'] == 1 ? 'selected' : '' }}>BDT</option>
                                                    <option style="padding: 10px" value="0" {{ $find_order['total_discount_type'] == 0 ? 'selected' : '' }}>%</option>
                                                </select>
                                            </div>

                                            <div style="padding-left: 0px" class="col-md-4">
                                                <input id="total_discount_0" type="text" class="form-control totalDiscount" name="total_discount_amount" value="{{ $find_order['total_discount_amount'] }}" oninput="calculateActualAmount(0)">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Dis. Note</label>
                                            <div class="col-md-7">
                                                <input id="total_discount_note" type="text" class="form-control width-xs" name="total_discount_note" value="{{ $find_order['total_discount_note'] }}" placeholder="Discount Note">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Coupon</label>
                                            <div class="col-md-7">
                                                <input id="coupon_code" type="number" class="form-control width-xs couponCode" name="coupon_code" onchange="couponMembership()" value="{{ $find_order['discount_code'] }}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Total Payable</label>
                                            <div class="col-md-7">
                                                <input type="text" id="totalBdt" class="form-control">
                                                <input style="display: none" type="text" id="totalBdtShow" name="total_amount" readonly>
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Cash Given</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="cash_given" name="cash_given" value="{{ $find_order['cash_given'] }}" oninput="calculateChangeAmount()">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Change</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="change_amount" name="change_amount" value="{{ $find_order['change_amount'] }}">
                                            </div>
                                        </div>

                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">Send SMS</label>
                                            <div class="col-md-12">
                                                <div style="padding-left: 0px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Non Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Masking
                                                    </label>
                                                </div>

                                                <div style="padding-left: 10px" class="form-check form-check-right">
                                                    <input style="margin-top: 0px !important" class="form-check-input" type="checkbox" id="defaultCheck2">
                                                    <label style="font-size: 14px;margin-top: -5px " class="form-check-label" for="defaultCheck2">
                                                        Voice
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>

                                    <div style="display: none;" class="row justify-content-end">
                                        <div class="col-lg-1 col-md-2 form-group">
                                            <input id="pos_add_button" type="button" class="btn btn-success btn-block inner add_field_button_pos" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important" class="row">
                                    <div style="background-color: #D2D2D2;height: 115px;padding-top: 32px" class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-12 ">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('invoices_index') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button_payment" type="button" class="btn btn-success btn-block inner add_field_button_payment" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Customer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type" name="contact_type" type="hidden" value="0" class="form-control">
                </div>
                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Reference</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">
                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name1" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number1" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address1" name="address" type="text" class="form-control">
                        </div>
                    </div>

                    <input id="contact_type1" name="contact_type" type="hidden" value="3" class="form-control">
                </div>
                
                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#reference_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 3)
                    {
                        return result['text'];
                    }
                },
            });

            $("#assign_to_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 2)
                    {
                        return result['text'];
                    }
                },
            });

            calculateActualAmount();
        });
    </script>

    <script type="text/javascript">
        function ProductEntriesList(x) 
        {
            //For getting item commission information from items table start
            var site_url = $(".site_url").val();

            $.get(site_url + '/tailors/orders/service-list/', function(data){
                var list5 = '';
                var list7 = '';

                $.each(data, function(i, service_data)
                {
                    list5 += '<option value = "' +  service_data.id + '">' + service_data.name  + '</option>';
                });

                list7 += '<option value = "">' + '--Select Service--' +'</option>';

                $("#product_entries_"+x).empty();
                $("#product_entries_"+x).append(list7);
                $("#product_entries_"+x).append(list5);
            });
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/tailors/orders/get-service-price/'+ entry_id, function(data){

                    $("#rate_"+x).val(data.price);
                    $("#quantity_"+x).val(1);
                    $("#discount_"+x).val(0);
      
                    calculateActualAmount(x);

                });
            }
            // calculateActualAmount(x);
        }

        function pad (str, max)
        {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = $("#contact_type").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });

        $('#submitBtn1').click(function() {
            
            var customer_name               = $("#customer_name1").val();
            var address                     = $("#address1").val();
            var mobile_number               = $("#mobile_number1").val();
            var contact_type                = $("#contact_type1").val();
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                        }
                        
                        $("#reference_id").empty();
                        $('#reference_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });    
    </script>

    <script type="text/javascript">
        var max_fields       = 50;                           //maximum input boxes allowed
        var wrapper          = $(".input_fields_wrap");      //Fields wrapper
        var add_button       = $(".add_field_button");       //Add button ID
        var add_button_pos   = $(".add_field_button_pos");   //Add button ID
        var index_no         = 1;

        //For apending another rows start
        var x = {{$entries_count}};
        $(add_button).click(function(e)
        {
            e.preventDefault();

            // var x = parseInt($('.getMultipleRow:last').attr('class').match(/(\d+)/g)[0]);

            if(x < max_fields)
            {   
                x++;

                var serial = x + 1;

                if (serial == 1)
                {
                    var product_label       = '<label class="hidden-xs" for="productname">Product *</label>\n';
                    var description_label   = '<label class="hidden-xs" for="productname">Description</label>\n';
                    var quantity_label      = '<label class="hidden-xs" for="productname">Qty *</label>\n';
                    var rate_label          = '<label class="hidden-xs" for="productname">Rate *</label>\n';
                    var discount_label      = '<label class="hidden-xs" style="padding-bottom: 0px;" for="productname">Discount</label>\n';
                    var type_label          = '<label class="hidden-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n';
                    var amount_label        = '<label class="hidden-xs" for="productname">Total</label>\n';
                    var action_label        = '<label class="hidden-xs" for="productname">Action</label>\n';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group add_field_button">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-plus btn btn-success btn-block inner" onclick="addButton()"></i>' +
                                                '</div>\n';
                }
                else
                {
                    var product_label       = '';
                    var description_label   = '';
                    var rate_label          = '';
                    var quantity_label      = '';
                    var discount_label      = '';
                    var type_label          = '';
                    var amount_label        = '';
                    var action_label        = '';

                    var add_btn             =   '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6 form-group remove_field" data-val="'+x+'">\n' + 
                                                    '<label style="display: none" class="show-xs" for="productname">Action</label>\n' +
                                                        action_label +
                                                        '<i style="padding: 0.68rem 0.75rem !important" class="fas fa-trash btn btn-danger btn-block inner"></i>'+
                                                '</div>\n';
                }

                ProductEntriesList(x);

                $('.getMultipleRow').append(' ' + '<div class="row di_'+x+'">' +
                                                    '<div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-12">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<select style="width: 100%" name="product_entries[]" class="inner form-control single_select2 productEntries" id="product_entries_'+x+'" onchange="getItemPrice('+x+')" required>\n' +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty *</label>\n' +
                                                        quantity_label  +
                                                        '<input type="text" name="quantity[]" class="inner form-control" id="quantity_'+x+'" placeholder="Quantity" oninput="calculateActualAmount('+x+')" required />\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Rate *</label>\n' +
                                                        rate_label +
                                                        '<input type="text" name="rate[]" class="inner form-control" id="rate_'+x+'" placeholder="Rate" oninput="calculateActualAmount('+x+')" />\n' + 
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12">\n' +
                                                        '<div class="row">' +
                                                            '<div style="padding-right: 0px;padding-bottom: 0px" class="col-lg-5 col-md-5 col-sm-6 col-6">\n' +
                                                                '<label style="display: none" class="show-xs margin-top-25-xs" style="padding-top: 13px" for="productname"></label>\n' +
                                                                type_label +
                                                                '<select style="cursor: pointer" name="discount_type[]" class="form-control" id="discount_type_'+x+'" oninput="calculateActualAmount('+x+')">\n' +
                                                                    '<option value="1">BDT</option>' +
                                                                    '<option value="0" selected>%</option>' +
                                                                '</select>\n' +
                                                            '</div>\n' +

                                                            '<div style="padding-left: 10px;padding-bottom: 0px" class="col-lg-7 col-md-7 col-sm-6 col-6">\n' +
                                                                '<label class="show-xs" style="padding-bottom: 0px;display: none" for="productname">Discount</label>\n' +
                                                                discount_label +
                                                                '<input type="text" name="discount[]" class="inner form-control" id="discount_'+x+'" placeholder="Discount" oninput="calculateActualAmount('+x+')"/>\n' +
                                                            '</div>\n' +
                                                        '</div>\n' +
                                                    '</div>\n' +

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">\n' + 
                                                        '<label style="display: none" class="show-xs" for="productname">Total</label>\n' +
                                                        amount_label +
                                                        '<input type="text" name="amount[]" class="inner form-control amount" id="amount_'+x+'" placeholder="Total"/>\n' + 
                                                    '</div>\n' + 

                                                    '<div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Description</label>\n' +
                                                        description_label +
                                                        '<textarea name="description[]" class="inner form-control" id="description_'+x+'" />\n' + '</textarea>'  + 
                                                    '</div>\n' +
                                                    
                                                    add_btn +
                                                    
                                                '</div>\n' 
                                            );

                                            $('.single_select2').select2();
            }                                    
        });
        //For apending another rows end

        $(wrapper).on("click",".remove_field", function(e)
        {
            e.preventDefault();

            var x = $(this).attr("data-val");

            $('.di_'+x).remove(); x--;

            calculateActualAmount(x);
        });

        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#quantity_"+x).val();
            var discount                = $("#discount_"+x).val();
            var discountType            = $("#discount_type_"+x).val();
            var vatType                 = $("#vat_type_0").val();
            var vatAmount               = $("#vat_amount_0").val();
            var taxType                 = $("#tax_type_0").val();
            var taxAmount               = $("#tax_amount_0").val();
            var totalDiscount           = $("#total_discount_0").val();
            var totalDiscountType       = $("#total_discount_type_0").val();

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#quantity_"+x).val();
            }

            if (discount == '')
            {
                var discountCal         = 1;
            }
            else
            {
                var discountCal         = $("#discount_"+x).val();
            }

            if (discount == '')
            {
                var discountTypeCal     = 0;
            }
            else
            {
                if (discountType == 0)
                {
                    var discountTypeCal     = (parseFloat(discountCal)*parseFloat(rateCal)*parseFloat(quantityCal))/100;
                }
                else
                {
                    var discountTypeCal     = $("#discount_"+x).val();
                }
            }

            var AmountIn              =  (parseFloat(rateCal)*parseFloat(quantityCal)) - parseFloat(discountTypeCal);
     
            $("#amount_"+x).val(AmountIn);

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total);
            $("#subTotalBdtShow").val(total);

            if (vatAmount == '')
            {   
                $("#vat_amount_0").val(0);
                var vatCal         = 0;
            }
            else
            {
                var vatCal         = $("#vat_amount_0").val();
            }

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(vatCal)*(parseFloat(total)))/100;
            }
            else
            {
                var vatTypeCal     = $("#vat_amount_0").val();
            }

            if (taxAmount == '')
            {   
                $("#tax_amount_0").val(0);
                var taxCal         = 0;
            }
            else
            {
                var taxCal         = $("#tax_amount_0").val();
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(taxCal)*(parseFloat(total)))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            if (totalDiscount > 0)
            {   
                if (totalDiscountType == 0)
                {
                    var totalDiscountTypeCal     = (parseFloat(totalDiscount)*(parseFloat(total) + parseFloat(vatTypeCal)))/100;
                }
                else
                {
                    var totalDiscountTypeCal     = $("#total_discount_0").val();
                }
            }
            else
            {
                var totalDiscountTypeCal     = 0;
            }

            var totalShow = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountTypeCal);

            $("#totalBdtShow").val(totalShow);
            $("#totalBdt").val(parseFloat(totalShow));
            $("#amount_paid_0").val(parseFloat(totalShow));
        }
    </script>

    <script type="text/javascript">
        $(document).on("change", "#defaultCheck2" , function() {

            var checkbox_value    = $("#defaultCheck2")[0].checked;

            if (checkbox_value == true)
            {
                $("#vatShow").show();
                $("#taxShow").show();
            }
            else
            {
                $("#vatShow").hide();
                $("#taxShow").hide();
            }
        });

        function couponMembership()
        {
            var site_url      = $(".site_url").val();
            var coupon_code   = $("#coupon_code").val();
    
            $('.DiscountType').val(1);
            $('.DiscountAmount').val(0);

            $.get(site_url + '/invoices/search/coupon-code/'+ coupon_code, function(data_coupon){

                if (data_coupon == '')
                {
                    alert('Invalid Coupon Code or Membership Card !!');
                    $('#coupon_code').val('');
                }
                else
                {
                    var state = 0;

                    $('.productEntries').each(function()
                    {
                        var entry_id    = $(this).val();
                        var value_x     = $(this).prop("id");
                        
                        if (data_coupon[0].product_id != null)
                        {
                            for (var i = data_coupon.length - 1; i >= 0; i--)
                            {   
                                if (data_coupon[i].product_id == entry_id)
                                {
                                    var explode    = value_x.split('_');
                                    var di_id      = explode[2];

                                    $('#discount_type_'+di_id).val(data_coupon[i].discount_type);
                                    $('#discount_'+di_id).val(data_coupon[i].discount_amount);
                                    
                                    calculateActualAmount(di_id);

                                    state++;
                                }
                            }
                        }
                        else
                        {
                            var explode    = value_x.split('_');
                            var di_id      = explode[2];

                            $('#discount_type_'+di_id).val(data_coupon[0].discount_type);
                            $('#discount_'+di_id).val(data_coupon[0].discount_amount);
                            
                            calculateActualAmount(di_id);
                        }    
                    });
                }
            });
        }

        function calculateChangeAmount()
        {
            var totalAmount     = $("#totalBdtShow").val();
            var cashGiven       = $("#cash_given").val();
            var changeAmount    = parseFloat(cashGiven) - parseFloat(totalAmount);

            if (changeAmount > 0)
            {   
                $("#change_amount").val(0);
                $("#change_amount").val(changeAmount);
            }

            if (changeAmount < 0)
            {   
                $("#change_amount").val(0);
            }

            if (changeAmount == '')
            {
                $("#change_amount").val(0);
            }
        }

        $(document).on("change", "#customer_id" , function() {

            var site_url        = $('.site_url').val();
            var customerId      = $("#customer_id").val();

            $.get(site_url + '/invoices/calculate-opening-balance/' + customerId, function(data){

                $("#balance").empty();
                $("#balance").val(data);
                
            });
        });
    </script>
@endsection