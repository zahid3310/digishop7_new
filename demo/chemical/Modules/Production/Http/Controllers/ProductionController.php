<?php

namespace Modules\Production\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Units;
use App\Models\BillEntries;
use App\Models\Branches;
use App\Models\ProductVariations;
use App\Models\ProductVariationValues;
use App\Models\ProductVariationEntries;
use App\Models\ProductSuppliers;
use App\Models\ProductCustomers;
use App\Models\ProductionItems;
use App\Models\ProductionProductChemist;
use App\Models\Customers;
use App\Models\Productions;
use App\Models\ProductionEntries;
use App\Models\Expenses;
use App\Models\PaidThroughAccounts;
use DB;
use Response;

class ProductionController extends Controller
{
    public function index()
    {
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

        return view('production::index', compact('paid_accounts'));
    }

    public function allProduction()
    {
        return view('production::all_productions');
    }

    public function create()
    {
        return view('production::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'selling_date'     => 'required',
            'reference_id'     => 'required',
            'order_id'         => 'required',
            'chemist_id'       => 'required',
            'product_id.*'     => 'required',
            'amount.*'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
   
        DB::beginTransaction();

        try{
            $production                     = new Productions;
            $production->buyer_id           = $data['reference_id'];
            $production->order_id           = $data['order_id'];
            $production->chemist_id         = $data['chemist_id'];
            $production->date               = date('Y-m-d', strtotime($data['selling_date']));
            $production->amount             = $data['sub_total_amount'];
            $production->paid_amount        = $data['total_paid_amount'] != null ? $data['total_paid_amount'] : 0;
            $production->note               = $data['production_note'];
            $production->created_by         = $user_id;

            if ($production->save())
            {   
                foreach ($data['product_id'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'     => $production->id,
                        'product_id'        => $value,
                        'buyer_id'          => $data['reference_id'],
                        'chemist_id'        => $data['chemist_id'],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                if (isset($data['amount_paid']))
                {
                   $data_find                       = Expenses::orderBy('created_at', 'DESC')->first();
                    $expense_number                 = $data_find != null ? $data_find['expense_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $expenses = [
                                'expense_number'        => $expense_number,
                                'expense_category_id'   => 1,
                                'production_id'         => $production->id,
                                'expense_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'salary_type'           => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            DB::table('expenses')->insert($expenses);      

                            // $update_production_dues      = Productions::find($production->id);
                            // $update_production_dues->due = $update_production_dues['paid_amount'] + $data['amount_paid'][$i];
                            // $update_production_dues->save();

                            $expense_number++;
                        }
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Production Created Successfully !!");
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('production::show');
    }

    public function edit($id)
    {
        $find_production        = Productions::leftjoin('customers AS buyer', 'buyer.id', '=', 'productions.buyer_id')
                                                ->leftjoin('customers AS chemist', 'chemist.id', '=', 'productions.chemist_id')
                                                ->leftjoin('orders', 'orders.id', 'productions.order_id')
                                                ->selectRaw('productions.*,
                                                             buyer.name as buyer_name,
                                                             chemist.name as chemist_name,
                                                             orders.order_number as order_number')
                                                ->find($id);

        $production_entries     = ProductionEntries::leftjoin('production_items', 'production_items.id', 'production_entries.product_id')
                                                ->where('production_entries.production_id', $id)
                                                ->selectRaw('production_entries.*,
                                                             production_items.name as product_name')
                                                ->get();  

        $entries_count          = $production_entries->count();

        $expenses               = Expenses::where('expenses.production_id', $id)
                                        ->selectRaw('expenses.*')
                                        ->get();

        $expenses_count         = $expenses->count();
        $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();

        return view('production::edit', compact('find_production', 'production_entries', 'entries_count', 'expenses', 'expenses_count', 'paid_accounts'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'selling_date'     => 'required',
            'reference_id'     => 'required',
            'order_id'         => 'required',
            'chemist_id'       => 'required',
            'product_id.*'     => 'required',
            'amount.*'         => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $production                     = productions::find($id);
            $production->buyer_id           = $data['reference_id'];
            $production->order_id           = $data['order_id'];
            $production->chemist_id         = $data['chemist_id'];
            $production->date               = date('Y-m-d', strtotime($data['selling_date']));
            $production->amount             = $data['sub_total_amount'];
            $production->paid_amount        = $data['total_paid_amount'] != null ? $data['total_paid_amount'] : 0;
            $production->note               = $data['producion_note'];
            $production->updated_by         = $user_id;

            if ($production->save())
            {   
                $delete_entries   = ProductionEntries::where('production_id', $id)->delete();
                $delete_payments  = Expenses::where('production_id', $production->id)->delete();
                $delete_expenses  = Expenses::where('production_id', $production->id)->delete();

                foreach ($data['product_id'] as $key => $value)
                {
                    $production_entries[] = [
                        'production_id'     => $production['id'],
                        'product_id'        => $value,
                        'buyer_id'          => $data['reference_id'],
                        'chemist_id'        => $data['chemist_id'][$key],
                        'rate'              => $data['rate'][$key],
                        'quantity'          => $data['quantity'][$key],
                        'total_amount'      => $data['amount'][$key],
                        'created_by'        => $user_id,
                        'created_at'        => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                if (isset($data['amount_paid']))
                {
                   $data_find                       = Expenses::orderBy('created_at', 'DESC')->first();
                    $expense_number                 = $data_find != null ? $data_find['expense_number'] + 1 : 1;

                    for($i = 0; $i < count($data['amount_paid']); $i++)
                    {   
                        if ($data['amount_paid'][$i] > 0)
                        {
                            $expenses = [
                                'expense_number'        => $expense_number,
                                'expense_category_id'   => 1,
                                'production_id'         => $production->id,
                                'expense_date'          => date('Y-m-d', strtotime($data['selling_date'])),
                                'amount'                => $data['amount_paid'][$i],
                                'account_information'   => $data['account_information'][$i],
                                'paid_through_id'       => $data['paid_through'][$i],
                                'note'                  => $data['note'][$i],
                                'salary_type'           => 0,
                                'created_by'            => $user_id,
                                'created_at'            => date('Y-m-d H:i:s'),
                            ];

                            DB::table('expenses')->insert($expenses);      

                            // $update_production_dues      = Productions::find($production->id);
                            // $update_production_dues->due = $update_production_dues['paid_amount'] + $data['amount_paid'][$i];
                            // $update_production_dues->save();

                            $expense_number++;
                        }
                    }
                }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Production Updated Successfully !!");
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $products  = ProductionItems::orderBy('created_at', 'DESC')->get();
        $chemists  = Customers::where('contact_type', 2)->get();

        return Response::json([
            'products' => $products,
            'chemists' => $chemists,
        ]);
    }

    public function chemistRateList($chemist_id, $product_id)
    {
        $rates          = ProductionProductChemist::where('chemist_id', $chemist_id)
                                        ->where('product_id', $product_id)
                                        ->orderBy('created_at', 'DESC')
                                        ->get();

        $default_rate   = ProductionItems::find($product_id);

        return Response::json([
            'rates'         => $rates,
            'default_rate'  => $default_rate
        ]);
    }

    public function productListLoad()
    {
        $productions  = Productions::leftjoin('customers AS buyer', 'buyer.id', '=', 'productions.buyer_id')
                                    ->leftjoin('customers AS chemist', 'chemist.id', '=', 'productions.chemist_id')
                                    ->leftjoin('orders', 'orders.id', 'productions.order_id')
                                    ->orderBy('created_at', 'DESC')
                                    ->selectRaw('productions.*,
                                                 buyer.name as buyer_name,
                                                 chemist.name as chemist_name,
                                                 orders.order_number as order_number')
                                    ->get();

        return Response::json([
            'productions' => $productions
        ]);
    }

    public function productionSearchList($from_date, $to_date, $chemist_id, $buyer_id)
    {   
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_chemist      = $chemist_id != 0 ? $chemist_id : 0;
        $search_by_buyer        = $buyer_id != 0 ? $buyer_id : 0;

        $productions  = Productions::leftjoin('customers AS buyer', 'buyer.id', '=', 'productions.buyer_id')
                                    ->leftjoin('customers AS chemist', 'chemist.id', '=', 'productions.chemist_id')
                                    ->leftjoin('orders', 'orders.id', 'productions.order_id')
                                    ->whereBetween('productions.date', [$search_by_from_date, $search_by_to_date])
                                    ->when($search_by_chemist != 0, function ($query) use ($search_by_chemist) {
                                        return $query->where('chemist.id', $search_by_chemist);
                                    })
                                    ->when($search_by_buyer != 0, function ($query) use ($search_by_buyer) {
                                        return $query->where('buyer.id', $search_by_buyer);
                                    })
                                    ->orderBy('created_at', 'DESC')
                                    ->selectRaw('productions.*,
                                                 buyer.name as buyer_name,
                                                 chemist.name as chemist_name,
                                                 orders.order_number as order_number')
                                    ->get();

        return Response::json([
            'productions' => $productions
        ]);
    }

    //Product Functions

    public function productIndex()
    {
        $units      = Units::orderBy('id', 'ASC')->get();
        $products   = ProductionItems::leftjoin('units', 'units.id', 'production_items.unit_id')
                                        ->orderBy('production_items.id', 'ASC')
                                        ->selectRaw('production_items.*, units.name as unit_name')
                                        ->get();

        return view('production::product_index', compact('units', 'products'));
    }

    public function productStore(Request $request)
    {
        $rules = array(
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product                    = new ProductionItems;
            $product->name              = $data['product_name'];
            $product->production_cost   = $data['production_cost'];

            if ($data['unit_id']        != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->status            = $data['status'];
            $product->created_by        = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return back()->with("success","Product Added Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function productEdit($id)
    {
        $units          = Units::orderBy('id', 'ASC')->get();
        $products       = ProductionItems::leftjoin('units', 'units.id', 'production_items.unit_id')
                                        ->orderBy('production_items.id', 'ASC')
                                        ->selectRaw('production_items.*, units.name as unit_name')
                                        ->get();

        $find_product   = ProductionItems::leftjoin('units', 'units.id', 'production_items.unit_id')
                                        ->orderBy('production_items.id', 'ASC')
                                        ->selectRaw('production_items.*, units.name as unit_name')
                                        ->find($id);

        return view('production::product_edit', compact('units', 'products', 'find_product'));
    }

    public function productUpdate(Request $request, $id)
    {
        $rules = array(
            'product_name'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id            = Auth::user()->id;
        $data               = $request->all();

        DB::beginTransaction();

        try{
            $product                    = ProductionItems::find($id);
            $product->name              = $data['product_name'];
            $product->production_cost   = $data['production_cost'];

            if ($data['unit_id']        != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->status            = $data['status'];
            $product->updated_by        = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return redirect()->route('production_product_index')->with("success","Product Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function getChemistList($product_id)
    {
        $chemists    = ProductionProductChemist::leftjoin('customers', 'customers.id', 'production_product_chemist.chemist_id')
                                        ->leftjoin('production_items', 'production_items.id', 'production_product_chemist.product_id')
                                        ->where('production_product_chemist.product_id', $product_id)
                                        ->selectRaw('production_product_chemist.*, customers.name as chemist_name, production_items.name as product_name')
                                        ->get();

        return Response::json($chemists);
    }

    public function chemistListUpdate(Request $request)
    {
        $rules = array(
            'product_id'   => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id      = Auth::user()->id;
        $data         = $request->all();

        DB::beginTransaction();

        try{
            $data_delete  = ProductionProductChemist::where('product_id', $data['product_id'])->delete();

            foreach ($data['chem_id'] as $key => $value)
            {
                $chemists[] = [
                    'product_id'   => $data['product_id'],
                    'chemist_id'   => $data['chem_id'][$key],
                    'rate'         => $data['rate'][$key],
                    'created_by'   => $user_id,
                    'created_at'   => date('Y-m-d H:i:s'),
                ];
            }

            if (isset($chemists))
            {
                DB::table('production_product_chemist')->insert($chemists);

                DB::commit();

                return redirect()->route('production_product_index')->with("success","Chemist List Updated Successfully !!");
            }

            DB::rollback();
            return redirect()->route('production_product_index')->with("unsuccess","Not Updated !!");

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
}
