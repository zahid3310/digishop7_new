@extends('layouts.app')

@section('title', 'Order')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Create Order</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Order</a></li>
                                    <li class="breadcrumb-item active">Create Order</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('orders_store') }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="row">
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Buyer *</label>
                                        <select style="width: 83%" id="customer_id" name="customer_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                           <option value="">--Select Buyer--</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group margin-bottom-10-xs">
                                        <label style="padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12 col-form-label">Product *</label>
                                        <select style="width: 83%" id="product_id" name="product_id" class="form-control select2 col-lg-10 col-md-10 col-sm-10 col-10" required>
                                           <option value="">--Select Product--</option>
                                        </select>

                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center;" class="form-control col-lg-2 col-md-2 col-sm-2 col-2" data-toggle="modal" data-target="#myModal1">
                                            <i class="bx bx-plus font-size-24"></i>
                                        </span>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label style="padding-left: 0px" class="col-lg-8 col-md-8 col-sm-8 col-8 col-form-label">Order Number *</label>
                                        <div style="padding-right: 0px;padding-left: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                            <input name="order_number" type="text" class="form-control" required />
                                        </div>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Order Date *</label>
                                        <input name="order_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label">Delivery Date *</label>
                                        <input name="delivery_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required />
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname" class="col-form-label"> Quantity</label>
                                        <input type="number" name="quantity" class="inner form-control" id="quantity" placeholder="Quantity" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-lg-12 col-md-12 col-sm-12 col-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('orders_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Orders</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL#</th>
                                            <th>Order#</th>
                                            <th>Buyer</th>
                                            <th>Product</th>
                                            <th>Order Date</th>
                                            <th>Delivery Date</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        @if(!empty($orders) && ($orders->count() > 0))
                                        @foreach($orders as $key => $order)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $order['order_number'] }}</td>
                                                <td>
                                                    {{ $order->buyer->name }}
                                                </td>
                                                <td>
                                                    {{ $order->product->name }}
                                                </td>
                                                <td>{{ date('d-m-Y', strtotime($order['order_date'])) }}</td>
                                                <td>{{ date('d-m-Y', strtotime($order['delivery_date'])) }}</td>
                                                <td>{{ $order['status'] == 1 ? 'Active' : 'Inactive' }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('orders_edit', $order['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Buyer</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="customer_name" name="customer_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Mobile Number</label>
                        <div class="col-md-12">
                            <input id="mobile_number" name="mobile_number" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Address</label>
                        <div class="col-md-12">
                            <input id="address" name="address" type="text" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="myModal1" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myModalLabel">Add New Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div style="padding-top: 0px !important" class="modal-body">

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Name *</label>
                        <div class="col-md-12">
                            <input id="product_name" name="product_name" type="text" class="form-control" required>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="productname" class="col-md-12 col-form-label">Unit</label>
                        <div class="col-md-12">
                            <select id="unit_id" style="width: 100%" class="form-control select2 unit" name="unit_id">
                                <option value="">--Select Unit--</option>
                                @if(!empty($units))
                                    @foreach($units as $key => $unit)
                                    <option value="{{ $unit->id }}">{{ $unit->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>

                    <div style="margin-bottom: 0px !important" class="form-group row">
                        <label for="productname" class="col-md-12 col-form-label">Production Cost</label>
                        <div class="col-md-12">
                            <input type="number" name="production_cost" class="inner form-control" id="production_cost" placeholder="Production Cost" />
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="submitBtn1" class="btn btn-primary waves-effect waves-light">Save</button>
                    <button id="CloseButton1" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#customer_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 && result['id'] != 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/orders/production-product-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        $('#submitBtn').click(function() {
            
            var customer_name               = $("#customer_name").val();
            var address                     = $("#address").val();
            var mobile_number               = $("#mobile_number").val();
            var contact_type                = 0;
            var site_url                    = $('.site_url').val();

            if (customer_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/invoices/customer/add/invoices',
                    data:   {  customer_name : customer_name,address : address, mobile_number : mobile_number, contact_type : contact_type, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton').click();
                        }
                        
                        $("#customer_id").empty();
                        $('#customer_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        }); 

        $('#submitBtn1').click(function() {
            
            var product_name                = $("#product_name").val();
            var unit_id                     = $("#unit_id").val();
            var production_cost             = $("#production_cost").val();
            var site_url                    = $('.site_url').val();

            if (product_name == '')
            {
                $('#mandatoryAlert').removeClass('hidden');
            }
            else
            {
                $.ajax({
                    type:   'post',
                    url:    site_url + '/orders/store-product',
                    data:   {  product_name : product_name,unit_id : unit_id, production_cost : production_cost, _token: '{{csrf_token()}}' },
        
                    success: function (data) {
                        if(data != 0)
                        {
                            $('#CloseButton1').click();
                        }
                        
                        $("#product_id").empty();
                        $('#product_id').append('<option value="'+ data.id +'" selected>' + data.name + '</option>');
                    }
                });
            }
        });   
    </script>
@endsection