

<?php $__env->startSection('title', 'Manual Customer Due List'); ?>

<?php $__env->startSection('styles'); ?>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Manual Customer Due List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active"> Manual Customer Due List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            
                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Customer</th>
                                            <th style="text-align: center">Address</th>
                                            <th style="text-align: center">Phone</th>
                                            <th style="text-align: center">Due Amount</th>
                                            <th style="text-align: center" class="d-print-none">Action</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i                  = 1;
                                        ?>
                                        <?php $__currentLoopData = $customerList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        
                                     
                                        <tr>
                                            <td style="text-align: center;"><?php echo e($i); ?></td>
                                            <td style="text-align: left;"><?php echo e($value['name']); ?> </td>
                                            <td style="text-align: left;"><?php echo e($value['address']); ?></td>
                                            <td style="text-align: center;"><?php echo e($value['phone']); ?></td>
                                            <td style="text-align: center;"><?php echo e($value['amount']); ?></td>
                                            <td style="text-align: center;" class="d-print-none">

                                                <a style="color:#fff" data-toggle="modal"  data-target="#exampleModalLong" data-customer-id="<?php echo e($value['id']); ?>" data-amount="<?php echo e($value['amount']); ?>" class="btn btn-info DueAmountModel">Amount Update</a>
                                            </td>
                                        </tr>
                                         
                                        <?php $i++; ?>

                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Edit Modal  -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Amount Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?php echo e(route('customerManualDueBalanceUpdate')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <label>Amount</label>
                <input type="hidden" class="form-control" name="customer_id" id="customer_id">
                <input type="text" class="form-control" name="amount" id="amount">
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>

      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>

<script>
    $(".DueAmountModel").click(function(){
        var customer_id   = $(this).data('customer-id');
        var amount = $(this).data('amount');

        $('#customer_id').val(customer_id);
        $('#amount').val(amount);
    });
</script>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/demo/dealer/Modules/Reports/Resources/views/manual_due_list_report_customer_details.blade.php ENDPATH**/ ?>