<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductSuppliers extends Model
{  
    protected $table = "product_suppliers";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Models\Customers','supplier_id');
    }

}
