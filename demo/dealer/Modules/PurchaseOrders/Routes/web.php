<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('purchaseorders')->group(function() {
    Route::get('/', 'PurchaseOrdersController@index')->name('purchase_orders_index');
    Route::get('/all-purchase-orders', 'PurchaseOrdersController@AllBills')->name('purchase_orders_all_po');

    Route::post('/store', 'PurchaseOrdersController@store')->name('purchase_orders_store');
    Route::get('/edit/{id}', 'PurchaseOrdersController@edit')->name('purchase_orders_edit');
    Route::post('/update/{id}', 'PurchaseOrdersController@update')->name('purchase_orders_update');
    Route::get('/show/{id}', 'PurchaseOrdersController@show')->name('purchase_orders_show');
    Route::get('/receive/{id}', 'PurchaseOrdersController@receive')->name('purchase_orders_receive');
   
    Route::get('/purchase-order/list/load', 'PurchaseOrdersController@billListLoad')->name('purchase_orders_list_load');
    Route::get('/purchase-order/search/list/{id}', 'PurchaseOrdersController@billListSearch')->name('purchase_orders_list_search');
});
