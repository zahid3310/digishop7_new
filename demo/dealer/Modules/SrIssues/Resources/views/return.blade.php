@extends('layouts.app')

@section('title', 'Return Issues')

<style type="text/css">
    .list-group{
            max-height: 620px;
            margin-bottom: 10px;
            overflow-y: auto;
            overflow-x: auto;
            -webkit-overflow-scrolling: touch;
        }
</style>

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_issue_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">

                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;padding-left: 10px" class="col-md-7 list-group">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">

                                        <div class="row">
                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="from_date">From Date *</label>
                                                    <input id="from_date" type="text" value="{{ date('01-01-1970') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="to_date">To Date *</label>
                                                    <input id="to_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select SM * </label>
                                                    <select style="width: 73%" id="sr_id" name="sr_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                           <option value="0">All</option>
                                                        </select>
                                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="findIssueList()">
                                                            <i class="bx bx-search font-size-24"></i>
                                                        </span>
                                                </div>
                                            </div>

                                            <div style="display: none" class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Issue Number * </label>
                                                    <div style="padding-left: 10px" class="row">
                                                        <select style="width: 73%" id="issue_id" name="issue_id" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                           <option value="0">All</option>
                                                        </select>
                                                        <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3">
                                                            <i class="bx bx-search font-size-24"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" placeholder="Enter Return Note" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                                <!-- <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Save & Print</button> -->
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>

                                        <input type="hidden" id="serial_value" value="1">

                                    </div>

                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();
            var type      = (new URL(location.href)).searchParams.get('return_type');

            $("#sr_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 4 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            $("#issue_id").select2({
                ajax: { 
                url:  site_url + '/srissues/issues-list/',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>

    <script type="text/javascript">
        function findIssueList()
        {
            var site_url        = $(".site_url").val();
            var from_date       = $("#from_date").val();
            var to_date         = $("#to_date").val();
            var sr_id           = $("#sr_id").val();
            var issue_id        = $("#issue_id").val();

            $.get(site_url + '/srissues/return-issues-product-list/' + from_date + '/' + to_date + '/' + sr_id + '/' + issue_id, function(data){

                if (data != '')
                {
                    $('.issueDetails').show();
                }
                else
                {
                    $('.issueDetails').hide();
                }

                var return_product_list = '';
                var serial              = 1;
                $.each(data.return_list, function(i, data_list)
                {
                    if (serial == 1)
                    {
                        var product_label           = '<label class="hidden-xs" for="productname">Product *</label>\n';
                        var converted_unit_label    = '<label class="hidden-xs" for="productname">U/M</label>\n';
                        var quantity_label          = '<label class="hidden-xs" for="productname">Qty. *</label>\n';
                        var return_label            = '<label class="hidden-xs" for="productname">Return Qty.</label>\n';
                    }
                    else
                    {
                        var product_label           = '';
                        var converted_unit_label    = '';
                        var quantity_label          = '';
                        var return_label            = '';
                    }

                    var quantity = parseFloat(data_list.receive_quantity) - parseFloat(data_list.sold_quantity) - parseFloat(data_list.return_quantity);

                    if (quantity == 0)
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" readonly />\n' 
                    }
                    else
                    {
                        var return_quantity = '<input id="return_quantity_'+ serial +'" name="return_quantity[]" type="text" class="inner form-control returnQuantity" value="0" oninput="calculate('+ serial +')" />\n' 
                    }

                    var unit_list    = '';
                    $.each(data[data_list.product_entry_id].unit_data, function(i, data_list_unit)
                    {   
                        unit_list    += '<option value = "' + data_list_unit.unit_id + '">' + data_list_unit.unit_name + '</option>';
                    });

                    if (quantity != 0)
                    {
                        return_product_list += ' ' + '<div style="margin-bottom: 15px !important" class="mb-3 row align-items-center">' +
                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-4 col-md-4 col-sm-4 col-12 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Product *</label>\n' +
                                                        product_label +
                                                        '<input type="text" class="inner form-control" value="'+ data_list.product_name +'" readonly />\n' +
                                                        '<input id="product_entries_'+serial+'" name="product_entries[]" type="hidden" value="'+ data_list.product_entry_id +'" readonly />\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: -18px" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Qty. *</label>\n' +
                                                        quantity_label +
                                                        '<input id="quantity_'+ serial +'" type="text" class="inner form-control" value="'+ quantity +'" readonly />\n' +
                                                        '<span id="main_unit_show_'+serial+'" style="color: black">' + data_list.unit_name + '</span>' +
                                                    '</div>\n' +

                                                    '<input id="main_unit_id_'+ serial +'" type="hidden" class="inner form-control" value="'+ data_list.unit_id +'" name="main_unit_id[]" />\n' +
                                                    '<input id="main_stock_'+ serial +'" type="hidden" class="inner form-control" value="'+ quantity +'" name="main_stock[]" />\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">U/M</label>\n' +
                                                        converted_unit_label +
                                                        '<select style="width: 100%;cursor: pointer" name="converted_unit_id[]" class="inner form-control CUID" id="converted_unit_id_'+serial+'" required onchange="getConversionParam('+serial+')">\n' +
                                                        unit_list +
                                                        '</select>\n' +
                                                    '</div>\n' +

                                                    '<div style="padding: 10px;padding-bottom: 0px;margin-bottom: 0px" class="col-lg-3 col-md-3 col-sm-6 col-6 form-group">\n' +
                                                        '<label style="display: none" class="show-xs" for="productname">Return Qty</label>\n' +
                                                        return_label +
                                                        return_quantity +
                                                    '</div>\n' +
                                                '</div>\n';

                        serial++;
                    }                         
                });

                $("#return_product_list").empty();
                $("#return_product_list").append(return_product_list);
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#converted_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#main_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

                    $("#main_unit_show_"+x).html(convertedUnitName);
                    $("#quantity_"+x).val(convertedStock);
                }

            });
        }

        function getItemPrice(x)
        {
            //For getting item commission information from items table start
            var entry_id  = $("#product_entries_"+x).val();
            var site_url  = $(".site_url").val();

            if(entry_id)
            {
                $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

                    var list    = '';
                    $.each(data.unit_conversions, function(i, data_list)
                    {   
                        list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
                    });

                    $("#converted_unit_id_"+x).empty();
                    $("#converted_unit_id_"+x).append(list);

                    if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
                    {
                        var stockInHand  = 0;
                    }
                    else
                    {
                        var stockInHand  = $("#main_stock_"+x).val();
                    }

                    if (data.product_entries.unit_id == null)
                    {
                        var unit  = '';
                    }
                    else
                    {
                        var unit  = data.product_entries.unit_name;
                    }

                    $("#quantity_"+x).val(0);
                    $("#quantity_"+x).val(parseFloat(stockInHand).toFixed(2));
                    $("#main_unit_show_"+x).html(unit);
                    $("#main_unit_id_"+x).val(data.product_entries.unit_id);
                    $("#main_unit_name_"+x).val(data.product_entries.unit_name);
                });
            }
        }
    </script>

    <script type="text/javascript">
        function calculate(x)
        {
        	var return_quantity     = $('.returnQuantity').map((_,el) => el.value).get();
         	var amount 				= $('.amount').map((_,el) => el.value).get();
         	var quantity			= $('.quantity').map((_,el) => el.value).get();
         	var rate				= $('.rate').map((_,el) => el.value).get();

         	var vatType             = $("#vat_type_0").val();
            var vatAmount           = $("#vat_amount_0").val();
            var taxType             = $("#tax_type_0").val();
            var taxAmount           = $("#tax_amount_0").val();
            var totalDiscount       = $("#total_discount_0").val();
            var totalDiscountType   = $("#total_discount_type_0").val();
            var totalBdt            = $("#totalBdt").val();
            var subTotal            = $("#subTotalBdt").val();

            var total                   = 0;
            var total_quantity          = 0;
            var total_return_quantity   = 0;

         	for (var i = 0; i < return_quantity.length; i++)
         	{
         		if (return_quantity[i] > 0)
         		{	
         			var result   = (parseFloat(amount[i])/parseFloat(quantity[i]))*parseFloat(return_quantity[i]);

         			total   	+= parseFloat(result);
         		}

                total_quantity          += parseFloat(quantity[i]);
                total_return_quantity   += parseFloat(return_quantity[i]);
         	}

            if (vatType == 0)
            {
                var vatTypeCal     = (parseFloat(total)*parseFloat(vatAmount))/100;
            }
            else
            {
                var vatTypeCal     = (parseFloat(vatAmount)*parseFloat(total))/parseFloat(subTotal);
            }

            if (totalDiscountType == 0)
            {
                var totalDiscountCal     = ((parseFloat(total) + parseFloat(vatTypeCal))*parseFloat(totalDiscount))/100;
            }
            else
            {
                var totalDiscountCal     = (parseFloat(total)*parseFloat(totalDiscount))/(parseFloat(subTotal) + parseFloat(vatAmount));
            }

            if (taxType == 0)
            {
                var taxTypeCal     = (parseFloat(total)*parseFloat(taxAmount))/100;
            }
            else
            {
                var taxTypeCal     = $("#tax_amount_0").val();
            }

            var total_result       = parseFloat(total) + parseFloat(vatTypeCal) - parseFloat(totalDiscountCal);

         	$('#totalReturnedBdt').val(total_result.toFixed(2));
            $("#amount_paid").val(parseFloat(total_result.toFixed(2)));
        }
    </script>
@endsection