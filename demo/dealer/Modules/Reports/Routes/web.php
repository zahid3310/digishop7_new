<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('reports/stock')->group(function() {
    Route::get('/', 'ReportsController@stockReport')->name('stock_report_index');
    Route::get('/stock-details/{id}', 'ReportsController@stockDetailsReport')->name('stock_report_details');
});

Route::prefix('reports/sales')->group(function() {
    Route::get('/', 'ReportsController@salesReport')->name('sales_report_index');
});

Route::prefix('reports/profit-loss')->group(function() {
    Route::get('/', 'ReportsController@profitLoss')->name('profit_loss_index');
    Route::get('/report', 'ReportsController@profitLossReduced')->name('profit_loss_index_reduced');
});

Route::prefix('reports/sales-summary')->group(function() {
    Route::get('/', 'ReportsController@salesSummary')->name('sales_summary_index');
    Route::get('/report', 'ReportsController@salesSummaryReduced')->name('sales_summary_index_reduced');
});

Route::prefix('reports/purchase')->group(function() {
    Route::get('/', 'ReportsController@purchaseReport')->name('purchase_report_index');
    Route::get('/report', 'ReportsController@purchaseReportReduced')->name('purchase_report_index_reduced');
});

Route::prefix('reports/purchase-summary')->group(function() {
    Route::get('/', 'ReportsController@purchaseSummary')->name('purchase_summary_index');
});

Route::prefix('reports/due-supplier')->group(function() {
    Route::get('/', 'ReportsController@dueReportSupplier')->name('due_report_supplier_index');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportSupplierDetails')->name('due_report_supplier_details');

    Route::get('/due-list', 'ReportsController@dueReportSupplierDue')->name('due_report_supplier_index_due');
});

Route::prefix('reports/expense')->group(function() {
    Route::get('/', 'ReportsController@expenseReport')->name('expense_report_index');
});

Route::prefix('reports/income')->group(function() {
    Route::get('/', 'ReportsController@incomeReport')->name('income_report_index');
});

Route::prefix('reports/collection')->group(function() {
    Route::get('/', 'ReportsController@collectionReport')->name('collection_report_index');
});

Route::prefix('reports/salary-report')->group(function() {
    Route::get('/', 'ReportsController@salaryReport')->name('salary_report_index');
});

Route::prefix('reports/sales-statement')->group(function() {
    Route::get('/', 'ReportsController@salesStatement')->name('sales_statement_index');
    Route::get('/print', 'ReportsController@salesStatementPrint')->name('sales_statement_print');
    Route::get('/invoices-list', 'ReportsController@invoiceListAjax');
    Route::get('/users-list', 'ReportsController@usersListAjax');
    Route::get('/category-list', 'ReportsController@categoryListAjax');
    Route::get('/product-list', 'ReportsController@productListAjax');
    Route::get('/product-code-list', 'ReportsController@productCodeListAjax');
});

Route::prefix('reports/purchase-statement')->group(function() {
    Route::get('/', 'ReportsController@purchaseStatement')->name('purchase_statement_index');
    Route::get('/print', 'ReportsController@purchaseStatementPrint')->name('purchase_statement_print');
    Route::get('/bill-list', 'ReportsController@billListAjax');
});

Route::prefix('reports/item-list')->group(function() {
    Route::get('/', 'ReportsController@itemList')->name('item_list_index');
    Route::get('/print', 'ReportsController@itemListPrint')->name('item_list_print');
    Route::get('/brand-list', 'ReportsController@brandListAjax');
    Route::get('/variation-list', 'ReportsController@variationListAjax');
});

Route::prefix('reports/register-list')->group(function() {
    Route::get('/', 'ReportsController@registerList')->name('register_list_index');
});

Route::prefix('reports/income-statement')->group(function() {
    Route::get('/', 'ReportsController@incomeStatement')->name('income_statement_index');
    Route::get('/print', 'ReportsController@incomeStatementPrint')->name('income_statement_print');
});

Route::prefix('reports/current-balance')->group(function() {
    Route::get('/', 'ReportsController@currentBalance')->name('current_balance_index');
    Route::get('/print', 'ReportsController@currentBalancePrint')->name('current_balance_print');
    Route::get('/paid-account-list', 'ReportsController@paidAccountListAjax');
});

Route::prefix('reports/product-suppliers')->group(function() {
    Route::get('/', 'ReportsController@productSuppliers')->name('product_suppliers_index');
    Route::get('/print', 'ReportsController@productSuppliersPrint')->name('product_suppliers_print');
});

Route::prefix('reports/product-customers')->group(function() {
    Route::get('/', 'ReportsController@productCustomers')->name('product_customers_index');
    Route::get('/print', 'ReportsController@productCustomersPrint')->name('product_customers_print');
});

Route::prefix('reports/customer-payment')->group(function() {
    Route::get('/', 'ReportsController@customerPaymentReport')->name('customer_payment_report_index');
    Route::get('/print', 'ReportsController@customerPaymentReportPrint')->name('customer_payment_report_print');
});

Route::prefix('reports/supplier-payment')->group(function() {
    Route::get('/', 'ReportsController@supplierPaymentReport')->name('supplier_payment_report_index');
    Route::get('/print', 'ReportsController@supplierPaymentReportPrint')->name('supplier_payment_report_print');
});

Route::prefix('reports/emergency-item-list')->group(function() {
    Route::get('/', 'ReportsController@emergencyItemList')->name('emergency_item_list_index');
    Route::get('/print', 'ReportsController@emergencyItemListPrint')->name('emergency_item_list_print');
    Route::get('/emergency-category-list', 'ReportsController@emergencyCategoryListAjax');
    Route::get('/emergency-product-list', 'ReportsController@emergencyProductListAjax');
});

Route::prefix('reports/due-customer')->group(function() {
    Route::get('/', 'ReportsController@dueReportCustomer')->name('due_report_customer_index');
    Route::get('/print', 'ReportsController@dueReportCustomerPrint')->name('due_report_customer_print');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportCustomerDetails')->name('due_report_customer_details');

    Route::get('/due-print', 'ReportsController@dueReportCustomerPrintDue')->name('due_report_customer_print_due');

    Route::get('/manual-list', 'ReportsController@manualDueList')->name('manual_due_list_report_customer_index');
    
    Route::post('/manual-list-update', 'ReportsController@customerManualDueBalanceUpdate')->name('customerManualDueBalanceUpdate');

    
});

Route::prefix('reports/sr-statement')->group(function() {
    Route::get('/', 'ReportsController@srStatement')->name('sr_statement_index');
    Route::get('/print', 'ReportsController@srStatementPrint')->name('sr_statement_print');
});

Route::prefix('reports/sr-stock-statement')->group(function() {
    Route::get('/', 'ReportsController@srStockStatement')->name('sr_stock_statement_index');
    Route::get('/print', 'ReportsController@srStockStatementPrint')->name('sr_stock_statement_print');
});

Route::prefix('reports/free-items-statement')->group(function() {
    Route::get('/', 'ReportsController@freeItemsStatement')->name('free_items_statement_index');
    Route::get('/print', 'ReportsController@freeItemsStatementPrint')->name('free_items_statement_print');
});

Route::prefix('reports/loan')->group(function() {
    Route::get('/', 'ReportsController@loanReport')->name('loan_report_index');
    Route::get('/details/{id}', 'ReportsController@loanReportDetails')->name('loan_report_details');
});

Route::prefix('reports/due-list/supplier')->group(function() {
    Route::get('/', 'ReportsController@dueListReportSupplier')->name('due_list_supplier_index');
    Route::get('/details/{customer_id}', 'ReportsController@dueListReportSupplierDetails')->name('due_list_supplier_details');
});


Route::prefix('reports/due-list/customer')->group(function() {
    Route::get('/', 'ReportsController@dueListReportCustomer')->name('due_list_report_customer_index');
    Route::get('/details/{customer_id}', 'ReportsController@dueListReportCustomerDetails')->name('due_list_customer_details');
});

Route::prefix('reports/inc-exp-ledger')->group(function() {
    Route::get('/', 'ReportsController@IncomeExpenseLedger')->name('income_expense_ledger_index');
    Route::get('/print', 'ReportsController@IncomeExpenseLedgerPrint')->name('income_expense_ledger_print');
});

Route::prefix('reports/order')->group(function() {
    Route::get('/', 'ReportsController@orderReport')->name('order_report_index');
    Route::get('/order-details', 'ReportsController@orderDetailsReport')->name('order_report_details');
});