

<?php $__env->startSection('title', 'Receive From SR/Customer'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Receive From SR/Customer</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Damages</a></li>
                                    <li class="breadcrumb-item active">Receive From SR/Customer</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                            Payment Successfull !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                            Payment Not Added !!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('damages_receive_from_customer_transfer_store', $find_damage['id'])); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                    
                                <?php echo e(csrf_field()); ?>

                                <div style="background-color: #F4F4F7;padding-top: 10px" class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">Date *</label>
                                            <div class="col-md-8">
                                                <input id="damage_date" name="damage_date" type="text" value="<?php echo e(date('d-m-Y', strtotime($find_damage['date']))); ?>" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-4 col-form-label">DP/Sup *</label>
                                            <select id="supplier_id" name="supplier_id" class="form-control select2 col-lg-8 col-md-8 col-sm-8 col-8" required>
                                                <option>--Select Supplier--</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div style="margin-bottom: 5px" class="form-group row">
                                            <label for="productname" class="col-md-3 col-form-label">Note</label>
                                            <div class="col-md-9">
                                                <input id="note" name="note" type="text" class="form-control" placeholder="Damage Note" value="<?php echo e($find_damage['note']); ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px" class="row">
                                    <div style="background-color: #D2D2D2;height: 530px;padding-top: 5px;overflow-y: auto;overflow-x: auto" class="col-md-12 input_fields_wrap getMultipleRow">
                                        <?php $__currentLoopData = $find_damage_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div style="margin-bottom: 0px !important" class="row di_<?php echo e($key); ?>">
                                                <div style="margin-bottom: 5px" class="col-lg-3 col-md-3 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Product *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Product *</label>
                                                    <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_<?php echo e($key); ?>" required>
                                                        <option value="<?php echo e($value['item_id']); ?>">
                                                        <?php
                                                            $vari       = ProductVariationName($value['item_id']);
                                                            $variation  = $vari != null ? ' - ' . $vari : '';
                                                        ?>

                                                        <?php echo e($value['item_name'] . $variation . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )'); ?>

                                                        </option>
                                                    </select>
                                                </div>

                                                <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_<?php echo e($key); ?>" value="<?php echo e($value['main_unit_id']); ?>" />

                                                <div class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Unit</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Unit</label>
                                                    <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_<?php echo e($key); ?>">
                                                        <option value="<?php echo e($value['conversion_unit_id']); ?>"><?php echo e($value['conversion_unit_id'] != null ? $value->convertedUnit->name : ''); ?></option>
                                                    </select>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-1 col-md-1 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Qty *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Qty *</label>
                                                    <input type="text" name="quantity[]" class="inner form-control quantityCheck" id="quantity_<?php echo e($key); ?>" value="<?php echo e(round($value['quantity'], 2)); ?>" placeholder="Quantity" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Rate *</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Rate *</label>
                                                    <input type="text" name="rate[]" class="inner form-control rateCheck" id="rate_<?php echo e($key); ?>" value="<?php echo e(round($value['buy_price'], 2)); ?>" placeholder="Rate" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Amount</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Amount</label>
                                                    <input type="text" name="amount[]" class="inner form-control amount" id="amount_<?php echo e($key); ?>" value="<?php echo e(round($value['amount'], 2)); ?>" placeholder="Amount" required/>
                                                </div>

                                                <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                    <?php if($key == 0): ?>
                                                        <label class="hidden-xs" for="productname">Transfer Qty</label>
                                                    <?php endif; ?>
                                                    <label style="display: none" class="show-xs" for="productname">Transfer Qty</label>
                                                    <input type="text" name="transfer_qty[]" class="inner form-control" id="transfer_qty_<?php echo e($key); ?>" value="" placeholder="Transfer Quantity" oninput="calculateActualAmount(<?php echo e($key); ?>)" required/>
                                                </div>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </div>

                                    <div style="display: none" class="row justify-content-end">
                                        <div style="margin-bottom: 0px !important"  class="col-lg-1 col-md-2 form-group">
                                            <input id="add_field_button" type="button" class="btn btn-success btn-block inner add_field_button" value="Add"/>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 5px !important;padding-bottom: 0px !important;background-color: #F4F4F7" class="row">
                                    <div class="col-md-12">
                                        <div class="form-group row">
                                            <div class="button-items col-lg-9">
                                                <button style="border-radius: 0px !important" name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <button style="border-radius: 0px !important" type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('damages_receive_from_customer_index')); ?>">Close</a></button>
                                            </div>
                                            <div class="button-items col-lg-3">
                                                 <div style="margin-bottom: 5px" class="form-group row">
                                                    <label style="text-align: right" class="col-md-5 col-form-label">Sub Total</label>
                                                    <div class="col-md-7">
                                                        <input type="text" id="subTotalBdt" class="form-control">
                                                        <input style="display: none"  type="text" id="subTotalBdtShow" name="sub_total_amount" readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        $( document ).ready(function() {

            var site_url  = $('.site_url').val();

            $("#supplier_id").select2({
                ajax: { 
                url:  site_url + '/invoices/customer/list/invoices',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 1)
                    {
                        return result['text'];
                    }
                },
            });
        });
    </script>

    <script type="text/javascript">
        function pad(number, length)
        {
            var str = '' + number;
            while (str.length < length)
            {
                str = '0' + str;
            }
           
            return str;
        }
    </script>

    <script type="text/javascript">
        function addButton()
        {
            $('.add_field_button').click();
        }
    </script>

    <script type="text/javascript">
        function calculateActualAmount(x)
        {
            var rate                    = $("#rate_"+x).val();
            var quantity                = $("#transfer_quantity_"+x).val();
        
            if (rate == '')
            {
                var rateCal             = 1;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            if (quantity == '')
            {
                var quantityCal         = 1;
            }
            else
            {
                var quantityCal         = $("#transfer_quantity_"+x).val();
            }

            //Calculating Subtotal Amount
            var total       = 0;

            $('.amount').each(function()
            {
                total       += parseFloat($(this).val());
            });

            $("#subTotalBdt").val(total.toFixed());
            $("#subTotalBdtShow").val(total.toFixed());
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/60/Modules/Damages/Resources/views/receive_from_customer/transfer.blade.php ENDPATH**/ ?>