<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DamageEntries extends Model
{
    protected $table = "damage_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Products','product_id');
    }

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }
}
