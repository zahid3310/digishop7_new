<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FreeItems extends Model
{  
    protected $table = "free_items";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoices','invoice_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function sr()
    {
        return $this->belongsTo('App\Models\Customers','sr_id');
    }
}
