<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Labels</title>
</style>
</head>
<body>
    <button class="btn btn-success" onclick="window.print()">Print</button>

    <?php if($print_type == 0): ?>
        <?php if($label_size == 0): ?>
        <div id="preview_body">
            <div style="height: 12in !important; width: 8.5in !important; line-height: 16px !important; page-break-after: always;" class="label-border-outer">
                <div style="margin-top: 0.5in !important; margin-bottom: 0.5in !important; margin-left: 0.25in !important; margin-right: 0.25in !important;" class="label-border-internal">
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php for($i = 0; $i < $value['quantity']; $i++): ?>
                    <div style="height: 1.1in !important; line-height: 1in; width: 1.94in !important; display: inline-block;" class="sticker-border text-center">
                        <div style="display: inline-block; vertical-align: middle; line-height: 16px !important;">
                            <?php if($value['organization_name_show'] == 1): ?>
                            <b style="display: block !important;" class="text-uppercase"><?php echo e($value['organization_name']); ?></b>
                            <?php endif; ?>

                            <?php if($value['product_name_show'] == 1): ?>
                            <span style="display: block !important;"><?php echo e($value['name']); ?></span>
                            <?php endif; ?>

                            <?php if($value['sell_price_show'] == 1): ?>
                            <b>Price: <?php echo e(number_format($value['sell_price'],2,'.',',')); ?></b>
                            <?php endif; ?>

                            <br />

                            <img class="center-block"
                                style="max-width:90%; !important;height: 0.24in !important;" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG($value['product_code'], 'C93',3,33)); ?>" alt="barcode" />

                            <br>
                            
                            <b><?php echo e($value['product_code']); ?></b>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if($label_size == 1): ?>
        <div id="preview_body">
            <div style="height: 12in !important; width: 8.5in !important; line-height: 16px !important; page-break-after: always;" class="label-border-outer">
                <div style="margin-top: 0.5in !important; margin-bottom: 0.5in !important; margin-left: 0.25in !important; margin-right: 0.25in !important;" class="label-border-internal">
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php for($i = 0; $i < $value['quantity']; $i++): ?>
                    <div style="height: 1.25in !important; line-height: 1.25in; width: 1.94in !important; display: inline-block;" class="sticker-border text-center">
                        <div style="display: inline-block; vertical-align: middle; line-height: 16px !important;">
                            <?php if($value['organization_name_show'] == 1): ?>
                            <b style="display: block !important;" class="text-uppercase"><?php echo e($value['organization_name']); ?></b>
                            <?php endif; ?>

                            <?php if($value['product_name_show'] == 1): ?>
                            <span style="display: block !important;"><?php echo e($value['name']); ?></span>
                            <?php endif; ?>

                            <?php if($value['sell_price_show'] == 1): ?>
                            <b>Price: <?php echo e(number_format($value['sell_price'],2,'.',',')); ?></b>
                            <?php endif; ?>

                            <br />

                            <img class="center-block"
                                style="max-width:90%; !important;height: 0.3in !important;" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG($value['product_code'], 'C93',3,33)); ?>" alt="barcode" />

                            <br>
                            
                            <b><?php echo e($value['product_code']); ?></b>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>

        <?php if($label_size == 2): ?>
        <div id="preview_body">
            <div style="height: 16in !important; width: 8.5in !important; line-height: 16px !important; page-break-after: always;" class="label-border-outer">
                <div style="margin-top: 0.5in !important; margin-bottom: 0.5in !important; margin-left: 0.2198in !important; margin-right: 0.2198in !important;" class="label-border-internal">
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php for($i = 0; $i < $value['quantity']; $i++): ?>
                    <div style="height: 1in !important; line-height: 1in; width: 3.6375in !important; display: inline-block;" class="sticker-border text-center">
                        <div style="display: inline-block; vertical-align: middle; line-height: 16px !important;">
                            <?php if($value['organization_name_show'] == 1): ?>
                            <b style="display: block !important;" class="text-uppercase"><?php echo e($value['organization_name']); ?></b>
                            <?php endif; ?>

                            <?php if($value['product_name_show'] == 1): ?>
                            <span style="display: block !important;"><?php echo e($value['name']); ?></span>
                            <?php endif; ?>

                            <?php if($value['sell_price_show'] == 1): ?>
                            <b>Price: <?php echo e(number_format($value['sell_price'],2,'.',',')); ?></b>
                            <?php endif; ?>

                            <br />

                            <img class="center-block"
                                style="max-width:90%; !important;height: 0.24in !important;" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG($value['product_code'], 'C93')); ?>" alt="barcode" />

                            <br>
                            
                            <b><?php echo e($value['product_code']); ?></b>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if($print_type == 1): ?>
        <div id="preview_body">
            <div style="height: auto !important; width: 4in !important; line-height: 16px !important; page-break-after: always;" class="label-border-outer">
                <div style="margin-top: 0.25in !important; margin-bottom: 0.25in !important; margin-left: 0.25in !important; margin-right: 0.25in !important;" class="label-border-internal">
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php for($i = 0; $i < $value['quantity']; $i++): ?>
                    <div style="height: 1.1in !important; line-height: 1in; width: 1.7in !important; display: inline-block;" class="sticker-border text-center">
                        <div style="display: inline-block; vertical-align: middle; line-height: 16px !important;">
                            <?php if($value['organization_name_show'] == 1): ?>
                            <b style="display: block !important;" class="text-uppercase"><?php echo e($value['organization_name']); ?></b>
                            <?php endif; ?>

                            <?php if($value['product_name_show'] == 1): ?>
                            <span style="display: block !important;"><?php echo e($value['name']); ?></span>
                            <?php endif; ?>

                            <?php if($value['sell_price_show'] == 1): ?>
                            <b>Price: <?php echo e(number_format($value['sell_price'],2,'.',',')); ?></b>
                            <?php endif; ?>

                            <br />

                            <img class="center-block"
                                style="max-width:90%; !important;height: 0.24in !important;" src="data:image/png;base64,<?php echo e(DNS1D::getBarcodePNG($value['product_code'], 'C93',3,33)); ?>" alt="barcode" />

                            <br>
                            
                            <b><?php echo e($value['product_code']); ?></b>
                        </div>
                    </div>
                    <?php endfor; ?>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <style type="text/css">
        .text-center {
            text-align: center;
        }

        .text-uppercase {
            text-transform: uppercase;
        }

        /*Css related to printing of barcode*/
        .label-border-outer {
            border: 0.1px solid grey !important;
        }
        .label-border-internal {
            /*border: 0.1px dotted grey !important;*/
        }
        .sticker-border {
            border: 0.1px dotted grey !important;
            overflow: hidden;
            box-sizing: border-box;
        }
        #preview_box {
            padding-left: 30px !important;
        }
        @media  print {
            .content-wrapper {
                border-left: none !important; /*fix border issue on invoice*/
            }
            .label-border-outer {
                border: none !important;
            }
            .label-border-internal {
                border: none !important;
            }
            .sticker-border {
                border: none !important;
            }
            #preview_box {
                padding-left: 0px !important;
            }
            #toast-container {
                display: none !important;
            }
            .tooltip {
                display: none !important;
            }
            .btn {
                display: none !important;
            }
        }

        @media  print {
            #preview_body {
                display: block !important;
            }
        }
        @page  {
            size: 8.5in 12in;

            /*width: 8.5in !important;*/
            /*height:11in !important ;*/
            margin-top: 0in;
            margin-bottom: 0in;
            margin-left: 0in;
            margin-right: 0in;
        }
    </style>
</body>

</html><?php /**PATH /home/digishop7/public_html/restaurant_demo/Modules/Products/Resources/views/barcodes_print.blade.php ENDPATH**/ ?>