@extends('layouts.app')

@section('title', 'Transfer To DP/Supplier')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Transfer To DP/Supplier</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Damages</a></li>
                                    <li class="breadcrumb-item active">Transfer To DP/Supplier</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="col-12">
                            <div id="success_message" style="display: none" class="alert alert-success alert-dismissible fade show" role="alert">
                                Payment Successfull !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="col-12">
                            <div id="unsuccess_message" style="display: none" class="alert alert-primary alert-dismissible fade show" role="alert">
                                Payment Not Added !!
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        </div>

                        <div class="card">
                            <div style="margin-top: -10px !important;margin-bottom: -20px !important" class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body table-responsive">

                                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>SL</th>
                                                            <th>Date</th>
                                                            <th>Damage Value</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @if(!empty($dealer_damages) && ($dealer_damages->count() > 0))
                                                        @foreach($dealer_damages as $key => $damage)
                                                            <tr>
                                                                <td>{{ $key + 1 }}</td>
                                                                <td>{{ date('d-m-Y', strtotime($damage['date'])) }}</td>
                                                                <td>{{ $damage['total_amount'] }}</td>
                                                                <td>
                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                                            <a class="dropdown-item" href="{{ route('damages_transfer_to_dp_edit', $damage['id']) }}">Edit</a>
                                                                            <!-- <a class="dropdown-item" href="{{ route('damages_receive_from_customer_transfer', $damage['id']) }}">Transfer To DP/Supplier</a> -->
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div> 
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
        });
    </script>
@endsection