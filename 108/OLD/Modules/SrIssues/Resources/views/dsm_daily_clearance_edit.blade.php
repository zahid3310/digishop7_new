@extends('layouts.app')

@section('title', 'Update DSM Daily Clearance')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Update DSM Daily Clearance</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issue To DSM</a></li>
                                    <li class="breadcrumb-item active">Update DSM Daily Clearance</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif
                                
                                <form method="post" action="{{ route('issues_dsm_daily_clearance_update', $find_clearance->id) }}" enctype="multipart/form-data">
                                    @csrf
                                    
                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Select Date </label>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="input-group-append col-md-5">
                                                    <span style="border-radius: 0px" class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                                    <input style="border-radius: 0px;height: 42px;" type="text" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" value="{{ date('d-m-Y', strtotime($find_clearance->date)) }}" name="from_date">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">DSM Name * </label>
                                        <div class="col-md-9">
                                            <div class="row">
	                                            <div class="col-md-5">
	                                                <select name="sr_id" class="form-control select2" required>
	                                                    <option value="{{ $find_clearance->dsm_id }}" selected>{{ $find_clearance->dsm->name }}</option>
	                                                </select>
	                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label">Amount </label>
                                        <div class="col-md-9">
                                            <div class="row">
	                                            <div class="col-md-5">
	                                                <input id="amount" name="amount" class="form-control" value="{{ $find_clearance->amount }}" required>
	                                            </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
                                        <div class="col-md-9">
                                            <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit">
                                                Update
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                
                                <hr>
                                
                                <div class="row">
                                    <div class="col-12">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">List of Clearance</h4>
                                                <br>
                
                                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                                    <thead>
                                                        <tr>
                                                            <th style="width: 5%;text-align: center">SL</th>
                                                            <th style="width: 15%;text-align: center">DSM</th>
                                                            <th style="width: 10%;text-align: center">Date</th>
                                                            <th style="width: 10%;text-align: center">Amount</th>
                                                            <th style="width: 10%;text-align: center">Action</th>
                                                        </tr>
                                                    </thead>
                
                
                                                    <tbody>
                                                        @if(!empty($clearances) && ($clearances->count() > 0))
                                                        @foreach($clearances as $key => $clearance)
                                                            <tr>
                                                                <td style="text-align: center">{{ $key + 1 }}</td>
                                                                <td style="text-align: left">{{ $clearance->dsm->name }}</td>
                                                                <td style="text-align: center">{{ date('d-m-Y', strtotime($clearance->date)) }}</td>
                                                                <td style="text-align: center">{{ $clearance->amount }}</td>
                                                                <td style="text-align: center">
                                                                    <div class="dropdown">
                                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                                        </a>
                                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                                            <a class="dropdown-item" href="{{ route('issues_dsm_daily_clearance_edit', $clearance['id']) }}">Edit</a>
                                                                        </div>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        @endif
                                                    </tbody>
                                                </table>
                
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$( document ).ready(function() {
            
	    var site_url    = $('.site_url').val();

	    $("#sr_id").select2({
	        ajax: { 
	        url:  site_url + '/invoices/customer/list/invoices',
	        type: "get",
	        dataType: 'json',
	        delay: 250,
	        data: function (params) {
	            return {
	                searchTerm: params.term // search term
	            };
	        },
	        processResults: function (response) {
	            return {
	                results: response
	            };
	        },
	            cache: true
	        },

	        minimumInputLength: 0,
	        escapeMarkup: function(result) {
	            return result;
	        },
	        templateResult: function (result) {
	            if (result.loading) return 'Searching...';

	            if (result['contact_type'] == 6)
	            {
	                return result['text'];
	            }
	        },
	    });
	});
</script>
@endsection