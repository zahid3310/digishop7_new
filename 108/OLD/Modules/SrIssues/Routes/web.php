<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('srissues')->group(function() {
    Route::get('/', 'SrIssuesController@index')->name('issues_index');
    Route::get('/all-issues', 'SrIssuesController@AllIssues')->name('all_issues');
    Route::post('/store', 'SrIssuesController@store')->name('issues_store');
    Route::get('/edit/{id}', 'SrIssuesController@edit')->name('issues_edit');
    Route::post('/update/{id}', 'SrIssuesController@update')->name('issues_update');
    Route::get('/show/{id}', 'SrIssuesController@show')->name('issues_show');
    Route::get('/calculate-opening-balance/{sr_id}', 'SrIssuesController@calculateOpeningBalance')->name('issues_calculate_opening_balance');
    Route::get('/print-dsm-order-list/{id}', 'SrIssuesController@printDsmOrderList')->name('issues_print_dsm_order_list');
    Route::get('/dsm-daily-report', 'SrIssuesController@dsmDailyReport')->name('issues_dsm_daily_report');
    Route::get('/dsm-daily-report-details', 'SrIssuesController@dsmDailyReportDetails')->name('issues_dsm_daily_report_details');

    Route::get('/convert-to-invoice/{issue_id}', 'SrIssuesController@convertToInvoice')->name('issues_dsm_convert_to_invoice');
    Route::post('/convert-to-invoice-store', 'SrIssuesController@convertToInvoiceStore')->name('issues_dsm_convert_to_invoice_store');
    
    Route::get('/dsm-daily-clearance', 'SrIssuesController@dsmDailyClearance')->name('issues_dsm_daily_clearance');
    Route::post('/dsm-daily-clearance-store', 'SrIssuesController@dsmDailyClearanceStore')->name('issues_dsm_daily_clearance_store');
    Route::get('/dsm-daily-clearance-edit/{id}', 'SrIssuesController@dsmDailyClearanceEdit')->name('issues_dsm_daily_clearance_edit');
    Route::post('/dsm-daily-clearance-update/{id}', 'SrIssuesController@dsmDailyClearanceUpdate')->name('issues_dsm_daily_clearance_update');
    Route::get('/get-dsm-balance/{date}/{dsm_id}', 'SrIssuesController@getDsmBalance');
    
    //Js Codes Start
    Route::get('/issues/list/load', 'SrIssuesController@issuesListLoad')->name('issues_list_load');
    Route::get('/issue/search/list/{from_date}/{to_date}/{sr_name}/{issue_number}', 'SrIssuesController@issueListSearch')->name('issues_list_search');
    Route::get('/print-issues-list', 'SrIssuesController@printIssuesList')->name('issues_print_issues_list');
    Route::get('/print-issues-search/{date}/{customer}/{issue_number}', 'SrIssuesController@printIssuesSearch')->name('issues_print_issues_search');
    Route::get('/issues-list', 'SrIssuesController@issuesList')->name('issues_list');
    Route::get('/issues-list-sr/{sr_id}', 'SrIssuesController@issuesListSr')->name('issues_list_sr');
    //Js Codes Start

    //Return Routes
    Route::get('/all-returns', 'SrIssuesController@AllReturns')->name('all_return_issues');
    Route::get('/return-issues', 'SrIssuesController@returnIssues')->name('issues_return_issues');
    Route::post('/return-issues-store', 'SrIssuesController@returnIssueStore')->name('issues_return_issue_store');
    Route::get('/return-issues-edit/{id}', 'SrIssuesController@editReturn')->name('issues_return_edit');
    Route::post('/return-issues-update/{id}', 'SrIssuesController@updateReturn')->name('issues_return_update');
    Route::get('/return-issues-show/{id}', 'SrIssuesController@showReturn')->name('issues_return_show');
    Route::get('/return-issues-product-list/{sr_id}/{issue_id}', 'SrIssuesController@returnIssueProductList')->name('issues_return_issue_product_list');
    Route::get('/returns/list/load', 'SrIssuesController@returnsListLoad')->name('issues_return_list_load');
    Route::get('/returns-converted-unit-lists/{product_entry_id}', 'SrIssuesController@returnsConvertedUnitList')->name('issues_return_converted_unit_list');
    Route::get('/issue-return-list', 'SrIssuesController@issueReturnList')->name('issues_return_list');
    Route::get('/return/search/list/{from_date}/{to_date}/{sr_name}/{issue_number}', 'SrIssuesController@allReturnsListSearch')->name('issues_return_list_search');
});
