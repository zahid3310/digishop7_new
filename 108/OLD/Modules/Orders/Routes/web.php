<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('orders')->group(function() {    
    Route::get('/', 'OrdersController@index')->name('orders_index');
    Route::get('/all-sales', 'OrdersController@AllSales')->name('orders_all_sales');
    Route::get('/all-delivery-list', 'OrdersController@orderListDelivery')->name('order_list_delivery');
    Route::post('/store', 'OrdersController@store')->name('orders_store');
    Route::get('/edit/{id}', 'OrdersController@edit')->name('orders_edit');
    Route::get('/delivery/{id}', 'OrdersController@delivery')->name('orders_delivery');
    Route::post('/update/{id}', 'OrdersController@update')->name('orders_update');
    Route::get('/show/{id}', 'OrdersController@show')->name('orders_show');
    Route::get('/show-pos/{id}', 'OrdersController@showPos')->name('orders_show_pos');
    Route::get('/products/list', 'OrdersController@productList')->name('product_list');
    Route::get('/products/price/list/{id}', 'OrdersController@productPriceList')->name('product_price_list');
    Route::get('/products-price-list/{product_entry_id}', 'OrdersController@productPriceListBySr')->name('product_price_list_by_sr');
    Route::get('/invoice/list/load', 'OrdersController@invoiceListLoad')->name('orders_list_load');
    Route::get('/invoice/delivery/list/load', 'OrdersController@invoiceDeliveryListLoad')->name('orders_delivery_list_load');
    Route::get('/invoice/search/list/{from_date}/{to_date}/{customer_name}/{sm_name}/{invoice_number}', 'OrdersController@invoiceListSearch')->name('orders_list_search');
    Route::get('/delivery/search/list/{from_date}/{to_date}/{customer_name}/{sm_name}/{invoice_number}', 'OrdersController@deliveryListSearch')->name('orders_delivery_list_search');
    Route::get('/invoice/pos/search/product/{id}', 'OrdersController@posSearchProduct')->name('orders_pos_search_product');
    Route::post('/customer/add/invoices', 'OrdersController@customerStore');
    Route::get('/customer/list/invoices', 'OrdersController@customersListInvoice');

    Route::get('/customer-make-payment/{id}', 'OrdersController@makePayment');
    Route::post('/pay-customer-bill/payments', 'OrdersController@storePayment')->name('pay_customer_bill');
    Route::get('/search/coupon-code/{id}', 'OrdersController@couponCode')->name('orders_coupon_code');

    Route::get('/print-invoices-list', 'OrdersController@printInvoicesList')->name('orders_print_orders_list');
    Route::get('/print-invoices-search/{date}/{customer}/{invoice_number}', 'OrdersController@printInvoicesSearch')->name('orders_print_orders_search');
    Route::get('/calculate-opening-balance/{customer_id}', 'OrdersController@calculateOpeningBalance')->name('orders_calculate_opening_balance');

    Route::get('/return-sr-product-stock/{sr_id}/{product_entry_id}', 'OrdersController@srProductStock')->name('orders_sr_product_stock');

    Route::get('/order-edit/{id}', 'OrdersController@orderEdit')->name('orders_edit_order');
    Route::post('/order-update/{id}', 'OrdersController@orderUpdate')->name('orders_update_order');
    Route::post('/issue-from-order', 'OrdersController@issueFromOrder')->name('orders_issue_from_order');
});
