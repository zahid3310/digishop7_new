<?php

namespace Modules\Bills\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\Customers;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\ProductEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\ProductVariations;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\SrItems;
use App\Models\FreeItems;
use Response;
use DB;
use App\Models\Units;

class BillsController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {   
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $products           = ProductEntries::orderBy('product_code', 'DESC')
                                            ->get();

        $product_id         = array_values($products->sortByDesc('product_code')->take(1)->toArray());
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts           = Accounts::where('account_type_id',12)->where('status', 1)->get();
        $units              = Units::orderBy('id', 'ASC')->get();
        $variations         = ProductVariations::orderBy('id', 'ASC')->get();

        return view('bills::index', compact('products', 'product_id', 'paid_accounts', 'units', 'variations', 'accounts'));
    }

    public function allBills()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::all_bills');
    }

    public function create()
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        return view('bills::create');
    }

    public function store(Request $request)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $data_find                      = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                    = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                           = new Bills;
            $bill->bill_number              = $bill_number;
            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $data['total_amount'];
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->previous_due             = $data['previous_due'];
            $bill->previous_due_type        = $data['balance_type'];
            $bill->adjusted_amount          = $data['adjustment'];
            $bill->account_id               = $data['account_id'];
            $bill->created_by               = $user_id;

            if ($bill->save())
            {
                foreach ($data['product_entries'] as $key1 => $value1)
                {
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'                 => $bill['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value1,
                        'vendor_id'               => $bill['vendor_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key1],
                        'conversion_unit_id'      => $data['unit_id'][$key1],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key1]) ? $data['free_item_main_unit_id'][$key1] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key1]) ? $data['free_unit_id'][$key1] : null,
                        'rate'                    => $data['rate'][$key1],
                        'free_product_entry_id'   => $data['free_items'][$key1],
                        'free_quantity'           => $data['free_quantity'][$key1],
                        'quantity'                => $data['quantity'][$key1],
                        'total_amount'            => $data['amount'][$key1],
                        'discount_type'           => $data['discount_type'][$key1],
                        'discount_amount'         => $data['discount'][$key1],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    if (isset($data['free_unit_id'][$key1]))
                    {

                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key1])
                                            ->where('converted_unit_id', $data['free_unit_id'][$key1])
                                            ->where('product_entry_id', $value1)
                                            ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key1]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key1];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($data['free_quantity'][$key1] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'bill_id'           => $bill['id'],
                            'customer_id'       => $data['vendor_id'],
                            'product_entry_id'  => $value1,
                            'total_quantity'    => $data['quantity'][$key1],
                            'free_quantity'     => $free_converted_quantity_to_main_unit,
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key1],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('bill_entries')->insert($bill_entries);

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                stockIn($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=9, $amount=$data['total_amount'], $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=9, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                credit($customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }

                    supplierBalanceUpdate($data['vendor_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_index')->with("success","Purchase Created Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                } 
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $bill       = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->select('bills.*',
                                         'customers.name as customer_name',
                                         'customers.address as address',
                                         'customers.phone as phone')
                                ->find($id);

        $entries    = BillEntries::leftjoin('products', 'products.id', 'bill_entries.product_id')
                                ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                ->where('bill_entries.bill_id', $id)
                                ->orderBy('bill_entries.id', 'DESC')
                                ->select('bill_entries.*',
                                         'product_entries.product_type as product_type',
                                         'product_entries.name as product_entry_name',
                                         'products.name as product_name')
                                ->get();  
                     
        $user_info  = userDetails();

        return view('bills::show', compact('entries', 'bill', 'user_info'));
    }

    public function edit($id)
    {
        $find_bill              = Bills::leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                        ->orderBy('bills.created_at', 'DESC')
                                        ->select('bills.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name')
                                        ->find($id);

        $find_bill_entries      = BillEntries::leftjoin('customers', 'customers.id', 'bill_entries.vendor_id')
                                        ->leftjoin('product_entries', 'product_entries.id', 'bill_entries.product_entry_id')
                                        ->where('bill_entries.bill_id', $id)
                                        ->select('bill_entries.*',
                                                 'customers.id as vendor_id',
                                                 'customers.name as vendor_name',
                                                 'product_entries.id as item_id',
                                                 'product_entries.product_code as product_code',
                                                 'product_entries.stock_in_hand as stock_in_hand',
                                                 'product_entries.name as item_name')
                                        ->get();

        $entries_count          = $find_bill_entries->count();

        $find_customer          = Customers::find($find_bill['vendor_id']);
        $current_balance        = JournalEntries::whereIn('transaction_head', ['payment-made'])
                                                ->where('bill_id', $id)
                                                ->where('debit_credit', 0)
                                                ->selectRaw('journal_entries.*')
                                                ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = Accounts::where('account_type_id', 4)
                                                ->whereNotIn('id', [2,3])
                                                ->where('status', 1)
                                                ->get();
        $accounts               = Accounts::where('account_type_id',12)->where('status', 1)->get();

        if ((Auth::user()->branch_id != $find_bill->branch_id) && (Auth::user()->branch_id != 1))
        {
            return back()->with('unsuccess', 'You are not allowed to edit this invoice');
        }

        return view('bills::edit', compact('find_bill', 'find_bill_entries', 'entries_count', 'paid_accounts', 'find_customer', 'current_balance', 'current_balance_count', 'accounts'));
    }

    public function update(Request $request, $id)
    {
        //Users Access Level Start
        $access_check  = userAccess(Auth::user()->id);
        if ($access_check == 0)
        {
            return back()->with('unsuccess', 'You have not enough permission to do this operation !!');
        }
        //Users Access Level End

        $rules = array(
            'selling_date'      => 'required',
            'vendor_id'         => 'required',
            'product_entries.*' => 'required',
            'amount.*'          => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];

            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            $old_bill       = Bills::find($id);
            $bill           = Bills::find($id);

            //Calculate Due Amount

                if ($data['total_amount'] > $bill['bill_amount']) 
                {
                    $bill_dues = $bill['due_amount'] + ($data['total_amount'] - $bill['bill_amount']);

                }
                
                if ($data['total_amount'] < $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'] - ($bill['bill_amount'] - $data['total_amount']);
                }

                if ($data['total_amount'] == $bill['bill_amount'])
                {
                    $bill_dues = $bill['due_amount'];
                }
                
            $bill->vendor_id                = $data['vendor_id'];
            $bill->bill_date                = date('Y-m-d', strtotime($data['selling_date']));
            $bill->bill_amount              = $data['total_amount'];
            $bill->due_amount               = $bill_dues;
            $bill->total_discount           = $discount;
            $bill->bill_note                = $data['bill_note'];
            $bill->total_vat                = $vat;
            $bill->vat_type                 = $data['vat_type'];
            $bill->total_discount_type      = $data['total_discount_type'];
            $bill->total_discount_amount    = $data['total_discount_amount'];
            $bill->total_discount_note      = $data['total_discount_note'];
            $bill->cash_given               = $data['cash_given'];
            $bill->change_amount            = $data['change_amount'];
            $bill->updated_by               = $user_id;

            if ($bill->save())
            {
                $item_id     = BillEntries::where('bill_id', $bill['id'])->get();
                $item_delete = BillEntries::where('bill_id', $bill['id'])->delete();
                $free_items  = FreeItems::where('bill_id', $bill['id'])->get();

                if ($free_items != null)
                {
                    $delete_free_items = FreeItems::where('bill_id', $bill['id'])->get();
                }
        
                foreach ($data['product_entries'] as $key1 => $value1)
                {
                    $product_buy_price      = ProductEntries::find($value1);

                    $bill_entries[] = [
                        'bill_id'                 => $bill['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value1,
                        'main_unit_id'            => $data['main_unit_id'][$key1],
                        'conversion_unit_id'      => $data['unit_id'][$key1],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key1]) ? $data['free_item_main_unit_id'][$key1] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key1]) ? $data['free_unit_id'][$key1] : null,
                        'vendor_id'               => $bill['vendor_id'],
                        'rate'                    => $data['rate'][$key1],
                        'free_product_entry_id'   => $data['free_items'][$key1],
                        'free_quantity'           => $data['free_quantity'][$key1],
                        'quantity'                => $data['quantity'][$key1],
                        'total_amount'            => $data['amount'][$key1],
                        'discount_type'           => $data['discount_type'][$key1],
                        'discount_amount'         => $data['discount'][$key1],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    if (isset($data['free_unit_id'][$key1]))
                    {

                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key1])
                                            ->where('converted_unit_id', $data['free_unit_id'][$key1])
                                            ->where('product_entry_id', $value1)
                                            ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key1]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key1];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($data['free_quantity'][$key1] != 0)
                    {
                        $free_item_list[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'bill_id'           => $bill['id'],
                            'customer_id'       => $data['vendor_id'],
                            'product_entry_id'  => $value1,
                            'total_quantity'    => $data['quantity'][$key1],
                            'free_quantity'     => $free_converted_quantity_to_main_unit,
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key1],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('bill_entries')->insert($bill_entries);

                if (isset($free_item_list))
                {
                    DB::table('free_items')->insert($free_item_list);
                }

                stockIn($data, $item_id);

                $jour_ent_debit     = JournalEntries::where('bill_id', $bill->id)
                                        ->where('transaction_head', 'purchase')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('bill_id', $bill->id)
                                        ->where('transaction_head', 'purchase')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Start
                    debitUpdate($jour_ent_debit['id'], $customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditUpdate($jour_ent_credit['id'], $customer_id=$data['vendor_id'], $date=$data['selling_date'], $account_id=9, $amount=$data['total_amount'], $note=null, $transaction_head='purchase', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $pay_debit  = JournalEntries::find($data['current_balance_id'][$i] - 1);
                                $pay_credit = JournalEntries::find($data['current_balance_id'][$i]);

                                if ($pay_debit != null)
                                {   
                                    debitUpdate($pay_debit['id'], $customer_id=$data['vendor_id'], $date=$data['current_balance_payment_date'][$i], $account_id=9, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    debit($customer_id=$data['vendor_id'], $date=$data['current_balance_payment_date'][$i], $account_id=9, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }

                                if ($pay_credit != null)
                                {
                                    creditUpdate($pay_credit['id'], $customer_id=$data['vendor_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    credit($customer_id=$data['vendor_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-made', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                
                            }
                            else
                            {
                                $pay_debit  = JournalEntries::where('id', $data['current_balance_id'][$i] - 1)->delete();
                                $pay_credit = JournalEntries::where('id', $data['current_balance_id'][$i])->delete();
                            }
                        }
                    }

                    supplierBalanceUpdate($data['vendor_id']);
                //Financial Accounting End

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return redirect()->route('bills_all_bills')->with("success","Purchase Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('bills_show', $bill['id']);
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function ProductEntriesList()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }
    
    public function ProductCategoryList()
    {
        $data  = Products::all();

        return Response::json($data);
    }
    
    public function ProductEntriesCategoryProductList($entry_id)
    {
        $data       = ProductEntries::where('product_entries.product_id',$entry_id)
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function ProductEntriesListInvoice()
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }
    
    public function ProductEntriesCategoryListInvoice($entry_id)
    {
        $data       = ProductEntries::where('product_entries.product_id',$entry_id)
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function billListLoad()
    {
        $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();

        return Response::json($data);
    }

    public function billListSearch($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->where('customers.name', 'LIKE', "%$id%")
                                ->orWhere('bills.bill_date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number, $table_id) {
                                    return $query->orWhere('bills.bill_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        else
        {
            $data           = Bills::leftjoin('purchase_return', 'purchase_return.bill_id', 'bills.id')
                                ->leftjoin('customers', 'customers.id', 'bills.vendor_id')
                                ->leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                                ->where('bills.type', 1)
                                ->orderBy('bills.created_at', 'DESC')
                                ->select('bills.*',
                                         'purchase_return.id as return_id',
                                         'customers.contact_type as contact_type',
                                         'customers.name as customer_name')
                                ->distinct('bills.id')
                                ->take(100)
                                ->get();
        }
        
        return Response::json($data);
    }

    public function storeProduct(Request $request)
    {
        $user_id                        = Auth::user()->id;
        $data                           = $request->all();

        DB::beginTransaction();

        try{
            $data_find                  = ProductEntries::orderBy('id', 'DESC')->first();
            $code                       = $data_find != null ? $data_find['product_code'] + 1 : 1;

            $product                    = new ProductEntries;
            $product->product_id        = $data['product_category_id'];
            $product->sub_category_id   = $data['product_sub_category_id'];
            $product->name              = $data['product_name'];
            $product->product_code      = $code;
            $product->sell_price        = $data['selling_price'];
            $product->buy_price         = $data['buying_price'];

            if ($data['unit_id'] != null)
            {
                $product->unit_id       = $data['unit_id'];
            }

            $product->status                        = $data['status'];
            $product->alert_quantity                = $data['alert_quantity'];
            $product->product_type                  = $data['product_type'];
            $product->created_by                    = $user_id;

            if ($product->save())
            {   
                DB::commit();
                return Response::json($product);
            }
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }
    
    public function customersListBills()
    {
        
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::where('customers.contact_type',1)
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orWhere('customers.code', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->where('customers.contact_type',1)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "code" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "code" =>$value['code'], "contact_type" =>$value['contact_type']);

            $i++;
        }

   
        return Response::json($data);
    }

    public function billProductList()
    {
        $data       = Products::orderBy('products.total_sold', 'DESC')
                                    ->select('products.*')
                                    ->get();

        return Response::json($data);
    }

    public function posSearchProductBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.product_code', $id)
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->first();

        return Response::json($data);
    }

    public function ProductEntriesListBill($id)
    {
        $data       = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();

        $product    = $data->sortBy('name')->all();
        $products   = collect($product);

        return Response::json($products);
    }

    public function productListLoadBill($id)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];
            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();   
        }


        foreach ($fetchData as $key => $value)
        {
            $name   = $value['name'] . ' ' . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function freeProductListLoadBill()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();
        }
        else
        { 
            $search         = $_GET['searchTerm'];
            $fetchData      = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                    ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                    ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->groupBy('product_entries.id')
                                    ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                 GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                 GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                 GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                 GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                 GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations
                                                ')
                                    ->orderBy('product_entries.product_id', 'ASC')
                                    ->get();   
        }


        foreach ($fetchData as $key => $value)
        {
            $name   = $value['name'] . ' ' . '( ' . str_pad($value['product_code'], 6, "0", STR_PAD_LEFT) . ' )';

            $data[] = array("id"=>$value['id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function getConversionParam($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.converted_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*')
                                    ->first();

        return Response::json($data);
    }

    public function getConversionParamMainUnit($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::leftjoin('product_entries', 'product_entries.id', 'unit_conversions.product_entry_id')
                                    ->where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.main_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*, product_entries.sell_price as sell_price_pr')
                                    ->first();

        return Response::json($data);
    }

    public function getConversionParamFreeItem($product_entry_id, $conversion_unit_id)
    {
        $data       = UnitConversions::where('unit_conversions.product_entry_id', $product_entry_id)
                                    ->where('unit_conversions.converted_unit_id', $conversion_unit_id)
                                    ->selectRaw('unit_conversions.*')
                                    ->first();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }

    public function adjustAdvancePayment($customer_id)
    {
        $data  = Customers::find($customer_id);

        if ($data['balance'] < 0)
        {
            $result = abs($data['balance']);
        }
        else
        {
            $result = 0;
        }

        return Response::json($result);
    }

    public function getProducts($brand_id, $category_id)
    {
        $data   = ProductEntries::where('brand_id', $brand_id)
                                    ->where('product_id', $category_id)
                                    ->select('product_entries.*')
                                    ->orderBy('id', 'ASC')
                                    ->get();

        if ($data->count() > 0)
        {
            foreach($data as $key => $val)
            {
                $data[$key]['id']                   = $val->id;
                $data[$key]['name']                 = $val->name;
                $data[$key]['main_unit_id']         = $val->unit_id;
                $data[$key]['main_unit_name']       = $val->unit->name;
                $data[$key]['rate']                 = $val->sell_price;
                $data[$key]['stock_in_hand']        = $val->stock_in_hand != null ? $val->stock_in_hand : 0;
                $data[$key]['units']                = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                            ->where('product_entry_id', $val->id)
                                                            ->select('units.id', 'units.name')
                                                            ->get();
            }
        }

        return Response::json($data);
    }
}
