@extends('layouts.app')

@section('title', 'Edit Stores')

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Basic Settings</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">Stores</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('unsuccess'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                <form id="FormSubmit" action="{{ route('products_stores_update', $find_store['id']) }}" method="post" files="true" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="form-group row">
                                    <div style="margin-bottom: 0px !important" class="col-md-3 form-group">
                                        <label for="productname">Name *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" value="{{ $find_store['name'] }}" />
                                    </div>

                                    <div style="margin-bottom: 0px !important" class="col-md-3 form-group">
                                        <label for="productname">Address</label>
                                        <input type="text" name="address" class="inner form-control" id="address" placeholder="Enter Store Address" value="{{ $find_store['address'] }}" />
                                    </div>

                                    <div style="margin-bottom: 0px !important" class="col-md-3 form-group">
                                        <label for="productname">Phone</label>
                                        <input type="text" name="phone" class="inner form-control" id="phone" placeholder="Enter Phone" value="{{ $find_store['phone'] }}" />
                                    </div>

                                    <div style="margin-bottom: 0px !important" class="col-md-3 form-group">
                                        <label for="productname">Email</label>
                                        <input type="text" name="email" class="inner form-control" id="email" placeholder="Enter Store Email" value="{{ $find_store['email'] }}" />
                                    </div>
                                </div>

                                <hr>

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('products_stores_index') }}">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">All Stores</h4>

                                <br>

                                <table id="datatable" class="table table-bordered" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Email</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        @if(!empty($stores) && ($stores->count() > 0))
                                        @foreach($stores as $key => $store)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $store['name'] }}</td>
                                                <td>{{ $store['address'] }}</td>
                                                <td>{{ $store['phone'] }}</td>
                                                <td>{{ $store['email'] }}</td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="{{ route('products_stores_edit', $store['id']) }}">Edit</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection