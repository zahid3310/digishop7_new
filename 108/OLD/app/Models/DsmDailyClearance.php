<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class DsmDailyClearance extends Model
{  
    protected $table = "dsm_daily_clearance";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
    
    public function dsm()
    {
        return $this->belongsTo('App\Models\Customers','dsm_id');
    }
}
