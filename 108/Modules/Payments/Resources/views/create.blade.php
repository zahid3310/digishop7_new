@extends('layouts.app')

<?php
    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 0))
    {
        $title = 'Due Collection';
    }

    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 1))
    {
        $title = 'Due Payment';
    }

    if (isset($_GET['payment_type']) && ($_GET['payment_type'] == 2))
    {
        $title = 'Salary Payment';
    }

    if (!isset($_GET['payment_type']))
    {
        $title = 'Payments';
    }
?>

@push('scripts')
<style type="text/css">
    body[data-sidebar=dark].vertical-collpsed {
        min-height: 0px !important;
    }
    
    .select2-container--default .select2-results>.select2-results__options {
        width: 450px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 450px;
    }
</style>
@endpush

@section('title', $title)

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">{{ __('messages.payment')}}</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">{{ __('messages.payment')}}</a></li>
                                    <li class="breadcrumb-item active">{{ __('messages.payment')}}</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
                        <div style="height: 271px" class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
                                            <label class="control-label">{{ __('messages.payment_type')}} *</label>
                                            <select id="type" style="width: 100%;cursor: pointer" class="form-control" onchange="searchContact()">
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 0))
                                                <option value="0">{{ __('messages.sr/customer_payment')}}</option>
                                                @endif
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 1))
                                                <option value="1">{{ __('messages.s_payment')}}</option>
                                                @endif
                                                @if(isset($_GET['payment_type']) && ($_GET['payment_type'] == 2))
                                                <option value="2">Salary Payment</option>
                                                @endif
                                                @if(!isset($_GET['payment_type']))
                                                <option value="0">{{ __('messages.sr/customer_payment')}}</option>
                                                <option value="1">{{ __('messages.s_payment')}}</option>
                                                <option value="2">Salary Payment</option>
                                                @endif
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                    	<div id="contact_id_reload" class="form-group ajax-select mt-3 mt-lg-0">
	                                        <label class="control-label">{{ __('messages.search_contact')}} *</label>
	                                        <select id="contact_id" style="width: 100%;cursor: pointer" class="form-control select2 contact_id" onchange="searchContact()">
	                                        	<option value="">--{{ __('messages.select_contact')}}--</option>
												@if(!empty($customers))
                                                    <?php 
                                                        if (isset($_GET['payment_type']))
                                                        {
                                                            $customers = $customers->where('contact_type', $_GET['payment_type']);
                                                        }
                                                        else
                                                        {
                                                            $customers = $customers;
                                                        }
                                                    ?>
													@foreach($customers as $key => $customer)
													<option @if(isset($find_customer)) {{ $find_customer['id'] == $customer['id'] ? 'selected' : '' }} @endif value="{{ $customer->id }}">{{ $customer->name }}</option>
													@endforeach
												@endif
	                                        </select>
	                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-8 col-sm-8 col-lg-8 col-xs-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('payments_store') }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                    <input id="customerIdHidden" name="customer_id" type="hidden" class="form-control">
                                    <input id="customerIdHiddenReload" name="" type="hidden" class="form-control" value="{{Session('find_customer')}}">
                                    <input id="typeHiddenReload" name="" type="hidden" class="form-control" value="{{Session('find_type')}}">
                                    <input id="typeInput" name="type" type="hidden" class="form-control">

            						<div class="row">
					                	<div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="payment_date">{{ __('messages.payment_date')}} *</label>
					                            <input id="payment_date" name="payment_date" type="text" value="{{ date('d-m-Y') }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true">
					                        </div>
					                    </div>

					                    <div class="col-sm-4">
					                        <div class="form-group">
					                            <label for="amount">{{ __('messages.amount')}} *</label>
					                            <input id="amount" name="amount" type="text" class="form-control">
					                        </div>
					                    </div>

                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="discount_amount">Discount</label>
                                                <input id="discount_amount" name="discount_amount" type="text" class="form-control">
                                            </div>
                                        </div>

					                    <div class="col-sm-6 form-group">
				                            <label class="control-label">{{ __('messages.paid_through')}}</label>
                                            <select style="cursor: pointer" name="paid_through" class="form-control select2">
                                                @if(!empty($paid_accounts) && ($paid_accounts->count() > 0))
                                                @foreach($paid_accounts as $key => $paid_account)
                                                    <option value="{{ $paid_account['id'] }}">{{ $paid_account['account_name'] }}</option>
                                                @endforeach
                                                @endif
                                            </select>
				                        </div>

				                        <div class="col-sm-6">
					                        <div class="form-group">
					                            <label for="note">{{ __('messages.note')}}</label>
					                            <input id="note" name="note" type="text" class="form-control">
					                        </div>
					                    </div>
				                	</div>

                                    <hr style="margin-top: 0px !important">

                                    <div class="form-group row">
                                        <div class="button-items col-md-12">
                                            <button id="submitButtonId" type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">{{ __('messages.make_payment')}}</button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('payments_create') }}">{{ __('messages.close')}}</a></button>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-md-12 col-sm-12 col-lg-12 col-xs-12">
                        <div class="row">

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div style="height: 100px" class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <h4 style="font-size: 14px" id="customerName" class="mb-0"></h4>
                                                <p style="font-size: 14px" id="customerAddress" class="text-muted font-weight-medium"></p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <p id="receivablePShow" class="text-muted font-weight-medium">{{ __('messages.total_receivables')}}</p>
                                                <p id="payablePShow" class="text-muted font-weight-medium">{{ __('messages.total_payable')}}</p>
                                                <h4 id="totalReceivable" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <p id="receivedPShow" class="text-muted font-weight-medium">{{ __('messages.total_received')}}</p>
                                                <p id="paidPShow" class="text-muted font-weight-medium">{{ __('messages.total_paid')}}</p>
                                                <h4 id="totalReceived" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3">
                                <div class="card col-md-12 col-sm-12 col-lg-12 col-xs-12">
                                    <div class="card-body">
                                        
                                        <div class="media">
                                            <div class="media-body">
                                                <p class="text-muted font-weight-medium">{{ __('messages.balance')}}</p>
                                                <h4 id="totalDues" class="mb-0">0.00</h4>
                                            </div>

                                            <div class="mini-stat-icon avatar-sm align-self-center rounded-circle bg-primary">
                                                <span class="avatar-title">
                                                    <i class="bx bx-credit-card font-size-24"></i>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr>

                <div class="row">

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title">{{ __('messages.payment_list')}}</h4>
                                
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label class="col-md-3 col-form-label">From</label>
                                            <div class="col-md-9">
                                                <input style="cursor: pointer" id="search_from_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label class="col-md-3 col-form-label">To</label>
                                            <div class="col-md-9">
                                                <input style="cursor: pointer" id="search_to_date" type="date" class="form-control" value="">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.customer')}} </label>
                                            <div class="col-md-7">
                                                <select style="width: 100%" id="payment_customer_id" class="form-control select2">
                                                    <option value="0">{{ __('messages.all')}}</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div style="margin-bottom: 15px;margin-top: 15px;padding-right: 15px" class="form-group row">
                                            <label style="text-align: right" class="col-md-5 col-form-label">{{ __('messages.payment')}}# </label>
                                            <div class="col-md-7">
                                                <div class="row">
                                                    <input type="text" style="width: 73%" id="payment_number" name="payment_number" class="form-control select2 col-lg-9 col-md-9 col-sm-9 col-9">
                                                    <span style="float: right;cursor: pointer;background-color: #556EE6;color: white;text-align: center" class="form-control col-lg-3 col-md-3 col-sm-3 col-3" onclick="searchPaymentList()">
                                                        <i class="bx bx-search font-size-22"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div> 
                                    </div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>{{ __('messages.sl')}}</th>
                                            <th>{{ __('messages.date')}}</th>
                                            <th>{{ __('messages.payment')}}#</th>
                                            <th>{{ __('messages.contact')}}</th>
                                            <th>{{ __('messages.type')}}</th>
                                            <th>{{ __('messages.paid_through')}}</th>
                                            <th>{{ __('messages.note')}}</th>
                                            <th>{{ __('messages.amount')}}</th>
                                            <th>{{ __('messages.action')}}</th>
                                        </tr>
                                    </thead>

                                    <tbody id="payment_list"></tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <input type="hidden" id="payment_id_put">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="example-text-input" class="col-md-12 col-form-label">Are you sure want to delete the entry ?</label>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="submit" id="delete_btn" class="btn btn-primary waves-effect waves-light delete_btn">Delete</button>
                    <button id="CloseButton" type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#payment_customer_id").select2({
                ajax: { 
                url:  site_url + '/payments/payment/customerList',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    if (result['contact_type'] == 0 || result['contact_type'] == 1 || result['id'] == 0)
                    {
                        return result['text'];
                    }
                },
            });

            var customerId      = $('#customerIdHiddenReload').val();
            var typeId          = $('#typeHiddenReload').val();
            var type_input      = $('#type').val();

            $('#contact_id').val(customerId).change();
            $('#typeInput').val(type_input);

            if(typeId != '')
            {    
                $('#type').val(typeId).change();
            }

            var site_url        = $('.site_url').val();
            var contact_id      = $('#contact_id').val();

            if (contact_id == '')
            {
                var contact_id_find = 0;
            }
            else
            {
                var contact_id_find = $('#contact_id').val();
            }

            $.get(site_url + '/payments/payment/list/' + contact_id_find, function(data){

                paymentList(data);    
            });

            searchContact();
        });

        function searchContact()
        {
            var contact_id  = $('#contact_id').val();
            var site_url    = $('.site_url').val();
            var amounts     = $('#amount').val();
            var type_input  = $('#type').val();

            $('#typeInput').val(type_input);

            if (amounts == '')
            {
                var amount = 0;
            }
            else
            {
                var amount = amounts;
            }

            if (type_input == 0)
            {
                $('#receivablePShow').show();
                $('#receivedPShow').show();
                $('#payablePShow').hide();
                $('#paidPShow').hide();

                $.get(site_url + '/payments/contact/list/' + contact_id, function(data){

                    if (data.phone != null)
                    {
                        var phone = data.phone;
                    }
                    else
                    {
                        var phone = '';
                    }

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + phone;
                    }
                    else
                    {
                        var address = '';
                    }

                    if (data.receivable != null)
                    {
                        var receivable = data.receivable;
                    }
                    else
                    {
                        var receivable = 0;
                    }

                    if (data.received != null)
                    {
                        var received = data.received;
                    }
                    else
                    {
                        var received = 0;
                    }

                    var dues = parseFloat(data.dues);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(receivable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(parseFloat(received));
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);    
                });
            }
            
            if (type_input == 1)
            {
                $('#receivablePShow').hide();
                $('#receivedPShow').hide();
                $('#payablePShow').show();
                $('#paidPShow').show();

                $.get(site_url + '/payments/contact/list/bill/' + contact_id, function(data){

                    if (data.phone != null)
                    {
                        var phone = data.phone;
                    }
                    else
                    {
                        var phone = '';
                    }

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + phone;
                    }
                    else
                    {
                        var address = '';
                    }

                    if (data.payable != null)
                    {
                        var payable = data.payable;
                    }
                    else
                    {
                        var payable = 0;
                    }

                    if (data.paid != null)
                    {
                        var paid = data.paid;
                    }
                    else
                    {
                        var paid = 0;
                    }

                    var dues = parseFloat(data.dues);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(payable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(paid);
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);
                });
            }

            if (type_input == 2)
            {
                $('#receivablePShow').hide();
                $('#receivedPShow').hide();
                $('#payablePShow').show();
                $('#paidPShow').show();

                $.get(site_url + '/payments/contact/list/employee/' + contact_id, function(data){

                    if (data.phone != null)
                    {
                        var phone = data.phone;
                    }
                    else
                    {
                        var phone = '';
                    }

                    if (data.address != null)
                    {
                        var address = data.address  + "<br>" + phone;
                    }
                    else
                    {
                        var address = '';
                    }

                    if (data.payable != null)
                    {
                        var payable = data.payable;
                    }
                    else
                    {
                        var payable = 0;
                    }

                    if (data.paid != null)
                    {
                        var paid = data.paid;
                    }
                    else
                    {
                        var paid = 0;
                    }

                    var dues = parseFloat(data.dues);

                    $("#customerName").empty();
                    $("#customerName").html(data.name);
                    $("#customerAddress").empty();
                    $("#customerAddress").html(address);
                    $("#totalReceivable").empty();
                    $("#totalReceivable").html(payable);
                    $("#totalReceived").empty();
                    $("#totalReceived").html(paid);
                    $("#totalDues").empty();
                    $("#totalDues").html(dues);
                });
            }
            
            $("#customerIdHidden").val(contact_id);

            //Payment List Show In The Index Section For A Specific Contact ID
            $.get(site_url + '/payments/payment/list/' + contact_id, function(data){

                paymentList(data);
            });
        }

        function formatDate(date)
        {
            var d       = new Date(date),
                month   = '' + (d.getMonth() + 1),
                day     = '' + d.getDate(),
                year    = d.getFullYear();

            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;

            return [day, month, year].join('-');
        }

        function searchPaymentList()
        {
            var site_url                = $('.site_url').val();
            var search_from_date        = $('#search_from_date').val();
            var search_to_date          = $('#search_to_date').val();
            var search_customer         = $('#payment_customer_id').val();
            var search_payment_number   = $('#payment_number').val();

            if (search_from_date != '')
            {
                var from_date = $('#search_from_date').val();
            }
            else
            {
                var from_date = 0;
            }

            if (search_to_date != '')
            {
                var to_date = $('#search_to_date').val();
            }
            else
            {
                var to_date = 0;
            }

            if (search_customer != '')
            {
                var customer = $('#payment_customer_id').val();
            }
            else
            {
                var customer = 0;
            }

            if (search_payment_number != '')
            {
                var payment = $('#payment_number').val();
            }
            else
            {
                var payment = 0;
            }

            $.get(site_url + '/payments/payment/search/list/' + from_date + '/' + to_date + '/' + customer + '/' +payment, function(data){

                paymentList(data);
            });
        }
    </script>

    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>

    <script type="text/javascript">
        function paymentList(data)
        {
            var payment_list    = '';
            var site_url        = $('.site_url').val();
            $.each(data, function(i, payment_data)
            {
                var serial      = parseFloat(i) + 1;

                var locationValue   = (new URL(location.href)).searchParams.get('payment_type');
                if (locationValue   != null)
                {
                    var show_url    = site_url + '/payments/show/' + payment_data.id + '?payment_type=' + locationValue;
                    var delete_url  = site_url + '/payments/delete/' + payment_data.id + '?payment_type=' + locationValue;
                }
                else
                {
                    var show_url    = site_url + '/payments/show/' + payment_data.id;
                    var delete_url  = site_url + '/payments/delete/' + payment_data.id;
                }

                if (payment_data.type == 0)
                {
                    var p_type = "Receive From Customer";
                }

                if (payment_data.type == 1)
                {
                    var p_type = "Pay To Supplier";
                }

                if (payment_data.type == 2)
                {
                    var p_type = "Salary Payment";
                }

                if(payment_data.note != null)
                {
                    var p_note = payment_data.note;
                }
                else
                {
                    var p_note = "";
                }
                
                if (locationValue != null)
                {
                    if (payment_data.type == locationValue)
                    {
                        payment_list += '<tr>' +
                                            '<input class="form-control paymentId" type="hidden" name="payment_id[]" value="' +  payment_data.id + '">' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(payment_data.payment_date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               'PM - ' + payment_data.payment_number.padStart(5, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.accounts_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_note +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (payment_data.amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + show_url +'" target="_blank">' + 'Show' + '</a>' +
                                                        '<a class="dropdown-item" href="' + delete_url +'" data-toggle="modal" data-target="#myModal" onclick="putId('+payment_data.id+')">' + 'Delete' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                    }
                }
                else
                {
                    payment_list += '<tr>' +
                                            '<input class="form-control paymentId" type="hidden" name="payment_id[]" value="' +  payment_data.id + '">' +
                                            '<td style="text-align: left">' +
                                                serial +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                formatDate(payment_data.payment_date) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               'PM - ' + payment_data.payment_number.padStart(5, '0') +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.customer_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_type +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               payment_data.accounts_name +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               p_note +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                               (payment_data.amount) +
                                            '</td>' +
                                            '<td style="text-align: left">' +
                                                '<div class="dropdown">' +
                                                    '<a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">' +
                                                        '<i class="mdi mdi-dots-horizontal font-size-18">' + '</i>' +
                                                    '</a>' +
                                                    '<div class="dropdown-menu dropdown-menu-right" style="">' +
                                                        '<a class="dropdown-item" href="' + show_url +'" target="_blank">' + 'Show' + '</a>' +
                                                        '<a class="dropdown-item delete" href="' + delete_url +'" data-toggle="modal" data-target="#myModal" onclick="putId('+payment_data.id+')">' + 'Delete' + '</a>' +
                                                    '</div>' +
                                                '</div>' +
                                            '</td>' +
                                        '</tr>';
                }
            });

            $("#payment_list").empty();
            $("#payment_list").append(payment_list);
        }
    </script>

    <script type="text/javascript">
        function putId(x)
        {
            $('#payment_id_put').val(x);
        }

        $('.delete_btn').click(function () 
        {
            var site_url            = $('.site_url').val();
            var id                  = $('#payment_id_put').val();
            window.location.href    = site_url + "/payments/delete/"+id;
        });
    </script>
@endsection