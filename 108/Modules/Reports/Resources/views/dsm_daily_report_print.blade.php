<!DOCTYPE html>
<html>

<head>
    <title>DSM Daily Report</title>
    <link rel="icon" href="{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: right; min-height: 82px;">
                        <p><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p>{{ $user_info['address'] }}</p>
                        <!-- <p>phone</p> -->
                        <!-- <p>Email</p> -->
                        <!-- <p>Website</p> -->
                        <p>{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">DSM Daily Report</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center">Date From</th>
                                    <th style="text-align: center">DSM Name</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center">{{ date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ date('d-m-Y', strtotime($to_date)) }}</td>
                                    
                                    <td style="text-align: center">
                                        @if($dsm != null)
                                            {{ $dsm['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%">SL</th>
                                    <th style="text-align: center;width: 15%">DSM</th>
                                    <th style="text-align: center;width: 10%">Receive</th>
                                    <th style="text-align: center;width: 10%">Return</th>
                                    <th style="text-align: center;width: 10%">Damage</th>
                                    <th style="text-align: center;width: 10%">Sales</th>
                                    <th style="text-align: center;width: 10%">Return Back</th>
                                    <th style="text-align: center;width: 10%">Balance</th>
                                </tr>
                            </thead>

                            <tbody class="theight">

                                @foreach($data as $key => $value)
                                <tr>
                                    <td style="text-align: left;" colspan="9"><strong>{{ date('d-m-Y', strtotime($key)) }}</strong></td>
                                </tr>
                                <?php $i = 1; ?>
                                @foreach($value as $key1 => $value1)
                                <tr>
                                    <td style="text-align: center;">{{ $i }}</td>
                                    <td style="text-align: left;">{{ $value1['dsm'] }}</td>
                                    <td style="text-align: right;">{{ round($value1['issue']) }}</td>
                                    <td style="text-align: right;">{{ round($value1['return']) }}</td>
                                    <td style="text-align: right;">{{ round($value1['damage']) }}</td>
                                    <td style="text-align: right;">{{ round($value1['sales']) }}</td>
                                    <td style="text-align: right;">{{ round($value1['return_back']) }}</td>
                                    <td style="text-align: right;">{{ abs(round($value1['issue'] - $value1['return'] - $value1['damage'] - $value1['sales'] - $value1['return_back'])) }}</td>
                                </tr>
                                <?php $i++; ?>
                                @endforeach
                                @endforeach
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>