function getProductList(x)
{
    var site_url    = $('.site_url').val();
    var group_id    = $("#group_id_"+x).val();
    var brand_id    = $("#brand_id_"+x).val();
    var category_id = $("#product_category_id_"+x).val();

    $("#product_entries_"+x).select2({
        ajax: { 
            url:  site_url + '/groups/product-list-ajax/' + group_id + '/' + brand_id + '/' + category_id,
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';

                return result['text'];
            },
    });
}

function getConversionParam(x)
{
    var site_url            = $(".site_url").val();
    var product_entry_id    = $("#product_entries_"+x).val();
    var unit_id             = $("#unit_id_"+x).val();

    $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

        if ($.isEmptyObject(data))
        {
            getItemPrice(x);
        }
        else
        {   
            var convertedUnitName = $('#unit_id_'+x).find(":selected").text();
            var mainStock         = $("#stock_"+x).val();
            var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed(2);

            $("#stock_show_"+x).html('Stock : ' + convertedStock + ' ( ' + convertedUnitName + ' )');
            $("#stock_"+x).val(convertedStock);

            calculateActualAmount(x);
        }

    });
}

function getItemPrice(x)
{
    //For getting item commission information from items table start
    var entry_id  = $("#product_entries_"+x).val();
    var site_url  = $(".site_url").val();

    if(entry_id)
    {
        $.get(site_url + '/invoices/products/price/list/'+ entry_id, function(data){

            var list    = '';
            $.each(data.unit_conversions, function(i, data_list)
            {   
                list    += '<option value = "' + data_list.unit_id + '">' + data_list.unit_name + '</option>';
            });

            $("#unit_id_"+x).empty();
            $("#unit_id_"+x).append(list);

            if (data.product_entries.stock_in_hand == '' || data.product_entries.stock_in_hand == null)
            {
                var stockInHand  = 0;
            }
            else
            {
                var stockInHand  = data.product_entries.stock_in_hand;
            }

            if (data.product_entries.unit_id == null)
            {
                var unit  = '';
            }
            else
            {
                var unit  = ' ( ' + data.product_entries.unit_name + ' )';
            }

            $("#stock_"+x).val(parseFloat(stockInHand).toFixed(2));
            $("#stock_show_"+x).html('Stock : ' + parseFloat(stockInHand).toFixed(2) + ' ' + unit);
            $("#main_unit_id_"+x).val(data.product_entries.unit_id);
            $("#main_unit_name_"+x).val(data.product_entries.unit_name);
            $("#quantity_"+x).val(0);

            calculateActualAmount(x);
        });
    }
    // calculateActualAmount(x);
}

function pad (str, max)
{
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}
