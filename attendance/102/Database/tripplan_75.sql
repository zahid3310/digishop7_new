-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 15, 2021 at 05:38 AM
-- Server version: 5.6.41-84.1
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tripplan_75`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tas_machine_data`
--

CREATE TABLE `tbl_tas_machine_data` (
  `AID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `DateTime` varchar(50) DEFAULT NULL,
  `MachineID` varchar(50) NOT NULL DEFAULT '0',
  `InOutStatus` int(11) NOT NULL DEFAULT '0' COMMENT '0 =in, 1 = Out',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tas_machine_data_log`
--

CREATE TABLE `tbl_tas_machine_data_log` (
  `AID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `DateTime` varchar(50) DEFAULT NULL,
  `MachineID` varchar(50) NOT NULL DEFAULT '0',
  `InOutStatus` int(11) NOT NULL DEFAULT '0' COMMENT '0 =in, 1 = Out',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_tas_machine_data`
--
ALTER TABLE `tbl_tas_machine_data`
  ADD PRIMARY KEY (`AID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_tas_machine_data`
--
ALTER TABLE `tbl_tas_machine_data`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
