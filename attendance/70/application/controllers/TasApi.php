<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class TasApi extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$array = array(
			'error'			=>	true,
			'api_error'		=>	'Access Denied'
		);
		
		echo json_encode($array);
	}
	
	public function generalShift($today, $user_id)
	{   
	    //check machine data with date and user_id
	    $Query_log_table            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND date = '$today'";
	    $query_result_log_table     = $this->db->query($Query_log_table)->result();
	    $count_log_table            = $this->db->query($Query_log_table)->num_rows();
	   
	    if($count_log_table == 1)
	    {   
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
	        //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $today);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
	    }
	    
	    if($count_log_table > 1)
	    {   
	        $Query_log_table_in_time            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND date = '$today' ORDER BY AID ASC LIMIT 1";
	        $query_result_log_table_in_time     = $this->db->query($Query_log_table_in_time)->result();
	        $in_time                            = date('H:i:s', strtotime($query_result_log_table_in_time[0]->DateTime));
	        
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    //  $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
		    //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $today);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
		    
	        $Query_log_table_out_time           = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND date = '$today' ORDER BY AID DESC LIMIT 1";
	        $query_result_log_table_out_time    = $this->db->query($Query_log_table_out_time)->result();
	        $out_time                           = date('H:i:s', strtotime($query_result_log_table_out_time[0]->DateTime));
	        
	        //Set Out Time Start
	        $Query_daily_attendance_table_user_out = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_attendance_table_user_out = $this->db->query($Query_daily_attendance_table_user_out)->result();
	        
	        if($count_daily_attendance_table_user_out > 0)
	        {
	            $daily_transaction_data_update = array(
					'out_time'  => date('H:i:s', strtotime($out_time)),
				);
	        }
    			
    		$this->db->where('user_id', $user_id)->where('date', $today);
    		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        //Set Out Time End
	    }
	}
	
	public function nightShift($today, $pre_day, $user_id, $shift_in_date, $shift_out_date, $shift_in_time, $shift_out_time)
	{   
	    //Check this employee shift previuos Start
	    $shift_start_date_time      = new DateTime($shift_in_date . ' ' . $shift_in_time);
	    $shift_start_date_times     = $shift_start_date_time->format('Y-m-d H:i:s');
	    $shift_end_date_time        = new DateTime($shift_out_date . ' ' . date("H:i:s", strtotime("+1 hour", strtotime($shift_out_time))));
	    $shift_end_date_times       = $shift_end_date_time->format('Y-m-d H:i:s');
	    
	    //check machine data with date and user_id
	    $Query_log_table            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$shift_start_date_times' AND DateTime <= '$shift_end_date_times'";
	    $query_result_log_table     = $this->db->query($Query_log_table)->result();
	    $count_log_table            = $this->db->query($Query_log_table)->num_rows();
	   
	    // echo '<pre>';
        //print_r($query_result_log_table);
        
	    if($count_log_table == 1)
	    {   
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$pre_day' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
	        //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$pre_day' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $pre_day);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
	    }
	    
	    if($count_log_table > 1)
	    {   
	        $Query_log_table_in_time            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$shift_start_date_times' AND DateTime <= '$shift_end_date_times' ORDER BY AID ASC LIMIT 1";
	        $query_result_log_table_in_time     = $this->db->query($Query_log_table_in_time)->result();
	        $in_time                            = date('H:i:s', strtotime($query_result_log_table_in_time[0]->DateTime));
	        
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$pre_day' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    //  $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
		    //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$pre_day' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $pre_day);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
		    
	        $Query_log_table_out_time           = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$shift_start_date_times' AND DateTime <= '$shift_end_date_times' ORDER BY AID DESC LIMIT 1";
	        $query_result_log_table_out_time    = $this->db->query($Query_log_table_out_time)->result();
	        $out_time                           = date('H:i:s', strtotime($query_result_log_table_out_time[0]->DateTime));
	        
	        //Set Out Time Start
	        $Query_daily_attendance_table_user_out = "SELECT * FROM daily_attendance WHERE date = '$pre_day' AND user_id = '$user_id'";
		    $count_daily_attendance_table_user_out = $this->db->query($Query_daily_attendance_table_user_out)->result();
	        
	        if($count_daily_attendance_table_user_out > 0)
	        {
	            $daily_transaction_data_update = array(
					'out_time'  => date('H:i:s', strtotime($out_time)),
				);
	        }
    			
    		$this->db->where('user_id', $user_id)->where('date', $pre_day);
    		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        //Set Out Time End
	    }
	    //Check this employee shift previuos End
	    
	    //Check this employee's shift current Start
	    $check_shift_query          = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		$check_shift_query_result   = $this->db->query($check_shift_query)->result();
		  
		$current_shift_in_date  = $check_shift_query_result[0]->shift_in_date;
		$current_shift_out_date = $check_shift_query_result[0]->shift_out_date;
		$current_shift_in_time  = $check_shift_query_result[0]->shift_in_time;
		$current_shift_out_time = $check_shift_query_result[0]->shift_out_time;
		
		$current_shift_start_date_time      = new DateTime($shift_out_date . ' ' . date("H:i:s", strtotime("+1 hour", strtotime($shift_out_time))));
	    $current_shift_start_date_times     = $current_shift_start_date_time->format('Y-m-d H:i:s');
	    $current_shift_end_date_time        = new DateTime($current_shift_out_date . ' ' . date("H:i:s", strtotime("+1 hour", strtotime($current_shift_out_time))));
	    $current_shift_end_date_times       = $current_shift_end_date_time->format('Y-m-d H:i:s');
	    
	    //check machine data with date and user_id
	    $Query_log_table            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$current_shift_start_date_times' AND DateTime <= '$current_shift_end_date_times'";
	    $query_result_log_table     = $this->db->query($Query_log_table)->result();
	    $count_log_table            = $this->db->query($Query_log_table)->num_rows();
	   
	    // echo '<pre>';
        // print_r($query_result_log_table);
    
	    if($count_log_table == 1)
	    {   
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
	        //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $today);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
	    }
	    
	    if($count_log_table > 1)
	    {   
	        $Query_log_table_in_time            = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$current_shift_start_date_times' AND DateTime <= '$current_shift_end_date_times' ORDER BY AID ASC LIMIT 1";
	        $query_result_log_table_in_time     = $this->db->query($Query_log_table_in_time)->result();
	        $in_time                            = date('H:i:s', strtotime($query_result_log_table_in_time[0]->DateTime));
	        
	        //Employee late in start
	        $Query_daily_att_table_user  = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_att_table_user  = $this->db->query($Query_daily_att_table_user)->result();
		    $late_in_time                = $count_daily_att_table_user[0]->late_in_time;
		    //  $in_time                     = date('H:i:s', strtotime($query_result_log_table[0]->DateTime));
		    
		    if(date('H:i:s', strtotime($in_time)) > date('H:i:s', strtotime($late_in_time)))
		    {
		        $status = 'L/C';
		    }
		    else
		    {
		        $status = 'Present';
		    }
		    //Employee late in end
		    
		    //Set In Time Start
	        $in_time_query          = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $in_time_query_result   = $this->db->query($in_time_query)->result();
		    $in_time_query_count    = $this->db->query($in_time_query)->num_rows();
		    
	        if($in_time_query_count > 0)
	        {   
	            $daily_transaction_data_update = array(
    					'in_time'  => date('H:i:s', strtotime($in_time)),
    					'status'   => $status,
    				);
        			
        		$this->db->where('user_id', $user_id)->where('date', $today);
        		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        }
	        //Set In Time End
		    
	        $Query_log_table_out_time           = "SELECT * FROM tbl_tas_machine_data_log WHERE UserID = '$user_id' AND DateTime >= '$current_shift_start_date_times' AND DateTime <= '$current_shift_end_date_times' ORDER BY AID DESC LIMIT 1";
	        $query_result_log_table_out_time    = $this->db->query($Query_log_table_out_time)->result();
	        $out_time                           = date('H:i:s', strtotime($query_result_log_table_out_time[0]->DateTime));
	        
	        //Set Out Time Start
	        $Query_daily_attendance_table_user_out = "SELECT * FROM daily_attendance WHERE date = '$today' AND user_id = '$user_id'";
		    $count_daily_attendance_table_user_out = $this->db->query($Query_daily_attendance_table_user_out)->result();
	        
	        if($count_daily_attendance_table_user_out > 0)
	        {
	            $daily_transaction_data_update = array(
					'out_time'  => date('H:i:s', strtotime($out_time)),
				);
	        }
    			
    		$this->db->where('user_id', $user_id)->where('date', $today);
    		$this->db->update('daily_attendance', $daily_transaction_data_update);
	        //Set Out Time End
	    }
	    //Check this employee shift current  End
	    
	    // echo '<pre>';
        // print_r($shift_start_date_times);
        // echo '<br>';
        // print_r($shift_end_date_times);
        
        // echo '<pre>';
        // print_r($current_shift_start_date_times);
        // echo '<br>';
        // print_r($current_shift_end_date_times);
	}

	public function process()
	{
		$tas_json           = file_get_contents('php://input');
		$tas_json_decode    = json_decode($tas_json);
		
		if($tas_json_decode)
		{   
			foreach($tas_json_decode as $data)
			{
				$machine_data = array(
					'UserID'        => trim($data->UserID),
					'date'          => date('Y-m-d', strtotime($data->DateTime)),
					'DateTime'      => trim($data->DateTime),
					'timestamp'     => date('Y-m-d H:i:s'),
					'MachineID'     => trim($data->MachineID),
					'InOutStatus'   => trim($data->InOutStatus)
				);

				$insert_data = $this->db->insert("tbl_tas_machine_data_log", $machine_data);

			    //My Code Start
    			$today   = date('Y-m-d', strtotime($data->DateTime));
    			$pre_day = date('Y-m-d', strtotime('-1 day'));
    			$user_id = trim($data->UserID);
        		
        		//Check this employee's shift Start
        		$check_shift_query        = "SELECT * FROM daily_attendance WHERE user_id = '$user_id' AND date = '$pre_day'";
    		    $check_shift_query_result = $this->db->query($check_shift_query)->result();
    		    $check_shift_query_count  = $this->db->query($check_shift_query)->num_rows();
        		//Check this employee's shift End
        		
        		if($check_shift_query_count > 0) //Not first attendance
        		{
        		    if($check_shift_query_result[0]->shift_in_date != $check_shift_query_result[0]->shift_out_date)  //Night shift
        		    {  
        		        $this->nightShift($today, $pre_day, $user_id, $check_shift_query_result[0]->shift_in_date, $check_shift_query_result[0]->shift_out_date, $check_shift_query_result[0]->shift_in_time, $check_shift_query_result[0]->shift_out_time);
        		    }
        		    else //generel shift
        		    {
        		        $this->generalShift($today, $user_id);
        		    }
        		}
        		else  //first attendance
        		{
        		    $this->generalShift($today, $user_id);
        		}
 			}
 		}
 		else
 		{
 			$array = array(
 				'error'			=>	true,
 				'api_error'		=>	'TasApi Not Response!',
 				'format_error'  =>	'Invalid Format, Only Json Format Allowed!'
 			);
 		}
		
 		header('Content-type: application/json');
 		echo json_encode($array);
	}
}


?>