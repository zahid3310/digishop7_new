<!DOCTYPE html>
<html>

<head>
    <title>Supplier Ledger Details</title>
    <link rel="icon" href="i{{ url('/public/favicon.png') }}" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/style.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ url('public/admin_panel_assets/report_assets/css/custom.css') }}">

    <style type="text/css" media="print">        
        @page {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-top:5mm;
            margin-bottom:5mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>  
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div>
                
                    <div style="width:50%;">
                        <img style="height: 80px; margin: 5px 0 0 5px;" src="{{ url('public/'.userDetails()->logo) }}">
                    </div>

                    <div class="company-head" style="text-align: center;line-height: .8">
                        <p style="font-size: 40px"><strong>{{ $user_info['organization_name'] }}</strong></p>
                        <p style="font-size: 20px">{{ $user_info['address'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_number'] }}</p>
                        <p style="font-size: 20px">{{ $user_info['contact_email'] }}</p>
                        <p style="font-size: 14px;text-align: right">{{ 'Print Date : ' . date('d-m-Y') }}</p>
                    </div>

                    <br>

                    <div class="ibox-title" style="padding: 14px 15px 0px;">
                        <h6 style="font-size: 18px; margin: 0px;font-weight: bold">Supplier Ledger Details</h6>
                    </div>

                    <div class="ibox-content">

                        <table class="table table-striped table-bordered table-hover dataTables-example" style="margin-bottom: 5px;">
                            <thead class="theight">
                                <tr>
                                    <th style="text-align: center;font-size: 12px !important">Date From</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Name</th>
                                    <th style="text-align: center;font-size: 12px !important">Supplier Phone</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr class="gradeC">
                                    <td style="text-align: center;font-size: 12px !important">{{ $from_date }} <strong>To</strong> {{ $to_date }}</td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['name'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                    
                                    <td style="text-align: center;font-size: 12px !important">
                                        @if($customer_name != null)
                                            {{ $customer_name['phone'] }}
                                        @else
                                            ALL
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >

                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 7%;vertical-align: bottom" rowspan="2">Date</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Details</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Trans#</th>
                                    <th style="text-align: center;width: 8%" colspan="5">Description</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Total Amount Dis.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Ship/Cost</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom" rowspan="2">Paid Through</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Debit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Credit</th>
                                    <th style="text-align: center;width: 10%;vertical-align: bottom" rowspan="2">Balance</th>
                                </tr>

                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Item Name</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Qty.</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">U/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">T/Price</th>
                                    <th style="text-align: center;width: 5%;vertical-align: bottom">Dis.</th>
                                </tr>
                            </thead>

                            <tbody class="theight">
                                <tr>
                                    <td style="text-align: right;font-size: 12px !important" colspan="11"><strong>Opening Balance</strong></a>
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;" colspan="3"><strong>{{  number_format($opening_balance,0,'.',',') }}</strong></td>
                                </tr>

                                <?php
                                    $i           = 1;
                                    $sub_total   = $opening_balance;
                                ?>
                                @if($data->count() > 0)
                                @foreach($data as $key => $value)

                                <?php
                                    if ($value['debit_credit'] == 0)
                                    {
                                        $debit  = $value['amount'];
                                        $credit = 0;
                                    }
                                    else
                                    {
                                        $credit = $value['amount'];
                                        $debit  = 0;
                                    }

                                    $sub_total = $sub_total + $credit - $debit;

                                    if ($value['transaction_head'] == 'supplier-opening-balance')
                                    {
                                        $trans_number     = '';
                                        $transaction_head = 'Opening Balance';
                                        $transaction_note = $value->note;
                                        $register         = $value->customer->name;
                                        $payment_methode  = '';
                                        $text             = 'Opening Balance ';
                                        $entries          = collect();
                                        $rowspan          = 1;
                                    }
                                    elseif ($value['transaction_head'] == 'purchase')
                                    {
                                        $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = 'Purchase of Items';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Item Purchase From ';
                                        $entries            = $value->bill->billEntries;
                                        $rowspan            = $entries->count();
                                    }
                                    elseif ($value['transaction_head'] == 'payment-receive')
                                    {
                                        if ($value->invoice_id != null)
                                        {
                                            $trans_number     = 'INV - '.str_pad($value->invoice->invoice_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Payment Received';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        elseif ($value->purchase_return_id != null)
                                        {
                                            $trans_number     = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = 'Payment Received';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                        else
                                        {
                                            $trans_number     = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head = $value->purchase_return_id != null ? 'Purchase Return of Items' : 'Previous Due Collection';
                                            $transaction_note = $value->note;
                                            $register         = $value->customer->name;
                                            $payment_methode  = $value->account->account_name;
                                            $text             = 'Received from ';
                                            $entries          = collect();
                                            $rowspan          = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'payment-made')
                                    {
                                        if ($value->bill_id != null)
                                        {
                                            $trans_number       = 'BILL - '.str_pad($value->bill->bill_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Payment Made';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        elseif ($value->sales_return_id != null)
                                        {
                                            $trans_number       = 'SR - '.str_pad($value->salesReturn->sales_return_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = 'Payment Made';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;

                                        }
                                        else
                                        {
                                            $trans_number       = 'PM - '.str_pad($value->payment->payment_number, 6, "0", STR_PAD_LEFT);
                                            $transaction_head   = $value->sales_return_id != null ? 'Sales Return of items' : 'Previous Due Paid';
                                            $transaction_note   = $value->note;
                                            $register           = $value->customer->name;
                                            $payment_methode    = $value->account->account_name;
                                            $text               = 'Paid to ';
                                            $entries            = collect();
                                            $rowspan            = 1;
                                        }
                                    }
                                    elseif ($value['transaction_head'] == 'purchase-return')
                                    {
                                        $trans_number       = 'PR - '.str_pad($value->purchaseReturn->purchase_return_number, 6, "0", STR_PAD_LEFT);
                                        $transaction_head   = $value->purchase_return_id != null ? 'Purchase Return of items' : 'Previous Due Paid';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Received Back from ';
                                        $entries            = $value->purchaseReturn->purchaseReturnEntries;
                                        $rowspan            = $entries->count();
                                    }
                                    elseif ($value['transaction_head'] == 'supplier-settlement')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Supplier Settlement';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = $value->account->account_name;
                                        $text               = 'Return Back Advance From ';
                                        $entries            = collect();
                                        $rowspan            = 1;
                                    }
                                    elseif ($value['transaction_head'] == 'production')
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = 'Production ';
                                        $transaction_note   = $value->note;
                                        $register           = $value->customer->name;
                                        $payment_methode    = '';
                                        $text               = 'Production ';
                                        $entries            = collect();
                                        $rowspan            = 1;
                                    }
                                    else
                                    {
                                        $trans_number       = '';
                                        $transaction_head   = '';
                                        $register           = '';
                                        $transaction_note   = '';
                                        $payment_methode    = '';
                                        $text               = '';
                                        $entries            = collect();
                                        $rowspan            = 1;
                                    } 
                                ?>

                                <tr>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ date('d-m-Y', strtotime($value['date'])) }}</a>
                                    </td>

                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ $transaction_head }}</td>
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ $trans_number }}</td>
                                    @if(($value['transaction_head'] == 'purchase') || ($value['transaction_head'] == 'purchase-return'))
                                    @if($entries->count() > 0)
                                    @foreach($entries as $key1 => $detail_data1)
                                    @if($key1 == 0)
                                    <td style="text-align: left;font-size: 12px !important;">{{ $detail_data1->productEntries->name }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ number_format($detail_data1->quantity,0,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ number_format($detail_data1->rate,0,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ number_format($detail_data1->total_amount,0,'.',',') }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ $detail_data1->discount_type == 1 ? number_format($detail_data1->discount_amount,0,'.',',') : number_format((($detail_data1->discount_amount*$detail_data1->quantity*$detail_data1->rate)/100),0,'.',',') }}</td>
                                    @endif
                                    @endforeach
                                    @else
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    @endif

                                    @if($entries->count() > 0 && $entries->count() == 0)
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    <td style="text-align: center;font-size: 12px !important;"></td>
                                    @endif
                                    @else
                                    <td colspan="5"></td>
                                    @endif
                                    
                                    @if(($value['transaction_head'] == 'purchase') || ($value['transaction_head'] == 'purchase-return'))
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ isset($value->invoice->total_discount_amount) ? number_format($value->invoice->total_discount_amount,0,'.',',') : '' }}</td>
                                    @else
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}"></td>
                                    @endif
                                    
                                    @if(($value['transaction_head'] == 'purchase') || ($value['transaction_head'] == 'purchase-return'))
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ isset($value->invoice->shipping_cost) ? number_format($value->invoice->shipping_cost,0,'.',',') : '' }}</td>
                                    @else
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}"></td>
                                    @endif
                                    <td style="text-align: center;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">
                                        {{ $payment_methode }}
                                    </td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ number_format($debit,0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ number_format($credit,0,'.',',') }}</td>
                                    <td style="text-align: right;font-size: 12px !important;vertical-align: middle" rowspan="{{$rowspan}}">{{ number_format($sub_total,0,'.',',') }}</td>
                                </tr>

                                @if(($value['transaction_head'] == 'purchase') || ($value['transaction_head'] == 'purchase-return'))
                                @if($entries->count() > 0)
                                @foreach($entries as $key2 => $detail_data2)
                                @if($key2 != 0)
                                <tr>
                                    <td style="text-align: left;font-size: 12px !important;">{{ $detail_data2->productEntries->name }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->quantity }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->rate }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->total_amount }}</td>
                                    <td style="text-align: center;font-size: 12px !important;">{{ $detail_data2->discount_type == 1 ? number_format($detail_data2->discount_amount,0,'.',',') : number_format((($detail_data2->discount_amount*$detail_data2->quantity*$detail_data2->rate)/100),0,'.',',') }}</td>
                                </tr>
                                @endif
                                @endforeach
                                @endif
                                @endif
                             
                            <?php $i++; ?>
                                
                            @endforeach
                            @endif
                            </tbody>

                            <tfoot class="tfheight">
                                <tr>
                                    <th colspan="13" style="text-align: right;font-size: 12px !important">TOTAL</th>
                                    <th colspan="1" style="text-align: right;font-size: 12px !important">{{ number_format($sub_total,0,'.',',') }}</th>
                                </tr>
                            </tfoot>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;{{ date('Y') }} Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_assets/js/jquery.base64.js') }}"></script>

</body>
</html>