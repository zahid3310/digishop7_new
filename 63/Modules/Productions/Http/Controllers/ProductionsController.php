<?php

namespace Modules\Productions\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\ProductEntriesRaw;
use App\Models\Productions;
use App\Models\ProductionEntries;
use App\Models\ProductEntriesFinish;
use App\Models\ProductionReturn;
use App\Models\ProductionReturnEntries;
use App\Models\ProductionDamage;
use App\Models\ProductionDamageEntries;
use App\Models\UnitConversions;
use App\Models\Stores;
use App\Models\Users;
use App\Models\Customers;
use App\Models\Deliveries;
use App\Models\DeliveryEntries;
use App\Models\Accounts;
use App\Models\JournalEntries;
use Carbon\Carbon;
use Response;
use DB;
use View;

class ProductionsController extends Controller
{
    public function transferIndex()
    {
        $productions = Productions::where('productions.type', 0)
                            ->orderBy('productions.id', 'DESC')
                            ->with('productionEntries')
                            ->get();

        return view('productions::send_to_transfer.index', compact('productions'));
    }

    public function transferCreate()
    {
        $productions        = Productions::orderBy('productions.created_at', 'DESC')->first();
        $stores             = Stores::orderBy('id', 'ASC')->get();

        return view('productions::send_to_transfer.create', compact('stores'));
    }

    public function transferStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $productions            = Productions::orderBy('productions.created_at', 'DESC')->first();
            $production_number      = $productions != null ? $productions['production_number'] + 1 : 1;

            $productions                        = new Productions;;
            $productions->customer_id           = $data['customer_id'];
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $data['production_number'];
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'customer_id'        => $data['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();

                    $product_entries_raw                = ProductEntriesRaw::where('product_entries_raw.customer_id', $data['customer_id'])->where('product_entries_raw.store_id', $data['store_id'][$key4])->where('product_entries_raw.product_entry_id', $value4)->first();
                    $product_entries_raw->stock_in_hand = $product_entries_raw['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries_raw->save();
                }

                DB::commit();

                return back()->with("success","Transfer To Production Created Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function transferEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count          = $find_production_entries->count();
        $stores                 = Stores::orderBy('id', 'ASC')->get();

        return view('productions::send_to_transfer.edit', compact('find_production', 'find_production_entries', 'entries_count', 'stores'));
    }

    public function transferUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $productions                        = Productions::find($id);
            $productions->customer_id           = $data['customer_id'];
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $data['production_number'];
            $productions->type                  = 0;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'customer_id'        => $data['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'rate'               => $data['rate'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    { 
                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2];
                        $quantity_add_to_product_entry->save();

                        $quantity_add_to_product_entry_raw                = ProductEntriesRaw::where('product_entries_raw.customer_id', $data['customer_id'])->where('product_entries_raw.store_id', $data['store_id'][$key2])->where('product_entries_raw.product_entry_id', $value2)->first();
                        $quantity_add_to_product_entry_raw->stock_in_hand = $quantity_add_to_product_entry_raw['stock_in_hand'] + $old_items_stock[$key2];
                        $quantity_add_to_product_entry_raw->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();

                    $product_entries_raw                = ProductEntriesRaw::where('product_entries_raw.customer_id', $data['customer_id'])->where('product_entries_raw.store_id', $data['store_id'][$key4])->where('product_entries_raw.product_entry_id', $value4)->first();
                    $product_entries_raw->stock_in_hand = $product_entries_raw['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries_raw->save();
                }

                DB::commit();

                return redirect()->route('productions_transfer_to_production_index')->with("success","Transfer Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsIndex()
    {
        $productions    = Productions::where('productions.type', 1)
                                    ->orderBy('productions.created_at', 'DESC')
                                    ->get();

        return view('productions::received_finished_goods.index', compact('productions'));
    }

    public function finishedGoodsCreate()
    {
        $production_list  = Productions::where('type', 0)->get();
        $stores           = Stores::orderBy('id', 'ASC')->get();
        $accounts         = Accounts::whereIn('parent_account_type_id',[3,4])->where('status', 1)->get();

        return view('productions::received_finished_goods.create', compact('production_list', 'stores', 'accounts'));
    }

    public function finishedGoodsStore(Request $request)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $find_production                    = Productions::find($data['production_id']);
            $productions                        = new Productions;
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->production_number     = $data['production_id'];
            $productions->customer_id           = $find_production['customer_id'];
            $productions->account_id            = $data['account_id'];
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->extra_paid            = $data['extra_paid'];
            $productions->note                  = $data['note'];
            $productions->created_by            = $user_id;

            if ($productions->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'customer_id'        => $find_production['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'production_id'      => $productions['id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'] != null ? $find_product_entries['stock_in_hand'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'kg_rate'            => $data['kg_rate'][$key],
                        'kg_quantity'        => $data['kg_quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                $total_weg_amount   = 0;
                $total_rice_amount  = 0;
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $find_product_entries   = ProductEntries::find($value4);
                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                    $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + $data['quantity'][$key4] + $converted_quantity;
                    $product_entries->save();

                    if ($find_product_entries['product_id'] == 2)
                    {
                        $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', $find_production['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();
                    }
                    else
                    {
                        $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', 63)->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();
                    }

                    if ($product_entries_finish != null)
                    {
                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] + $data['quantity'][$key4] + $converted_quantity;
                        $product_entries_finish->save();
                    }
                    else
                    {
                        $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                        $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                        $find_product_id                        = ProductEntries::find($value4);
                        $product_entry_add                      = new ProductEntriesFinish;
                        $product_entry_add->store_id            = $data['store_id'][$key4];

                        if ($find_product_entries['product_id'] == 2)
                        {
                            $product_entry_add->customer_id     = $find_production['customer_id'];
                        }
                        else
                        {
                            $product_entry_add->customer_id     = 63;
                        }
                        
                        $product_entry_add->product_id          = $find_product_id['product_id'];
                        $product_entry_add->product_entry_id    = $value4;
                        $product_entry_add->main_unit_id        = $data['main_unit_id'][$key4];
                        $product_entry_add->conversion_unit_id  = $data['unit_id'][$key4];
                        $product_entry_add->stock_in_hand       = $data['quantity'][$key4] + $converted_quantity;
                        $product_entry_add->created_by          = Auth::user()->id;
                        $product_entry_add->save();
                    }

                    if ($find_product_entries->product_id != 2)
                    {
                        $total_weg_amount   = $total_weg_amount + ($data['rate'][$key4]*$data['quantity'][$key4]) + ($data['kg_rate'][$key4]*$data['kg_quantity'][$key4]);
                    }

                    if ($find_product_entries->product_id == 2)
                    {
                        $total_rice_amount   = $total_rice_amount + ($data['rate'][$key4]*$data['quantity'][$key4]) + ($data['kg_rate'][$key4]*$data['kg_quantity'][$key4]);
                    }
                }

                $payable_to_party = $total_weg_amount - $total_rice_amount - $data['extra_paid'];

                //Financial Accounting Start
                    debit($customer_id=$find_production['customer_id'], $date=$data['production_date'], $account_id=$data['account_id'], $amount=$payable_to_party, $note=null, $transaction_head='production', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=$productions->id, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$find_production['customer_id'], $date=$data['production_date'], $account_id=97, $amount=$payable_to_party, $note=null, $transaction_head='production', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=$productions->id, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting End

                supplierBalanceUpdate($find_production['customer_id']);

                DB::commit();

                return redirect()->route('productions_received_finished_goods_show', $productions->id)->with("success","Transfer To Production Created Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function finishedGoodsEdit($id)
    {
        $find_production            = Productions::select('productions.*')->find($id);

        $find_production_entries    = ProductionEntries::leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                            ->where('production_entries.production_id', $id)
                                            ->select('production_entries.*',
                                                    'product_entries.id as item_id',
                                                    'product_entries.product_code as product_code',
                                                    'product_entries.stock_in_hand as stock_in_hand',
                                                    'product_entries.name as item_name')
                                            ->get();

        $entries_count      = $find_production_entries->count();
        $stores             = Stores::orderBy('id', 'ASC')->get();
        $accounts           = Accounts::whereIn('parent_account_type_id',[3,4])->where('status', 1)->get();

        return view('productions::received_finished_goods.edit', compact('find_production', 'find_production_entries', 'entries_count', 'stores', 'accounts'));
    }

    public function finishedGoodsUpdate(Request $request, $id)
    {
        $rules = array(
            'production_date'       => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $find_production                    = Productions::find($data['production_id']);
            $productions                        = Productions::find($id);
            $productions->date                  = date('Y-m-d', strtotime($data['production_date']));
            $productions->customer_id           = $find_production['customer_id'];
            $productions->account_id            = $data['account_id'];
            $productions->type                  = 1;
            $productions->total_amount          = $data['sub_total_amount'];
            $productions->extra_paid            = $data['extra_paid'];
            $productions->note                  = $data['note'];
            $productions->updated_by            = $user_id;

            if ($productions->save())
            {  
                $item_id     = ProductionEntries::where('production_id', $productions['id'])->get();
                $item_delete = ProductionEntries::where('production_id', $productions['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $find_product_entries    = ProductEntries::find($value);

                    $production_entries[] = [
                        'production_id'      => $productions['id'],
                        'customer_id'        => $find_production['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'] != null ? $find_product_entries['stock_in_hand'] : 0,
                        'rate'               => $data['rate'][$key],
                        'kg_quantity'        => $data['kg_quantity'][$key],
                        'kg_rate'            => $data['kg_rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('production_entries')->insert($production_entries);

                //Here Item Id is used for update purpose
                $total_weg_amount   = 0;
                $total_rice_amount  = 0;
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                        $old_items_stock_kg[]   = $value['kg_quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    {   
                        $old_find_product_entries   = ProductEntries::find($value2);
                        $old_conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key2])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value2)
                                                ->first();

                        $old_converted_quantity     = $old_conversion_rate_find != null ? $old_items_stock_kg[$key2]/$old_conversion_rate_find['conversion_rate'] : $old_items_stock_kg[$key2];

                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - $old_items_stock[$key2] - $old_converted_quantity;
                        $quantity_add_to_product_entry->save();

                        if ($old_find_product_entries['product_id'] == 2)
                        {
                            $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', $find_production['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key2])->where('product_entries_finish.product_entry_id', $value2)->first();
                        }
                        else
                        {
                            $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', 63)->where('product_entries_finish.store_id', $data['store_id'][$key2])->where('product_entries_finish.product_entry_id', $value2)->first();
                        }

                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] - $old_items_stock[$key2] - $old_converted_quantity;
                        $product_entries_finish->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $find_product_entries   = ProductEntries::find($value4);
                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                    $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + ($converted_quantity+$data['quantity'][$key4]);
                    $product_entries->save();

                    if ($find_product_entries['product_id'] == 2)
                    {
                        $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', $find_production['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();
                    }
                    else
                    {
                        $product_entries_finish  = ProductEntriesFinish::where('product_entries_finish.customer_id', 63)->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();
                    }

                    if ($product_entries_finish != null)
                    {
                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] + ($data['quantity'][$key4] + $converted_quantity);
                        $product_entries_finish->save();
                    }
                    else
                    {
                        $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                        $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                        $find_product_id                        = ProductEntries::find($value2);
                        $product_entry_add                      = new ProductEntriesFinish;
                        $product_entry_add->store_id            = $data['store_id'][$key4];

                        if ($find_product_entries['product_id'] == 2)
                        {
                            $product_entry_add->customer_id     = $find_production['customer_id'];
                        }
                        else
                        {
                            $product_entry_add->customer_id     = 63;
                        }
                        
                        $product_entry_add->product_id          = $find_product_id['product_id'];
                        $product_entry_add->product_entry_id    = $value4;
                        $product_entry_add->main_unit_id        = $data['main_unit_id'][$key4];
                        $product_entry_add->conversion_unit_id  = $data['unit_id'][$key4];
                        $product_entry_add->stock_in_hand       = $data['quantity'][$key4] + $converted_quantity;
                        $product_entry_add->created_by          = Auth::user()->id;
                        $product_entry_add->save();
                    }

                    if ($find_product_entries->product_id != 2)
                    {
                        $total_weg_amount   = $total_weg_amount + ($data['rate'][$key4]*$data['quantity'][$key4]) + ($data['kg_rate'][$key4]*$data['kg_quantity'][$key4]);
                    }

                    if ($find_product_entries->product_id == 2)
                    {
                        $total_rice_amount   = $total_rice_amount + ($data['rate'][$key4]*$data['quantity'][$key4]) + ($data['kg_rate'][$key4]*$data['kg_quantity'][$key4]);
                    }
                }

                $payable_to_party   = $total_weg_amount - $total_rice_amount - $data['extra_paid'];

                $jour_ent_debit     = JournalEntries::where('production_id', $productions->id)
                                        ->where('transaction_head', 'production')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = JournalEntries::where('production_id', $productions->id)
                                        ->where('transaction_head', 'production')
                                        ->where('debit_credit', 0)
                                        ->first();

                debitUpdate($jour_ent_debit['id'], $customer_id=$find_production['customer_id'], $date=$data['production_date'], $account_id=$data['account_id'], $amount=$payable_to_party, $note=null, $transaction_head='production', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=$productions->id, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                creditUpdate($jour_ent_credit['id'], $customer_id=$find_production['customer_id'], $date=$data['production_date'], $account_id=9, $amount=$payable_to_party, $note=null, $transaction_head='production', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=null, $payment_id=null, $production_id=$productions->id, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                supplierBalanceUpdate($find_production['customer_id']);

                DB::commit();

                return redirect()->route('productions_received_finished_goods_index')->with("success","Transfer Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function getCusDetails($production_id)
    {
        $production   = Productions::find($production_id);
        $customer     = Customers::find($production->customer_id);

        return Response::json($customer);
    }

    public function rawMaterialsList($supplier_id, $store_id)
    {
        if(!isset($_GET['searchTerm']))
        {
            $fetchData  = ProductEntriesRaw::where('product_entries_raw.customer_id', $supplier_id)
                                    ->where('product_entries_raw.store_id', $store_id)
                                    ->selectRaw('product_entries_raw.*')
                                    ->orderBy('product_entries_raw.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        {
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntriesRaw::where('product_entries_raw.customer_id', $supplier_id)
                                    ->where('product_entries_raw.store_id', $store_id)
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->selectRaw('product_entries_raw.*')
                                    ->orderBy('product_entries_raw.product_id', 'ASC')
                                    ->get();
        }

        $fetchData->where('brand_id', 1);
        foreach ($fetchData as $key => $value)
        {
            $name   = $value->productEntries->name;

            $data[] = array("id"=>$value['product_entry_id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function finishedGoodsList($supplier_id, $store_id)
    {
        if(!isset($_GET['searchTerm']))
        {
            $fetchData  = ProductEntriesFinish::leftjoin('product_entries', 'product_entries.id', 'product_entries_finish.product_entry_id')
                                    ->where('product_entries_finish.customer_id', $supplier_id)
                                    ->where('product_entries_finish.store_id', $store_id)
                                    ->where('product_entries.brand_id', 2)
                                    ->selectRaw('product_entries_finish.*')
                                    ->orderBy('product_entries_finish.product_id', 'ASC')
                                    ->take(100)
                                    ->get();
        }
        else
        {
            $search     = $_GET['searchTerm'];   
            $fetchData  = ProductEntriesFinish::leftjoin('product_entries', 'product_entries.id', 'product_entries_finish.product_entry_id')
                                    ->where('product_entries_finish.customer_id', $supplier_id)
                                    ->where('product_entries_finish.store_id', $store_id)
                                    ->where('product_entries.brand_id', 2)
                                    ->where('product_entries.name', 'LIKE', "%$search%")
                                    ->selectRaw('product_entries_finish.*')
                                    ->orderBy('product_entries_finish.product_id', 'ASC')
                                    ->get();
        }

        $fetchData->where('brand_id', 1);
        foreach ($fetchData as $key => $value)
        {
            $name   = $value->productEntries->name;

            $data[] = array("id"=>$value['product_entry_id'], "text"=>$name);
        }

        return Response::json($data);
    }

    public function getProductionnembers()
    {
        if(!isset($_GET['searchTerm']))
        {
            $fetchData  = Productions::orderBy('created_at', 'DESC')
                                    ->get();
        }
        else
        {
            $search     = $_GET['searchTerm'];   
            $fetchData  = Productions::where('productions.production_number', 'LIKE', "%$search%")
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {
            $data[] = array("id"=>$value['id'], "text"=>$value['production_number'], "type"=>$value['type']);
        } 

        return Response::json($data);
    }

    public function finishedGoodsShow($id)
    {
        $productions    = Productions::find($id);
        $entries        = ProductionEntries::where('production_entries.production_id', $id)->get(); 
        $user_info      = Users::find(1);
        
        $debit      = JournalEntries::where('customer_id', $productions['customer_id'])
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

        $credit     = JournalEntries::where('customer_id', $productions['customer_id'])
                                ->whereIn('transaction_head', ['payment-made'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

        $balance    = $debit - $credit;

        return view('productions::received_finished_goods.show', compact('productions', 'entries', 'user_info', 'balance'));
    }

    //Production Return
    public function returnToWarehouseIndex()
    {
        $stores    = Stores::orderBy('id', 'ASC')->get();

        return view('productions::production_return.index', compact('stores'));
    }

    public function returnToWarehouseStore(Request $request)
    {
        $rules = array(
            'return_date'       => 'required',
            'production_number' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $find_production                         = Productions::find($data['production_number']);
            $production_return                       = new ProductionReturn;
            $production_return->date                 = date('Y-m-d', strtotime($data['return_date']));
            $production_return->customer_id          = $find_production['customer_id'];
            $production_return->production_id        = $data['production_number'];
            $production_return->note                 = $data['return_note'];
            $production_return->total_amount         = $data['total_return_amount'];
            $production_return->created_by           = $user_id;

            if ($production_return->save())
            {   
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if ($data['return_quantity'][$key] != 0)
                    {   
                        $update_return_quantity                     = ProductionEntries::where('product_entry_id', $value)->where('production_id', $production_return['production_id'])->first();
                        $update_return_quantity->return_quantity    = $update_return_quantity['return_quantity'] + $data['return_quantity'][$key];
                        $update_return_quantity->save();

                        $production_return_entries[] = [
                            'customer_id'          => $find_production['customer_id'],
                            'store_id'             => $data['store_id'][$key],
                            'production_return_id' => $production_return['id'],
                            'product_entry_id'     => $value,
                            'main_unit_id'         => $data['main_unit_id'][$key],
                            'conversion_unit_id'   => $data['unit_id'][$key],
                            'rate'                 => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'             => $data['return_quantity'][$key],
                            'amount'               => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'created_by'           => $user_id,
                            'created_at'           => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('production_return_entries')->insert($production_return_entries);

                stockInReturn($data, $item_id=null);

                DB::commit();
                return back()->with("success","Back Raw Materials To Warehouse Successfull !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function productionList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Productions::where('productions.type', 0)
                                    ->orderBy('productions.id', 'DESC')
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Productions::where('productions.type', 0)
                                    ->where('productions.production_number', 'LIKE', "$search%")
                                    ->orderBy('productions.id', 'DESC')
                                    ->get();
        }

        foreach ($fetchData as $key => $value)
        {   
            $data[] = array("id"=>$value['id'], "text"=>$value['production_number']);
        }

        return Response::json($data);
    }

    public function findProductionNumber($id)
    {
        $data  = Productions::find($id);

        return Response::json($data);
    }

    public function returnList()
    {
        $data  = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();

        return Response::json($data);
    }

    public function returnSearchList($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data   = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orWhere('production_return.date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('production_return.production_return_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();
        }
        else
        {
            $data   = ProductionReturn::leftjoin('productions', 'productions.id', 'production_return.production_id')
                                ->orderBy('production_return.created_at', 'DESC')
                                ->select('production_return.*',
                                         'productions.production_number as production_number')
                                ->get();
        }

        return Response::json($data);
    }

    public function productionEntriesListByProductionNumber($id)
    {
        $production_entry           = ProductionEntries::leftjoin('productions', 'productions.id', 'production_entries.production_id')
                                                    ->leftjoin('product_entries', 'product_entries.id', 'production_entries.product_entry_id')
                                                    ->leftjoin('units', 'units.id', 'production_entries.conversion_unit_id')
                                                    ->where('production_entries.production_id', $id)
                                                    ->orderBy('production_entries.created_at', 'DESC')
                                                    ->select('production_entries.*',
                                                             'product_entries.name as entry_name',
                                                             'product_entries.product_code as product_code',
                                                             'units.name as conversion_unit_name')
                                                    ->get();

        $production_return_entry    = ProductionReturnEntries::leftjoin('production_return', 'production_return.id', 'production_return_entries.production_return_id')
                                            ->where('production_return.production_id', $id)
                                            ->get();

        $production                 = Productions::find($id);

        if ($production_entry->count() > 0)
        {
            foreach ($production_entry as $key => $value)
            {
                $production_return_entry_sum                        = $production_return_entry->where('product_entry_id', $value['product_entry_id']) ->sum('quantity');

                $entry_data[$value['id']]['id']                     =  $value['id'];
                $entry_data[$value['id']]['store_id']               =  $value['store_id'];
                $entry_data[$value['id']]['store_name']             =  $value->store->name;
                $entry_data[$value['id']]['production_id']          =  $value['production_id'];
                $entry_data[$value['id']]['product_code']           =  $value['product_code'];
                $entry_data[$value['id']]['entry_name']             =  $value['entry_name'];
                $entry_data[$value['id']]['product_entry_id']       =  $value['product_entry_id'];
                $entry_data[$value['id']]['main_unit_id']           =  $value['main_unit_id'];
                $entry_data[$value['id']]['conversion_unit_id']     =  $value['conversion_unit_id'];
                $entry_data[$value['id']]['conversion_unit_name']   =  $value['conversion_unit_name'];
                $entry_data[$value['id']]['rate']                   =  $value['rate'];
                $entry_data[$value['id']]['original_quantity']      =  $value['quantity'];
                $entry_data[$value['id']]['quantity']               =  $value['quantity'] - $production_return_entry_sum;
                $entry_data[$value['id']]['total_amount']           =  $value['amount'];
            }
        }
        else
        {
            $entry_data = [];
        }

        $data['production']             = $production;
        $data['production_entries']     = $entry_data;
   
        return Response::json($data);
    }

    //Damage finished goods
    public function damagedFinishedGoodsIndex()
    {
        return view('productions::damage_finished_goods.index');
    }

    public function damagedFinishedGoodsStore(Request $request)
    {
        $rules = array(
            'damage_date'       => 'required',
            'production_number' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $production_damage                       = new ProductionDamage;
            $production_damage->date                 = date('Y-m-d', strtotime($data['damage_date']));
            $production_damage->production_id        = $data['production_number'];
            $production_damage->note                 = $data['return_note'];
            $production_damage->total_amount         = $data['total_return_amount'];
            $production_damage->created_by           = $user_id;

            if ($production_damage->save())
            {   
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if ($data['return_quantity'][$key] != 0)
                    {   
                        $update_damage_quantity                     = ProductionEntries::where('product_entry_id', $value)->where('production_id', $production_damage['production_id'])->first();
                        $update_damage_quantity->damage_quantity    = $update_damage_quantity['damage_quantity'] + $data['return_quantity'][$key];
                        $update_damage_quantity->save();

                        $production_return_entries[] = [
                            'production_return_id' => $production_damage['id'],
                            'product_entry_id'     => $value,
                            'main_unit_id'         => $data['main_unit_id'][$key],
                            'conversion_unit_id'   => $data['unit_id'][$key],
                            'rate'                 => $data['amount'][$key]/$data['quantity'][$key],
                            'quantity'             => $data['return_quantity'][$key],
                            'amount'               => ($data['amount'][$key]/$data['quantity'][$key])*$data['return_quantity'][$key],
                            'created_by'           => $user_id,
                            'created_at'           => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('production_damage_entries')->insert($production_return_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4];
                    $product_entries->save();
                }

                DB::commit();
                return back()->with("success","Damage Added Successfull !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function damageList()
    {
        $data  = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();

        return Response::json($data);
    }

    public function damageSearchList($id)
    {
        $search_by_date              = date('Y-m-d', strtotime($id));
        $search_by_payment_numbers   = explode('-', $id);

        if (isset($search_by_payment_numbers[1]))
        {
            $search_by_payment_number    = ltrim($search_by_payment_numbers[1], "0");
        }
        else
        {
            $search_by_payment_number    = 0;
        }

        if ($id != 'No_Text')
        {
            $data   = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orWhere('production_damage.date', 'LIKE', "%$search_by_date%")
                                ->when($search_by_payment_number != 0, function ($query) use ($search_by_payment_number) {
                                    return $query->orWhere('production_damage.production_number', 'LIKE', "%$search_by_payment_number%");
                                })
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();
        }
        else
        {
            $data   = ProductionDamage::leftjoin('productions', 'productions.id', 'production_damage.production_id')
                                ->orderBy('production_damage.created_at', 'DESC')
                                ->select('production_damage.*',
                                         'productions.production_number as production_number')
                                ->get();
        }

        return Response::json($data);
    }

    //Delivery finished goods
    public function deliveryGoodsIndex()
    {
        $deliveries    = Deliveries::orderBy('deliveries.created_at', 'DESC')
                                    ->get();

        return view('productions::delivery_finished_goods.index', compact('deliveries'));
    }

    public function deliveryGoodsCreate()
    {
        $deliveries    = Deliveries::orderBy('deliveries.created_at', 'DESC')->get();
        $stores           = Stores::orderBy('id', 'ASC')->get();

        return view('productions::delivery_finished_goods.create', compact('deliveries', 'stores'));
    }

    public function deliveryGoodsStore(Request $request)
    {
        $rules = array(
            'delivery_date'         => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $delivery                        = new Deliveries;
            $delivery->date                  = date('Y-m-d', strtotime($data['delivery_date']));
            $delivery->delivery_number       = $data['delivery_number'];
            $delivery->customer_id           = $data['customer_id'];
            $delivery->total_amount          = $data['sub_total_amount'];
            $delivery->note                  = $data['note'];
            $delivery->created_by            = $user_id;

            if ($delivery->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {   
                    $find_product_entries    = ProductEntries::find($value);

                    $delivery_entries[] = [
                        'delivery_id'        => $delivery->id,
                        'customer_id'        => $data['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'kg_rate'            => $data['kg_rate'][$key],
                        'kg_quantity'        => $data['kg_quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('delivery_entries')->insert($delivery_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                    $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - $data['quantity'][$key4] - $converted_quantity;
                    $product_entries->save();

                    $product_entries_finish             = ProductEntriesFinish::where('product_entries_finish.customer_id', $data['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();

                    if ($product_entries_finish != null)
                    {
                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] - $data['quantity'][$key4] - $converted_quantity;
                        $product_entries_finish->save();
                    }
                }

                DB::commit();

                return back()->with("success","Delivery Created Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function deliveryGoodsEdit($id)
    {
        $find_delivery              = Deliveries::select('deliveries.*')->find($id);

        $find_delivery_entries      = DeliveryEntries::where('delivery_entries.delivery_id', $id)
                                            ->select('delivery_entries.*')
                                            ->get();

        $entries_count          = $find_delivery_entries->count();
        $stores                 = Stores::orderBy('id', 'ASC')->get();

        return view('productions::delivery_finished_goods.edit', compact('find_delivery', 'find_delivery_entries', 'entries_count', 'stores'));
    }

    public function deliveryGoodsUpdate(Request $request, $id)
    {
        $rules = array(
            'delivery_date'         => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();
      
        DB::beginTransaction();

        try{
            $find_delivery                   = Deliveries::find($id);
            $delivery                        = Deliveries::find($id);
            $delivery->date                  = date('Y-m-d', strtotime($data['delivery_date']));
            $delivery->delivery_number       = $data['delivery_number'];
            $delivery->customer_id           = $data['customer_id'];
            $delivery->total_amount          = $data['sub_total_amount'];
            $delivery->note                  = $data['note'];
            $delivery->updated_by            = $user_id;

            if ($delivery->save())
            {  
                $item_id     = DeliveryEntries::where('delivery_id', $delivery['id'])->get();
                $item_delete = DeliveryEntries::where('delivery_id', $delivery['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $find_product_entries    = ProductEntries::find($value);

                    $delivery_entries[] = [
                        'delivery_id'        => $delivery->id,
                        'customer_id'        => $data['customer_id'],
                        'store_id'           => $data['store_id'][$key],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'stock_quantity'     => $find_product_entries['stock_in_hand'],
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'kg_rate'            => $data['kg_rate'][$key],
                        'kg_quantity'        => $data['kg_quantity'][$key],
                        'amount'             => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('delivery_entries')->insert($delivery_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]    = $value['product_entry_id'];
                        $old_items_stock[]      = $value['quantity'];
                        $old_items_stock_kg[]   = $value['kg_quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    {
                        $old_conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key2])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value2)
                                                ->first();

                        $old_converted_quantity     = $old_conversion_rate_find != null ? $old_items_stock_kg[$key2]/$old_conversion_rate_find['conversion_rate'] : $old_items_stock_kg[$key2];

                        $quantity_add_to_product_entry                   = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] + $old_items_stock[$key2] + $old_converted_quantity;
                        $quantity_add_to_product_entry->save();

                        $product_entries_finish             = ProductEntriesFinish::where('product_entries_finish.customer_id', $find_delivery['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key2])->where('product_entries_finish.product_entry_id', $value2)->first();

                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] + $old_items_stock[$key2] + $old_converted_quantity;
                        $product_entries_finish->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $conversion_rate_find   = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                ->where('converted_unit_id', 2)
                                                ->where('product_entry_id', $value4)
                                                ->first();

                    $converted_quantity     = $conversion_rate_find != null ? $data['kg_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['kg_quantity'][$key4];

                    $product_entries                    = ProductEntries::find($value4);
                    $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] - ($data['quantity'][$key4] + $converted_quantity);
                    $product_entries->save();

                    $product_entries_finish             = ProductEntriesFinish::where('product_entries_finish.customer_id', $data['customer_id'])->where('product_entries_finish.store_id', $data['store_id'][$key4])->where('product_entries_finish.product_entry_id', $value4)->first();

                    if ($product_entries_finish != null)
                    {
                        $product_entries_finish->stock_in_hand  = $product_entries_finish['stock_in_hand'] - ($data['quantity'][$key4] + $converted_quantity);
                        $product_entries_finish->save();
                    }
                }

                DB::commit();

                return redirect()->route('productions_delivery_finished_goods_index')->with("success","Delivery Updated Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function deliveryGoodsShow($id)
    {
        $delivery       = Deliveries::find($id);
        $entries        = DeliveryEntries::where('delivery_entries.delivery_id', $id)->get(); 
        $user_info      = Users::find(1);

        return view('productions::delivery_finished_goods.show', compact('delivery', 'entries', 'user_info'));
    }
}