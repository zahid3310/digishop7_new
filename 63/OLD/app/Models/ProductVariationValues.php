<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ProductVariationValues extends Model
{  
    protected $table = "product_variation_values";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productVarion()
    {
        return $this->belongsTo('App\Models\ProductVariations','variation_id');
    }

}
