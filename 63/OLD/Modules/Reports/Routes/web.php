<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('reports/stock')->group(function() {
    Route::get('/', 'ReportsController@stockReport')->name('stock_report_index');
    Route::get('/stock-details/{id}', 'ReportsController@stockDetailsReport')->name('stock_report_details');
});

Route::prefix('reports/sales')->group(function() {
    Route::get('/', 'ReportsController@salesReport')->name('sales_report_index');
});

Route::prefix('reports/profit-loss')->group(function() {
    Route::get('/', 'ReportsController@profitLoss')->name('profit_loss_index');
    Route::get('/report', 'ReportsController@profitLossReduced')->name('profit_loss_index_reduced');
});

Route::prefix('reports/sales-summary')->group(function() {
    Route::get('/', 'ReportsController@salesSummary')->name('sales_summary_index');
    Route::get('/report', 'ReportsController@salesSummaryReduced')->name('sales_summary_index_reduced');
});

Route::prefix('reports/purchase')->group(function() {
    Route::get('/', 'ReportsController@purchaseReport')->name('purchase_report_index');
    Route::get('/report', 'ReportsController@purchaseReportReduced')->name('purchase_report_index_reduced');
});

Route::prefix('reports/purchase-summary')->group(function() {
    Route::get('/', 'ReportsController@purchaseSummary')->name('purchase_summary_index');
});

Route::prefix('reports/due-supplier')->group(function() {
    Route::get('/', 'ReportsController@dueReportSupplier')->name('due_report_supplier_index');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportSupplierDetails')->name('due_report_supplier_details');

    Route::get('/due-list', 'ReportsController@dueReportSupplierDue')->name('due_report_supplier_index_due');
});

Route::prefix('reports/expense')->group(function() {
    Route::get('/', 'ReportsController@expenseReport')->name('expense_report_index');
});

Route::prefix('reports/income')->group(function() {
    Route::get('/', 'ReportsController@incomeReport')->name('income_report_index');
});

Route::prefix('reports/collection')->group(function() {
    Route::get('/', 'ReportsController@collectionReport')->name('collection_report_index');
});

Route::prefix('reports/salary-report')->group(function() {
    Route::get('/', 'ReportsController@salaryReport')->name('salary_report_index');
});

Route::prefix('reports/sales-statement')->group(function() {
    Route::get('/', 'ReportsController@salesStatement')->name('sales_statement_index');
    Route::get('/print', 'ReportsController@salesStatementPrint')->name('sales_statement_print');
    Route::get('/invoices-list', 'ReportsController@invoiceListAjax');
    Route::get('/users-list', 'ReportsController@usersListAjax');
    Route::get('/category-list', 'ReportsController@categoryListAjax');
    Route::get('/product-list', 'ReportsController@productListAjax');
    Route::get('/product-code-list', 'ReportsController@productCodeListAjax');
});

Route::prefix('reports/purchase-statement')->group(function() {
    Route::get('/', 'ReportsController@purchaseStatement')->name('purchase_statement_index');
    Route::get('/print', 'ReportsController@purchaseStatementPrint')->name('purchase_statement_print');
    Route::get('/bill-list', 'ReportsController@billListAjax');
});

Route::prefix('reports/item-list')->group(function() {
    Route::get('/', 'ReportsController@itemList')->name('item_list_index');
    Route::get('/print', 'ReportsController@itemListPrint')->name('item_list_print');
    Route::get('/brand-list', 'ReportsController@brandListAjax');
    Route::get('/variation-list', 'ReportsController@variationListAjax');
});

Route::prefix('reports/register-list')->group(function() {
    Route::get('/', 'ReportsController@registerList')->name('register_list_index');
});

Route::prefix('reports/income-statement')->group(function() {
    Route::get('/', 'ReportsController@incomeStatement')->name('income_statement_index');
    Route::get('/print', 'ReportsController@incomeStatementPrint')->name('income_statement_print');
});

Route::prefix('reports/current-balance')->group(function() {
    Route::get('/', 'ReportsController@currentBalance')->name('current_balance_index');
    Route::get('/print', 'ReportsController@currentBalancePrint')->name('current_balance_print');
    Route::get('/details/{from_date}/{to_date}/{account_id}', 'ReportsController@currentBalanceDetails')->name('current_balance_details');
    Route::get('/paid-account-list', 'ReportsController@paidAccountListAjax');
});

Route::prefix('reports/product-suppliers')->group(function() {
    Route::get('/', 'ReportsController@productSuppliers')->name('product_suppliers_index');
    Route::get('/print', 'ReportsController@productSuppliersPrint')->name('product_suppliers_print');
});

Route::prefix('reports/product-customers')->group(function() {
    Route::get('/', 'ReportsController@productCustomers')->name('product_customers_index');
    Route::get('/print', 'ReportsController@productCustomersPrint')->name('product_customers_print');
});

Route::prefix('reports/customer-payment')->group(function() {
    Route::get('/', 'ReportsController@customerPaymentReport')->name('customer_payment_report_index');
    Route::get('/print', 'ReportsController@customerPaymentReportPrint')->name('customer_payment_report_print');
});

Route::prefix('reports/supplier-payment')->group(function() {
    Route::get('/', 'ReportsController@supplierPaymentReport')->name('supplier_payment_report_index');
    Route::get('/print', 'ReportsController@supplierPaymentReportPrint')->name('supplier_payment_report_print');
});

Route::prefix('reports/emergency-item-list')->group(function() {
    Route::get('/', 'ReportsController@emergencyItemList')->name('emergency_item_list_index');
    Route::get('/print', 'ReportsController@emergencyItemListPrint')->name('emergency_item_list_print');
    Route::get('/emergency-category-list', 'ReportsController@emergencyCategoryListAjax');
    Route::get('/emergency-product-list', 'ReportsController@emergencyProductListAjax');
});

Route::prefix('reports/due-customer')->group(function() {
    Route::get('/', 'ReportsController@dueReportCustomer')->name('due_report_customer_index');
    Route::get('/print', 'ReportsController@dueReportCustomerPrint')->name('due_report_customer_print');
    Route::get('/details/{customer_id}', 'ReportsController@dueReportCustomerDetails')->name('due_report_customer_details');

    Route::get('/due-print', 'ReportsController@dueReportCustomerPrintDue')->name('due_report_customer_print_due');
});

Route::prefix('reports/due-ledger')->group(function() {
    Route::get('/', 'ReportsController@dueLedger')->name('due_ledger_index');
    Route::get('/print', 'ReportsController@dueLedgerPrint')->name('due_ledger_print');
});

Route::prefix('reports/product-wise-sales-report')->group(function() {
    Route::get('/', 'ReportsController@productWiseSalesReport')->name('product_wise_sales_report_index');
    Route::get('/print', 'ReportsController@productWiseSalesReportPrint')->name('product_wise_sales_report_print');
});

Route::prefix('reports/inc-exp-ledger')->group(function() {
    Route::get('/', 'ReportsController@IncomeExpenseLedger')->name('income_expense_ledger_index');
    Route::get('/print', 'ReportsController@IncomeExpenseLedgerPrint')->name('income_expense_ledger_print');
});

Route::prefix('reports/general-ledger')->group(function() {
    Route::get('/', 'ReportsController@GeneralLedger')->name('general_ledger_index');
    Route::get('/print', 'ReportsController@GeneralLedgerPrint')->name('general_ledger_print');
});

Route::prefix('reports/stock-status-details-report')->group(function() {
    Route::get('/', 'ReportsController@stockStatusDetails')->name('stock_status_details_index');
    Route::get('/print', 'ReportsController@stockStatusDetailsPrint')->name('stock_status_details_print');
});

Route::prefix('reports/daily-report')->group(function() {
    Route::get('/', 'ReportsController@DailyReport')->name('daily_report_index');
    Route::get('/print', 'ReportsController@DailyReportPrint')->name('daily_report_print');
});

Route::prefix('reports/daily-collection')->group(function() {
    Route::get('/', 'ReportsController@DailyCollectionReport')->name('daily_collection_report_index');
    Route::get('/print', 'ReportsController@DailyCollectionReportPrint')->name('daily_collection_report_print');
    Route::get('/major-caegory-list', 'ReportsController@MajorCategoryList');
});

Route::prefix('reports/invoice-due-report')->group(function() {
    Route::get('/', 'ReportsController@invoiceDueReport')->name('sales_invoice_due_index');
});

Route::prefix('reports/stock-transfer-report')->group(function() {
    Route::get('/', 'ReportsController@stockTransferReport')->name('stock_transfer_report_index');
    Route::get('/print', 'ReportsController@stockTransferReportPrint')->name('stock_transfer_report_index_print');
});

Route::prefix('reports/branch-contacts')->group(function() {
    Route::get('/get-contacts/{id}', 'ReportsController@branchContacts');
});

Route::prefix('reports/production')->group(function() {
    Route::get('/production', 'ReportsController@productionReport')->name('production_report_index');
    Route::get('/production-print', 'ReportsController@productionReportPrint')->name('production_report_print');

    Route::get('/finished-goods', 'ReportsController@finishedGoodsReport')->name('finished_goods_report_index');
    Route::get('/finished-goods-print', 'ReportsController@finishedGoodsReportPrint')->name('finished_goods_report_print');

    Route::get('/production-finished', 'ReportsController@productionFinishedReport')->name('production_finished_report_index');
    Route::get('/production-finished-print', 'ReportsController@productionFinishedReportPrint')->name('production_finished_report_print');
});

Route::prefix('reports/attendance-report')->group(function() {
    Route::get('/', 'ReportsController@attendanceReport')->name('attendance_report_index');
    Route::get('/print', 'ReportsController@attendanceReporttPrint')->name('attendance_report_print');
});

Route::prefix('reports/salary-sheet')->group(function() {
    Route::get('/', 'ReportsController@monthlySalaryReport')->name('monthly_alary_report_index');
    Route::get('/print', 'ReportsController@monthlySalaryReportPrint')->name('monthly_alary_report_print');
});
