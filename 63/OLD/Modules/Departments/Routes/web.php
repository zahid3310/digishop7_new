<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('departments')->group(function() {
    Route::get('/', 'DepartmentsController@index')->name('department_index');
    Route::post('/store', 'DepartmentsController@store')->name('department_store');
    Route::get('/edit/{id}', 'DepartmentsController@edit')->name('department_edit');
    Route::post('/update/{id}', 'DepartmentsController@update')->name('department_update');
});
