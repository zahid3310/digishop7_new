<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('payroll/salary-grades')->group(function() {
    Route::get('/', 'SalaryGradesController@index')->name('salary_grades_index');
    Route::get('/create', 'SalaryGradesController@create')->name('salary_grades_create');
    Route::post('/store', 'SalaryGradesController@store')->name('salary_grades_store');
    Route::get('/edit/{id}', 'SalaryGradesController@edit')->name('salary_grades_edit');
    Route::post('/update/{id}', 'SalaryGradesController@update')->name('salary_grades_update');
    Route::get('/show/{id}', 'SalaryGradesController@show')->name('salary_grades_show');
});

Route::prefix('payroll/salary-statements')->group(function() {
    Route::get('/', 'SalaryStatementsController@index')->name('salary_statements_index');
    Route::get('/create', 'SalaryStatementsController@create')->name('salary_statements_create');
    Route::post('/store', 'SalaryStatementsController@store')->name('salary_statements_store');
    Route::get('/edit/{id}', 'SalaryStatementsController@edit')->name('salary_statements_edit');
    Route::post('/update/{id}', 'SalaryStatementsController@update')->name('salary_statements_update');
    Route::get('/show/{id}', 'SalaryStatementsController@show')->name('salary_statements_show');
    Route::get('/grade-details/{id}', 'SalaryStatementsController@gradeDetails')->name('salary_statements_grade_details');
});

Route::prefix('payroll/process-monthly-salary')->group(function() {
    Route::get('/', 'ProcessMonthlySalaryController@index')->name('process_monthly_salary_index');
    Route::get('/create', 'ProcessMonthlySalaryController@create')->name('process_monthly_salary_create');
    Route::post('/store', 'ProcessMonthlySalaryController@store')->name('process_monthly_salary_store');
    Route::get('/edit/{id}', 'ProcessMonthlySalaryController@edit')->name('process_monthly_salary_edit');
    Route::get('/delete/{id}', 'ProcessMonthlySalaryController@delete')->name('process_monthly_salary_delete');
    Route::post('/update/{id}', 'ProcessMonthlySalaryController@update')->name('process_monthly_salary_update');
    Route::get('/show/{id}', 'ProcessMonthlySalaryController@show')->name('process_monthly_salary_show');
    Route::get('/monthly-salary-data', 'ProcessMonthlySalaryController@processMonthlySalary')->name('process_monthly_salary_grade_details');
});

Route::prefix('payroll/salary-increaments')->group(function() {
    Route::get('/', 'IncreamentController@index')->name('salary_increaments_index');
    Route::get('/create', 'IncreamentController@create')->name('salary_increaments_create');
    Route::post('/store', 'IncreamentController@store')->name('salary_increaments_store');
    Route::get('/edit/{id}', 'IncreamentController@edit')->name('salary_increaments_edit');
    Route::post('/update/{id}', 'IncreamentController@update')->name('salary_increaments_update');
    Route::get('/show/{id}', 'IncreamentController@show')->name('salary_increaments_show');
    Route::get('/employee-details/{employee_id}', 'IncreamentController@employeeDetails')->name('salary_increaments_employee_details');
});