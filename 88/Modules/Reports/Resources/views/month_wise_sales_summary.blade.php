@extends('layouts.app')

@section('title', 'Sales Summary')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales Summary</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Sales Summary</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Sales Summary Report</h4>
                                        <p style="text-align: center"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('month_wise_sales_summary_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-5 col-md-5 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker" data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div style="display: none" class="col-lg-5 col-md-5 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>-- All Customers --</option>
                                                    @if(!empty($customers) && ($customers->count() > 0))
                                                    @foreach($customers as $key => $value)
                                                        <option {{ isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Date</th>
                                            <th style="text-align: right">Total Sales</th>
                                            <th style="text-align: right">Discount</th>
                                            <th style="text-align: right">Receivable</th>
                                            <th style="text-align: right">Received</th>
                                            <th style="text-align: right">Due</th>
                                            <th style="text-align: right">Return Amount</th>
                                            <th style="text-align: right">Payable</th>
                                            <th style="text-align: right">Profit</th>
                                            <th style="text-align: right">Loss</th>
                                            <th style="text-align: right">Income</th>
                                            <th style="text-align: right">Expense</th>
                                            <th style="text-align: right">Net Profit</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        

                                        <?php 

                                        $serial = 1; 
                                        $total_paid_amount_cal = 0; ?>

                                        @foreach($data as $key => $value)
                                        
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>{{ $key }}</td>
                                                <td style="text-align: right">{{ number_format($value['invoice_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">
                                                    {{ number_format($value['total_discount'] + $value['discount'],2,'.',',') }}
                                                </td>
                                                <td style="text-align: right">{{ number_format($value['invoice_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['paid_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['due_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['return_amount'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['return_due'],2,'.',',') }}</td>
                                                <td style="text-align: right">
                                                    @if($value['profit_loss'] >= 0)
                                                    {{ number_format($value['profit_loss'],2,'.',',') }}
                                                    @endif
                                                </td>
                                                <td style="text-align: right">
                                                    @if($value['profit_loss'] < 0)
                                                    {{ number_format($value['profit_loss'],2,'.',',') }}
                                                    @endif
                                                </td>
                                                <td style="text-align: right">{{ number_format($value['income'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format($value['expense'],2,'.',',') }}</td>
                                                <td style="text-align: right">{{ number_format(($value['profit_loss'] - $value['expense']),2,'.',',') }}</td>
                                            </tr>

                                            <?php 
                                                $serial++; 
                                                $total_paid_amount_cal = $total_paid_amount_cal + $value['paid_amount']; 
                                            ?>

                                        @endforeach
                                    

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right" colspan="2">Total</th>
                                        <th style="text-align: right">{{ number_format($total_invoice_amount,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_discount_amount,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_receivable,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_paid_amount_cal,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_due_amount,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_sales_return,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_return_due,2,'.',',') }}</th>
                                        <th style="text-align: right">
                                            @if(isset($total_profit))
                                            {{$total_profit}}
                                            @else
                                            0.00
                                            @endif
                                        </th>
                                        <th style="text-align: right">
                                            @if(isset($total_loss))
                                            {{$total_loss}}
                                            @else
                                            0.00
                                            @endif
                                        </th>
                                        <th style="text-align: right">{{ number_format($total_income,2,'.',',') }}</th>
                                        <th style="text-align: right">{{ number_format($total_expense,2,'.',',') }}</th>
                                        <th style="text-align: right">
                                            {{$total_net_profit}}
                                        </th>
                                    </tr>
                                </table>

                                <div class="row">
                                    <div class="col-12">
                                        <span>Net Profit = </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection