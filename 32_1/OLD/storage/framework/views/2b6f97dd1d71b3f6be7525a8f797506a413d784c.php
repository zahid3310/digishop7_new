

<?php $__env->startSection('title', 'Print Pos'); ?>

<?php if($user_info['printer_type'] == 0): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 60mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['printer_type'] == 1): ?>
<style type="text/css">
    @media  print {
        a[href]:after {
            content:"" !important;
        }

        header nav, footer {
            display: none;
        }

        @page  {
            margin: 0cm ! important;
            margin-top: 0cm ! important;
            color: black;
        }

        .card {
            width: 90mm;
            padding: 0px;
            color: black;
        }

        .card-body{
            padding: 0rem !important;
        }

        ::-webkit-scrollbar {
            display: none;
        }

        .marginTopPrint{
            margin-top: -0px !important;
        }
    }
</style>
<?php endif; ?>

<?php if($user_info['printer_type'] == 2 || $user_info['printer_type'] == 3): ?>
<style>
    table,th {
        border: 1px solid; black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px dashed #b9b9b9;
        border-right: 1px solid black;
        padding: 2px;
    }

    tr{
    border: 1px dashed #b9b9b9!important;
    }
    
    .tr-height 
    {
        font-size: 15px!important;
        line-height: 20px!important;
    }
    
    .baal {
        display: none;
    }
    
    .border-none{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
    
    .border-none-2{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-right: 1px solid #fff!important;
    }
    
    .border-none-3{
        border-right: 1px solid black!important;
        border-bottom: 1px solid #fff!important;
    }
    
    @page  {
        size: A4;
        page-break-after: always;
    }
    
    @media  print { .baal { display: block !important; } }

    @media  print {
        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px;
        }
    
        td{
            border-left: 1px dashed #b9b9b9;
            border-right: 1px solid black;
            padding: 2px;
        }
    
        tr{
            border: 1px dashed #b9b9b9!important;
        }
        
        .tr-height 
        {
            font-size: 15px!important;
            line-height: 20px!important;
        }
        
        .aaa {page-break-after: always;}
    }
</style>
<?php endif; ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content marginTopPrint">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row d-print-none">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Print Pos</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <?php if($user_info['printer_type'] == 0): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 18px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 18px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 14px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 14px"><?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 14px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px;" class="col-2"><strong>Qt</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item</strong></div>
                                    <div style="font-size: 12px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 12px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Total</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 12px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 12px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 12px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 12px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 12px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 12px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 14px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 14px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 14px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 14px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['cash_given'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 12px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['printer_type'] == 1): ?>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div id="hedColor" style="margin-top: 0px !important" class="col-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['address']); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 10px;text-align: center;font-size: 20px;font-weight: bold"><?php echo e($user_info['contact_number']); ?></p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 0px !important;border-top: 2px dashed black;border-bottom: 2px dashed black;padding-top: 3px;padding-bottom: 3px" class="row">
                                    <div class="col-12">
                                        <p style="font-size: 20px;text-align: center;margin-top: 0px;margin-bottom: 0px;font-weight: bold">Invoice</p>
                                    </div>
                                </div>

                                <div style="margin-bottom: 10px !important;padding-top: 10px" class="row">
                                    <div class="col-7">
                                        <span style="font-size: 16px">Date : <?php echo e(date('d/m/Y', strtotime($invoice['invoice_date']))); ?></span> 
                                    </div>
                                    <div style="text-align: right" class="col-5">
                                        <span style="font-size: 16px">Time : <?php echo e(date('h:i a', strtotime(now()))); ?></span> 
                                    </div>
                                    <div class="col-12">
                                        <span style="font-size: 16px">Invoice# : <?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?></span> 
                                    </div>
                                </div>
 
                                <div style="margin-bottom: 0px !important" class="row">
                                    
                                </div>

                                <div style="border-top: 2px dashed black;border-bottom: 2px dashed black;margin-bottom: 10px" class="row">
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-2"><strong>Qty</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-4"><strong>Item Name</strong></div>
                                    <div style="font-size: 16px;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>Price</strong></div>
                                    <div style="font-size: 16px;text-align: right;padding-top: 10px;padding-bottom: 10px" class="col-3"><strong>T.Price</strong></div>
                                </div>

                                <?php if(!empty($entries) && ($entries->count() > 0)): ?>

                                    <?php 
                                        $sub_total              = 0; 
                                        $total_discount         = 0;
                                    ?>

                                    <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                    <div style="padding-bottom: 10px;" id="DetailsFontSize" class="row">

                                        <?php 
                                            $discount           = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; 
                                            $total_discount     = $total_discount + $discount;
                                        ?>

                                        <div style="font-size: 16px" class="col-2">
                                            <?php echo e($value['quantity']); ?> <br> <?php echo e($value->convertedUnit->name); ?>

                                        </div>

                                        <div style="font-size: 16px" class="col-4">
                                            <?php if($value['product_type'] == 1): ?>
                                                <?php echo $value['product_entry_name']; ?>
                                            <?php else: ?>
                                                <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div style="font-size: 16px" class="col-3">
                                            <?php echo e(number_format($value['rate'])); ?>

                                        </div>
                                        <div style="font-size: 16px;text-align: right" class="col-3"><?php echo e(number_format($value['total_amount'])); ?></div>
                                    </div>

                                    <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endif; ?>

                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>GROSS Total :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-bottom: 10px" class="row"></div> -->

                                <?php
                                    $total_vat_amount       =  $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total VAT :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_vat_amount)); ?></strong></div>
                                </div>

                                <?php
                                    $total_discount_amount       = $invoice['total_discount_type'] == 0 ? ((($sub_total + $total_vat_amount)*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount']; 
                                ?>

                                <div style="border-top: 2px dashed black;padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px" class="col-6"><strong>Total Discount :</strong></div>
                                    <div style="font-size: 18px;text-align: right" class="col-6"><strong><?php echo e(number_format($total_discount + $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="padding-top: 10px" class="row"></div> -->

                                <div style="border-top: 2px dashed black;padding-top: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-6"><strong>NET Total :</strong></div>
                                    <div style="font-size: 20px;text-align: right" class="col-6"><strong><?php echo e(number_format($sub_total + $total_vat_amount - $total_discount_amount)); ?></strong></div>
                                </div>

                                <!-- <div style="border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 20px" class="col-12"><strong>VAT Included</strong></div>
                                </div> -->

                                <!-- <div style="padding-top: 10px;" class="row"></div> -->
                                <!-- <div style="border-top: 2px dashed black;padding-top: 10px;" class="row"></div> -->

                               <!--  <div class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-12">Payments</div>
                                </div> -->

                                <div style="padding-top: 10px;padding-bottom: 10px" class="row">
                                    <div style="font-size: 18px;font-weight: bold" class="col-6">Total Paid</div>
                                    <div style="font-size: 18px;text-align: right;font-weight: bold" class="col-6"><?php echo e(number_format($invoice['cash_given'])); ?></div>
                                </div>

                                <!-- <div style="padding-left: 15px;border-bottom: 2px dashed black;padding-bottom: 10px;" class="row">
                                    <div style="font-size: 16px" class="col-6">-TOTAL DUES</div>
                                    <div style="font-size: 16px;text-align: right" class="col-6"><?php echo e(number_format($invoice['due_amount'],2,'.',',')); ?></div>
                                </div> -->

                                <div style="border-top: 2px dashed black;" class="row"></div>

                                <div style="margin-top: 10px" class="row">
                                    <div style="text-align: center;font-size: 16px" class="col-12">Developed & Maintained By www.cyberdynetechnologyltd.com | 01715317133</div>
                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

                <?php if($user_info['printer_type'] == 2 || $user_info['printer_type'] == 3): ?>
                   <div style="padding-right: 10px;padding: 10px;padding-top: 25px" class="row">
                        <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                            <div class="float-right">
                                <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                            </div>
                        </div>

                        <div class="col-12">
                            
                            <div style="width: 8in;" class="card">
                                <div class="card-body">
                                    <div style="height: 2.8in;" class="row">
                                        
                                    </div>

                                    <?php
                                        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                                        // $dt = new DateTime($invoice['created_at'], new DateTimezone('Asia/Dhaka'));

                                        $date           = date('d-m-Y', strtotime($invoice['invoice_date']));
                                        $phone          = $invoice->customer->phone != null ? $invoice->customer->phone : '';
                                        $invoice_number = str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT);

                                        $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                        $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                        $date           = str_replace($search,$replace,$date);   
                                        $phone          = str_replace($search,$replace,$phone);   
                                        $invoice_number = str_replace($search,$replace,$invoice_number);
                                        $time           = str_replace($search,$replace,$dt->format('g:i a')); 
                                    ?>

                                    <div class="row">
                                        <div style="font-size: 15px;" class="col-7">
                                            
                                                <strong>মেমো নং : </strong><?php echo e($invoice_number); ?> <br>
                                                <strong>নাম :    </strong><?php echo e($invoice['customer_name']); ?><br>
                                                <strong>ঠিকানা : </strong> <?php echo e($invoice->customer->address != null ? $invoice->customer->address : ''); ?>

                                            
                                        </div>
                                        <div style="font-size: 15px;" class="col-5 text-sm-right">
                                            
                                                <strong>তারিখ : </strong><?php echo e($date); ?> || <?php echo e($time); ?> <br>
                                                <strong>মোবাইল : </strong> <?php echo e($phone); ?>

                                            
                                        </div>
                                    </div>

                                    <div style="padding-top: 10px;padding-bottom: 20px;min-height: 175mm!important;">
                                        <table  style="width: 100%;">
                                            <tr>
                                                <th style="font-size: 18px;width: 10%;text-align: center;">ক্রঃ নং</th>
                                                <th style="font-size: 18px;width: 50%;text-align: center;">পণ্যের বিবরণ</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center;">পরিমাণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center;">দর</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center;">টাকা</th>
                                            </tr>

                                            <?php if($entries->count() > 0): ?>

                                            <?php
                                                $total_amount                   = 0;
                                            ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($key < 20): ?>

                                            
                                            <?php
                                                $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['product_name'] != null)
                                                {
                                                    $category  = ' - '.$value['product_name'];
                                                }
                                                else
                                                {
                                                    $category  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = $value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = '';
                                                }

                                                if ($variation_name != null)
                                                {
                                                    $variation  = ' '.$variation_name;
                                                }
                                                else
                                                {
                                                    $variation  = '';
                                                }

                                                $pre_dues = $invoice['previous_due'];
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($invoice['cash_given'], 2);
                                                $dues     = round($net_paya - $paid, 2);
                                            ?>


                                            <?php

                                                $key            = $key + 1;
                                                $quantity       = $value['quantity'];
                                                $rate           = $value['rate'];
                                                $totalamount    = round($value['total_amount'], 2);


                                                $search     = array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                $replace    = array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                                $key        = str_replace($search,$replace,$key);    
                                                $quantity   = str_replace($search,$replace,$quantity);    
                                                $rate       = str_replace($search,$replace,$rate);    
                                                $totalamount= str_replace($search,$replace,$totalamount);    
                                            ?>

                                            <tr class="tr-height">
                                                <td style="text-align: center"><?php echo e($key); ?></td>
                                                <td style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $productCode); ?></td>
                                                <td style="text-align: center"><?php echo e($quantity); ?></td>
                                                <td style="text-align: center"><?php echo e($rate . $unit); ?></td>
                                                <td style="text-align: center"><?php echo e($totalamount); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                            <?php
                                                if ($invoice['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $invoice['total_vat'];
                                                }

                                                if ($invoice['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $invoice['total_discount_amount'];
                                                }
                                            ?>
                                            <?php if(count($entries) < 21): ?>

                                            <?php
                                                $total          = $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '';
                                                $discount       = round($discount_on_total_amount + $invoice['total_discount']);
                                                $paid_amount    = $paid != 0 ? round($paid, 2) : '';
                                                $due_amount     = $invoice['invoice_amount']  - $paid != 0 ? round($invoice['invoice_amount']  - $paid) : '';

                                                $search     = array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                $replace    = array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                                $total          = str_replace($search,$replace,$total);    
                                                $discount       = str_replace($search,$replace,$discount);    
                                                $paid_amount    = str_replace($search,$replace,$paid_amount);    
                                                $due_amount     = str_replace($search,$replace,$due_amount);    
                                            ?>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: left;text-align: right" colspan="1"><strong>মোট</strong></th>
                                                <th style="text-align: center"><?php echo e($total); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>ছাড়</strong></th>
                                                <th style="text-align: center"><?php echo e($discount); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>অগ্রিম</strong></th>
                                                <th style="text-align: center"><?php echo e($paid_amount); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>বাকি</strong></th>
                                                <th style="text-align: center"><?php echo e($due_amount); ?></th>
                                            </tr> 
                                            <?php endif; ?> 
                                        </table>
                                    </div>


                                    <div class="col-12 baal" style="padding-top:0%!important;width: 8in">
                                        <div class="row">
                                            <div class="col-5">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">ক্রেতার সাক্ষর  </span> </h6>
                                            </div>
        
                                            <div class="col-2">
                                                <h6> Page 1....... </h6>
                                            </div>
        
                                            <div class="col-5">
                                                <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">বিক্রেতার সাক্ষর </span> </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             
                             <?php if(count($entries) > 20): ?>
                             <div class="aaa"></div>
                             <div style="width: 8in;" class="card">
                                <div class="card-body">
                                    <div style="height: 2.8in;" class="row">
                                        
                                    </div>

                                    <?php
                                    
                                        $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                                        // $dt = new DateTime($invoice['created_at'], new DateTimezone('Asia/Dhaka'));


                                        $date = date('d-m-Y', strtotime($invoice['invoice_date']));
                                        $phone = $invoice->customer->phone != null ? $invoice->customer->phone : '';
                                        $invoice_number = str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT);

                                        $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                        $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                        $date = str_replace($search,$replace,$date);   
                                        $phone = str_replace($search,$replace,$phone);   
                                        $invoice_number = str_replace($search,$replace,$invoice_number);
                                        $time  = str_replace($search,$replace,$dt->format('g:i a')); 
                                    ?>

                                    <div class="row" style="padding-top:30px;">
                                        <div style="font-size: 15px;" class="col-7">
                                            
                                                <strong>মেমো নং : </strong><?php echo e($invoice_number); ?> <br>
                                                <strong>নাম :    </strong><?php echo e($invoice['customer_name']); ?><br>
                                                <strong>ঠিকানা : </strong> <?php echo e($invoice->customer->address != null ? $invoice->customer->address : ''); ?>

                                            
                                        </div>
                                        <div style="font-size: 15px;" class="col-5 text-sm-right">
                                            
                                                <strong>তারিখ : </strong><?php echo e($date); ?> || <?php echo e($time); ?> <br>
                                                <strong>মোবাইল : </strong> <?php echo e($phone); ?>

                                            
                                        </div>
                                    </div>

                                    <div style="padding-top: 10px;padding-bottom: 20px;min-height: 178mm!important;">
                                        <table  style="width: 100%;">
                                            <tr>
                                                <th style="font-size: 18px;width: 10%;text-align: center;">ক্রঃ নং</th>
                                                <th style="font-size: 18px;width: 50%;text-align: center;">পণ্যের বিবরণ</th>
                                                <th style="font-size: 18px;width: 10%;text-align: center;">পরিমাণ</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center;">দর</th>
                                                <th style="font-size: 18px;width: 15%;text-align: center;">টাকা</th>
                                            </tr>

                                            <?php if($entries->count() > 0): ?>

                                            <?php
                                                $total_amount                   = 0;
                                            ?>

                                            <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php if($key > 19): ?>

                                            
                                            <?php
                                                $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                                $variation_name = ProductVariationName($value['product_entry_id']);

                                                if ($value['product_code'] != null)
                                                {
                                                    $productCode  = ' - '.$value['product_code'];
                                                }
                                                else
                                                {
                                                    $productCode  = '';
                                                }

                                                if ($value['product_name'] != null)
                                                {
                                                    $category  = ' - '.$value['product_name'];
                                                }
                                                else
                                                {
                                                    $category  = '';
                                                }

                                                if ($value['brand_name'] != null)
                                                {
                                                    $brandName  = $value['brand_name'];
                                                }
                                                else
                                                {
                                                    $brandName  = '';
                                                }

                                                if ($value['unit_name'] != null)
                                                {
                                                    $unit  = ' '.$value['unit_name'];
                                                }
                                                else
                                                {
                                                    $unit  = '';
                                                }

                                                if ($variation_name != null)
                                                {
                                                    $variation  = ' '.$variation_name;
                                                }
                                                else
                                                {
                                                    $variation  = '';
                                                }

                                                $pre_dues = $invoice['previous_due'];
                                                $net_paya = round($total_amount, 2);
                                                $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                                                $dues     = round($net_paya - $paid, 2);
                                            ?>


                                            <?php

                                                $key = $key + 1;
                                                $quantity = $value['quantity'];
                                                $rate = $value['rate'];
                                                $totalamount = round($value['total_amount'], 2);


                                                $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                                $key = str_replace($search,$replace,$key);    
                                                $quantity = str_replace($search,$replace,$quantity);    
                                                $rate = str_replace($search,$replace,$rate);    
                                                $totalamount = str_replace($search,$replace,$totalamount);    
                                            ?>

                                            <tr class="tr-height">
                                                <td style="text-align: center"><?php echo e($key); ?></td>
                                                <td style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $productCode); ?></td>
                                                <td style="text-align: center"><?php echo e($quantity); ?></td>
                                                <td style="text-align: center"><?php echo e($rate . $unit); ?></td>
                                                <td style="text-align: center"><?php echo e($totalamount); ?></td>
                                            </tr>
                                            <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>

                                            <?php
                                                if ($invoice['vat_type'] == 0)
                                                {
                                                    $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $vat_amount  = $invoice['total_vat'];
                                                }

                                                if ($invoice['total_discount_type'] == 0)
                                                {
                                                    $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                                }
                                                else
                                                {
                                                    $discount_on_total_amount  = $invoice['total_discount_amount'];
                                                }
                                            ?>
                                             <?php if(count($entries) > 20): ?>

                                            <?php

                                                $total = $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '';
                                                $discount = round($discount_on_total_amount + $invoice['total_discount']);
                                                $paid_amount = $paid != 0 ? round($paid, 2) : '';
                                                $due_amount = $invoice['invoice_amount']  - $paid != 0 ? round($invoice['invoice_amount']  - $paid) : '';

                                                $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                                $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                                $total          = str_replace($search,$replace,$total);    
                                                $discount           = str_replace($search,$replace,$discount);    
                                                $paid_amount = str_replace($search,$replace,$paid_amount);    
                                                $due_amount = str_replace($search,$replace,$due_amount);    
       
                                            ?>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: left;text-align: right" colspan="1"><strong>মোট</strong></th>
                                                <th style="text-align: center"><?php echo e($total); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>ছাড়</strong></th>
                                                <th style="text-align: center"><?php echo e($discount); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>অগ্রিম</strong></th>
                                                <th style="text-align: center"><?php echo e($paid_amount); ?></th>
                                            </tr>

                                            <tr>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-2" style="text-align: center"></th>
                                                <th class="border-none-3" style="text-align: center"></th>
                                                <th style="text-align: right"><strong>বাকি</strong></th>
                                                <th style="text-align: center"><?php echo e($due_amount); ?></th>
                                            </tr> 
                                            <?php endif; ?> 
                                        </table>
                                    </div>


                                    <div class="col-12 baal" style="padding-top:0%!important;width: 8in">
                                        <div class="row">
                                            <div class="col-5">
                                                <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">ক্রেতার সাক্ষর  </span> </h6>
                                            </div>
        
                                            <div class="col-2">
                                                <h6> Page 2....... </h6>
                                            </div>
        
                                            <div class="col-5">
                                                <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">বিক্রেতার সাক্ষর </span> </h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                             <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    $( document ).ready(function() {
        javascript:window.print();
    });

    window.onafterprint = function(e){
        var site_url  = $('.site_url').val();
        window.location.replace(site_url + '/invoices');
    };
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/66/Modules/Invoices/Resources/views/show_pos.blade.php ENDPATH**/ ?>