<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\MachineDataLog;
use App\Models\DailyAttendance;
use App\Models\Customers;
use App\Models\GovernmentHolidays;
use App\Models\LeaveTransactions;
use App\Models\LeaveCategories;
use App\Models\Shifts;
use App\Models\DutyRosters;
use DateTime;
use DB;

class HolidayAttendance extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'holiday:attendance';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Insert an attendance row to daily_attendance table for predefined working holidays';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //My Code Start
            $today              = date('Y-m-d');
            $day                = date('D');
            $year               = date('Y');
            $govement_holi_data = GovernmentHolidays::where('year', $year)->get();
              
            $govement_holidays = 0;                                      
            foreach($govement_holi_data as $val)
            {  
                 $start_date = $val['start_date'];
                 $end_date   = $val['end_date'];
                 
                 if(($today >= $start_date) && ($today <= $end_date))
                 {
                     $govement_holidays++;
                 }
             }
            
            //check daily attendance with date
            $query_result_daily_attendance_table     = DailyAttendance::where('date', $today)->get();
            $count_daily_attendance_table            = $query_result_daily_attendance_table->count();
            
            //If no data found with current date then add 1 row for each employee
            if($count_daily_attendance_table == 0)
            {
                $query_result_active_employees  = Customers::where('status', 1)->where('contact_type', 2)->get();
                $count_daily_active_employees   = $query_result_active_employees->count();
                
                if($count_daily_active_employees > 0)
                {
                    foreach($query_result_active_employees as $data)
                    {   
                        $duty_roster    = DutyRosters::where('employee_id', $data['id'])->first();
                        
                        if($day == 'Fri')
                        {   
                            if($duty_roster['fri'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['fri']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Sat')
                        {   
                            if($duty_roster['sat'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['sat']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Sun')
                        {   
                            if($duty_roster['sun'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['sun']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Mon')
                        {   
                            if($duty_roster['mon'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['mon']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Tue')
                        {   
                            if($duty_roster['tue'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['tue']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Wed')
                        {   
                            if($duty_roster['wed'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['wed']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        elseif($day == 'Thu')
                        {   
                            if($duty_roster['thu'] != 0)
                            {
                                $find_shift = Shifts::find($duty_roster['thu']);
                            }
                            else
                            {
                                $find_shift = '0';
                            }
                        }
                        
                        $leave_data     = LeaveTransactions::where('year', $year)->where('employee_id', $data['id'])->get();
                      
                        $leave = 0;                                      
                        foreach($leave_data as $val1)
                        {  
                            $leave_start_date = $val1['start_date'];
                            $leave_end_date   = $val1['end_date'];
                             
                            if(($today >= $leave_start_date) && ($today <= $leave_end_date))
                            {
                                $leave++;
                                $leave_category         = LeaveCategories::find($val1['leave_category'])->first();
                                $leave_category_name    = $leave_category['short_name'];
                                break;
                            }
                        }
                        
                        //find shift hour start
                            if(is_numeric($find_shift))
                            {   
                                $shift_hour = 0;
                            }
                            else
                            {
                                $start  = strtotime($find_shift['start']);
                                $end    = strtotime($find_shift['end']);
                                $shift_hour = ($end - $start) / 3600;
                            }
                        //find shift hour end
                        
                        //Check if night shift start
                            if(is_numeric($find_shift))
                            {   
                                $shift_start_date       = null;
                                $shift_start_time       = null;
                                $shift_end_date         = null;
                                $shift_end_time         = null;
                            }
                            else
                            {
                                $shift_start_date       = date('Y-m-d');
                                $shift_start_time       = date('H:i:s', strtotime($find_shift['start']));
                                $shift_start_date_times = new DateTime($shift_start_date . ' ' . $shift_start_time);
                                $shift_start_date_time  = $shift_start_date_times->format('Y-m-d H:i:s');
                                $add_shift_hour         = '+'.$shift_hour.' hours';
                                $add_shift_day          = '+1 day';
                                $shift_end_date_time    = date("Y-m-d H:i:s", strtotime($add_shift_hour, strtotime($shift_start_date_time)));
                             //   $shift_end_date_time    = date("Y-m-d H:i:s", strtotime($add_shift_day, strtotime($shift_end_time)));
                                $shift_end_date         = date('Y-m-d', strtotime($shift_end_date_time));
                                $shift_end_time         = date('H:i:s', strtotime($shift_end_date_time));
                                
                            }
                        //Check if night shift end
                        
                        if($leave > 0)
                        {     
                            $daily_transaction_data[] = array(
                                'date'              => date('Y-m-d'),
                                'status'            => $leave_category_name,
                                'user_id'           => $data->id,
                                'shift_id'          => is_numeric($find_shift) ? 0 : $find_shift['id'],
                                'shift_in_date'     => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_start_date)),
                                'shift_out_date'    => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_end_date_time)),
                                'shift_in_time'     => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_start_time)),
                                'shift_out_time'    => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_end_time)),
                                'late_in_time'      => is_numeric($find_shift) ? null : date('H:i:s', strtotime($find_shift['late_in'])),
                                'created_at'        => date('Y-m-d H:i:s'),
                            );
                        }
                        else
                        {
                            if(!is_numeric($find_shift))
                            {
                                $daily_transaction_data[] = array(
                                    'date'              => date('Y-m-d'),
                                    'status'            => 'Absent',
                                    'user_id'           => $data->id,
                                    'shift_id'          => is_numeric($find_shift) ? 0 : $find_shift['id'],
                                    'shift_in_date'     => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_start_date)),
                                    'shift_out_date'    => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_end_date_time)),
                                    'shift_in_time'     => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_start_time)),
                                    'shift_out_time'    => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_end_time)),
                                    'late_in_time'      => is_numeric($find_shift) ? null : date('H:i:s', strtotime($find_shift['late_in'])),
                                    'created_at'        => date('Y-m-d H:i:s'),
                                );
                            }
                            elseif(is_numeric($find_shift))
                            {
                                $daily_transaction_data[] = array(
                                    'date'              => date('Y-m-d'),
                                    'status'            => 'W/H',
                                    'user_id'           => $data->id,
                                    'shift_id'          => is_numeric($find_shift) ? 0 : $find_shift['id'],
                                    'shift_in_date'     => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_start_date)),
                                    'shift_out_date'    => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_end_date_time)),
                                    'shift_in_time'     => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_start_time)),
                                    'shift_out_time'    => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_end_time)),
                                    'late_in_time'      => is_numeric($find_shift) ? null : date('H:i:s', strtotime($find_shift['late_in'])),
                                    'created_at'        => date('Y-m-d H:i:s'),
                                );
                            }
                            elseif($govement_holidays > 0)
                            {
                                $daily_transaction_data[] = array(
                                    'date'              => date('Y-m-d'),
                                    'status'            => 'G/H',
                                    'user_id'           => $data->id,
                                    'shift_id'          => is_numeric($find_shift) ? 0 : $find_shift['id'],
                                    'shift_in_date'     => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_start_date)),
                                    'shift_out_date'    => is_numeric($find_shift) ? null : date('Y-m-d', strtotime($shift_end_date_time)),
                                    'shift_in_time'     => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_start_time)),
                                    'shift_out_time'    => is_numeric($find_shift) ? null : date('H:i:s', strtotime($shift_end_time)),
                                    'late_in_time'      => is_numeric($find_shift) ? null : date('H:i:s', strtotime($find_shift['late_in'])),
                                    'created_at'        => date('Y-m-d H:i:s'),
                                );
                            }
                        }
                    }
                    
                    DB::table('daily_attendance')->insert($daily_transaction_data);
                }
            }
        //My Code End
    }
}
