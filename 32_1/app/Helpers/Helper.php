<?php
//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\SalesReturn;
use App\Models\SalesReturnEntries;
use App\Models\PurchaseReturn;
use App\Models\PurchaseReturnEntries;
use App\Models\Bills;
use App\Models\BillEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Expenses;
use App\Models\Users;
use App\Models\Units;
use App\Models\Discounts;
use App\Models\DiscountProducts;
use App\Models\URLS;
use App\Models\Permissions;
use App\Models\Modules;
use App\Models\ModulesAccess;
use App\Models\UnitConversions;
use App\Models\BranchInventories;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\Branches;

function userAccess($user_id)
{
    $current_route      = Route::currentRouteName();

    $access             = Permissions::leftjoin('urls', 'urls.id', 'permissions.url_id')
                                ->where('permissions.user_id', $user_id)
                                ->where('urls.url', $current_route)
                                ->first();

    return $access['access_level'];
}

function moduleAccess($user_id)
{
    $access     = ModulesAccess::leftjoin('modules', 'modules.id', 'modules_access.module_id')
                                ->where('modules_access.user_id', $user_id)
                                ->get();

    return $access;
}

function stockOut($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    if ($branch_id == 1)
    {
        if ($item_id != null)
        {
            foreach ($item_id as $key => $value)
            {
                $old_item_entry_id[]           = $value['product_entry_id'];
                $old_items_main_unit_id[]      = $value['main_unit_id'];
                $old_items_converted_unit_id[] = $value['conversion_unit_id'];
                $old_items_stock[]             = $value['quantity'];
            }

            foreach ($old_item_entry_id as $key2 => $value2)
            {
                $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                            ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                            ->where('product_entry_id', $value2)
                                            ->first();

                $quantity_add_to_product_entry                  = ProductEntries::find($value2);
                $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity;
                $quantity_add_to_product_entry->total_sold      = $quantity_add_to_product_entry ['total_sold'] - $old_converted_quantity;
                $quantity_add_to_product_entry->save();
            }
        }

        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                ->where('product_entry_id', $value4)
                                                                ->first();

            $product_entries                = ProductEntries::find($value4);
            $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
            $product_entries->total_sold    = $product_entries ['total_sold'] + $converted_quantity;
            $product_entries->save();
        }
    }
    else
    {
        if ($item_id != null)
        {
            foreach ($item_id as $key => $value)
            {
                $old_item_entry_id[]           = $value['product_entry_id'];
                $old_items_main_unit_id[]      = $value['main_unit_id'];
                $old_items_converted_unit_id[] = $value['conversion_unit_id'];
                $old_items_stock[]             = $value['quantity'];
            }

            foreach ($old_item_entry_id as $key2 => $value2)
            {
                $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                            ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                            ->where('product_entry_id', $value2)
                                            ->first();

                $quantity_add_to_product_entry                  = BranchInventories::where('product_entry_id', $value2)->where('branch_id', $branch_id)->first();
                $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity;
                $quantity_add_to_product_entry->save();

                //update main stock total_sold
                $old_total_sold_update              = ProductEntries::find($value2);
                $old_total_sold_update->total_sold  = $old_total_sold_update['total_sold'] - $old_converted_quantity;
                $old_total_sold_update->save();
            }
        }

        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                ->where('product_entry_id', $value4)
                                                                ->first();

            $product_entries                = BranchInventories::where('product_entry_id', $value4)->where('branch_id', $branch_id)->first();
            $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
            $product_entries->save();

            //update main stock total_sold
            $total_sold_update              = ProductEntries::find($value4);
            $total_sold_update->total_sold  = $total_sold_update['total_sold'] + $converted_quantity;
            $total_sold_update->save();
        }
    }
    
    return 0;
}

function stockIn($data, $item_id)
{
    if ($item_id != null)
    {
        foreach ($item_id as $key => $value)
        {
            $old_item_entry_id[]           = $value['product_entry_id'];
            $old_items_main_unit_id[]      = $value['main_unit_id'];
            $old_items_converted_unit_id[] = $value['conversion_unit_id'];
            $old_items_stock[]             = $value['quantity'];
        }

        foreach ($old_item_entry_id as $key2 => $value2)
        {
            $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                        ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                        ->where('product_entry_id', $value2)
                                        ->first();

            $quantity_add_to_product_entry                  = ProductEntries::find($value2);
            $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
            $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] - $old_converted_quantity;
            $quantity_add_to_product_entry->save();
        }
    }

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

        $product_entries                = ProductEntries::find($value4);
        $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
        $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity;
        $product_entries->buy_price     = $data['rate'][$key4];
        $product_entries->save();
    }

    return 0;
}

function stockInReturn($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    if ($branch_id == 1)
    {
        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

            $converted_quantity_to_main_unit= $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

            $product_entries                = ProductEntries::find($value4);
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity_to_main_unit;
            $product_entries->buy_price     = $data['rate'][$key4];
            $product_entries->save();
        }
    }
    else
    {
        foreach ($data['product_entries'] as $key4 => $value4)
        {
            $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                            ->where('converted_unit_id', $data['unit_id'][$key4])
                                                            ->where('product_entry_id', $value4)
                                                            ->first();

            $converted_quantity_to_main_unit= $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

            $product_entries                = BranchInventories::where('product_entry_id', $value4)->first();
            $product_entries->stock_in_hand = $product_entries['stock_in_hand'] + $converted_quantity_to_main_unit;
            $product_entries->save();
        }
    }

    return 0;
}

function stockOutReturn($data, $item_id)
{
    $branch_id  = Auth::user()->branch_id;

    foreach ($data['product_entries'] as $key4 => $value4)
    {
        $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                        ->where('product_entry_id', $value4)
                                        ->first();

        $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['return_quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key4];

        $product_entries                = ProductEntries::find($value4);
        $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity_to_main_unit;
        $product_entries->save();
    }

    return 0;
}

function createCustomer($data)
{
    $customer                       = new Customers;
    $customer->name                 = $data['customer_name'];
    $customer->phone                = $data['mobile_number'];
    $customer->address              = $data['address'];
    $customer->nid_number           = $data['nid_number'];
    $customer->alternative_contact  = $data['alternative_mobile_number'];
    $customer->contact_type         = $data['contact_type'];
    $customer->save();

    return $customer['id'];
}

function addPayment($data, $invoice_bill_id, $customer_id ,$type)
{
    $user_id                    = Auth::user()->id;
    $branch_id                  = Auth::user()->branch_id;

    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['selling_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->branch_id         = $branch_id;
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 0)
        {
            $payment_entries[] = [
                'invoice_id'        => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'branch_id'         => $branch_id,
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 0,  // 0 = In , 1 = Out
                'transaction_head'      => 'sales-return',
                'associated_id'         => $invoice_bill_id,
                'branch_id'             => $branch_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $current_balance[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 0,  // 0 = In , 1 = Out
                'transaction_head'      => 'sales-return',
                'associated_id'         => $invoice_bill_id,
                'branch_id'             => $branch_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_dues                = Invoices::find($invoice_bill_id);
            $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            $update_dues->save();
        }
        else
        {
            $payment_entries[] = [
                'bill_id'           => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'branch_id'         => $branch_id,
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            $account_transactions[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 1,  // 0 = In , 1 = Out
                'transaction_head'      => 'purchase-return',
                'associated_id'         => $invoice_bill_id,
                'branch_id'             => $branch_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $current_balance[] = [
                'customer_id'           => $customer_id,
                'transaction_date'      => date('Y-m-d', strtotime($data['selling_date'])),
                'amount'                => $data['amount_paid'],
                'paid_through_id'       => $data['paid_through'],
                'note'                  => $data['note'],
                'type'                  => 1,  // 0 = In , 1 = Out
                'transaction_head'      => 'purchase-return',
                'associated_id'         => $invoice_bill_id,
                'branch_id'             => $branch_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            $update_bill_dues                = Bills::find($invoice_bill_id);
            $update_bill_dues->due_amount    = $update_bill_dues['due_amount'] - $data['amount_paid'];
            $update_bill_dues->save();
        }

        DB::table('payment_entries')->insert($payment_entries);
        DB::table('account_transactions')->insert($account_transactions);
        DB::table('current_balance')->insert($current_balance);

        return 0;
    }
}

function addPaymentReturn($data, $invoice_bill_id, $customer_id ,$type, $account_adjustment)
{
    $user_id                    = Auth::user()->id;
    $branch_id                  = Auth::user()->branch_id;
    $data_find                  = Payments::orderBy('created_at', 'DESC')->first();
    $payment_number             = $data_find != null ? $data_find['payment_number'] + 1 : 1;
    
    $payment                    = new Payments;
    $payment->payment_number    = $payment_number;
    $payment->customer_id       = $customer_id;
    $payment->payment_date      = date('Y-m-d', strtotime($data['return_date']));
    $payment->amount            = $data['amount_paid'];
    $payment->paid_through      = $data['paid_through'];
    $payment->note              = $data['note'];
    $payment->branch_id         = $branch_id;
    $payment->type              = $type;
    $payment->created_by        = $user_id;

    if ($payment->save())
    {   
        if ($type == 2)
        {
            $payment_entries[] = [
                'sales_return_id'   => $invoice_bill_id,
                'payment_id'        => $payment['id'],
                'amount'            => $data['amount_paid'],
                'branch_id'         => $branch_id,
                'created_by'        => $user_id,
                'created_at'        => date('Y-m-d H:i:s'),
            ];

            // $update_dues                = SalesReturn::find($invoice_bill_id);
            // $update_dues->due_amount    = $update_dues['due_amount'] - $data['amount_paid'];
            // $update_dues->save();

            // $find_invoice_id                    = $tables['sales_return_entries']->where('sales_return_id', $invoice_bill_id)->first();
            // $update_invoice_dues                = $tables['invoices']->find($find_invoice_id['invoice_id']);
            // $update_invoice_dues->return_amount = $update_invoice_dues['return_amount'] - $data['amount_paid'];
            // $update_invoice_dues->save();

            
            if ($data['amount_paid'] > 0)
            {   
                if ($account_adjustment == 0)
                {   
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'sales-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন বাবদ প্রদান';
                    $transaction_data['amount']         = $data['amount_paid'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                }
                
                if ($account_adjustment == 1)
                {   
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 0;
                    $transaction_data['account_head']   = 'sales-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন বাবদ প্রদেয়';
                    $transaction_data['amount']         = $data['sub_total_amount'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                    
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'sales-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন বাবদ প্রদান';
                    $transaction_data['amount']         = $data['amount_paid'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                    
                    $account_transactions = [
                        'customer_id'           => $customer_id,
                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                        'amount'                => $data['amount_paid'],
                        'paid_through_id'       => $data['paid_through'],
                        'note'                  => $data['note']. '(রিটার্ন বাবদ ব্যায়)',
                        'type'                  => 0,  // 0 = In , 1 = Out
                        'transaction_head'      => 'sales-return',
                        'associated_id'         => $invoice_bill_id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];
    
                    $current_balance = [
                        'customer_id'           => $customer_id,
                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                        'amount'                => $data['amount_paid'],
                        'paid_through_id'       => $data['paid_through'],
                        'note'                  => $data['note']. '(রিটার্ন বাবদ ব্যায়)',
                        'type'                  => 1,  // 0 = In , 1 = Out
                        'transaction_head'      => 'sales-return',
                        'associated_id'         => $invoice_bill_id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];
                    
                    DB::table('account_transactions')->insert($account_transactions);
                    DB::table('current_balance')->insert($current_balance);
                }
            }
        }

        if ($type == 3)
        {
            $payment_entries[] = [
                'purchase_return_id'    => $invoice_bill_id,
                'payment_id'            => $payment['id'],
                'amount'                => $data['amount_paid'],
                'branch_id'             => $branch_id,
                'created_by'            => $user_id,
                'created_at'            => date('Y-m-d H:i:s'),
            ];

            // $update_return_dues              = PurchaseReturn::find($invoice_bill_id);
            // $update_return_dues->due_amount  = $update_return_dues['due_amount'] - $data['amount_paid'];
            // $update_return_dues->save();

            // $find_bill_id                    = $tables['purchase_return_entries']->where('purchase_return_id', $invoice_bill_id)->first();
            // $update_bill_dues                = $tables['bills']->find($find_bill_id['bill_id']);
            // $update_bill_dues->return_amount = $update_bill_dues['return_amount'] - $data['amount_paid'];
            // $update_bill_dues->save();
            
            
            if ($data['amount_paid'] > 0)
            {   
                if ($account_adjustment == 0)
                {   
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 0;
                    $transaction_data['account_head']   = 'purchase-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন গ্রহণ';
                    $transaction_data['amount']         = $data['amount_paid'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                }
                
                if ($account_adjustment == 1)
                {   
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 1;
                    $transaction_data['account_head']   = 'purchase-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন গ্রহণযোগ্য';
                    $transaction_data['amount']         = $data['amount_paid'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                    
                    $transaction_data['date']           = date('Y-m-d', strtotime($data['return_date']));
                    $transaction_data['type']           = 0;
                    $transaction_data['account_head']   = 'purchase-return';
                    $transaction_data['transaction_id'] = $invoice_bill_id;
                    $transaction_data['customer_id']    = $customer_id;
                    $transaction_data['note']           = 'বিক্রয় রিটার্ন গ্রহণ';
                    $transaction_data['amount']         = $data['amount_paid'];
                    $transaction_data['paid_through']   = 1;
                    transactions($transaction_data);
                    
                    $account_transactions = [
                        'customer_id'           => $customer_id,
                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                        'amount'                => $data['amount_paid'],
                        'paid_through_id'       => $data['paid_through'],
                        'note'                  => $data['note']. '(রিটার্ন বাবদ আয়)',
                        'type'                  => 0,  // 0 = In , 1 = Out
                        'transaction_head'      => 'purchase-return',
                        'associated_id'         => $invoice_bill_id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];
    
                    $current_balance = [
                        'customer_id'           => $customer_id,
                        'transaction_date'      => date('Y-m-d', strtotime($data['return_date'])),
                        'amount'                => $data['amount_paid'],
                        'paid_through_id'       => $data['paid_through'],
                        'note'                  => $data['note']. '(রিটার্ন বাবদ আয়)',
                        'type'                  => 0,  // 0 = In , 1 = Out
                        'transaction_head'      => 'purchase-return',
                        'associated_id'         => $invoice_bill_id,
                        'branch_id'             => $branch_id,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];
                    
                    DB::table('account_transactions')->insert($account_transactions);
                    DB::table('current_balance')->insert($current_balance);
                }
            }
        }

        DB::table('payment_entries')->insert($payment_entries);

        return 0;
    }
}

function userDetails()
{
    $branch_id  = Auth::user()->branch_id;
    $user       = Branches::find($branch_id);
    return $user;
}

function stockSellValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['sell_price']);
    }

    return $stock_value;
}

function stockPurchaseValue($product_id)
{
    $product_entries    = ProductEntries::where('product_entries.id', $product_id)
                                ->get();

    $stock_value   = 0;
    foreach ($product_entries as $key => $value)
    {
       $stock_value   = $stock_value + ($value['stock_in_hand']*$value['buy_price']);
    }

    return $stock_value;
}

function accessLevel($route)
{
    //All Routes start

    //products Module Start
        $products_create        = 'products_create';
        $products_store         = 'products_store';
        $products_edit          = 'products_edit';
        $products_update        = 'products_update';
    //products Module End

    //Invoice Module Start
        $invoices_edit       = 'invoices_edit';
        $invoices_update     = 'invoices_update';
    //Invoice Module End

    //Bill Module Start
       $bills_index    = 'bills_index';
       $bills_create   = 'bills_create';
       $bills_store    = 'bills_store';
       $bills_edit     = 'bills_edit';
       $bills_update   = 'bills_update';
       $bills_show     = 'bills_show';
    //Bill Module End

    //Payments Module Start
        $payments_edit              = 'payments_edit';
        $payments_update            = 'payments_update';
    //Payments Module End

    //expenses Module Start
        $expenses_edit             = 'expenses_edit';
        $expenses_update           = 'expenses_update';
    //expenses Module End

    //Users Module Start
        $users_index             = 'users_index';
        $users_create            = 'users_create';
        $users_store             = 'users_store';
        $users_edit              = 'users_edit';
        $users_update            = 'users_update';
    //Users Module End

    //Customers Module Start
      $customers_edit            = 'customers_edit';
      $customers_update          = 'customers_update';
    //Customers Module End

    //All Routes End

    if (Auth::user()->role == 0)
    {
        if ( ($route == $products_create) || 
            ($route == $products_store) || 
            ($route == $products_edit) || 
            ($route == $products_update) ||
            ($route == $invoices_edit) || 
            ($route == $invoices_update) || 
            ($route == $bills_index) || 
            ($route == $bills_create) || 
            ($route == $bills_store) || 
            ($route == $bills_edit) ||
            ($route == $bills_update) || 
            ($route == $bills_show) ||  
            ($route == $expenses_edit) ||  
            ($route == $expenses_update) ||  
            ($route == $payments_edit) || 
            ($route == $users_index) || 
            ($route == $users_create) || 
            ($route == $users_store) || 
            ($route == $users_edit) || 
            ($route == $users_update) )
        {
            return "No";
        }
    else{
      return "accepted";
    }
    }   
    else
    {
            return "accepted";
    }
}

function salesReturn($invoice_id)
{
    $returns                = SalesReturn::leftjoin('customers', 'customers.id', 'sales_return.customer_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return.sales_return_date as sales_return_date',
                                                'sales_return.id as id',
                                                'sales_return.invoice_id as invoice_id',
                                                'sales_return.sales_return_number as sales_return_number',
                                                'sales_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'sales_return.return_amount as return_amount',
                                                'sales_return.due_amount as due_amount',
                                                'sales_return.return_note as return_note',
                                                'sales_return.vat_type as vat_type',
                                                'sales_return.total_vat as total_vat',
                                                'sales_return.tax_type as tax_type',
                                                'sales_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = SalesReturnEntries::leftjoin('sales_return', 'sales_return.id', 'sales_return_entries.sales_return_id')
                                            ->leftjoin('products', 'products.id', 'sales_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'sales_return_entries.product_entry_id')
                                            ->where('sales_return.invoice_id', $invoice_id)
                                            ->select('sales_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                            = $return_entries->where('invoice_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['sales_return_number']  = 'SR - ' . str_pad($value['sales_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['sales_return_date']    = date('d-m-Y', strtotime($value['sales_return_date']));
            $data[$value['id']]['sales_return_id']      = $value['id'];
            $data[$value['id']]['customer_name']        = $value['customer_name'];
            $data[$value['id']]['invoice_id']           = $value['invoice_id'];
            $data[$value['id']]['return_amount']        = $value['return_amount'];
            $data[$value['id']]['paid_amount']          = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']           = $value['due_amount'];
            $data[$value['id']]['return_note']          = $value['return_note'];
            $data[$value['id']]['sub_total']            = $sub_total_value;
            $data[$value['id']]['return_entries']       = $return_entries->where('sales_return_id', $value['id']);

            $total_return_amount                        = $total_return_amount + $value['return_amount'];
            $total_paid_amount                          = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                           = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data                       = [];
        $total_return_amount        = 0;
        $total_paid_amount          = 0;
        $total_due_amount           = 0;
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
        ];
}

function purchaseReturn($bill_id)
{
    $returns                = PurchaseReturn::leftjoin('customers', 'customers.id', 'purchase_return.customer_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return.purchase_return_date as purchase_return_date',
                                                'purchase_return.id as id',
                                                'purchase_return.bill_id as bill_id',
                                                'purchase_return.purchase_return_number as purchase_return_number',
                                                'purchase_return.customer_id as customer_id',
                                                'customers.name as customer_name',
                                                'purchase_return.return_amount as return_amount',
                                                'purchase_return.due_amount as due_amount',
                                                'purchase_return.return_note as return_note',
                                                'purchase_return.vat_type as vat_type',
                                                'purchase_return.total_vat as total_vat',
                                                'purchase_return.tax_type as tax_type',
                                                'purchase_return.total_tax as total_tax')
                                            ->get();

    $return_entries         = PurchaseReturnEntries::leftjoin('purchase_return', 'purchase_return.id', 'purchase_return_entries.purchase_return_id')
                                            ->leftjoin('products', 'products.id', 'purchase_return_entries.product_id')
                                            ->leftjoin('product_entries', 'product_entries.id', 'purchase_return_entries.product_entry_id')
                                            ->where('purchase_return.bill_id', $bill_id)
                                            ->select('purchase_return_entries.*',
                                                     'product_entries.name as product_entry_name',
                                                     'products.name as product_name',
                                                     'product_entries.product_code as product_code')
                                            ->get();

    $total_return_amount        = 0;
    $total_paid_amount          = 0;
    $total_due_amount           = 0;
    $total_tax_amount           = 0;
    $total_vat_amount           = 0;

    if ($returns->count() > 0)
    {
        foreach ($returns as $key => $value)
        {   
            $sub_total_value                                = $return_entries->where('bill_id', $value['id'])->sum('total_amount');

            $data[$value['id']]['purchase_return_number']   = 'PR - ' . str_pad($value['purchase_return_number'], 6, "0", STR_PAD_LEFT);
            $data[$value['id']]['purchase_return_date']     = date('d-m-Y', strtotime($value['purchase_return_date']));
            $data[$value['id']]['purchase_return_id']       = $value['id'];
            $data[$value['id']]['customer_name']            = $value['customer_name'];
            $data[$value['id']]['invoice_id']               = $value['invoice_id'];
            $data[$value['id']]['return_amount']            = $value['return_amount'];
            $data[$value['id']]['paid_amount']              = $value['return_amount'] - $value['due_amount'];
            $data[$value['id']]['due_amount']               = $value['due_amount'];
            $data[$value['id']]['return_note']              = $value['return_note'];
            $data[$value['id']]['sub_total']                = $sub_total_value;
            $data[$value['id']]['return_entries']           = $return_entries->where('purchase_return_id', $value['id']);

            $total_return_amount                            = $total_return_amount + $value['return_amount'];
            $total_paid_amount                              = $total_paid_amount + ($value['return_amount'] - $value['due_amount']);
            $total_due_amount                               = $total_due_amount + $value['due_amount'];
        }
    }
    else
    {
        $data = [];
    }

    return ['data'                  => $data,
            'total_return_amount'   => $total_return_amount,
            'total_paid_amount'     => $total_paid_amount,
            'total_due_amount'      => $total_due_amount,
            'total_tax_amount'      => $total_tax_amount,
            'total_vat_amount'      => $total_vat_amount,
        ];
}

function discountProducts($discount_id)
{
    $discounts      = DiscountProducts::leftjoin('product_entries', 'product_entries.id', 'discount_products.product_id')
                            ->where('discount_products.discount_id', $discount_id)
                            ->orderBy('discount_products.discount_id', 'DESC')
                            ->select('product_entries.name as product_name')
                            ->get();

    return $discounts;
}

function openingBalanceStore($amount, $customer_id, $contact_type)
{
    $branch_id   = Auth::user()->branch_id;
    if ($contact_type == 0)
    {
        $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
        $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

        $invoice                    = new Invoices;
        $invoice->invoice_number    = $invoice_number;
        $invoice->customer_id       = $customer_id;
        $invoice->invoice_date      = date('Y-m-d');
        $invoice->invoice_amount    = $amount;
        $invoice->due_amount        = $amount;
        $invoice->total_buy_price   = 0;
        $invoice->total_discount    = 0;
        $invoice->type              = 2;
        $invoice->branch_id         = $branch_id;
        $invoice->created_by        = Auth::user()->id;
        $invoice->save();

        //Financial Accounting Part Start
        debit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=8, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
        credit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=2, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
        customerBalanceUpdate($customer_id);
        //Financial Accounting Part End
    }
    else
    {
        $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
        $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

        $bill                       = new Bills;
        $bill->bill_number          = $bill_number;
        $bill->vendor_id            = $customer_id;
        $bill->bill_date            = date('Y-m-d');
        $bill->bill_amount          = $amount;
        $bill->due_amount           = $amount;
        $bill->total_discount       = 0;
        $bill->type                 = 2;
        $bill->branch_id            = $branch_id;
        $bill->created_by           = Auth::user()->id;
        $bill->save();

        //Financial Accounting Part Start
        debit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=3, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
        credit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=9, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
        supplierBalanceUpdate($customer_id);
        //Financial Accounting Part End
    }
}

function openingBalanceUpdate($amount, $customer_id, $contact_type)
{
    $branch_id  = Auth::user()->branch_id;
    if ($contact_type == 0)
    {
        $invoice                    = Invoices::leftjoin('invoice_entries', 'invoice_entries.invoice_id', 'invoices.id')
                                                ->where('invoices.customer_id', $customer_id)
                                                ->where('invoices.type', 2)
                                                ->select('invoices.*')
                                                ->first();

        if ($invoice != null)
        {
            $invoice->invoice_amount    = $amount;
            $invoice->updated_by        = Auth::user()->id;
            $invoice->save();

            $find_journal_debit = JournalEntries::where('invoice_id', $invoice['id'])
                                                ->where('transaction_head', 'customer-opening-balance')
                                                ->where('debit_credit', 1)
                                                ->first();
            $find_journal_credit= JournalEntries::where('invoice_id', $invoice['id'])
                                                ->where('transaction_head', 'customer-opening-balance')
                                                ->where('debit_credit', 0)
                                                ->first();

            //Financial Accounting Part Start
            debitUpdate($find_journal_debit['id'], $customer_id=$customer_id, $date=date('Y-m-d'), $account_id=8, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            creditUpdate($find_journal_credit['id'], $customer_id=$customer_id, $date=date('Y-m-d'), $account_id=2, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            customerBalanceUpdate($customer_id);
            //Financial Accounting Part End
        }
        else
        {
            $data_find                  = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number             = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                    = new Invoices;
            $invoice->invoice_number    = $invoice_number;
            $invoice->customer_id       = $customer_id;
            $invoice->invoice_date      = date('Y-m-d');
            $invoice->invoice_amount    = $amount;
            $invoice->due_amount        = $amount;
            $invoice->total_buy_price   = 0;
            $invoice->total_discount    = 0;
            $invoice->type              = 2;
            $invoice->branch_id         = $branch_id;
            $invoice->created_by        = Auth::user()->id;
            $invoice->save();

            //Financial Accounting Part Start
            debit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=8, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            credit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=2, $amount=$amount, $note='Customer Opening Balance', $transaction_head='customer-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            customerBalanceUpdate($customer_id);
            //Financial Accounting Part End
        }
    }
    else
    {
        $bill   = Bills::leftjoin('bill_entries', 'bill_entries.bill_id', 'bills.id')
                        ->where('bills.vendor_id', $customer_id)
                        ->where('bills.type', 2)
                        ->select('bills.*')
                        ->first();

        if ($bill != null)
        {
            $bill->bill_amount          = $amount;
            $bill->updated_by           = Auth::user()->id;
            $bill->save();

            $find_journal_debit = JournalEntries::where('bill_id', $bill['id'])
                                                ->where('transaction_head', 'supplier-opening-balance')
                                                ->where('debit_credit', 1)
                                                ->first();
            $find_journal_credit= JournalEntries::where('bill_id', $bill['id'])
                                                ->where('transaction_head', 'supplier-opening-balance')
                                                ->where('debit_credit', 0)
                                                ->first();

            //Financial Accounting Part Start
            debitUpdate($find_journal_debit['id'], $customer_id=$customer_id, $date=date('Y-m-d'), $account_id=3, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            creditUpdate($find_journal_credit['id'], $customer_id=$customer_id, $date=date('Y-m-d'), $account_id=9, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            supplierBalanceUpdate($customer_id);
            //Financial Accounting Part End
        }
        else
        {
            $data_find                  = Bills::orderBy('created_at', 'DESC')->first();
            $bill_number                = $data_find != null ? $data_find['bill_number'] + 1 : 1;

            $bill                       = new Bills;
            $bill->bill_number          = $bill_number;
            $bill->vendor_id            = $customer_id;
            $bill->bill_date            = date('Y-m-d');
            $bill->bill_amount          = $amount;
            $bill->due_amount           = $amount;
            $bill->total_discount       = 0;
            $bill->type                 = 2;
            $bill->branch_id            = $branch_id;
            $bill->created_by           = Auth::user()->id;
            $bill->save();

            //Financial Accounting Part Start
            debit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=3, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            credit($customer_id=$customer_id, $date=date('Y-m-d'), $account_id=9, $amount=$amount, $note='Supplier Opening Balance', $transaction_head='supplier-opening-balance', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=null, $bill_id=$bill->id, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
            supplierBalanceUpdate($customer_id);
            //Financial Accounting Part End
        }
    }
    
    return 0;
}

function customersTableDetails($id)
{
    $customers  = Customers::find($id);

    return $customers;
}

function ProductVariationName($product_entry_id)
{
    $product            = ProductEntries::leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations')
                                            ->find($product_entry_id);

    return $product['variations'];
}

function ProductName($product_entry_id)
{
    $product            = ProductEntries::find($product_entry_id);

    return $product['name'];
}

function numberTowords(float $amount)
{
    $amount_after_decimal = round($amount - ($num = floor($amount)), 2) * 100;
   // Check if there is any number after decimal
   $amt_hundred = null;
   $count_length = strlen($num);
   $x = 0;
   $string = array();
   $change_words = array(0 => '', 1 => 'One', 2 => 'Two',
     3 => 'Three', 4 => 'Four', 5 => 'Five', 6 => 'Six',
     7 => 'Seven', 8 => 'Eight', 9 => 'Nine',
     10 => 'Ten', 11 => 'Eleven', 12 => 'Twelve',
     13 => 'Thirteen', 14 => 'Fourteen', 15 => 'Fifteen',
     16 => 'Sixteen', 17 => 'Seventeen', 18 => 'Eighteen',
     19 => 'Nineteen', 20 => 'Twenty', 30 => 'Thirty',
     40 => 'Forty', 50 => 'Fifty', 60 => 'Sixty',
     70 => 'Seventy', 80 => 'Eighty', 90 => 'Ninety');
  $here_digits = array('', 'Hundred','Thousand','Lakh', 'Crore');
  while( $x < $count_length ) {
       $get_divider = ($x == 2) ? 10 : 100;
       $amount = floor($num % $get_divider);
       $num = floor($num / $get_divider);
       $x += $get_divider == 10 ? 1 : 2;
       if ($amount) {
         $add_plural = (($counter = count($string)) && $amount > 9) ? 's' : null;
         $amt_hundred = ($counter == 1 && $string[0]) ? ' and ' : null;
         $string [] = ($amount < 21) ? $change_words[$amount].' '. $here_digits[$counter]. $add_plural.' 
         '.$amt_hundred:$change_words[floor($amount / 10) * 10].' '.$change_words[$amount % 10]. ' 
         '.$here_digits[$counter].$add_plural.' '.$amt_hundred;
         }else $string[] = null;
       }
   $implode_to_Rupees = implode('', array_reverse($string));
   $get_paise = ($amount_after_decimal > 0) ? "And " . ($change_words[$amount_after_decimal / 10] . " 
   " . $change_words[$amount_after_decimal % 10]) . ' Paisa' : '';
   return ($implode_to_Rupees ? $implode_to_Rupees . 'Taka ' : '') . $get_paise;
}

function previousDues($customer_id)
{
    $data  = Invoices::where('customer_id', $customer_id)->sum('due_amount');

    return $data;
}

function previousDuesBill($supplier_id)
{
    $data  = Bills::where('vendor_id', $supplier_id)->sum('due_amount');

    return $data;
}

function eng2bang($number)
{
    $bn   = array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
    $en   = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    return  str_replace($en, $bn, $number);
}

function ban2eng($number)
{
    $bn   = array("০", "১", "২", "৩", "৪", "৫", "৬", "৭", "৮", "৯");
    $en   = array("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");

    return  str_replace($bn, $en, $number);
}

function eng2bangMonth($number)
{
    $bn   = array("জানুয়ারি", "ফেব্রুয়ারী", "মার্চ", "এপ্রিল", "মে", "জুন", "জুলাই", "আগস্ট", "সেপ্টেম্বর", "অক্টতবর", "নভেম্বার", "ডিসেম্বর");
    $en   = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");

    return  str_replace($en, $bn, $number);
}

function findStock($product_entry_id, $branch_id)
{
    $data  = BranchInventories::where('product_entry_id', $product_entry_id)
                            ->where('branch_id', $branch_id)
                            ->first();

    return $data;
}

function actualStockInEdit($product_entry_id, $quantity, $main_unit_id, $conversion_unit_id)
{
    if ($main_unit_id == $conversion_unit_id)
    {
        $data   = $quantity;
    }
    else
    {
        $find_conversion_rate   = UnitConversions::where('product_entry_id', $product_entry_id)
                                                ->where('main_unit_id', $main_unit_id)
                                                ->where('conversion_unit_id', $conversion_unit_id)
                                                ->first();

        $data                   = $quantity/$find_conversion_rate['conversion_rate'];
    }

    return $data;
}

function invoiceDetails($invoice_id)
{
    $data = InvoiceEntries::where('invoice_id', $invoice_id)->get();

    return $data;
}

function billDetails($bill_id)
{
    $data = BillEntries::where('bill_id', $bill_id)->get();

    return $data;
}

function customerBalanceUpdate($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    //Customer Balance Update Start
        $cus_update             = Customers::find($customer_id);
        $cus_update->balance    = $balance;
        $cus_update->save();
    //Customer Balance Update End
}

function supplierBalanceUpdate($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    //Customer Balance Update Start
        $cus_update             = Customers::find($customer_id);
        $cus_update->balance    = $balance;
        $cus_update->save();
    //Customer Balance Update End
}

function employeeBalanceUpdate($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['employee-salary'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-made'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    //Customer Balance Update Start
        $cus_update             = Customers::find($customer_id);
        $cus_update->balance    = $balance;
        $cus_update->save();
    //Customer Balance Update End
}

function debit($customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id)
{
    $branch_id                                  = Auth::User()->branch_id;   
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = new JournalEntries;
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 1;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->created_by            = $user_id;
    $journal_entry_debit->save();
}

function credit($customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id)
{
    $branch_id                                  = Auth::User()->branch_id;   
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = new JournalEntries;
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 0;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->created_by            = $user_id;
    $journal_entry_debit->save();
}

function debitUpdate($transaction_id, $customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id)
{
    $branch_id                                  = Auth::User()->branch_id;   
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = JournalEntries::find($transaction_id);
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 1;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->updated_by            = $user_id;
    $journal_entry_debit->save();
}

function creditUpdate($transaction_id, $customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id)
{
    $branch_id                                  = Auth::User()->branch_id;   
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = JournalEntries::find($transaction_id);
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 0;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->updated_by            = $user_id;
    $journal_entry_debit->save();
}

function customerBalance($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['sales', 'customer-opening-balance', 'payment-made', 'customer-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-receive', 'sales-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    return $balance;
}

function supplierBalance($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['production', 'purchase', 'supplier-opening-balance', 'payment-receive', 'supplier-settlement'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-made', 'purchase-return', 'discount'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    return $balance;
}

function employeeBalance($customer_id)
{
    $debit      = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['employee-salary'])
                                ->where('debit_credit', 1)
                                ->sum('amount');

    $credit     = JournalEntries::where('customer_id', $customer_id)
                                ->whereIn('transaction_head', ['payment-made'])
                                ->where('debit_credit', 0)
                                ->sum('amount');

    $balance    = $debit - $credit;

    return $balance;
}

function calculateOpeningBalance($customer_id)
{
    $customer_advance  = Customers::find($customer_id);

    if ($customer_advance['balance'] > 0)
    {
        $data['balance']   = $customer_advance['balance'];
        $data['type']      = 1; //1 = previous dues
    }

    if ($customer_advance['balance'] < 0)
    {
        $data['balance']   = abs($customer_advance['balance']);
        $data['type']      = 2; //2 = advance amount
    }

    if ($customer_advance['balance'] == 0)
    {
        $data['balance']   = 0;
        $data['type']      = 1; //1 = previous dues
    }

    return $data;
}

//Use Only For Balance Transfer Start
function debitBT($customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id, $branch_id)
{
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = new JournalEntries;
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 1;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->created_by            = $user_id;
    $journal_entry_debit->save();
}

function creditBT($customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id, $branch_id)
{
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = new JournalEntries;
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 0;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->created_by            = $user_id;
    $journal_entry_debit->save();
}

function debitUpdateBT($transaction_id, $customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id, $branch_id)
{
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = JournalEntries::find($transaction_id);
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 1;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->updated_by            = $user_id;
    $journal_entry_debit->save();
}

function creditUpdateBT($transaction_id, $customer_id, $date, $account_id, $amount, $note, $transaction_head, $income_id, $expense_id, $balance_transfer_id, $invoice_id, $bill_id, $payment_id, $production_id, $settlement_id, $sales_return_id, $purchase_return_id, $branch_id)
{
    $user_id                                    = Auth::User()->id;   
    $journal_entry_debit                        = JournalEntries::find($transaction_id);
    $journal_entry_debit->date                  = date('Y-m-d', strtotime($date));
    $journal_entry_debit->customer_id           = $customer_id;
    $journal_entry_debit->account_id            = $account_id;
    $journal_entry_debit->debit_credit          = 0;
    $journal_entry_debit->amount                = $amount;
    $journal_entry_debit->note                  = $note;
    $journal_entry_debit->transaction_head      = $transaction_head;
    $journal_entry_debit->income_id             = $income_id;
    $journal_entry_debit->expense_id            = $expense_id;
    $journal_entry_debit->balance_transfer_id   = $balance_transfer_id;
    $journal_entry_debit->invoice_id            = $invoice_id;
    $journal_entry_debit->bill_id               = $bill_id;
    $journal_entry_debit->payment_id            = $payment_id;
    $journal_entry_debit->production_id         = $production_id;
    $journal_entry_debit->settlement_id         = $settlement_id;
    $journal_entry_debit->sales_return_id       = $sales_return_id;
    $journal_entry_debit->purchase_return_id    = $purchase_return_id;
    $journal_entry_debit->branch_id             = $branch_id;
    $journal_entry_debit->updated_by            = $user_id;
    $journal_entry_debit->save();
}
//Use Only For Balance Transfer End

function profitLoss($invoice_id)
{
    $sell_price = Invoices::find($invoice_id);
    
    $profit_loss = $sell_price['invoice_amount'] - $sell_price['total_buy_price'];
    return $profit_loss;
    
}