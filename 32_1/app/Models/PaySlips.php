<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class PaySlips extends Model
{  
    protected $table = "pay_slips";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function monthlySalarySheets()
    {
        return $this->belongsTo('App\Models\MonthlySalarySheets','monthly_salary_sheet_id');
    }

    public function employee()
    {
        return $this->belongsTo('App\Models\Customers','employee_id');
    }

}
