

<?php $__env->startSection('title', 'Leave Categories'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Leave Categories</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Basic Settings</a></li>
                                    <li class="breadcrumb-item active">Leave Categories</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>

                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('leave_category_store')); ?>" method="post" files="true" enctype="multipart/form-data">
            					<?php echo e(csrf_field()); ?>


                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-4 form-group">
                                        <label for="productname">Name *</label>
                                        <input type="text" name="name" class="inner form-control" id="name" placeholder="Enter Name" required />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Short Name</label>
                                        <input type="text" name="short_name" class="inner form-control" id="short_name" placeholder="Enter Short Name" />
                                    </div>

                                    <div class="col-md-4 form-group">
                                        <label for="productname">Total Leave</label>
                                        <input type="text" name="total_leave" class="inner form-control" id="total_leave" placeholder="Enter Total Leave" />
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('leave_category_index')); ?>">Close</a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/32_1/Modules/Attendance/Resources/views/leave_category/create.blade.php ENDPATH**/ ?>