

<?php $__env->startSection('title', 'Show'); ?>

<style>
    table,th {
        border: 1px solid; black;
        border-collapse: collapse;
        padding: 2px;
        line-height: 1;
    }

    td {
        border-left: 1px dashed #b9b9b9;
        border-right: 1px solid black;
        padding: 4px;
    }

    tr{
    border: 1px dashed #b9b9b9!important;
    }
    
    .tr-height 
    {
        font-size: 15px!important;
        line-height: 20px!important;
    }
    
   /* .baal {
        display: none;
    }*/
    
    .border-none{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-top: 1px solid #fff!important;
    }
    
    .border-none-2{
        border-bottom: 1px solid #fff!important;
        border-left: 1px solid #fff!important;
        border-right: 1px solid #fff!important;
    }
    
    .border-none-3{
        border-right: 1px solid black!important;
        border-bottom: 1px solid #fff!important;
    }

    
    @page  {
        size: A4;
        page-break-after: always;
    }
    
    @media  print { .baal { display: block !important; } }

    @media  print {
        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 2px;
            line-height: 1;
        }
    
        td {
            border-left: 1px dashed #b9b9b9;
            border-right: 1px solid black;
            padding: 4px;
        }
    
        tr{
            border: 1px dashed #b9b9b9!important;
        }

        
        .tr-height 
        {
            font-size: 15px!important;
            line-height: 20px!important;
        }
        
        .pagebrack {page-break-after: always;}
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.sales')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.sales')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.show')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <?php 
                            $totalRowCount = ceil(count($entries)/20);
                            $rows = number_format($totalRowCount, 0, ".", "");
                            $forSerial = 19;
                            $forSerials = -1;
                         ?>


                        <?php for($count = 0; $count < $rows; $count++): ?>

                        <div  class="card" style="width: 8.5in;height: 11.1in;">
                            <div class="card-body">
                                <?php
                                    $dt = new DateTime('now', new DateTimezone('Asia/Dhaka'));
                                    // $dt = new DateTime($invoice['created_at'], new DateTimezone('Asia/Dhaka'));

                                    $date           = date('d-m-Y', strtotime($invoice['invoice_date']));
                                    $phone          = $invoice->customer->phone != null ? $invoice->customer->phone : '';
                                    $invoice_number = str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT);

                                    $search=array("0","1","2","3","4","5",'6',"7","8","9"); 
                                    $replace=array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                    $date           = str_replace($search,$replace,$date);   
                                    $phone          = str_replace($search,$replace,$phone);   
                                    $invoice_number = str_replace($search,$replace,$invoice_number);
                                    $time           = str_replace($search,$replace,$dt->format('g:i a')); 
                                ?>

                                <div class="row" style="height: 3.1in;">
                                    
                                </div>

                                <div class="row" style="padding-right:0px!important;;padding-left: 20px!important;">
                                    <div style="font-size: 15px;" class="col-7">
                                        
                                            <strong>মেমো নং : </strong><?php echo e($invoice_number); ?> <br>
                                            <strong>নাম :    </strong><?php echo e($invoice['customer_name']); ?><br>
                                            <strong>ঠিকানা : </strong> <?php echo e($invoice->customer->address != null ? $invoice->customer->address : ''); ?>

                                        
                                    </div>
                                    <div style="font-size: 15px;" class="col-5 text-sm-right">
                                        
                                            <strong>তারিখ : </strong><?php echo e($date); ?> || <?php echo e($time); ?> <br>
                                            <strong>মোবাইল : </strong> <?php echo e($phone); ?>

                                        
                                    </div>
                                </div>

                                <div class="row" style="padding-right:0px!important;padding-left: 30px!important;height:7in;">
                                    <table  style="width: 100%;">
                                        <tr>
                                            <th style="font-size: 20px;width: 5%;text-align: center;padding: 5px;">নং</th>
                                            <th style="font-size: 20px;width: 57%;text-align: center;padding: 5px;">পণ্যের বিবরণ</th>
                                            <th style="font-size: 20px;width: 8%;text-align: center;padding: 5px;">পরিমাণ</th>
                                            <th style="font-size: 20px;width: 15%;text-align: center;padding: 5px;">দর</th>
                                            <th style="font-size: 20px;width: 15%;text-align: center;padding: 5px;">টাকা</th>
                                        </tr>

                                        

                                        <?php
                                            $total_amount   = 0;
                                            $i              = 0;
                                        ?>


                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                      
                                        
                                        <?php if(($key <= $forSerial) && ($key > $forSerials)): ?>

                                            
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>


                                        <?php

                                            $key            = $key + 1;
                                            $quantity       = $value['quantity'];
                                            $rate           = $value['rate'];
                                            $totalamount    = round($value['total_amount'], 2);


                                            $search     = array("0","1","2","3","4","5",'6',"7","8","9"); 
                                            $replace    = array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                            $key        = str_replace($search,$replace,$key);    
                                            $quantity   = str_replace($search,$replace,$quantity);    
                                            $rate       = str_replace($search,$replace,$rate);    
                                            $totalamount= str_replace($search,$replace,$totalamount);    
                                        ?>

                                        <tr>
                                            <td style="text-align: center"><?php echo e($key); ?></td>
                                            <td><?php echo e($value['product_entry_name'] . $productCode); ?></td>
                                            <td style="text-align: center"><?php echo e($quantity); ?></td>
                                            <td style="text-align: center"><?php echo e($rate . $unit); ?></td>
                                            <td style="text-align: center"><?php echo e($totalamount); ?></td>
                                        </tr>

                                        <?php elseif($key > $forSerial): ?>

                                        <?php 
                                        $forSerial = $forSerial + 20; 
                                        $forSerials = $forSerials + 20;
                                        ?>

                                        <?php break; ?>

                                        <?php endif; ?>

                                        
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>
                                       

                                        <?php
                                            $total          = $net_paya != 0 ? round($net_paya - $invoice['total_discount']) : '';
                                            $discount       = round($discount_on_total_amount + $invoice['total_discount']);
                                            $paid_amount    = $paid != 0 ? round($paid, 2) : '';
                                            $due_amount     = $invoice['invoice_amount']  - $paid != 0 ? round($invoice['invoice_amount']  - $paid) : '';

                                            $search     = array("0","1","2","3","4","5",'6',"7","8","9"); 
                                            $replace    = array("০","১","২","৩","৪","৫","৬","৭","৮","৯");

                                            $total          = str_replace($search,$replace,$total);    
                                            $discount       = str_replace($search,$replace,$discount);    
                                            $paid_amount    = str_replace($search,$replace,$paid_amount);    
                                            $due_amount     = str_replace($search,$replace,$due_amount);
                                            $sub_total_amount     = str_replace($search,$replace,$sub_total_amount);     
                                        ?>

                                        

                                        <?php if($rows == $count+1): ?>
                                        <tr>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-3" style="text-align: center"></th>
                                            <th style="text-align: left;text-align: right" colspan="1"><strong>মোট</strong></th>
                                            <th style="text-align: center"><?php echo e($sub_total_amount); ?></th>
                                        </tr>

                                        <tr>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-3" style="text-align: center"></th>
                                            <th style="text-align: right"><strong>ছাড়</strong></th>
                                            <th style="text-align: center"><?php echo e($discount); ?></th>
                                        </tr>

                                        <tr>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-3" style="text-align: center"></th>
                                            <th style="text-align: right"><strong>নগদ প্রদান</strong></th>
                                            <th style="text-align: center"><?php echo e($paid_amount); ?></th>
                                        </tr>

                                        <tr>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-2" style="text-align: center"></th>
                                            <th class="border-none-3" style="text-align: center"></th>
                                            <th style="text-align: right"><strong>বাকি</strong></th>
                                            <th style="text-align: center"><?php echo e($due_amount); ?></th>
                                        </tr>
                                        <?php endif; ?> 
  

                                    </table>
                                </div>



                                <div class="row" style="padding-top:35px;padding-bottom: 10px;padding-right:25px!important;;padding-left: 30px!important;">
                                    <div class="col-5">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">ক্রেতার সাক্ষর  </span> </h6>
                                    </div>

                                    <div class="col-2">
                                        <h6> Page <?php echo e($count+1); ?>....... </h6>
                                    </div>

                                    <div class="col-5">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">বিক্রেতার সাক্ষর </span> </h6>
                                    </div>
                                </div>
                     

                            </div>
                        </div>

                        <div class="pagebrack"></div>
                        <?php endfor; ?>

                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">
    var date        = new Date(); 
    var currenttime = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    document.getElementById("currenttime").innerHTML = currenttime;
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp-7.3\htdocs\pos\32-update\Modules/Invoices\Resources/views/show.blade.php ENDPATH**/ ?>