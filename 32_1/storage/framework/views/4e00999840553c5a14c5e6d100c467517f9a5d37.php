

<?php $__env->startSection('title', 'Opening Stock'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.opening_stock')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.opening_stock')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.opening_stock')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>
                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>
                                <form method="get" action="<?php echo e(route('products_opening_stock')); ?>" enctype="multipart/form-data">
                                    <div class="col-md-4 form-group">
                                        <label style="text-align: left" for="productname"><?php echo e(__('messages.search_by_product')); ?> </label>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <select style="width: 100%" id="product_id" name="product_id" class="form-control select2">
                                                   <option value="0"><?php echo e(__('messages.all')); ?></option>
                                                </select>
                                            </div>
                                            
                                            <div class="col-md-4">
                                                <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.search')); ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                                <hr>

                                <form id="FormSubmit" action="<?php echo e(route('products_opening_stock_store')); ?>" method="post" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>

      
                                <?php if(!empty($product_entries) && ($product_entries->count() > 0)): ?>
                                    <?php $__currentLoopData = $product_entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="form-group row">
                                            <label for="example-text-input" class="col-md-4 col-form-label">
                                                <?php echo e($value['name']); ?>

                                                <?php
                                                    if ($value['product_type'] == 2)
                                                    {
                                                        echo ' - ' . $value['variations'];
                                                    } 
                                                ?>
                                            </label>
                                            
                                            <div class="col-md-8">
                                                <input class="form-control" type="number" value="" name="stock_in_hand[]" id="stock_in_hand" placeholder="<?php echo e(__('messages.opening_stock')); ?>" required>
                                                <input class="form-control" type="hidden" value="<?php echo e($value['id']); ?>" name="product_entry_id[]">
                                                <input class="form-control" type="hidden" value="<?php echo e($value['product_id']); ?>" name="product_id[]">
                                            </div>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    <div class="form-group row">
                                        <div class="col-md-4"></div>
                                        <div class="button-items col-md-2 pull-right">
                                            <button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()"><?php echo e(__('messages.save')); ?></button>
                                            <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                        </div>
                                        <div class="col-md-6"></div>
                                    </div>
                                <?php else: ?>
                                <div class="form-group row">
                                    <div style="text-align: center" class="col-md-12">
                                        <h6>No Product Available For Adding Opening Stock.</h6>
                                    </div>
                                </div>
                                <?php endif; ?>
                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
        
        $( document ).ready(function() {
            
            var site_url  = $('.site_url').val();

            $("#product_id").select2({
                ajax: { 
                url:  site_url + '/reports/sales-statement/product-list',
                type: "get",
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        searchTerm: params.term // search term
                    };
                },
                processResults: function (response) {
                    return {
                        results: response
                    };
                },
                    cache: true
                },

                minimumInputLength: 0,
                escapeMarkup: function(result) {
                    return result;
                },
                templateResult: function (result) {
                    if (result.loading) return 'Searching...';

                    return result['text'];
                },
            });
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/32_1/Modules/Products/Resources/views/opening_stock.blade.php ENDPATH**/ ?>