

<?php $__env->startSection('title', 'Edit Branch'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.edit')); ?> <?php echo e(__('messages.branch')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.branch')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.edit')); ?> <?php echo e(__('messages.branch')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('branch_update', $find_branch['id'])); ?>" method="post" files="true" enctype="multipart/form-data">
                                <?php echo e(csrf_field()); ?>


                                <div style="margin-bottom: 0px !important" class="form-group row">
                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.branch_name')); ?> *</label>
                                        <input type="text" name="branch_name" class="inner form-control" id="branch_name" value="<?php echo e($find_branch['name']); ?>" required />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.organization_name')); ?></label>
                                        <input type="text" name="organization_name" class="inner form-control" id="organization_name" placeholder="Organization Name" value="<?php echo e($find_branch['organization_name']); ?>" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.address')); ?></label>
                                        <input type="text" name="address" class="inner form-control" id="address" placeholder="Address" value="<?php echo e($find_branch['address']); ?>" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.phone')); ?></label>
                                        <input type="text" name="contact_number" class="inner form-control" id="contact_number" placeholder="<?php echo e(__('messages.phone')); ?>" value="<?php echo e($find_branch['contact_number']); ?>" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.email')); ?></label>
                                        <input type="text" name="email" class="inner form-control" id="email" placeholder="Email Address" value="<?php echo e($find_branch['email']); ?>" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.website')); ?></label>
                                        <input type="text" name="website" class="inner form-control" id="website" placeholder="<?php echo e(__('messages.website_link')); ?>" value="<?php echo e($find_branch['website']); ?>" />
                                    </div>

                                    <div class="col-md-3 form-group">
                                        <label for="productname"><?php echo e(__('messages.printer_type')); ?></label>
                                        <select style="cursor: pointer" class="form-control" id="printer_type" name="printer_type">
                                            <option value="0" <?php echo e($find_branch['printer_type'] == 0 ? 'selected' : ''); ?>>58 mm Thermal Receipt</option>
                                            <option value="1" <?php echo e($find_branch['printer_type'] == 1 ? 'selected' : ''); ?>>80 mm Thermal Receipt</option>
                                            <option value="2" <?php echo e($find_branch['printer_type'] == 2 ? 'selected' : ''); ?>>A4 Size Paper</option>
                                            <option value="3" <?php echo e($find_branch['printer_type'] == 3 ? 'selected' : ''); ?>>Letter Size Paper</option>
                                        </select>
                                    </div>

                                    <div class="col-lg-3 col-md-3 col-sm-12 col-12 form-group">
                                        <label for="productname"><?php echo e(__('messages.logo')); ?></label>
                                        <input class="form-control" type="file" name="logo" >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Previous Logo</label>
                                    <div class="col-md-10">
                                        <?php if($find_branch['logo'] != null): ?>
                                        <img style="height: 60px;width: 200px" src="<?php echo e(url('public/'.$find_branch['logo'])); ?>">
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <hr style="margin-top: 0px !important">

                                <div class="form-group row">
                                    <div class="button-items col-md-12">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.update')); ?></button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('branch_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <h4 class="card-title"><?php echo e(__('messages.all_branch')); ?></h4>

                                <br>

                                <table id="datatable" class="table table-bordered nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th><?php echo e(__('messages.sl')); ?></th>
                                            <th><?php echo e(__('messages.branch_name')); ?></th>
                                            <th><?php echo e(__('messages.address')); ?></th>
                                            <th><?php echo e(__('messages.phone')); ?></th>
                                            <th><?php echo e(__('messages.logo')); ?></th>
                                            <th><?php echo e(__('messages.action')); ?></th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php if(!empty($branches) && ($branches->count() > 0)): ?>
                                        <?php $__currentLoopData = $branches; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $branch): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                                <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($branch['name']); ?></td>
                                                <td><?php echo e($branch['address']); ?></td>
                                                <td><?php echo e($branch['contact_number']); ?></td>
                                                <td>
                                                    <img style="height: 40px;width: 40px" src="<?php echo e(url('public/'.$branch->logo)); ?>">
                                                </td>
                                                <td>
                                                    <div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('branch_edit', $branch['id'])); ?>"><?php echo e(__('messages.edit')); ?></a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH F:\xampp-7.3\htdocs\pos\32-update\Modules/Branch\Resources/views/edit.blade.php ENDPATH**/ ?>