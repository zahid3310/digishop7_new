<div class="vertical-menu">
    <div data-simplebar class="h-100">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title"><?php echo e(__('messages.menu')); ?></li>

                <li>
                    <a href="<?php echo e(route('home')); ?>" class="waves-effect">
                        <i class="fa fa-home"></i>
                        <span><?php echo e(__('messages.dashboard')); ?></span>
                    </a>
                </li>

                <?php if(Auth::user()->branch_id == 1): ?>
                <li class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : '' || Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span><?php echo e(__('messages.receive_or_purchase_report')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('bills_index')); ?>"><?php echo e(__('messages.receive_purchase_item')); ?></a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'bills_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'bills_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('bills_all_bills')); ?>"><?php echo e(__('messages.list_of_receive/purchase')); ?></a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'purchase_return_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('purchase_return_index')); ?>"><?php echo e(__('messages.receive/purchase_return')); ?></a> </li>
                    </ul>
                </li>
                <?php endif; ?>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-suitcase"></i><span><?php echo e(__('messages.stock_transfer')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('stock_transfer_index')); ?>"><?php echo e(__('messages.new_transfer')); ?></a> </li>
                    </ul>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_index' ? 'mm-active' : '' || Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-shopping-basket"></i><span><?php echo e(__('messages.sales_to_sr/customer')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('invoices_index')); ?>"><?php echo e(__('messages.daily_sales')); ?></a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'invoices_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show' ? 'mm-active' : '' || Route::currentRouteName() == 'invoices_show_pos' ? 'mm-active' : ''); ?>" href="<?php echo e(route('invoices_all_sales')); ?>"><?php echo e(__('messages.list_of_sales')); ?></a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'sales_return_show' ? 'mm-active' : ''); ?>"  href="<?php echo e(route('sales_return_index')); ?>"><?php echo e(__('messages.sales_return')); ?></a> </li>
                    </ul>
                </li>

                <li class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'payment_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : '' ||  Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fab fa-adn"></i><span><?php echo e(__('messages.accounts')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'expenses_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'expenses_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('expenses_index')); ?>"><?php echo e(__('messages.expenses')); ?></a> </li>

                        <li> <a class="<?php echo e(Route::currentRouteName() == 'incomes_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'incomes_categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('incomes_index')); ?>"><?php echo e(__('messages.incomes')); ?></a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'balance_transfer_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('balance_transfer_index')); ?>"><?php echo e(__('messages.balance_transfer')); ?></a>
                        </li>
                        <li> <a class="<?php echo e(Request::getQueryString() == 'payment_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('payments_create').'?payment_type=0'); ?>"><?php echo e(__('messages.receive_from_customer')); ?></a> </li>
                        <?php if(Auth::user()->branch_id == 1): ?>
                        <li> <a class="<?php echo e(Request::getQueryString() == 'payment_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('payments_create').'?payment_type=1'); ?>"><?php echo e(__('messages.pay_to_supplier')); ?></a> </li>
                        <?php endif; ?>
                        <li> <a class="<?php echo e(Request::getQueryString() == 'settlement_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('settlements_create').'?settlement_type=0'); ?>"><?php echo e(__('messages.customer_settlement')); ?></a> </li>
                        <?php if(Auth::user()->branch_id == 1): ?>
                        <li> <a class="<?php echo e(Request::getQueryString() == 'settlement_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('settlements_create').'?settlement_type=1'); ?>"><?php echo e(__('messages.supplier_settlement')); ?></a> </li>
                        <?php endif; ?>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="far fa-envelope"></i><span><?php echo e(__('messages.messaging')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('messages_send_index')); ?>"><?php echo e(__('messages.send_meg')); ?></a> </li>
                    </ul>
                </li>
                
                <li class="<?php echo e(Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_create' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_index' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'attendance_manual_attendance_index' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_create' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_application_index' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-fingerprint"></i><span>Attendance</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('attendance_manual_attendance_index')); ?>">Manual Attendance</a> </li>
                        <li> <a href="<?php echo e(route('leave_application_create')); ?>">Leave Application</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'leave_application_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('leave_application_index')); ?>">Leave Application List</a> </li>
                    </ul>
                </li>
                
                <li class="<?php echo e(Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : '' || Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-money-check-alt"></i><span>Payroll</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li> <a href="<?php echo e(route('process_monthly_salary_index')); ?>">List of Salary Sheet</a> </li>
                        <li> <a href="<?php echo e(route('process_monthly_salary_create')); ?>">Process Salary Sheet</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'pay_slip_all_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('pay_slip_index')); ?>">Pay Slip</a> </li>
                        <li> <a class="<?php echo e(Route::currentRouteName() == 'pay_slip_show' ? 'mm-active' : ''); ?>" href="<?php echo e(route('pay_slip_list')); ?>">List of Pay Slip</a> </li>
                    </ul>
                </li>

                <li>
                    <a class="has-arrow waves-effect">
                        <i class="fas fa-th"></i><span><?php echo e(__('messages.reports')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li class="">
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.sales')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('sales_statement_index')); ?>"><?php echo e(__('messages.sales_statement')); ?></a> </li>
                                <li> <a href="<?php echo e(route('sales_summary_index')); ?>"><?php echo e(__('messages.sales_summary')); ?></a> </li>
                                <li> <a href="<?php echo e(route('product_wise_sales_report_index')); ?>"><?php echo e(__('messages.product_wise_sales')); ?></a> </li>
                            </ul>
                        </li>

                        <?php if(Auth()->user()->branch_id == 1): ?>
                        <li class="">
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.receive_or_purchase_report')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('purchase_statement_index')); ?>"><?php echo e(__('messages.purchase_statement')); ?></a> </li>
                                <li> <a href="<?php echo e(route('purchase_summary_index')); ?>"><?php echo e(__('messages.receive/purchase_summary')); ?></a> </li>
                            </ul>
                        </li>
                        <?php endif; ?>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.accounts')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('expense_report_index')); ?>"><?php echo e(__('messages.list_of_expense')); ?></a> </li>
                                <li> <a href="<?php echo e(route('income_report_index')); ?>"><?php echo e(__('messages.list_of_income')); ?></a> </li>
                                <!-- <li> <a href="<?php echo e(route('general_ledger_index')); ?>"><?php echo e(__('messages.ledger_book')); ?></a> </li> -->
                                <li> <a href="<?php echo e(route('income_expense_ledger_index')); ?>"><?php echo e(__('messages.cash_book')); ?></a> </li>
                                <li> <a href="<?php echo e(route('income_statement_index')); ?>"><?php echo e(__('messages.bank_book')); ?></a> </li>
                                <!-- <li> <a href="<?php echo e(route('daily_collection_report_index')); ?>"><?php echo e(__('messages.daily_report')); ?></a> </li> -->
                            </ul>
                        </li>
                        
                        <!-- <li class="">
                            <a class="has-arrow waves-effect">
                                Payments
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('customer_payment_report_index')); ?>">SR/Customer Payments</a> </li>
                                <li> <a href="<?php echo e(route('supplier_payment_report_index')); ?>">Supplier Payments</a> </li>
                            </ul>
                        </li> -->

                        <li class="">
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.mis')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('stock_report_index')); ?>"><?php echo e(__('messages.stock_status')); ?></a> </li>
                                <!--<li> <a href="<?php echo e(route('stock_status_details_index')); ?>">Stock Status Details</a> </li>-->
                                <li> <a href="<?php echo e(route('stock_transfer_report_index')); ?>"><?php echo e(__('messages.stock_transfer_report')); ?></a> </li>
                                <li> <a href="<?php echo e(route('current_balance_print')); ?>" target="_blank"><?php echo e(__('messages.current_balance')); ?></a> </li>
                                <?php if(Auth::user()->branch_id == 1): ?>
                                <li> <a href="<?php echo e(route('due_report_supplier_index')); ?>"><?php echo e(__('messages.supplier_Ledger')); ?></a> </li>
                                <?php endif; ?>
                                <li> <a href="<?php echo e(route('due_report_customer_index')); ?>"><?php echo e(__('messages.SR/Customer_Ledger')); ?></a> </li>
                                <li> <a href="<?php echo e(route('due_report_customer_print_due')); ?>"><?php echo e(__('messages.Customer_due_list')); ?></a> </li>
                                <?php if(Auth::user()->branch_id == 1): ?>
                                <li> <a href="<?php echo e(route('due_report_supplier_index_due')); ?>"><?php echo e(__('messages.Supplier_due_list')); ?></a> </li>
                                <?php endif; ?>
                                <!-- <li> <a href="<?php echo e(route('due_ledger_index')); ?>">Due Ledger</a> </li> -->
                            </ul>
                        </li>

                        <li class="">
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.basic_report')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <?php if(Auth()->user()->branch_id == 1): ?>
                                <li> <a href="<?php echo e(route('item_list_index')); ?>"><?php echo e(__('messages.list_items')); ?></a> </li>
                                <li> <a href="<?php echo e(route('emergency_item_list_index')); ?>"><?php echo e(__('messages.emergency_purchase')); ?></a> </li>
                                <li> <a href="<?php echo e(route('product_suppliers_index')); ?>"><?php echo e(__('messages.item_wise_supplier')); ?></a> </li>
                                <li> <a href="<?php echo e(route('product_customers_index')); ?>"><?php echo e(__('messages.item_wise_SR/Customer')); ?></a> </li>
                                <?php endif; ?>
                                <li> <a href="<?php echo e(route('register_list_index').'?type=0'); ?>" target="_blank"><?php echo e(__('messages.SR/Customer_list')); ?></a> </li>
                                <li> <a href="<?php echo e(route('register_list_index').'?type=1'); ?>" target="_blank"><?php echo e(__('messages.supplier_list')); ?></a> </li>
                            </ul>
                        </li>
                        
                        <!-- <li> <a href="#">List of Sending SMS</a> </li>
                        <li> <a href="<?php echo e(route('salary_report_index')); ?>">Salary Report</a> </li> -->

                    </ul>
                </li>

                <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || 
                Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || 
                Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || 
                Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' ||
                Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>">
                    <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || 
                    Request::getQueryString() == 'contact_type=3' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'users_edit' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'branch_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' ||
                    Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                    Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                        <i class="fas fa-wrench"></i><span><?php echo e(__('messages.basic_settings')); ?></span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <?php if(Auth()->user()->branch_id == 1): ?>
                        <li class="<?php echo e(Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'products_variations_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_opening_stock' ? 'mm-active' : '' || Route::currentRouteName() == 'products_barcode_print' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_index' ? 'mm-active' : '' || Route::currentRouteName() == 'categories_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_index' ? 'mm-active' : '' || Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                 <?php echo e(__('messages.product')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_index')); ?>"><?php echo e(__('messages.add_product')); ?></a> </li>
                                <li> <a class="" href="<?php echo e(route('products_index_all')); ?>"><?php echo e(__('messages.list_product')); ?></a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_category_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_category_index')); ?>"><?php echo e(__('messages.add_categories')); ?></a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'categories_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('categories_index')); ?>" href="<?php echo e(route('categories_index')); ?>"><?php echo e(__('messages.add_major_categories')); ?></a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_units_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_units_index')); ?>"><?php echo e(__('messages.add_unit_measure')); ?></a> </li>
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'products_variations_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('products_variations_index')); ?>"><?php echo e(__('messages.product_variations')); ?></a> </li>
                                <li> <a href="<?php echo e(route('products_barcode_print')); ?>"><?php echo e(__('messages.print_barcode')); ?></a> </li>
                                <li> <a href="<?php echo e(route('products_opening_stock')); ?>"><?php echo e(__('messages.bulk_opening_stock')); ?></a> </li>
                                <li> <a href="<?php echo e(route('products_bulk_product_list_update')); ?>"><?php echo e(__('messages.bulk_product_update')); ?></a> </li>
                            </ul>
                        </li>
                        <?php endif; ?>

                        <li class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=1' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=2' ? 'mm-active' : '' || Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                 <?php echo e(__('messages.registers')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=0' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=0'); ?>"><?php echo e(__('messages.add_sr/customer')); ?></a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=1' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=1'); ?>"><?php echo e(__('messages.add_supplier')); ?></a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=2' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=2'); ?>"><?php echo e(__('messages.add_employee')); ?></a> </li>
                                <li> <a class="<?php echo e(Request::getQueryString() == 'contact_type=3' ? 'mm-active' : ''); ?>" href="<?php echo e(route('customers_index').'?contact_type=3'); ?>"><?php echo e(__('messages.add_reference')); ?></a> </li>
                                <li> <a class="" href="<?php echo e(route('add_area')); ?>"><?php echo e(__('messages.add_area')); ?></a> </li>
                                <li> <a class="" href="<?php echo e(route('add_area_zone')); ?>"><?php echo e(__('messages.add_area_zone')); ?></a> </li>
                            </ul>
                        </li>

                        <li class="<?php echo e(Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || 
                        Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' ||
                        Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'account_types_index' ? 'mm-active' : '' || Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : '' || 
                            Route::currentRouteName() == 'chart_of_accounts_index' ? 'mm-active' : '' || Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                <?php echo e(__('messages.accounts')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <!-- <li> <a class="<?php echo e(Route::currentRouteName() == 'account_types_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('account_types_index')); ?>">Account Types</a> </li> -->
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'chart_of_accounts_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('chart_of_accounts_index')); ?>"><?php echo e(__('messages.chart_of_accounts')); ?></a> </li>
                            </ul>
                        </li>

                        <li>
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.messaging')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('messages_index')); ?>"><?php echo e(__('messages.create_message')); ?></a> </li>
                                <li> <a href="<?php echo e(route('messages_phone_book_index')); ?>"><?php echo e(__('messages.pb')); ?></a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                      Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_grades_create' ? 'mm-active' : ''|| Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_statements_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_statements_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'salary_increaments_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'salary_increaments_create' ? 'mm-active' : '' || Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Payroll Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Grades
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_grades_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_grades_index')); ?>">List of Grades</a> </li>
                                        <li> <a href="<?php echo e(route('salary_grades_create')); ?>">New Grade</a> </li>
                                    </ul>
                                </li>

                                <li class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Salary Statements
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_statements_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_statements_index')); ?>">List of Statements</a> </li>
                                        <li> <a href="<?php echo e(route('salary_statements_create')); ?>">New Statement</a> </li>
                                    </ul>
                                </li>

                                <!-- <li class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Increaments
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'salary_increaments_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('salary_increaments_index')); ?>">List of Increaments</a> </li>
                                        <li> <a href="<?php echo e(route('salary_increaments_create')); ?>">Add Increament</a> </li>
                                    </ul>
                                </li> -->
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Leave Settings
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>">
                                    <a class="<?php echo e(Route::currentRouteName() == 'leave_category_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                        Leave Category
                                    </a>
                                    <ul class="sub-menu" aria-expanded="false">
                                        <li> <a class="<?php echo e(Route::currentRouteName() == 'leave_category_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('leave_category_index')); ?>">List of Category</a> </li>
                                        <li> <a href="<?php echo e(route('leave_category_create')); ?>">New Category</a> </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li class="<?php echo e(Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                                      Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'holiday_calender_index' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'holiday_calender_create' ? 'mm-active' : '' || 
                                         Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Holiday Calender
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li class="<?php echo e(Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>"> <a class="<?php echo e(Route::currentRouteName() == 'holiday_calender_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('holiday_calender_index')); ?>">List of Holidays</a> </li>
                                <li> <a href="<?php echo e(route('holiday_calender_create')); ?>">Add Holiday</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'shifts_index' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_create' ? 'mm-active' : '' ||
                                         Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Shift
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a class="<?php echo e(Route::currentRouteName() == 'shifts_edit' ? 'mm-active' : ''); ?>" href="<?php echo e(route('shifts_index')); ?>">List of Shift</a> </li>
                                <li> <a href="<?php echo e(route('shifts_create')); ?>">Add Shift</a> </li>
                            </ul>
                        </li>
                        
                        <li class="<?php echo e(Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : ''); ?>">
                            <a class="<?php echo e(Route::currentRouteName() == 'duty_roster_index' ? 'mm-active' : '' || Route::currentRouteName() == 'general_roster_index' ? 'mm-active' : ''); ?> has-arrow waves-effect">
                                Duty Roster
                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('duty_roster_index')); ?>">Add Roster</a> </li>
                            </ul>
                        </li>

                        <?php if(Auth()->user()->branch_id == 1): ?>
                        <li>
                            <a class="has-arrow waves-effect">
                                <?php echo e(__('messages.security_system')); ?>

                            </a>
                            <ul class="sub-menu" aria-expanded="false">
                                <li> <a href="<?php echo e(route('branch_index')); ?>"><?php echo e(__('messages.add_branch')); ?></a> </li>
                                <li> <a href="<?php echo e(route('users_index')); ?>"><?php echo e(__('messages.add_user')); ?></a> </li>
                                <li> <a href="<?php echo e(route('users_index_all')); ?>"><?php echo e(__('messages.list_user')); ?></a> </li>
                                <li> <a href="<?php echo e(route('set_access_index')); ?>"><?php echo e(__('messages.permission')); ?></a> </li>
                            </ul>
                        </li>
                        <?php endif; ?>
                        
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div><?php /**PATH /home/digishop7/public_html/32_1/resources/views/layouts/headers.blade.php ENDPATH**/ ?>