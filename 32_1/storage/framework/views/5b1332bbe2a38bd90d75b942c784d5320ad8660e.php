

<?php $__env->startSection('title', 'Area Add'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18"><?php echo e(__('messages.add_area')); ?></h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);"><?php echo e(__('messages.registers')); ?></a></li>
                                    <li class="breadcrumb-item active"><?php echo e(__('messages.add_area')); ?></li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('unsuccess')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <?php if(Session::has('errors')): ?>
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo 'Some required fields are missing..!! Please try again..'; ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

                                <form id="FormSubmit" action="<?php echo e(route('area_zone_store')); ?>" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
                                <?php echo e(csrf_field()); ?>


                                <div class="row">

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Area *</label>
                                        <select class="form-control select2" name="area_id" required="">
                                            <option value="" hidden="">--Select Area--</option>
                                            <?php $__currentLoopData = $area; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <option value="<?php echo e($item->id); ?>"><?php echo e($item->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Zone Name *</label>
                                        <input name="name" type="text" class="form-control" placeholder="Zone Name" required>
                                    </div>

                                    <div class="col-lg-4 col-md-4 col-sm-12 col-4 form-group">
                                        <label for="example-text-input" class=" col-form-label">Position Number *</label>
                                        <input name="position" type="text" class="form-control" placeholder="Position Number" required>
                                    </div>

                                </div>

                                <div class="form-group row">
                                    <div class="button-items col-md-12" style="text-align: right;">
                                        <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('messages.save')); ?></button>
                                        <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('customers_index')); ?>"><?php echo e(__('messages.close')); ?></a></button>
                                    </div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th width="5%"><?php echo e(__('messages.sl')); ?></th>
                                            <th width="40%"><?php echo e(__('messages.area')); ?></th>
                                            <th width="40%">Zone Name</th>
                                            <th width="5%">Position</th>
                                            <th width="10%">Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        <?php $__currentLoopData = $lists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td><?php echo e($loop->index+1); ?></td>
                                            <td><?php echo e($list->area_name); ?></td>
                                            <td><?php echo e($list->name); ?></td>
                                            <td><?php echo e($list->position); ?></td>
                                             <td>
                                               <!--  <a data-toggle="modal"  data-target="#exampleModalLong" data-id="<?php echo e($list->id); ?>" data-area-id="<?php echo e($list->area_id); ?>" data-area-zone="<?php echo e($list->name); ?>" class="btn btn-info areaZoneModel">Edit</a> -->

                                                <a href="<?php echo e(route('area_zone_edit',$list->id)); ?>" class="btn btn-info areaZoneModel">Edit</a>

                                            </td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
    
    
<!-- Area Edit Modal  -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Area Name Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="<?php echo e(route('area_update')); ?>" method="post">
            <?php echo csrf_field(); ?>
            <div class="form-group">
                <label>Area Name</label>
                <input type="hidden" class="form-control" name="area_id" id="area_id">
                <input type="text" class="form-control" name="area_name" id="area_name">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>

      </div>
    </div>
  </div>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<!-- <script>
    $(".areaZoneModel").click(function(){
        var zone_id   = $(this).data('id');
        var area_id   = $(this).data('area-id');
        var zone_name = $(this).data('area-zone');

        $('#zone_id').val(zone_id);
        $('#area_id').val(area_id);
        $('#zone_name').val(zone_name);
    });
</script> -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/32_1/Modules/Customers/Resources/views/area/add_area_zone.blade.php ENDPATH**/ ?>