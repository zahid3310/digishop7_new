

<?php $__env->startSection('title', 'Show'); ?>

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page  {
        size: A4;
        page-break-after: always;
    }
</style>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center"><?php echo e($user_info['proprietor_name']); ?></p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <?php echo e(QrCode::size(70)->generate("string")); ?>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> <?php echo e(userDetails()->address); ?> </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: <?php echo e(userDetails()->contact_number); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: <?php echo e(userDetails()->email); ?></p>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong><?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?> <br>
                                            <span style="font-weight: bold">SR/Customer :   </span><?php echo e($invoice['customer_name']); ?>

                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            <?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?><br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> <?php echo e($invoice->customer->address != null ? $invoice->customer->address : ''); ?>

                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> <?php echo e($invoice->customer->phone != null ? $invoice->customer->phone : ''); ?>

                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%">SL</th>
                                            <th style="font-size: 18px;width: 60%">Item Name & Code</th>
                                            <th style="font-size: 18px;width: 10%">Quantity</th>
                                            <th style="font-size: 18px;width: 15%">Rate</th>
                                            <th style="font-size: 18px;width: 10%">Taka</th>
                                        </tr>

                                        <?php if($entries->count() > 0): ?>

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                            <td style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $productCode); ?></td>
                                            <td style="text-align: center"><?php echo e($value['quantity']); ?></td>
                                            <td style="text-align: center"><?php echo e($value['rate'] . $unit); ?></td>
                                            <td style="text-align: center"><?php echo e(round($value['total_amount'], 2)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong>Total</strong></th>
                                            <th style="text-align: center"><?php echo e($net_paya != 0 ? round($net_paya - $invoice['total_discount']) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Discount</strong></th>
                                            <th style="text-align: center"><?php echo e(round($discount_on_total_amount + $invoice['total_discount'])); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Payable</strong></th>
                                            <th style="text-align: center"><?php echo e(round($invoice['invoice_amount'])); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                                            <th style="text-align: center"><?php echo e($pre_dues  != 0 ? round($pre_dues) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Total Due</strong></th>
                                            <th style="text-align: center"><?php echo e($invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                                            <th style="text-align: center"><?php echo e($paid != 0 ? round($paid, 2) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>
                                            <th style="text-align: center"><?php echo e($invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : ''); ?></th>
                                        </tr>  
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Authorization Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="<?php echo e(url('public/'.userDetails()->logo)); ?>">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="line-height: 1;font-size: 20px;font-weight: bold" class="text-center"><?php echo e($user_info['proprietor_name']); ?></p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <?php echo e(QrCode::size(70)->generate("string")); ?>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> <?php echo e(userDetails()->address); ?> </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: <?php echo e(userDetails()->contact_number); ?></p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: <?php echo e(userDetails()->email); ?></p>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Invoice No : - </strong><?php echo e('INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT)); ?> <br>
                                            <span style="font-weight: bold">SR/Customer :   </span><?php echo e($invoice['customer_name']); ?>

                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            <?php echo e(date('d-m-Y', strtotime($invoice['invoice_date']))); ?><br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> <?php echo e($invoice->customer->address != null ? $invoice->customer->address : ''); ?>

                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> <?php echo e($invoice->customer->phone != null ? $invoice->customer->phone : ''); ?>

                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="font-size: 18px;width: 5%">SL</th>
                                            <th style="font-size: 18px;width: 60%">Item Name & Code</th>
                                            <th style="font-size: 18px;width: 10%">Quantity</th>
                                            <th style="font-size: 18px;width: 15%">Rate</th>
                                            <th style="font-size: 18px;width: 10%">Taka</th>
                                        </tr>

                                        <?php if($entries->count() > 0): ?>

                                        <?php
                                            $total_amount                   = 0;
                                        ?>

                                        <?php $__currentLoopData = $entries; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $variation_name = ProductVariationName($value['product_entry_id']);

                                            if ($value['product_code'] != null)
                                            {
                                                $productCode  = ' - '.$value['product_code'];
                                            }
                                            else
                                            {
                                                $productCode  = '';
                                            }

                                            if ($value['product_name'] != null)
                                            {
                                                $category  = ' - '.$value['product_name'];
                                            }
                                            else
                                            {
                                                $category  = '';
                                            }

                                            if ($value['brand_name'] != null)
                                            {
                                                $brandName  = $value['brand_name'];
                                            }
                                            else
                                            {
                                                $brandName  = '';
                                            }

                                            if ($value['unit_name'] != null)
                                            {
                                                $unit  = ' '.$value['unit_name'];
                                            }
                                            else
                                            {
                                                $unit  = '';
                                            }

                                            if ($variation_name != null)
                                            {
                                                $variation  = ' '.$variation_name;
                                            }
                                            else
                                            {
                                                $variation  = '';
                                            }

                                            $pre_dues = $invoice['previous_due'];
                                            $net_paya = round($total_amount, 2);
                                            $paid     = round($invoice['cash_given'], 2);
                                            $dues     = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center"><?php echo e($key + 1); ?></td>
                                            <td style="padding-left: 30px"><?php echo e($value['product_entry_name'] . $productCode); ?></td>
                                            <td style="text-align: center"><?php echo e($value['quantity']); ?></td>
                                            <td style="text-align: center"><?php echo e($value['rate'] . $unit); ?></td>
                                            <td style="text-align: center"><?php echo e(round($value['total_amount'], 2)); ?></td>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>

                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="4"><strong>Total</strong></th>
                                            <th style="text-align: center"><?php echo e($net_paya != 0 ? round($net_paya - $invoice['total_discount']) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Discount</strong></th>
                                            <th style="text-align: center"><?php echo e(round($discount_on_total_amount + $invoice['total_discount'])); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4">Payable</strong></th>
                                            <th style="text-align: center"><?php echo e(round($invoice['invoice_amount'])); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Previous Due</strong></th>
                                            <th style="text-align: center"><?php echo e($pre_dues  != 0 ? round($pre_dues) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Total Due</strong></th>
                                            <th style="text-align: center"><?php echo e($invoice['invoice_amount'] + $pre_dues ? round($invoice['invoice_amount'] + $pre_dues) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Paid</strong></th>
                                            <th style="text-align: center"><?php echo e($paid != 0 ? round($paid, 2) : ''); ?></th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: right" colspan="4"><strong>Due Balance</strong></th>
                                            <th style="text-align: center"><?php echo e($invoice['invoice_amount'] + $pre_dues - $paid != 0 ? round($invoice['invoice_amount'] + $pre_dues - $paid) : ''); ?></th>
                                        </tr>  
                                    </table>
                                </div>

                                <br>
                                <br>
                                <br>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Authorization Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<script type="text/javascript">

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/93/Modules/Invoices/Resources/views/show.blade.php ENDPATH**/ ?>