@extends('layouts.app')

@section('title', 'Show')

<style>
    table,th {
        border: 1px solid black;
        border-collapse: collapse;
        padding: 2px;
    }

    td {
        border-left: 1px solid black;
        border-right: 1px solid black;
        padding: 2px;
    }

    @page {
        size: A4;
        page-break-after: always;
    }

    @media print {
      .pagebreak { 
         page-break-before: always; 
      }
    }
</style>

@section('content')
    <div class="main-content pagebreak">
        <div style="padding-bottom: 0px;margin-bottom: 0px;" class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="padding: 10px;padding-top: 25px" class="row">
                    <div style="padding-bottom: 30px" class="d-print-none col-md-12">
                        <div class="float-right">
                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold">{{ userDetails()->organization_name }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> {{ userDetails()->address }} </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 0{{ userDetails()->contact_number }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: {{ userDetails()->email }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("FARHAN MULTIMEDIA, Godagari, Rajshahi") }}
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important">

                                            <span style="font-weight: bold">Customer Code :   </span>{{ $invoice['customer_code'] }}<br>
                                            
                                            <strong>Invoice No : - </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <span style="font-weight: bold">Customer :   </span>{{ $invoice['customer_name'] }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}<br><br>
                                        </address>
                                    </div>
                                    <div style="font-size: 15px" class="col-md-12">
                                        <address style="margin-bottom: 0px !important">
                                            <strong>Address : </strong> {{ $invoice->customer->address != null ? $invoice->customer->address : ''  }}
                                        </address>

                                        <address style="margin-bottom: 0px !important">
                                            <strong>Mobile : </strong> {{ $invoice->customer->phone != null ? $invoice->customer->phone : ''  }}
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 10px;padding-bottom: 20px">
                                    <table style="width: 100%">
                                        <tr>
                                            <th style="text-align: center;font-size: 15px;width: 5%">SL</th>
                                            <th style="text-align: center;font-size: 15px;width: 20%">Product Name</th>
                                            <th style="text-align: center;font-size: 15px;width: 5%">CC</th>
                                            <th style="text-align: center;font-size: 15px;width: 15%">Chasis No</th>
                                            <th style="text-align: center;font-size: 15px;width: 15%">Engine No</th>
                                            <th style="text-align: center;font-size: 15px;width: 15%">Key No</th>
                                            <th style="text-align: center;font-size: 15px;width: 10%">Color</th>
                                            <th style="text-align: center;font-size: 15px;width: 10%">Qty</th>
                                            <th style="text-align: center;font-size: 15px;width: 10%">Rate</th>
                                            <th style="text-align: center;font-size: 15px;width: 15%">Amount</th>
                                        </tr>

                                        @if($entries->count() > 0)

                                        <?php
                                            $total_amount   = 0;
                                        ?>

                                        @foreach($entries as $key => $value)
                                        <?php
                                            $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                                            $net_paya       = round($total_amount, 2);
                                            $paid           = round($invoice['cash_given'], 2);
                                            $dues           = round($net_paya - $paid, 2);
                                        ?>

                                        <tr class="tr-height">
                                            <td style="text-align: center;font-size: 14px">{{ $key + 1 }}</td>
                                            <td style="text-align: left;font-size: 14px">{{ $value->productEntries->brand->name }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ isset($value->warrentyProducts->ccs->name) ? $value->warrentyProducts->ccs->name : '' }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ isset($value->warrentyProducts->chasis_number) ? $value->warrentyProducts->chasis_number : '' }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ isset($value->warrentyProducts->engine_number) ? $value->warrentyProducts->engine_number : '' }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ $value->key_no }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ isset($value->warrentyProducts->color->name) ? $value->warrentyProducts->color->name : '' }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ $value->quantity }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ $value->rate }}</td>
                                            <td style="text-align: center;font-size: 14px">{{ $value->quantity*$value->rate }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                        
                                        <?php
                                            if ($invoice['vat_type'] == 0)
                                            {
                                                $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $vat_amount  = $invoice['total_vat'];
                                            }

                                            if ($invoice['total_discount_type'] == 0)
                                            {
                                                $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                                            }
                                            else
                                            {
                                                $discount_on_total_amount  = $invoice['total_discount_amount'];
                                            }
                                            
                                            if ($is_installment > 0)
                                            {
                                                $service_charge = ($invoice['invoice_amount'] - $invoice['cash_given'])*($invoice['service_charge_amount']/100);
                                            }
                                            else
                                            {
                                                $service_charge = 0;
                                            }
                                        ?>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="9"><strong></strong></th>
                                            <th style="text-align: center;color: white;">.</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: left;text-align: right" colspan="9"><strong>Total</strong></th>
                                            <th style="text-align: center;">{{ $total_amount }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: center;" colspan="3">Discount%/BDT={{ $discount_on_total_amount + $invoice['total_discount'] }}</th>
                                            <th style="text-align: center;" colspan="2">Payable Amount={{ $total_amount - $discount_on_total_amount - $invoice['total_discount'] }}</th>
                                            <th style="text-align: center;" colspan="3">Shipping Cost={{ $invoice['shipping_cost'] != null ? $invoice['shipping_cost'] : 0 }}</th>
                                            <th style="text-align: center;" colspan="2">{{ $total_amount - $discount_on_total_amount - $invoice['total_discount'] + $invoice['shipping_cost'] }}</th>
                                        </tr>

                                        <tr>
                                            <th style="text-align: center;" colspan="3">Paid Amount={{ $invoice['cash_given'] }}</th>
                                            <th style="text-align: center;" colspan="2">Due={{ $invoice['invoice_amount'] - $invoice['cash_given'] }}</th>
                                            <th style="text-align: center;" colspan="3">{{$invoice['service_charge_amount']}}% Bank Charge={{ round($service_charge) }}</th>
                                            <th style="text-align: center;" colspan="2">{{ $total_amount - $discount_on_total_amount - $invoice['total_discount'] + $invoice['shipping_cost'] - $invoice['cash_given'] + round($service_charge) }}</th>
                                        </tr>
                                    </table>
                                    <p style="padding-top: 10px;text-align: justify;"><span style="font-size: 14px"><b>Guarantor Certification/Commitment:</b></span> {{ $invoice['commitment'] }}</p>
                                </div>

                                <br>

                                <div class="row">
                                    <div class="col-md-4">
                                        <h6 style="text-align: left"> </h6>
                                    </div>

                                    <div class="col-md-4">
                                        <h6 style="text-align: center"> <span style="border-top: 1px dotted black;padding: 5px">Guarantor Sign </span> </h6>
                                    </div>

                                    <div class="col-md-4">
                                        <h6 style="text-align: right"> </h6>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <h6 style="text-align: left"> <span style="border-top: 1px dotted black;padding: 5px">Customer Sign </span> </h6>
                                    </div>
                                    <div class="col-md-6">
                                        <h6 style="text-align: right"> <span style="border-top: 1px dotted black;padding: 5px">Manager Sign</span> </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="main-content pagebreak">
        <div style="padding-top: 0px;margin-top: 0px;" class="page-content">
            <div class="container-fluid">
                <div style="padding: 10px;padding-top: 0px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        <img style="width: 70px;height: 70px;padding-top: 10px;text-align: left" src="{{ url('public/'.userDetails()->logo) }}">
                                    </div>
                                    <div class="col-md-8 col-sm-8 col-lg-8 col-8">
                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 22px;padding-top: 10px;font-weight: bold">বিক্রয় চালান</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 16px"> {{ userDetails()->address }} </p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 15px">Mob: 0{{ userDetails()->contact_number }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 13px">Email: {{ userDetails()->email }}</p>
                                    </div>
                                    <div class="col-md-2 col-sm-2 col-lg-2 col-2">
                                        {{ QrCode::size(70)->generate("FARHAN MULTIMEDIA, Godagari, Rajshahi") }}
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 15px" class="col-md-8">
                                        <address style="margin-bottom: 0px !important;padding-bottom: 0px;">
                                            <strong>Challan No : </strong>{{ str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }}
                                        </address>
                                    </div>
                                    <div style="font-size: 15px;" class="col-md-4 text-sm-right">
                                        <address style="margin-bottom: 0px !important;padding-bottom: 0px;">
                                            <strong>Date :  </strong>
                                            {{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}
                                        </address>
                                    </div>

                                    <div style="font-size: 15px;" class="col-md-12 text-sm-left">
                                        <address style="margin-bottom: 0px !important;padding-bottom: 0px;">
                                            <strong>১ম পক্ষ :  </strong>
                                        </address>
                                        <address style="margin-bottom: 0px !important;padding-bottom: 0px;">
                                            <strong>২য় পক্ষ :  </strong>
                                        </address>
                                    </div>
                                </div>

                                <div style="padding-top: 0px;padding-bottom: 20px">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <table style="width: 100%">
                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Name</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ $invoice->customer->name }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Father Name</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ $invoice->customer->father_name }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Address</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ $invoice->customer->address }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Mobile</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ $invoice->customer->phone }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Model</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ $invoice->invoiceEntries[0]->productEntries->name }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Chasis No</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ isset($invoice->warrentyProducts[0]->chasis_number) ? $invoice->warrentyProducts[0]->chasis_number : '' }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Engine No</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ isset($invoice->warrentyProducts[0]->engine_number) ? $invoice->warrentyProducts[0]->engine_number : '' }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">Color</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ isset($invoice->warrentyProducts[0]->color->name) ? $invoice->warrentyProducts[0]->color->name : '' }}</td>
                                                </tr>

                                                <tr class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">CC</td>
                                                    <td style="text-align: left;font-size: 14px;width: 10%;">:-</td>
                                                    <td style="text-align: left;font-size: 14px;width: 65%;">{{ isset($invoice->warrentyProducts[0]->ccs->name) ? $invoice->warrentyProducts[0]->ccs->name : '' }}</td>
                                                </tr>
                                            </table>

                                            <br>
                                            <br>
                                            <br>
                                            <br>

                                            <table style="width: 100%">
                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">১ম জামিনদার</td>
                                                    <td style="text-align: left;font-size: 14px;width: 35%;">{{ $invoice->guarantor_1 != null ? $invoice->guarantor1->name : '' }}</td>
                                                    <td style="text-align: left;font-size: 14px;width: 40%;">{{ $invoice->guarantor_1 != null ? $invoice->guarantor1->address : '' }} {{ $invoice->guarantor_1 != null ? ' || ' . $invoice->guarantor1->phone : '' }}</td>
                                                </tr>
                                                
                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: left;font-size: 14px;width: 25%;">২য় জামিনদার</td>
                                                    <td style="text-align: left;font-size: 14px;width: 35%;">{{ $invoice->guarantor_2 != null ? $invoice->guarantor2->name : '' }}</td>
                                                    <td style="text-align: left;font-size: 14px;width: 40%;">{{ $invoice->guarantor_2 != null ? $invoice->guarantor2->address : '' }} {{ $invoice->guarantor_2 != null ? ' || ' . $invoice->guarantor2->phone : '' }}</td>
                                                </tr>
                                            </table>
                                        </div>
                                        
                                        <?php
                                            if ($is_installment > 0)
                                            {
                                                $service_charge = ($invoice['invoice_amount'] - $invoice['cash_given'])*0.07;
                                            }
                                            else
                                            {
                                                $service_charge = 0;
                                            }
                                        ?>

                                        <div class="col-md-6">
                                            <table style="width: 100%">
                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">মূল্য</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ $invoice['invoice_amount'] + $invoice['total_discount'] }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">অফার</td>                                                   
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ $invoice['total_discount'] }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">অফার বাদে মূল্য</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ $invoice['invoice_amount'] }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">নগদ জমা</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ $invoice['cash_given'] }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">পাওনা</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ $invoice['invoice_amount'] - $invoice['cash_given'] }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">৬ মাসের ব্যাংক চার্জ</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ round($service_charge) }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;">মোট পাওনা</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;">{{ ($invoice['invoice_amount'] - $invoice['cash_given']) + round($service_charge) }}</td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>

                                                <tr style="border-bottom: 1px solid black" class="tr-height">
                                                    <td style="text-align: right;font-size: 14px;width: 50%;color: white;">.</td>
                                                    <td style="text-align: left;font-size: 14px;width: 50%;"></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <h3 style="padding-top: 10px;text-align: center;font-weight: bold;color: black;"><span style="border-bottom: 1px dotted black">গ্যারান্টার /জামিনদারগনের অঙ্গীকার নামাঃ-</span></h3>
                                    <p style="text-align: justify"><span style="font-size: 14px"><b>Guarantor Certification/Commitment:</b></span>{{ $invoice['commitment'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">

</script>
@endsection
