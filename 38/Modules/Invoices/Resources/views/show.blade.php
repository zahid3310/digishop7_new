<!DOCTYPE html >
<html>

<head>
    <meta charset="utf-8">
    <title>Cash Memo</title>
    <link href="https://fonts.maateen.me/bangla/font.css" rel="stylesheet">

    <style>
        *{margin:0;padding:0;outline:0}

        body {
            font-size:14px;
            line-height:18px;
            color:#000;
            height:292mm;
            width: 203mm;/*297*/
            margin:0 auto;
            font-family: 'Bangla', Arial, sans-serif !important;  
        }

        table,th {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 10px;
        }

        td {
            border-left: 1px solid black;
            border-right: 1px solid black;
            padding: 10px;
        }

        .bg_templete{
            background:url(images/bg3.png) no-repeat;
            background-size: 100%;
            -webkit-print-color-adjust: exact;
        }

        .templete{
            width:90%;
            background:none;
            margin:0 auto;
        }
        
        .clear{
            overflow:hidden;
        }

        .text-center{
            text-align: center;
        }

        .text-end{
            text-align: end;
        }

        .item2 {
            grid-area: com_logo;
        }

        .item3 {
            grid-area: com_info;
        }

        .item4 {
            grid-area: other_info;
        }

        .grid-container {
            display: grid;
            grid-template-areas: "com_logo com_info other_info ";
            /*padding-top: 10px;*/
        }

        .grid-container div {
            text-align: center;
        }

        .grid-container-aline-end {
            text-align: end !important;
        }

        .grid-container-aline-start {
            text-align: start !important;
        }

        .com_logo_img {
            height: 80px;
            width: 80px;
        }

        .tr-height{
            padding: 10px;
        }

        .item11 { grid-area: sig_text2; }
        .item22 { grid-area: some_info; }
        .item33 { grid-area: sig1; }
        .item44 { grid-area: sig2; }
        .item55 { grid-area: sig_text1; }

        .grid-container-1 {
            display: grid;
            grid-template-areas:
            ' sig1 sig1 some_info some_info sig2 sig2'
            ' sig_text1 sig_text1 some_info some_info sig_text2 sig_text2';
        }

        .signaturesection1 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        .signaturesection2 {
            border-top: 1px solid #000;
            font-weight: bold;
            margin-top: 45px;
        }

        @page {
            size: A4;
            page-break-after: always;
        }
    </style>
</head>

<body>
    <div class="bg_templete clear">

        <div class="templete clear">

            <br>
            <br>
            <p style="line-height: 1;text-align: center"><span style="padding: 5px;font-size: 20px">বিসমিল্লাহির রাহমানির রাহিম</span></p>
            <div class="grid-container">
                <div class="item2 grid-container-aline-start">
                    <!-- <img class="com_logo_img" src="{{ url('public/'.userDetails()->logo) }}"alt="qr sample"
                    /> -->
                </div>

                <div class="item3">
                    <h2 style="line-height: 1;font-size: 60px">সেভ বাজার</h2>
                    <h4 style="line-height: 1;font-size: 20px;font-weight: normal">রাধানগর, পি.আই রোড, পাবনা । (আলিয়া মাদ্রাসার পশ্চিম পার্শ্বে)</h4>
                    <h3 style="line-height: 1;font-size: 26px; text-align: center;">আপনার নিত্য প্রয়োজনীয় গ্রোসারী পণ্য <br> পাচ্ছেন খুব দ্রুত সময়ে হোম ডেলিভারিতে</h3>
                    
                </div>

                <div class="item4 grid-container-aline-end">

                    <!-- <p style="padding-right: 25px;line-height: 1;font-size: 28px">ডিলার:</p>
                    <p style="line-height: 1;font-size: 15px;">মীর সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">শেলটেক সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">ফু ওয়াং সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 15px;">সাউথইষ্ট সিরামিক লিঃ</p>
                    <p style="line-height: 1;font-size: 11px;">বাংলাদেশ হার্ডল্যান্ড সিরামিক </p> -->
                    
                </div>
            </div>

            
            <p style="line-height: 1;font-size: 20px; text-align: center">এখানে চাউল, ডাউল, তেল, আটা-ময়দা, সুজি, লবণ, চিনি, সকল প্রকার মসলা, মাছ, মাংস, <br> দুধ, ডিম, সকল প্রকার কাঁচা বাজার ও কসমেটিক্স ইত্যাদি পাচ্ছেন বাজার থেকে কম দামে । </p>

            <div style="padding-bottom: 10px" class="text-center">
                <p style="line-height: 2;font-size: 20px;"><span style="border: 1px solid gray;border-radius: 23px;padding: 5px">Hotline: 01712-800607</span></p>
            </div>

            <hr>
            
            <div style="font-size: 18px;padding-top: 5px;line-height: 1.5">
                <strong>Memo No &nbsp;:  </strong>{{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <strong style="float: right;">Date : <span>{{ date('d-m-Y', strtotime($invoice['invoice_date'])) }}</span></strong>
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>{{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name'] }}
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Address &nbsp;&nbsp;: </strong>{{ $invoice['customer_address'] != null ? $invoice['customer_address'] : $invoice['contact_address'] }}
            </div>

            <div style="font-size: 18px;line-height: 1.5">
                <strong>Phone &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>{{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice['contact_phone'] }}
            </div>

            <div style="padding-top: 10px;padding-bottom: 20px">
                <table style="width: 100%">
                    <tr>
                        <th style="font-size: 17px;width: 5%">নং</th>
                        <th style="font-size: 17px;width: 40%">মালের বিবরণ</th>
                        <th style="font-size: 17px;width: 15%">পরিমান</th>
                        <th style="font-size: 17px;width: 10%">MRP</th>
                        <th style="font-size: 17px;width: 15%">Save TK</th>
                        <th style="font-size: 15px;width: 15%">সেভ বাজার মূল্য</th>
                    </tr>

                    @if($entries->count() > 0)

                    <?php
                        $total_save     = 0;
                        $total_amount   = 0;
                    ?>

                    @foreach($entries as $key => $value)
                    <?php
                        $total_amount   = $total_amount + ($value['quantity']*$value['rate']);
                        $total_save     = $total_save + (($value['quantity']*$value['mrp_price']) - $value['total_amount']);
                        $variation_name = ProductVariationName($value['product_entry_id']);

                        if ($value['unit_name'] != null)
                        {
                            $unit  = ' '.$value['unit_name'];
                        }
                        else
                        {
                            $unit  = ' ';
                        }

                        if ($variation_name != null)
                        {
                            $variation  = ' '.$variation_name;
                        }
                        else
                        {
                            $variation  = '';
                        }

                        $net_paya = round($total_amount, 2);
                        $paid     = round($invoice['invoice_amount'] - $invoice['due_amount'], 2);
                        $dues     = round($net_paya - $paid, 2);
                    ?>

                    <tr class="tr-height">
                        <td style="text-align: center">{{ $key + 1 }}</td>
                        <td style="padding-left: 30px">{{ $value['product_entry_name'] . $variation }}</td>
                        <td style="text-align: center">{{ $value['quantity'] . $unit }}</td>
                        <td style="text-align: center">{{ $value['mrp_price'] }}</td>
                        <td style="text-align: center">{{ round(($value['mrp_price']*$value['quantity']) - $value['total_amount'], 2) }}</td>
                        <td style="text-align: center">{{ round($value['total_amount'], 2) }}</td>
                    </tr>
                    @endforeach
                    @endif

                    <?php
                        if ($invoice['vat_type'] == 0)
                        {
                            $vat_amount  = ($invoice['total_vat']*($net_paya - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $vat_amount  = $invoice['total_vat'];
                        }

                        if ($invoice['total_discount_type'] == 0)
                        {
                            $discount_on_total_amount  = ($invoice['total_discount_amount']*($net_paya + $vat_amount - $invoice['total_discount']))/100;
                        }
                        else
                        {
                            $discount_on_total_amount  = $invoice['total_discount_amount'];
                        }
                    ?>

                    <tr>
                        <th style="text-align: left" colspan="2">In Words : <span style="font-weight: normal;">{{ $value['contact_type'] != 4 ? numberTowords(round($net_paya - $invoice['total_discount'])) : '' }}</span></th>
                        <th></th>
                        <th style="text-align: right"><strong>Total</strong></th>
                        <th style="text-align: center">{{ round($total_save, 2) }}</th>
                        <th style="text-align: center">{{ $net_paya != 0 ? round($net_paya - $invoice['total_discount'], 2) : '' }}</th>
                    </tr>
                </table>
            </div>

            <div class="grid-container-1">

                <div class="item33"></div>  
                <div class="item55"><p class="signaturesection1" style="font-size: 18px;text-align: center">ক্রেতার স্বাক্ষর</p></div>

                <div class="item22" style="text-align: center;">
                    <h3 style="font-size: 18px">আপনার কোন মেডিসিন প্রয়োজন ? মোবাইল করলেই পাচ্ছেন দ্রুত হোম ডেলিভারি : ০১৭৬০-৪৫৯৬৫৫</h3>
                    <div>
                        <p style="font-size: 18px">হোম ডেলিভারি ফ্রি : (মিনিমাম অর্ডার ৫০০ টাকা)</p>
                        <p style="font-size: 18px">অনলাইন এ অর্ডার করুন : www.savebazar24.com </p>
                    </div>
                </div>

                <div class="item44"></div>
                <div class="item1"><p class="signaturesection2" style="font-size: 18px;text-align: center">বিক্রেতার স্বাক্ষর</p></div>
            </div>
        </div>
    </div>
</body>

</html>