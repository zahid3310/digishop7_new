@extends('layouts.app')

@section('title', 'Salary Sheet')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Salary Sheet</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Salary Sheet</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div style="margin-left: 150px;margin-right: 150px" class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <form method="get" action="{{ route('monthly_alary_report_print') }}" enctype="multipart/form-data" target="_blank">
	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"> Month </label>
	                                    <div class="col-md-9">
	                                    	<div class="row">
	                                    		<div class="input-group-append col-md-4">
		                                    		<select style="cursor: pointer" id="month" class="form-control select2" name="month">
                                                    <option value="1" {{ date('m') == '01' ? 'selected' : '' }}>January</option>
                                                    <option value="2" {{ date('m') == '02' ? 'selected' : '' }}>February</option>
                                                    <option value="3" {{ date('m') == '03' ? 'selected' : '' }}>March</option>
                                                    <option value="4" {{ date('m') == '04' ? 'selected' : '' }}>Appril</option>
                                                    <option value="5" {{ date('m') == '05' ? 'selected' : '' }}>May</option>
                                                    <option value="6" {{ date('m') == '06' ? 'selected' : '' }}>Jun</option>
                                                    <option value="7" {{ date('m') == '07' ? 'selected' : '' }}>July</option>
                                                    <option value="8" {{ date('m') == '08' ? 'selected' : '' }}>August</option>
                                                    <option value="9" {{ date('m') == '09' ? 'selected' : '' }}>September</option>
                                                    <option value="10" {{ date('m') == '10' ? 'selected' : '' }}>October</option>
                                                    <option value="11" {{ date('m') == '11' ? 'selected' : '' }}>November</option>
                                                    <option value="12" {{ date('m') == '12' ? 'selected' : '' }}>December</option>
                                                </select>
		                                    	</div>

		                                    	<div class="input-group-append col-md-8">
		                                    		<label style="text-align: right" for="productname" class="col-md-2 col-form-label"> Year </label>
		                                    		<select style="cursor: pointer" id="year" class="form-control select2" name="year">
                                                    <option value="2018" {{ date('Y') == '2018' ? 'selected' : '' }}>2018</option>
                                                    <option value="2019" {{ date('Y') == '2019' ? 'selected' : '' }}>2019</option>
                                                    <option value="2021" {{ date('Y') == '2021' ? 'selected' : '' }}>2021</option>
                                                    <option value="2022" {{ date('Y') == '2022' ? 'selected' : '' }}>2022</option>
                                                    <option value="2023" {{ date('Y') == '2023' ? 'selected' : '' }}>2023</option>
                                                    <option value="2024" {{ date('Y') == '2024' ? 'selected' : '' }}>2024</option>
                                                    <option value="2025" {{ date('Y') == '2025' ? 'selected' : '' }}>2025</option>
                                                </select>
		                                    	</div>
	                                    	</div>
	                                    </div>
	                                </div>

	                                <div class="form-group row">
	                                    <label style="text-align: right" for="productname" class="col-md-3 col-form-label"></label>
	                                    <div class="col-md-9">
	                                        <button style="border-radius: 0px;background-color: #297049;color: white" class="btn" type="submit" target="_blank">
	                                        	Print
	                                    	</button>
	                                    </div>
	                                </div>
                            	</form>
                                
                            </div>

                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }
    </script>
@endsection