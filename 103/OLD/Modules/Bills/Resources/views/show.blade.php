@extends('layouts.app')

@section('title', 'Show')
<style type="text/css">
    @media print {
        /*.card {
            width: 145mm;
            padding-right: 0px;
            padding-left: 15px;
            margin-top: 20px;
            color: black;
        }*/
    }
    
    .table td, .table th {
        padding: 0px !important;
    }
    
    .pb-2, .py-2 {
        padding-bottom: 0px !important;
    }
    
    .pt-2, .py-2 {
        padding-top: 0px !important;
    }
    .mt-3, .my-3 {
        margin-top: 0px !important;
    }
</style>
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Bills</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Bills</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-12 col-xs-12 col-sm-12">

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px">Order/Memo</h2>

                                        <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 20px;font-weight:bold;">{{ isset($user_info['organization_name']) ? $user_info['organization_name'] : $user_info['name'] }}</h2>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                        <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ isset($user_info['contact_number']) ? $user_info['contact_number'] : $user_info['phone'] }}</p>
                                    </div>
                                    <div class="col-md-12 col-xs-12 col-sm-12"></div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div style="font-size: 16px" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Payable To:</strong><br>
                                            {{ $bill->vendor->name }}
                                            @if($bill->vendor->address != null)
                                               <br> <?php echo $bill->vendor->address; ?> <br>
                                            @endif
                                            @if($bill->vendor->address == null)
                                                <br>
                                            @endif
                                            {{ $bill->vendor->phone }}
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;text-align: right;" class="col-sm-6 col-6">
                                        <address style="margin-bottom: 0px;padding-bottom: 0px;margin-top: 0px;padding-top: 0px">
                                            <strong>Bill Date:</strong>
                                            {{ date('d-m-Y', strtotime($bill['bill_date'])) }}<br>
                                        </address>
                                    </div>
                                </div>

                                <div style="margin-top: 0px !important" class="py-2 mt-3">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold">Bill Summary</h3>
                                        </div>
                                        <div class="col-md-6">
                                            <h3 style="font-size: 16px" class="font-weight-bold float-right">Bill# {{ 'BILL - ' . str_pad($bill['bill_number'], 6, "0", STR_PAD_LEFT) }}</h3>
                                        </div>
                                    </div>
                                </div>

                                <table class="table">
                                    <thead style="font-size: 18px">
                                        <tr>
                                            <th style="text-align:left;width: 5%">No.</th>
                                            <th style="text-align:left;width: 40%">Item</th>
                                            <th style="text-align:left;width: 15%">Rate</th>
                                            <th style="text-align:left;width: 15%">Qty</th>
                                            <th style="text-align:left;width: 15%">Discount</th>
                                            <th style="text-align:right;width: 10%">Price</th>
                                        </tr>
                                    </thead>
                                    <tbody style="font-size: 16px">

                                        @if(!empty($entries) && ($entries->count() > 0))

                                        <?php $sub_total = 0; ?>

                                        @foreach($entries as $key => $value)
                                        <tr>
                                            <td class="text-left">{{ $key + 1 }}</td>
                                            <td style="text-align:left;">
                                                <?php echo $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name; ?>
                                            </td>
                                            <td class="text-left">{{ $value['rate'] }}</td>
                                            <td class="text-left">{{ $value['quantity'] . ' ' .$value->convertedUnit->name }}</td>
                                            <td style="text-align:left;">
                                                <?php $total_dis = $value['discount_type'] == 0 ? (($value['rate']*$value['quantity']*$value['discount_amount'])/100) : $value['discount_amount']; ?>
                                                @if($value['discount_type'] == 0)
                                                    <?php echo number_format($total_dis,2,'.',','); ?>
                                                    <?php echo '('.$value['discount_amount'].'%'.')'; ?>
                                                @else
                                                    <?php echo number_format($value['discount_amount'],2,'.',','); ?>
                                                @endif
                                            </td>
                                            <td class="text-right">{{ $value['total_amount'] }}</td>
                                        </tr>

                                        <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                        @endforeach
                                        @endif
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Bill Total</td>
                                            <td style="font-size: 16px" class="text-right">{{ number_format($bill['bill_amount'],2,'.',',') }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <td style="font-size: 16px" colspan="5" class="text-right">Previous Due</td>
                                            <td style="font-size: 16px" class="text-right">{{ number_format($bill['previous_due'],2,'.',',') }}</td>
                                        </tr>

                                        <tr style="display:none;">
                                            <td style="font-size: 16px" colspan="5" class="text-right">VAT ({{ $bill['vat_type'] == 0 ? $bill['total_vat'].'%' : 'BDT' }})</td>
                                            <td style="font-size: 16px">{{ $bill['vat_type'] == 0 ? (($sub_total*$bill['total_vat'])/100) : number_format($bill['total_vat'],2,'.',',') }}</td>
                                        </tr>

                                        <!-- <tr style="line-height: 0px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">
                                                Discount ({{ $bill['total_discount_type'] == 0 ? $bill['total_discount_amount'].'%' : 'BDT' }})</td>
                                            <td style="font-size: 16px" class="border-0 text-right">{{ $bill['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$bill['total_vat'])/100))*$bill['total_discount_amount'])/100) : number_format($bill['total_discount_amount'],2,'.',',') }}</td>
                                        </tr> -->

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px;font-weight: bold" colspan="5" class="border-0 text-right">Net Total</td>
                                            <td style="font-size: 16px;font-weight: bold" class="border-0 text-right">
                                                {{ number_format($bill['bill_amount']+ $bill['previous_due'],2,'.',',') }}
                                            </td>
                                        </tr>

                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Paid</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                {{ number_format($bill['cash_given'],2,'.',',') }}
                                            </td>
                                        </tr>
                                        
                                        <tr style="line-height: 16px">
                                            <td style="font-size: 16px" colspan="5" class="border-0 text-right">Net Dues</td>
                                            <td style="font-size: 16px" class="border-0 text-right">
                                                {{ number_format($bill['bill_amount'] + $bill['previous_due'] - $bill['cash_given'],2,'.',',') }}
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            
                                @if($bill['bill_note'] != null)
                                <br>
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h6 style="font-size: 16px"><strong>Note :</strong> {{ $bill['bill_note'] }}</h6>
                                    </div>
                                </div>
                                @endif
                                
                                <br>

                                <div class="row">
                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;">Owner Signature</p>
                                    </div>

                                    <div class="col-6">
                                    <p style="text-decoration: overline dotted;text-align: right;">Customer Signature</p>
                                    </div>

                                </div>

                                <div class="d-print-none">
                                    <div class="float-right">
                                        <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
@endsection