

<?php $__env->startSection('title', 'Customer Due List'); ?>

<?php $__env->startSection('styles'); ?>
    <style type="text/css">
        @media  print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page  {
                margin: 0cm ! important;
            }
        }
    </style>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Customer Due List</h4>
                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Customer Due List</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['organization_name']); ?></h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['address']); ?></p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><?php echo e($user_info['contact_number']); ?></p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Customer Due List</h4>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="<?php echo e(route('due_list_report_customer_index')); ?>" enctype="multipart/form-data">
                                    <div class="form-group row">

                                        <div class="col-lg-4 col-md-4 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="0" selected>--All Customers--</option>
                                                    <?php if(!empty($customers) && ($customers->count() > 0)): ?>
                                                    <?php $__currentLoopData = $customers; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <option <?php echo e(isset($_GET['customer_id']) && ($_GET['customer_id'] == $value['id']) ? 'selected' : ''); ?> value="<?php echo e($value['id']); ?>"><?php echo e($value['name']); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>

                                 

                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select style="width: 100" class="form-control select2" name="sr_id">
                                                    <option value="0" selected>--All Sr--</option>
                                                    <?php $__currentLoopData = $srList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($list->id); ?>" <?php echo e($list->id == $sr_id ? 'selected' : ''); ?>><?php echo e($list->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>



                                        <div class="col-lg-3 col-md-3 col-sm-8 col-6 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="area" style="width: 100" class="form-control select2" name="area_id">
                                                    <option value="0" selected>--All Area--</option>
                                                    <?php $__currentLoopData = $areaList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $list): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($list->id); ?>" <?php echo e($list->id == $area_id ? 'selected' : ''); ?>><?php echo e($list->name); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-success inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center">SL</th>
                                            <th style="text-align: center">Customer</th>
                                            <th style="text-align: center">Address</th>
                                            <th style="text-align: center">Phone</th>
                                            <th style="text-align: center">Balance</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php
                                            $i              = 1;
                                            $total_balance  = 0;
                                        ?>
                                        <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <tr>
                                            <td style="text-align: center;"><?php echo e($i); ?></td>
                                            <td style="text-align: left;"><?php echo e($value['customer_name']); ?> </td>
                                            <td style="text-align: left;"><?php echo e($value['address']); ?></td>
                                            <td style="text-align: center;"><?php echo e($value['phone']); ?></td>
                                            <td style="text-align: right;"><?php echo e(number_format($value['balance'],0,'.',',')); ?></td>
                                        </tr>
                                         
                                        <?php $i++; $total_balance  = $total_balance + $value['balance']; ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                        <tr>
                                            <th colspan="4" style="text-align: right;">Total</th>
                                            <th colspan="1" style="text-align: right;"><?php echo e(number_format($total_balance,0,'.',',')); ?></th>
                                        </tr>

                                    </tbody>
                                    
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/85/Modules/Reports/Resources/views/due_list_report_customer.blade.php ENDPATH**/ ?>