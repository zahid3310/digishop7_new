<!DOCTYPE html>
<html>

<head>
    <title>Salary Sheet</title>
    <link rel="icon" href="i<?php echo e(url('/public/favicon.png')); ?>" type="image/x-icon" />
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/bootstrap.min.css')); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/style.css')); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo e(url('public/admin_panel_assets/report_assets/css/custom.css')); ?>">

    <style type="text/css" media="print">        
        @page  {
            size: auto;   
            margin: 15mm 5mm 5mm 10mm;
        }

        #print-footer {
            display: none;
            position: fixed;
            bottom: 0;
            left:0;
        }

        #hide-print {
            display: none;
        }

        #print-footer {
            display: none;
        }

        body {
            margin-bottom:10mm;
        }

        .table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
            background-color: #ddd;
        }
    </style>   
</head>

<body id="print-container-body" class="printwindow">

<div style="display: none;">
    <button id="btnExport">Export to excel</button>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="" style="">

                    <div style="text-align: center" class="ibox-title" style="padding: 14px 15px 0px;">
                        <?php
                            if ($_GET['month'] == 1)
                            {
                                $month  = 'January';
                            }
                            elseif ($_GET['month'] == 1)
                            {
                                $month  = 'February';
                            }
                            elseif ($_GET['month'] == 3)
                            {
                                $month  = 'March';
                            }
                            elseif ($_GET['month'] == 4)
                            {
                                $month  = 'April';
                            }
                            elseif ($_GET['month'] == 5)
                            {
                                $month  = 'May';
                            }
                            elseif ($_GET['month'] == 6)
                            {
                                $month  = 'Jun';
                            }
                            elseif ($_GET['month'] == 7)
                            {
                                $month  = 'July';
                            }
                            elseif ($_GET['month'] == 8)
                            {
                                $month  = 'August';
                            }
                            elseif ($_GET['month'] == 9)
                            {
                                $month  = 'September';
                            }
                            elseif ($_GET['month'] == 10)
                            {
                                $month  = 'October';
                            }
                            elseif ($_GET['month'] == 11)
                            {
                                $month  = 'November';
                            }
                            elseif ($_GET['month'] == 12)
                            {
                                $month  = 'December';
                            }
                        ?>
                        <p style="margin-bottom: 0px;font-size: 15px"><strong><?php echo e($user_info['organization_name']); ?></strong></p>
                        <p style="margin-bottom: 0px;font-size: 12px"><?php echo e($user_info['address']); ?></p>
                        <p style="margin-bottom: 0px;font-size: 12px"><?php echo e($month . ', ' . $_GET['year']); ?></p>
                    </div>

                    <div class="ibox-content">

                        <table id="tblExport" class="table table-striped table-bordered table-hover dataTables-example" >
                            <thead class="theight">
                                <tr style="background:#ddd;">
                                    <th style="text-align: center;width: 3%">SL</th>
                                    <th style="text-align: center;width: 15%">NAME</th>
                                    <th style="text-align: center;width: 10%">DESIGNATION</th>
                                    <th style="text-align: center;width: 10%">TOTAL ATTENDANCE</th>
                                    <th style="text-align: center;width: 10%">MONTHLY SALARY</th>
                                    <th style="text-align: center;width: 10%">PAYABLE</th>
                                    <th style="text-align: center;width: 10%">ADVANCE</th>
                                    <th style="text-align: center;width: 10%">NET PAYABLE</th>
                                    <th style="text-align: center;width: 10%">SIGNATURE</th>

                                </tr>
                            </thead>

                            <tbody class="theight">
                                <?php 
                                    $i = 1;
                                ?>

                                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key1 => $value1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                <tr>
                                    <td style="text-align: center;" colspan="9"><strong><?php echo e($key1); ?></strong></td>
                                </tr>

                                <?php 
                                    $total_advance      = 0;
                                    $totalNetPayable    = 0;
                                ?>

                                <?php $__currentLoopData = $value1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td style="text-align: center;"><?php echo e($i); ?></td>
                                    <td style="text-align: left;"><?php echo e($value['employee_name']); ?></td>
                                    <td style="text-align: left;"><?php echo e($value['designation']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['total_attendance']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['gross']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['payable']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['advance']); ?></td>
                                    <td style="text-align: right;"><?php echo e($value['net_payable']); ?></td>
                                    <td style="text-align: right;"></td>
                                </tr>
                                <?php
                                    $i++; 
                                    $total_advance     = $total_advance + $value['advance']; 
                                    $totalNetPayable   = $totalNetPayable + $value['net_payable']; 
                                    ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                <tr>
                                    <td style="text-align: right" colspan="6"><strong>TOTAL</strong></td>
                                    <td style="text-align: right"><strong><?php echo e(isset($total_advance) ? $total_advance : 0); ?></strong></td>
                                    <td style="text-align: right"><strong><?php echo e(isset($totalNetPayable) ? $totalNetPayable : 0); ?></strong></td>
                                    <td style="text-align: right"></td>
                                </tr>
                            </tbody>
                        </table>

                        <div>
                            <table class="table table-striped table-hover dataTables-example">
                                <thead>
                                    <tr>
                                        <td style="font-size:8px;"><strong>&copy;<?php echo e(date('Y')); ?> Cyberdyne Technology Ltd. | Contact : 01715317133 | Cyberdyne Technology Ltd.</strong></td>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.btechco.excelexport.js')); ?>"></script>
<script src="<?php echo e(url('public/admin_panel_assets/report_assets/js/jquery.base64.js')); ?>"></script>

</body>
</html><?php /**PATH /home/digishop7/public_html/85/Modules/Reports/Resources/views/monthly_salary_report_print.blade.php ENDPATH**/ ?>