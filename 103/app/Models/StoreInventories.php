<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class StoreInventories extends Model
{  
    protected $table = "store_inventories";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Stores','store_id');
    }

}
