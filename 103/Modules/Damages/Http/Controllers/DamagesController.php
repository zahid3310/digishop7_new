<?php

namespace Modules\Damages\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Damages;
use App\Models\DamageEntries;
use App\Models\DealerDamages;
use App\Models\DealerDamageEntries;
use App\Models\UnitConversions;
use App\Models\SrItems;
use App\Models\Issues;
use App\Models\IssueOrders;
use App\Models\Stores;
use App\Models\StoreInventories;
use Carbon\Carbon;
use Response;
use DB;
use View;

class DamagesController extends Controller
{
    public function index()
    {
        $damages = Damages::orderBy('id', 'DESC')->get();

        return view('damages::index', compact('damages'));
    }

    public function create()
    {
        return view('damages::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'damage_date'           => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $damage                  = new Damages;
            $damage->damage_date     = date('Y-m-d', strtotime($data['damage_date']));
            $damage->type            = 1;
            $damage->total_buy_price = $buy_price;
            $damage->total_amount    = $data['sub_total_amount'];
            $damage->damage_note     = $data['note'];
            $damage->created_by      = $user_id;

            if ($damage->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $damage_entries[] = [
                        'damage_id'          => $damage['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'store_id'           => $data['store_id'][$key],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['rate'][$key]*$data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('damage_entries')->insert($damage_entries);

                //Here Item Id is used for update purpose
                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                        ->where('product_entry_id', $value4)
                                                                        ->first();

                    $product_entries                = ProductEntries::find($value4);
                    $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
                    $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
                    // $product_entries->buy_price     = $data['rate'][$key4];


                    if ($product_entries->save()) {
                        $store           = StoreInventories::where('store_id', $data['store_id'][$key4])
                                                                        ->where('product_entry_id', $value4)
                                                                        ->first();

                        $store->store_stock_in_hand     = $store['store_stock_in_hand'] - $converted_quantity;
                        $store->save();
                    }
                }

                DB::commit();

                return back()->with("success","Damage Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        return view('damages::show');
    }

    public function edit($id)
    {
        $find_damage            = Damages::select('damages.*')->find($id);

        $find_damage_entries    = DamageEntries::where('damage_entries.damage_id', $id)->get();

        $entries_count          = $find_damage_entries->count();

        return view('damages::edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'damage_date'           => 'required|date',
            'note'                  => 'nullable|string',
            'product_entries.*'     => 'required|integer',
            'quantity.*'            => 'required|numeric',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();


      
        DB::beginTransaction();

        try{
            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $damage                  = Damages::find($id);
            $damage->damage_date     = date('Y-m-d', strtotime($data['damage_date']));
            $damage->type            = 1;
            $damage->total_buy_price = $buy_price;
            $damage->total_amount    = $data['sub_total_amount'];
            $damage->damage_note     = $data['note'];
            $damage->created_by      = $user_id;

            if ($damage->save())
            {   
                $item_id     = DamageEntries::where('damage_id', $damage['id'])->get();
                $item_delete = DamageEntries::where('damage_id', $damage['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $damage_entries[] = [
                        'damage_id'          => $damage['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'store_id'           => $data['store_id'][$key],
                        'buy_price'          => $product_buy_price['buy_price'] != null ? $product_buy_price['buy_price'] : 0,
                        'rate'               => $data['rate'][$key],
                        'quantity'           => $data['quantity'][$key],
                        'total_amount'       => $data['rate'][$key]*$data['quantity'][$key],
                        'updated_by'         => $user_id,
                        'updated_at'         => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('damage_entries')->insert($damage_entries);

                //Here Item Id is used for update purpose
                if ($item_id != null)
                {
                    foreach ($item_id as $key => $value)
                    {
                        $old_item_entry_id[]           = $value['product_entry_id'];
                        $old_items_main_unit_id[]      = $value['main_unit_id'];
                        $old_items_converted_unit_id[] = $value['conversion_unit_id'];
                        $old_items_stock[]             = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    {
                        $conversion_rate_find       = UnitConversions::where('main_unit_id', $old_items_main_unit_id[$key2])
                                                    ->where('converted_unit_id', $old_items_converted_unit_id[$key2])
                                                    ->where('product_entry_id', $value2)
                                                    ->first();

                        $quantity_add_to_product_entry                  = ProductEntries::find($value2);
                        $old_converted_quantity                         = $conversion_rate_find != null ? $old_items_stock[$key2]/$conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                        $quantity_add_to_product_entry->stock_in_hand   = $quantity_add_to_product_entry['stock_in_hand'] + $old_converted_quantity;
                        $quantity_add_to_product_entry->save();
                    }
                }

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                                                        ->where('product_entry_id', $value4)
                                                                        ->first();

                    $product_entries                = ProductEntries::find($value4);
                    $converted_quantity             = $conversion_rate_find != null ? $data['quantity'][$key4]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key4];
                    $product_entries->stock_in_hand = $product_entries['stock_in_hand'] - $converted_quantity;
                    
                    if ($product_entries->save()) {
                        $store           = StoreInventories::where('store_id', $data['store_id'][$key4])
                                                                        ->where('product_entry_id', $value4)
                                                                        ->first();



                        $store->store_stock_in_hand     = $store['store_stock_in_hand'] - $converted_quantity;
                        $store->save();
                    }

                }

                DB::commit();

                return redirect()->route('damages_index')->with("success","Damage Updated Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            dd($exception);
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    //Receive From Customer/SR Start
        public function ReceiveFromCustomerIndex()
        {
            $dealer_damages = DealerDamages::where('dealer_damages.type', 0)
                                ->orderBy('dealer_damages.id', 'DESC')
                                ->get();

            return view('damages::receive_from_customer.index', compact('dealer_damages'));
        }

        public function ReceiveFromCustomerCreate()
        {
            return view('damages::receive_from_customer.create');
        }

        public function ReceiveFromCustomerStore(Request $request)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
                'customer_id'           => 'required|integer',
                'issue_number'          => 'required|integer',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $data_find              = DealerDamages::orderBy('created_at', 'DESC')->first();
                $number                 = $data_find != null ? $data_find['number'] + 1 : 1;

                $damage                 = new DealerDamages;
                $damage->date           = date('Y-m-d', strtotime($data['damage_date']));
                $damage->number         = $number;
                $damage->customer_id    = $data['customer_id'];
                $damage->issue_id       = $data['issue_number'];
                // $damage->order_id       = $data['order_number'];
                $damage->type           = 0;
                $damage->total_amount   = $data['sub_total_amount'];
                $damage->note           = $data['note'];
                $damage->created_by     = $user_id;

                if ($damage->save())
                {
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'store_id'           => $data['store_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];

                        $sr_items[] = [
                            'issue_id'              => $data['issue_number'],
                            'sr_id'                 => $data['customer_id'],
                            'date'                  => date('Y-m-d', strtotime($data['damage_date'])),
                            'product_entry_id'      => $value,
                            'damage_id'             => $damage->id,
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['unit_id'][$key],
                            'store_id'              => $data['store_id'][$key],
                            'type'                  => 4,
                            'quantity'              => $data['quantity'][$key],
                            'rate'                  => $data['rate'][$key],
                            'amount'                => $data['amount'][$key],
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);
                    DB::table('sr_items')->insert($sr_items);

                    DB::commit();

                    return back()->with("success","Damage Added Successfully !!");
                }
            }catch (\Exception $exception){
                dd($exception);
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }

        public function ReceiveFromCustomerEdit($id)
        {
            $find_damage            = DealerDamages::select('dealer_damages.*')->find($id);

            $find_damage_entries    = DealerDamageEntries::leftjoin('product_entries', 'product_entries.id', 'dealer_damage_entries.product_entry_id')
                                                ->where('dealer_damage_entries.dealer_damage_id', $id)
                                                ->select('dealer_damage_entries.*',
                                                        'product_entries.id as item_id',
                                                        'product_entries.product_code as product_code',
                                                        'product_entries.stock_in_hand as stock_in_hand',
                                                        'product_entries.name as item_name')
                                                ->get();

            $entries_count          = $find_damage_entries->count();

            return view('damages::receive_from_customer.edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
        }

                public function ReceiveFromCustomerUpdate(Request $request, $id)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
                'issue_number'          => 'required|integer',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $damage                 = DealerDamages::find($id);
                $damage->date           = date('Y-m-d', strtotime($data['damage_date']));
                $damage->customer_id    = $data['customer_id'];
                $damage->issue_id       = $data['issue_number'];
                $damage->type           = 0;
                $damage->total_amount   = $data['sub_total_amount'];
                $damage->note           = $data['note'];
                $damage->updated_by     = $user_id;

                if ($damage->save())
                {   
                    $item_delete    = DealerDamageEntries::where('dealer_damage_id', $damage['id'])->delete();
                    $sr_item_delete = SrItems::where('damage_id', $damage['id'])->where('sr_id', $data['customer_id'])->delete();
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'store_id'           => $data['store_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];

                        $sr_items[] = [
                            'issue_id'              => $data['issue_number'],
                            'sr_id'                 => $data['customer_id'],
                            'date'                  => date('Y-m-d', strtotime($data['damage_date'])),
                            'product_entry_id'      => $value,
                            'damage_id'             => $damage->id,
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['unit_id'][$key],
                            'store_id'              => $data['store_id'][$key],
                            'type'                  => 4,
                            'quantity'              => $data['quantity'][$key],
                            'rate'                  => $data['rate'][$key],
                            'amount'                => $data['amount'][$key],
                            'updated_by'            => $user_id,
                            'updated_at'            => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);
                    DB::table('sr_items')->insert($sr_items);

                    DB::commit();

                    return back()->with("success","Damage Updated Successfully !!");
                }
            }catch (\Exception $exception){
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }

        public function productPriceList($id)
        {
            $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->selectRaw('product_entries.*, units.name as unit_name')
                                                        ->find($id);

            $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                            ->where('product_entries.id', $id)
                                                            ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                            ->get()
                                                            ->toArray();

            $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                        ->where('unit_conversions.product_entry_id', $id)
                                                        ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

            $data['unit_conversions']       = collect(array_merge($data1, $data2));

            return Response::json($data);
        }

        public function ReceiveFromCustomerTransfer($id)
        {
            $find_damage            = DealerDamages::select('dealer_damages.*')->find($id);

            $find_damage_entries    = DealerDamageEntries::leftjoin('product_entries', 'product_entries.id', 'dealer_damage_entries.product_entry_id')
                                                ->where('dealer_damage_entries.dealer_damage_id', $id)
                                                ->select('dealer_damage_entries.*',
                                                        'product_entries.id as item_id',
                                                        'product_entries.product_code as product_code',
                                                        'product_entries.stock_in_hand as stock_in_hand',
                                                        'product_entries.buy_price as buy_price',
                                                        'product_entries.name as item_name')
                                                ->get();

            $entries_count          = $find_damage_entries->count();

            return view('damages::receive_from_customer.transfer', compact('find_damage', 'find_damage_entries', 'entries_count'));
        }

        public function receiveFromCustomerTransferStore(Request $request, $id)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $find_number             = DealerDamages::find($id);
                $damage                  = new DealerDamages;
                $damage->number          = $find_number['number'];
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->supplier_id     = $data['supplier_id'];
                $damage->type            = 1;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->updated_by      = $user_id;

                if ($damage->save())
                {   
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Transfer To DP/Supplier Successfull !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function listOfIssues($dsm_id)
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = Issues::where('sr_id', $dsm_id)
                                    ->orderBy('created_at', 'DESC')
                                    ->take(50)
                                    ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = Issues::where('sr_id', $dsm_id)
                                    ->where('issue_number', 'LIKE', "%$search%")
                                    ->orderBy('created_at', 'DESC')
                                    ->get();
            }

            $data = array();
            foreach ($fetchData as $key => $value)
            {   
                $data[]  = array("id"=>$value['id'], "text"=>$value['issue_number']);
            }
       
            return Response::json($data);
        }

        public function listOfOrders($issue_id)
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = IssueOrders::leftjoin('orders', 'orders.id', 'issue_orders.order_id')
                                    ->where('issue_orders.issue_id', $issue_id)
                                    ->orderBy('issue_orders.id', 'DESC')
                                    ->selectRaw('issue_orders.*, orders.invoice_number as order_number')
                                    ->take(50)
                                    ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = IssueOrders::leftjoin('orders', 'orders.id', 'issue_orders.order_id')
                                    ->where('issue_orders.issue_id', $issue_id)
                                    ->where('orders.id', 'LIKE', "%$search%")
                                    ->orderBy('issue_orders.id', 'DESC')
                                    ->selectRaw('issue_orders.*, orders.invoice_number as order_number')
                                    ->get();
            }

            $data = array();
            foreach ($fetchData as $key => $value)
            {   
                $data[]  = array("id"=>$value['order_id'], "text"=>$value['order_number']);
            }
       
            return Response::json($data);
        }
    //Receive From Customer/SR End

    //Transfer To Dp Start
        public function TransferToDpIndex()
        {
            $dealer_damages = DealerDamages::where('dealer_damages.type', 1)
                                ->orderBy('dealer_damages.created_at', 'DESC')
                                ->get();

            return view('damages::transfer_to_dp.index', compact('dealer_damages'));
        }

        public function TransferToDpEdit($id)
        {
            $find_damage            = DealerDamages::select('dealer_damages.*')->find($id);

            $find_damage_entries    = DealerDamageEntries::leftjoin('product_entries', 'product_entries.id', 'dealer_damage_entries.product_entry_id')
                                                ->where('dealer_damage_entries.dealer_damage_id', $id)
                                                ->select('dealer_damage_entries.*',
                                                        'product_entries.id as item_id',
                                                        'product_entries.product_code as product_code',
                                                        'product_entries.stock_in_hand as stock_in_hand',
                                                        'product_entries.buy_price as buy_price',
                                                        'product_entries.name as item_name')
                                                ->get();

            $entries_count          = $find_damage_entries->count();

            return view('damages::transfer_to_dp.edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
        }

        public function TransferToDpUpdate(Request $request, $id)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $damage                  = DealerDamages::find($id);
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->supplier_id     = $data['supplier_id'];
                $damage->type            = 1;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->updated_by      = $user_id;

                if ($damage->save())
                {   
                    $item_delete   = DealerDamageEntries::where('dealer_damage_id', $damage['id'])->delete();
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Transfered Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }
    //Tranfer To Dp End

    //Receive From Dp Start
        public function ReceiveFromDpIndex()
        {
            $dealer_damages = DealerDamages::where('dealer_damages.type', 2)
                                ->orderBy('dealer_damages.id', 'DESC')
                                ->get();

            return view('damages::received_from_dp.index', compact('dealer_damages'));
        }

        public function ReceiveFromDpCreate()
        {
            return view('damages::received_from_dp.create');
        }

        public function ReceiveFromDpStore(Request $request)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $data_find               = DealerDamages::orderBy('created_at', 'DESC')->first();
                $number                  = $data_find != null ? $data_find['number'] + 1 : 1;

                $damage                  = new DealerDamages;
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->number          = $number;
                $damage->supplier_id     = $data['supplier_id'];
                $damage->type            = 2;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->created_by      = $user_id;

                if ($damage->save())
                {
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Received Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function ReceiveFromDpEdit($id)
        {
            $find_damage            = DealerDamages::select('dealer_damages.*')->find($id);

            $find_damage_entries    = DealerDamageEntries::leftjoin('product_entries', 'product_entries.id', 'dealer_damage_entries.product_entry_id')
                                                ->where('dealer_damage_entries.dealer_damage_id', $id)
                                                ->select('dealer_damage_entries.*',
                                                        'product_entries.id as item_id',
                                                        'product_entries.product_code as product_code',
                                                        'product_entries.stock_in_hand as stock_in_hand',
                                                        'product_entries.name as item_name')
                                                ->get();

            $entries_count          = $find_damage_entries->count();

            return view('damages::received_from_dp.edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
        }

        public function ReceiveFromDpUpdate(Request $request, $id)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $damage                  = DealerDamages::find($id);
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->supplier_id     = $data['supplier_id'];
                $damage->type            = 2;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->updated_by      = $user_id;

                if ($damage->save())
                {   
                    $item_delete   = DealerDamageEntries::where('dealer_damage_id', $damage['id'])->delete();
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Received Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }
    //Receive From Dp End

    //Return To Customer Start
        public function ReturnToCustomerIndex()
        {
            $dealer_damages = DealerDamages::where('dealer_damages.type', 3)
                                ->orderBy('dealer_damages.id', 'DESC')
                                ->get();

            return view('damages::return_to_customer.index', compact('dealer_damages'));
        }

        public function ReturnToCustomerCreate()
        {
            return view('damages::return_to_customer.create');
        }

        public function ReturnToCustomerStore(Request $request)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $data_find               = DealerDamages::orderBy('created_at', 'DESC')->first();
                $number                  = $data_find != null ? $data_find['number'] + 1 : 1;

                $damage                  = new DealerDamages;
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->number          = $number;
                $damage->customer_id     = $data['customer_id'];
                $damage->type            = 3;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->created_by      = $user_id;

                if ($damage->save())
                {
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Received Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function ReturnToCustomerEdit($id)
        {
            $find_damage            = DealerDamages::select('dealer_damages.*')->find($id);

            $find_damage_entries    = DealerDamageEntries::leftjoin('product_entries', 'product_entries.id', 'dealer_damage_entries.product_entry_id')
                                                ->where('dealer_damage_entries.dealer_damage_id', $id)
                                                ->select('dealer_damage_entries.*',
                                                        'product_entries.id as item_id',
                                                        'product_entries.product_code as product_code',
                                                        'product_entries.stock_in_hand as stock_in_hand',
                                                        'product_entries.name as item_name')
                                                ->get();

            $entries_count          = $find_damage_entries->count();

            return view('damages::return_to_customer.edit', compact('find_damage', 'find_damage_entries', 'entries_count'));
        }

        public function ReturnToCustomerUpdate(Request $request, $id)
        {
            $rules = array(
                'damage_date'           => 'required|date',
                'note'                  => 'nullable|string',
                'product_entries.*'     => 'required|integer',
                'quantity.*'            => 'required|numeric',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $damage                  = DealerDamages::find($id);
                $damage->date            = date('Y-m-d', strtotime($data['damage_date']));
                $damage->customer_id     = $data['customer_id'];
                $damage->type            = 3;
                $damage->total_amount    = $data['sub_total_amount'];
                $damage->note            = $data['note'];
                $damage->updated_by      = $user_id;

                if ($damage->save())
                {   
                    $item_delete   = DealerDamageEntries::where('dealer_damage_id', $damage['id'])->delete();
                    foreach ($data['product_entries'] as $key => $value)
                    {
                        $damage_entries[] = [
                            'dealer_damage_id'   => $damage['id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'rate'               => $data['rate'][$key],
                            'quantity'           => $data['quantity'][$key],
                            'amount'             => $data['amount'][$key],
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('dealer_damage_entries')->insert($damage_entries);

                    DB::commit();

                    return back()->with("success","Received Successfully !!");
                }
                else
                {
                    DB::rollback();
                    return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
                }

            }catch (\Exception $exception){
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }
    //Return To Customer End
}
