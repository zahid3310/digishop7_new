@extends('layouts.app')

@section('title', 'Edit Return Issues')

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                @if(Session::has('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {!! Session::get('success') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			@endif

                    			@if(Session::has('unsuccess'))
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! Session::get('unsuccess') !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

                                @if(Session::has('errors'))
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    {!! 'Some required fields are missing..!! Please try again..' !!}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                @endif

            					<form id="FormSubmit" action="{{ route('issues_return_update', $find_return['id']) }}" method="post" files="true" enctype="multipart/form-data" onkeypress="return event.keyCode != 13;">
            					{{ csrf_field() }}

                                <div class="row">

                                    <div style="background-color: #D2D2D2;height: 620px;padding-top: 10px;overflow-y: auto;overflow-x: hidden;" class="col-md-7">
                                        <div style="display: none" class="inner-repeater mb-4 issueDetails">
                                            <div data-repeater-list="inner-group" class="inner form-group row">
                                                <div id="return_product_list" class="inner col-lg-12 ml-md-auto">

                                                    @foreach($find_ireturn_entries as $key => $value)
                                                    <div class="row di_{{$key}}">
                                                        @if($value->order_id != null)
                                                        <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <h4>
                                                                {{ 'Order# ' . 'ORD - ' . str_pad($value->order->invoice_number, 6, "0", STR_PAD_LEFT) . ' || ' .  'Customer : ' . $value->order->customer->name . ' || ' .  'SM : ' . $value->order->sr->name }}
                                                            </h4>
                                                        </div>
                                                        @endif

                                                        <input id="order_id" type="hidden" class="inner form-control" value="{{$value->order_id}}" name="order_id[]" />

      
                                                        <div style="margin-bottom: 5px;padding-right: 0px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                            <label style="" class="show-xs" for="Storename">Store *</label>
                                                            <select style="width: 100%" class="inner form-control select2" name="store_id[]" id="store_id_{{$key}}">
                            
                                                                <option value="{{ $value->store->id }}" selected>{{ $value->store->name }}</option>
                                                                
                                                            </select>
                                                        </div>


                                                        <div style="margin-bottom: 5px;padding-left: 10px" class="col-lg-4 col-md-4 col-sm-6 col-12">
                                                            <label  class="show-xs" for="productname">Product *</label>
                                                            <select style="width: 100%" name="product_entries[]" class="inner form-control select2 productEntries" id="product_entries_{{$key}}" onchange="getItemPrice({{$key}})" required>
                                                                <option value="{{ $value['product_entry_id'] }}" selected>
                                                                    {{ $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name }}
                                                                </option>
                                                            </select>
                                                        </div>

                                                        <input type="hidden" name="stock[]" class="inner form-control" id="stock_{{$key}}" placeholder="Stock" oninput="calculateActualAmount({{$key}})" readonly />
                                                        <input type="hidden" name="main_unit_id[]" class="inner form-control" id="main_unit_id_{{$key}}" value="{{ $value->main_unit_id }}" />
                                                        <input type="hidden" class="inner form-control" id="main_unit_name_{{$key}}" value="{{$value->productEntries->unit->name}}" />

                                                        <div style="padding-left: 0px" class="col-lg-2 col-md-2 col-sm-12 col-12 form-group">
                                                            <label class="show-xs" for="productname">Unit</label>
                                                            <select style="width: 100%;cursor: pointer" name="unit_id[]" class="inner form-control single_select2" id="unit_id_{{$key}}" required onchange="getConversionParam({{$key}})">
                                                                <option value="{{ $value->conversion_unit_id }}" selected>{{ $value->convertedUnit->name }}</option>
                                                            </select>
                                                        </div>

                                                        <div style="margin-bottom: 5px;padding-left: 0px" class="col-lg-2 col-md-2 col-sm-6 col-6">
                                                            <label class="show-xs" for="productname">Return Qty</label>
                                                            <input type="text" name="return_quantity[]" class="inner form-control returnQuantity" id="return_quantity_{{$key}}" placeholder="Quantity" oninput="calculate({{$key}})" value="{{ $value->quantity }}" required />
                                                        </div>
                                                    </div>

                                                    <hr style="margin-top: 5px !important;margin-bottom: 5px !important">
                                                    @endforeach
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div style="background-color: #F4F4F7;height: 620px;padding-top: 10px" class="col-md-5">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label class="control-label">Select DSM * </label>
                                                    <select id="sr_id" name="sr_id" class="form-control col-md-12" required>
                                                       <option value="{{ $value['sr_id'] }}">{{ $value->srName->name }}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label class="control-label">Select Issue Number * </label>
                                                    <select style="width: 100%" id="issue_id" name="issue_id" class="form-control select2" onchange="findIssueList()">
                                                       <option value="{{ $value['issue_id'] }}">{{ 'SIN - ' . str_pad($value->issueReturn->return_number, 6, "0", STR_PAD_LEFT) }}</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="return_date">Return Date *</label>
                                                    <input id="return_date" name="return_date" type="text" value="{{ date('d-m-Y', strtotime($find_return['date'])) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" required>
                                                </div>
                                            </div>

                                            <div class="col-sm-7">
                                                <div class="form-group">
                                                    <label for="return_note">Note</label>
                                                    <input id="return_note" name="return_note" type="text" value="{{ $find_return['note'] }}" class="form-control">
                                                </div>
                                            </div>

                                            <div style="display: none;margin-top: 20px" class="button-items col-lg-12 issueDetails">
                                                <button name="print" value="1" type="submit" class="btn btn-primary waves-effect waves-light">Update</button>
                                                <!-- <button name="print" value="2" type="submit" class="btn btn-success waves-effect waves-light">Update & Print</button> -->
                                                <button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="{{ route('all_return_issues') }}">Close</a></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            
            $('.issueDetails').show();

            // calculate(0);
        });

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }

        function calculate(x)
        {
            var return_quantity         = $("#return_quantity_"+x).val();
            var rate                    = $("#rate_"+x).val();

            if (return_quantity == '')
            {
                var returnQuantityCal   = 0;
            }
            else
            {
                var returnQuantityCal   = $("#return_quantity_"+x).val();
            }

            if (rate == '')
            {
                var rateCal             = 0;
            }
            else
            {
                var rateCal             = $("#rate_"+x).val();
            }

            //Calculating Subtotal Amount
            var total       = 0;

            $('.quantity').each(function()
            {
                total       += parseFloat($(this).val());
            });

            var AmountIn  =  (parseFloat(rateCal)*parseFloat(returnQuantityCal));
     
            $("#amount_"+x).val(AmountIn);

            var totalAmount       = 0;
            $('.amount').each(function()
            {
                totalAmount       += parseFloat($(this).val());
            });

            $("#total_amount").val(totalAmount.toFixed());
            $("#total_amount").val(totalAmount.toFixed());
        }
    </script>

    <script type="text/javascript">
        function getItemPrice(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var main_unit_id        = $("#main_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param-main-unit/'+ product_entry_id + '/' + main_unit_id, function(data){
  
                var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                var mainStock         = $("#main_stock_"+x).val();
                var convertedStock    = (parseFloat(mainStock)/parseFloat(data.conversion_rate)).toFixed(2);
                var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                $("#quantity_"+x).val(convertedStock);
                $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
            });
        }

        function getConversionParam(x)
        {
            var site_url            = $(".site_url").val();
            var product_entry_id    = $("#product_entries_"+x).val();
            var unit_id             = $("#converted_unit_id_"+x).val();

            $.get(site_url + '/bills/get-conversion-param/'+ product_entry_id + '/' + unit_id, function(data){

                if ($.isEmptyObject(data))
                {
                    getItemPrice(x);
                }
                else
                {   
                    var convertedUnitName = $('#converted_unit_id_'+x).find(":selected").text();
                    var mainStock         = $("#main_stock_"+x).val();
                    var convertedStock    = (parseFloat(data.conversion_rate)*parseFloat(mainStock)).toFixed();
                    var mainStock         = $("#main_stock_"+x).val(parseFloat(convertedStock).toFixed(2));

                    $("#quantity_"+x).val(convertedStock);
                    $("#rate_"+x).val(parseFloat(data.sell_price).toFixed(2));
                    $("#main_unit_show_"+x).html(convertedUnitName);
                }

            });
        }

        function getCategoryList(x)
        {
            var site_url  = $('.site_url').val();
            var group_id  = $('#group_id_'+x).val();

            $("#product_category_id_"+x).select2({
                ajax: { 
                    url:  site_url + '/groups/category-list-ajax/' + group_id,
                    type: "get",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            searchTerm: params.term // search term
                        };
                    },
                    processResults: function (response) {
                        return {
                            results: response
                        };
                    },
                    cache: true
                }
            });
        }
    </script>
@endsection