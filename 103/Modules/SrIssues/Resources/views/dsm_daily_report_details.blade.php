@extends('layouts.app')

@section('title', 'DSM Daily Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">DSM Daily Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Issue To DSM</a></li>
                                    <li class="breadcrumb-item active">DSM Daily Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">DSM Daily Report</h4>
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $sr != null ? $sr->name : '' }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>Date : </strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} </p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <table class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                   
                                    <tbody>
                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Receive</strong></td>
                                            <td style="text-align: left;width: 25%"><strong>{{ number_format($receive,0,'.',',') }}</strong></td>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Return</strong></td>
                                            <td style="text-align: left;width: 25%"><strong>{{ number_format($return,0,'.',',') }}</strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Sales</strong></td>
                                            <td style="text-align: left;width: 25%"><strong>{{ number_format($sales,0,'.',',') }}</strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Damage</strong></td>
                                            <td style="text-align: left;width: 25%"><strong>{{ number_format($damage,0,'.',',') }}</strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>

                                        <tr>
                                            <td style="text-align: right;width: 25%"><strong></strong></td>
                                            <td style="text-align: right;width: 25%"><strong>Net Receivable</strong></td>
                                            <td style="text-align: left;width: 25%"><strong>{{ number_format($receive - $return - $sales - $damage,0,'.',',') }}</strong></td>
                                            <td style="text-align: left;width: 25%"><strong></strong></td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
@endsection