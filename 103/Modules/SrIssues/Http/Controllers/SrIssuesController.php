<?php

namespace Modules\SrIssues\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Users;
use App\Models\Issues;
use App\Models\Orders;
use App\Models\OrderEntries;
use App\Models\IssueEntries;
use App\Models\SrItems;
use App\Models\IssueReturn;
use App\Models\IssueReturnEntries;
use App\Models\UnitConversions;
use App\Models\JournalEntries;
use App\Models\IssueOrders;
use App\Models\StoreInventories;
use Response;
use DB;
use View;
use Carbon\Carbon;

class SrIssuesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $issues           = Issues::orderBy('id', 'DESC')->first();

        return view('srissues::index', compact('issues'));
    }

    public function AllIssues()
    {
        return view('srissues::all_issues');
    }

    public function store(Request $request)
    {
        $rules = array(
            'issue_date'            => 'required',
            'sr_id'                 => 'required',
            'product_entries.*'     => 'required',
            'quantity.*'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find              = Issues::orderBy('created_at', 'DESC')->first();
            $issue_number           = $data_find != null ? $data_find['issue_number'] + 1 : 1;

            $issue                  = new Issues;
            $issue->issue_number    = $issue_number;
            $issue->sr_id           = $data['sr_id'];
            $issue->issue_date      = date('Y-m-d', strtotime($data['issue_date']));
            $issue->issue_note      = $data['issue_note'];
            // $issue->total_amount    = $data['total_amount'];
            $issue->created_by      = $user_id;

            if ($issue->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $issue_entries[] = [
                        'issue_id'           => $issue['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'store_id'           => $data['store_id'][$key],
                        'sr_id'              => $data['sr_id'],
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    $sr_items[] = [
                        'issue_id'              => $issue['id'],
                        'sr_id'                 => $data['sr_id'],
                        'date'                  => date('Y-m-d', strtotime($data['issue_date'])),
                        'product_id'            => $product_buy_price['product_id'],
                        'product_entry_id'      => $value,
                        'main_unit_id'          => $data['main_unit_id'][$key],
                        'conversion_unit_id'    => $data['unit_id'][$key],
                        'store_id'              => $data['store_id'][$key],
                        'type'                  => 1,
                        'quantity'              => $converted_quantity_to_main_unit,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];

                    //Decrease Stock Start
                        $product_buy_price->stock_in_hand     = $product_buy_price['stock_in_hand'] - round($converted_quantity_to_main_unit, 2);
                        $product_buy_price->total_sold        = $product_buy_price['total_sold'] + round($converted_quantity_to_main_unit, 2);
                    //Decrease Stock End

                    //Decrease Store Stock Start
                    if ($product_buy_price->save()) {
                        $store           = StoreInventories::where('store_id', $data['store_id'][$key])
                                                                        ->where('product_entry_id', $value)
                                                                        ->first();

                        $store->store_stock_in_hand     = $store['store_stock_in_hand'] - round($converted_quantity_to_main_unit, 2);
                        $store->save();
                    }
                    //Decrease Store Stock End
                }

                DB::table('issue_entries')->insert($issue_entries);
                DB::table('sr_items')->insert($sr_items);

                // if (isset($data['order_id'])) 
                // {
                //     $order_ids = array_unique($data['order_id']);
                //     foreach ($order_ids as $key_order_ids => $value_order_ids)
                //     {   
                //         if($value_order_ids != 0)
                //         {
                //             $order_ids_entries[] = [
                //                 'issue_id'   => $issue['id'],
                //                 'order_id'   => $value_order_ids,
                //             ];

                //             $update_order_status            = Orders::find($value_order_ids);
                //             $update_order_status->type      = 3;
                //             $update_order_status->dsm_id    = $data['sr_id'];
                //             $update_order_status->save();
                //         }
                //     }

                //     if (isset($order_ids_entries))
                //     {
                //         DB::table('issue_orders')->insert($order_ids_entries);
                //     }
                // }

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Issue Created Successfully !!");
                }
                else
                {
                    return redirect()->route('issues_show', $issue->id);
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        $issue      = Issues::select('issues.*')->find($id);
        $entries    = IssueEntries::leftjoin('stores', 'stores.id', 'issue_entries.store_id')
                                ->where('issue_entries.issue_id', $id)
                                ->select('issue_entries.*','stores.name as stores_name')
                                ->orderBy('issue_entries.id', 'DESC')
                                ->get();      
        $user_info  = userDetails();

        return view('srissues::show', compact('entries', 'issue', 'user_info'));
    }

    public function edit($id)
    {
        $find_issue             = Issues::find($id);
        $find_issue_entries     = IssueEntries::where('issue_entries.issue_id', $id)->get();
        $entries_count          = $find_issue_entries->count();
        
        return view('srissues::edit', compact('find_issue', 'find_issue_entries', 'entries_count'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'issue_date'            => 'required',
            'sr_id'                 => 'required',
            'product_entries.*'     => 'required',
            'quantity.*'            => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $issue                  = Issues::find($id);
            $issue->sr_id           = $data['sr_id'];
            $issue->issue_date      = date('Y-m-d', strtotime($data['issue_date']));
            $issue->issue_note      = $data['issue_note'];
            // $issue->total_amount    = $data['total_amount'];
            $issue->updated_by      = $user_id;

            if ($issue->save())
            {
                $issue_entry        = IssueEntries::where('issue_id', $issue['id'])->get();
                $issue_entry_delete = IssueEntries::where('issue_id', $issue['id'])->delete();
                $sr_items_delete    = SrItems::where('issue_id', $issue['id'])->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $issue_entries[] = [
                        'order_id'           => $data['order_id'][$key],
                        'issue_id'           => $issue['id'],
                        'product_id'         => $product_buy_price['product_id'],
                        'product_entry_id'   => $value,
                        'main_unit_id'       => $data['main_unit_id'][$key],
                        'conversion_unit_id' => $data['unit_id'][$key],
                        'store_id'           => $data['store_id'][$key],
                        'sr_id'              => $data['sr_id'],
                        'quantity'           => $data['quantity'][$key],
                        'created_by'         => $user_id,
                        'created_at'         => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    $sr_items[] = [
                        'order_id'              => $data['order_id'][$key],
                        'issue_id'              => $issue['id'],
                        'sr_id'                 => $data['sr_id'],
                        'date'                  => date('Y-m-d', strtotime($data['issue_date'])),
                        'product_id'            => $product_buy_price['product_id'],
                        'product_entry_id'      => $value,
                        'main_unit_id'          => $data['main_unit_id'][$key],
                        'conversion_unit_id'    => $data['unit_id'][$key],
                        'store_id'              => $data['store_id'][$key],
                        'type'                  => 1,
                        'quantity'              => $converted_quantity_to_main_unit,
                        'created_by'            => $user_id,
                        'created_at'            => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('issue_entries')->insert($issue_entries);
                DB::table('sr_items')->insert($sr_items);

                //Update Stock Start
                    foreach ($issue_entry as $key => $value)
                    {
                        $old_item_entry_id[]      = $value['product_entry_id'];
                        $old_main_unit_id[]       = $value['main_unit_id'];
                        $old_conversion_unit_id[] = $value['conversion_unit_id'];
                        $old_items_stock[]        = $value['quantity'];
                    }

                    foreach ($old_item_entry_id as $key2 => $value2)
                    {

                        $old_conversion_rate_find    = UnitConversions::where('main_unit_id', $old_main_unit_id[$key2])
                                        ->where('converted_unit_id', $old_conversion_unit_id[$key2])
                                        ->where('product_entry_id', $value2)
                                        ->first();

                        $old_converted_quantity_to_main_unit          = $old_conversion_rate_find != null ? $old_items_stock[$key2]/$old_conversion_rate_find['conversion_rate'] : $old_items_stock[$key2];
                        
                        $quantity_add_to_product_entry                = ProductEntries::find($value2);
                        $quantity_add_to_product_entry->stock_in_hand = $quantity_add_to_product_entry['stock_in_hand'] + round($old_converted_quantity_to_main_unit, 2);
                        $quantity_add_to_product_entry->total_sold    = $quantity_add_to_product_entry['total_sold'] - round($old_converted_quantity_to_main_unit, 2);
                        $quantity_add_to_product_entry->save();


                        //Update Store Stock Start
                            $store           = StoreInventories::where('store_id', $data['store_id'][$key])
                                                    ->where('product_entry_id', $value2)
                                                    ->first();

                            $store->store_stock_in_hand     = $store['store_stock_in_hand'] + round($old_converted_quantity_to_main_unit, 2);
                            $store->save();
                        //Update Store Stock End
                    }
                //Update Stock End

                foreach ($data['product_entries'] as $key4 => $value4)
                {
                    $product_buy_price1 = ProductEntries::find($value4);

                    $conversion_rate_find1    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                        ->where('product_entry_id', $value4)
                                        ->first();

                    $converted_quantity_to_main_unit1  = $conversion_rate_find1 != null ? $data['quantity'][$key4]/$conversion_rate_find1['conversion_rate'] : $data['quantity'][$key4];

                    //Decrease Stock Start
                        $product_buy_price1->stock_in_hand     = $product_buy_price1['stock_in_hand'] - round($converted_quantity_to_main_unit1, 2);
                        $product_buy_price1->total_sold        = $product_buy_price1['total_sold'] + round($converted_quantity_to_main_unit1, 2);
                        $product_buy_price1->save();
                    //Decrease Stock End


                    //Decrease Store Stock Start
                    $store           = StoreInventories::where('store_id', $data['store_id'][$key])
                                                    ->where('product_entry_id', $value4)
                                                    ->first();

                    $store->store_stock_in_hand     = $store['store_stock_in_hand'] - round($converted_quantity_to_main_unit1, 2);
                    $store->save();
                    //Decrease Store Stock End

                }

                DB::commit();
                
                if ($data['print'] == 1)
                {
                    return back()->with("success","Issue Updated Successfully !!");
                }
                else
                {
                    return redirect()->route('issues_show', $issue->id);
                }
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Updated");
        }
    }

    public function destroy($id)
    {
    }

    public function issuesListLoad()
    {
        $user_id        = Auth::user()->id;
        $data           = Issues::leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                    ->leftjoin('customers', 'customers.id', 'issues.sr_id')
                                    ->leftjoin('users', 'users.id', 'issues.created_by')
                                    ->where('issues.created_by', $user_id)
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->select('issues.*',
                                            'customers.name as sr_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('issues.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function issueListSearch($from_date, $to_date, $sr, $issue)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_sr           = $sr != 0 ? $sr : 0;
        $search_by_issue        = $issue != 0 ? $issue : 0;

        $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                        ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                        ->whereBetween('issues.issue_date', [$search_by_from_date, $search_by_to_date])
                                        ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                            return $query->where('customers.id', $search_by_sr);
                                        })
                                        ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                            return $query->where('issues.id', $search_by_issue);
                                        })
                                        ->orderBy('issues.created_at', 'DESC')
                                        ->select('issues.*',
                                                 'customers.name as sr_name',
                                                 'customers.phone as phone')
                                        ->distinct('issues.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function printIssuesSearch($date, $sr, $issue)
    {
        $search_by_date         = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_sr           = $sr != 0 ? $sr : 0;
        $search_by_issue        = $issue != 0 ? $issue : 0;

        $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                        ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                        ->leftjoin('users', 'users.id', 'issues.created_by')
                                        ->where('issues.issue_date', $search_by_date)
                                        ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                            return $query->where('customers.id', $search_by_sr);
                                        })
                                        ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                            return $query->where('issues.id', $search_by_issue);
                                        })
                                        ->orderBy('issues.created_at', 'DESC')
                                        ->select('issues.*',
                                                 'customers.name as sr_name',
                                                 'users.name as user_name',
                                                 'customers.phone as phone')
                                        ->distinct('issues.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function printIssuesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Issues::leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                    ->leftjoin('customers', 'customers.id', 'issues.sr_id')
                                    ->leftjoin('users', 'users.id', 'issues.created_by')
                                    ->where('issues.created_by', $user_id)
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->select('issues.*',
                                            'customers.name as sr_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('issues.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function issuesList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Issues::orderBy('issues.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Issues::where('issues.issue_number', 'LIKE', "%$search%")
                                    ->orderBy('issues.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $issueNumber  = 'SIN - '.str_pad($value['issue_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$issueNumber);

            $i++;
        }
   
        return Response::json($data);
    }

    public function issuesListSr($sr_id)
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Issues::where('sr_id', $sr_id)
                                    ->orderBy('issues.id', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Issues::where('sr_id', $sr_id)
                                    ->where('issues.issue_number', 'LIKE', "%$search%")
                                    ->orderBy('issues.id', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            $issueNumber  = 'SIN - '.str_pad($value['issue_number'], 6, "0", STR_PAD_LEFT);

            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }

            $data[]         = array("id"=>$value['id'], "text"=>$issueNumber);

            $i++;
        }
   
        return Response::json($data);
    }

      //Return Start
        public function returnIssues()
        {
            return view('srissues::return');
        }

        public function returnIssueProductList($sr_id, $issue_id)
        {
            $result = SrItems::where('sr_items.sr_id', $sr_id)
                                ->where('sr_items.issue_id', $issue_id)
                                ->where('sr_items.type', 1)
                                ->select('sr_items.*')
                                ->get();
            
            foreach ($result as $key => $value)
            {   
                $d1 = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                ->where('product_entries.id', $value['product_entry_id'])
                                ->selectRaw('units.id as unit_id, units.name as unit_name')
                                ->get()
                                ->toArray();

                $d2 = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                ->where('unit_conversions.product_entry_id', $value['product_entry_id'])
                                ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                ->get()
                                ->toArray();

                $receive    = SrItems::where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.issue_id', $issue_id)
                                    ->where('sr_items.order_id', $value->order_id)
                                    ->where('sr_items.product_entry_id', $value->product_entry_id)
                                    ->where('sr_items.type', 1)
                                    ->select('sr_items.*')
                                    ->sum('quantity');
                $sales      = SrItems::where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $value->product_entry_id)
                                    ->where('sr_items.type', 2)
                                    ->select('sr_items.*')
                                    ->sum('quantity');
                $return     = SrItems::where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.issue_id', $issue_id)
                                    ->where('sr_items.order_id', $value->order_id)
                                    ->where('sr_items.product_entry_id', $value->product_entry_id)
                                    ->where('sr_items.type', 3)
                                    ->select('sr_items.*')
                                    ->sum('quantity');
                $damage     = SrItems::where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $value->product_entry_id)
                                    ->where('sr_items.type', 4)
                                    ->select('sr_items.*')
                                    ->sum('quantity');

                if ($value->order_id != null)
                {
                    $data[$value->order_id]['order_details']                              = 'Order# ' . 'ORD - ' . str_pad($value->order->invoice_number, 6, "0", STR_PAD_LEFT) . ' || ' .  'Customer : ' . $value->order->customer->name . ' || ' .  'SM : ' . $value->order->sr->name;
                }
                else
                {
                    $data[$value->order_id]['order_details']  = '';
                }

                $qty    = $receive - $sales - $return - $damage;

                $conversion_rate_find   = UnitConversions::where('main_unit_id', $value->main_unit_id)
                                                        ->where('converted_unit_id', $value->conversion_unit_id)
                                                        ->where('product_entry_id', $value->product_entry_id)
                                                        ->first();

                $converted_quantity     = $conversion_rate_find != null ? $qty*$conversion_rate_find['conversion_rate'] : $qty;
                
                $data[$value->order_id][$value->product_entry_id]['issue_id']         = $value->issue_id;
                $data[$value->order_id][$value->product_entry_id]['order_id']         = $value->order_id;
                $data[$value->order_id][$value->product_entry_id]['group_id']         = $value->productEntries->group_id;
                $data[$value->order_id][$value->product_entry_id]['brand_id']         = $value->productEntries->brand_id;
                $data[$value->order_id][$value->product_entry_id]['category_id']      = $value->productEntries->product_id;
                $data[$value->order_id][$value->product_entry_id]['product_entry_id'] = $value->product_entry_id;
                $data[$value->order_id][$value->product_entry_id]['group_name']       = $value->productEntries->group->name;
                $data[$value->order_id][$value->product_entry_id]['brand_name']       = $value->productEntries->brand->name;
                $data[$value->order_id][$value->product_entry_id]['category_name']    = $value->productEntries->product->name;
                $data[$value->order_id][$value->product_entry_id]['product_name']     = $value->productEntries->group->name . ' - ' . $value->productEntries->brand->name . ' - ' . $value->productEntries->product->name . ' - ' . $value->productEntries->name;
                $data[$value->order_id][$value->product_entry_id]['unit_id']          = $value->productEntries->unit_id;
                $data[$value->order_id][$value->product_entry_id]['converter_unit']   = $value->convertedUnit->name;
                $data[$value->order_id][$value->product_entry_id]['converter_unit_id']= $value->conversion_unit_id;
                $data[$value->order_id][$value->product_entry_id]['store_id']         = $value->store_id;
                $data[$value->order_id][$value->product_entry_id]['store_name']       = $value->store->name;
                $data[$value->order_id][$value->product_entry_id]['stock']            = $value->productEntries->stock_in_hand;
                $data[$value->order_id][$value->product_entry_id]['unit_data']        = collect(array_merge($d1, $d2));
                $data[$value->order_id][$value->product_entry_id]['quantity']         = round($converted_quantity);
                $data[$value->order_id][$value->product_entry_id]['rate']             = $value->rate;
                $data[$value->order_id][$value->product_entry_id]['amount']           = $value->amount;

            }

            return Response::json($data);
        }

        public function returnIssueStore(Request $request)
        {
            $rules = array(
                'return_date'   => 'required',
                'sr_id'         => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $data_find                     = IssueReturn::orderBy('id', 'DESC')->first();
                $return_number                 = $data_find != null ? $data_find['return_number'] + 1 : 1;

                $issue_return                  = new IssueReturn;;
                $issue_return->return_number   = $return_number;
                $issue_return->issue_id        = $data['issue_id'];
                $issue_return->date            = date('Y-m-d', strtotime($data['return_date']));
                $issue_return->sr_id           = $data['sr_id'];
                // $issue_return->total_amount    = $data['total_amount'];
                $issue_return->created_by      = $user_id;

                if ($issue_return->save())
                {
                    foreach ($data['product_entries'] as $key => $value)
                    {   
                        $product    = ProductEntries::find($value);

                        $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                    ->where('converted_unit_id', $data['converted_unit_id'][$key])
                                    ->where('product_entry_id', $value)
                                    ->first();

                        $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['return_quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key];


                        $issue_return_entries[] = [
                            // 'order_id'           => $data['order_id'][$key],
                            'issue_id'           => $data['issue_id'],
                            'issue_return_id'    => $issue_return['id'],
                            'sr_id'              => $data['sr_id'],
                            'product_id'         => $product['product_id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['converted_unit_id'][$key],
                            'store_id'           => $data['store_id'][$key],
                            'quantity'           => round($converted_quantity_to_main_unit, 2),
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];

                        $sr_items[] = [
                            // 'order_id'              => $data['order_id'][$key],
                            'issue_id'              => $data['issue_id'],
                            'issue_return_id'       => $issue_return['id'],
                            'sr_id'                 => $data['sr_id'],
                            'date'                  => date('Y-m-d', strtotime($data['return_date'])),
                            'product_id'            => $product['product_id'],
                            'product_entry_id'      => $value,
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['converted_unit_id'][$key],
                            'store_id'              => $data['store_id'][$key],
                            'type'                  => 3,
                            'quantity'              => round($converted_quantity_to_main_unit, 2),
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        //Increase Main Stock Start
                            $product->stock_in_hand     = $product['stock_in_hand'] + round($converted_quantity_to_main_unit, 2);
                            $product->total_sold        = $product['total_sold'] - round($converted_quantity_to_main_unit, 2);
                            $product->save();
                        //Increase Main Stock End

                        //Increase Store Stock Start
                        $store                          = StoreInventories::where('store_id', $data['store_id'][$key])
                                                            ->where('product_entry_id', $value)
                                                            ->first();

                        $store->store_stock_in_hand     = $store['store_stock_in_hand'] + round($converted_quantity_to_main_unit, 2);
                        $store->save();
                        //Increase Store Stock End
                    }

                    DB::table('issue_return_entries')->insert($issue_return_entries);
                    DB::table('sr_items')->insert($sr_items);

                    $delete_0 = SrItems::where('quantity', 0)->delete();
                    DB::commit();

                    if ($data['print'] == 1)
                    {
                        return back()->with("success","Products Received Successfully !!");
                    }
                }
            }catch (\Exception $exception){
                dd($exception);
                DB::rollback();
                return back()->with("unsuccess","Not Added");
            }
        }

        public function showReturn($id)
        {
            $issue_return   = IssueReturn::select('issue_return.*')->find($id);
            $entries        = IssueReturnEntries::where('issue_return_entries.issue_return_id', $id)
                                    ->where('issue_return_entries.quantity', '>', 0)
                                    ->select('issue_return_entries.*')
                                    ->get();  
                         
            $user_info  = userDetails();

            return view('srissues::return_show', compact('entries', 'issue_return', 'user_info'));
        }

        public function AllReturns()
        {
            return view('srissues::all_returns');
        }

        public function returnsListLoad()
        {
            $user_id        = Auth::user()->id;
            $data           = IssueReturn::leftjoin('issue_return_entries', 'issue_return_entries.issue_return_id', 'issue_return.id')
                                        ->leftjoin('customers', 'customers.id', 'issue_return.sr_id')
                                        ->leftjoin('users', 'users.id', 'issue_return.created_by')
                                        ->where('issue_return.created_by', $user_id)
                                        ->orderBy('issue_return.created_at', 'DESC')
                                        ->select('issue_return.*',
                                                'customers.name as sr_name',
                                                'customers.phone as phone',
                                                'users.name as user_name')
                                        ->distinct('issue_return.id')
                                        ->take(100)
                                        ->get();

            return Response::json($data);
        }

        public function returnsListSearch($from_date, $to_date, $sr, $issue)
        {
            $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
            $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
            $search_by_sr           = $sr != 0 ? $sr : 0;
            $search_by_issue        = $issue != 0 ? $issue : 0;

            $data           = Issues::leftjoin('customers', 'customers.id', 'issues.sr_id')
                                            ->leftjoin('issue_entries', 'issue_entries.issue_id', 'issues.id')
                                            ->whereBetween('issues.issue_date', [$search_by_from_date, $search_by_to_date])
                                            ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                                return $query->where('customers.id', $search_by_sr);
                                            })
                                            ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                                return $query->where('issues.id', $search_by_issue);
                                            })
                                            ->orderBy('issues.created_at', 'DESC')
                                            ->select('issues.*',
                                                     'customers.name as sr_name',
                                                     'customers.phone as phone')
                                            ->distinct('issues.id')
                                            ->take(20)
                                            ->get();

            return Response::json($data);
        }

        public function issueReturnList()
        {
            if(!isset($_GET['searchTerm']))
            { 
                $fetchData  = IssueReturn::orderBy('issue_return.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }
            else
            { 
                $search     = $_GET['searchTerm'];   
                $fetchData  = IssueReturn::where('issue_return.return_number', 'LIKE', "%$search%")
                                        ->orderBy('issue_return.created_at', 'DESC')
                                        ->take(100)
                                        ->get();
            }

            $data = array();
            $i    = 0;
            foreach ($fetchData as $key => $value)
            {   
                $returnNumber  = 'IRN - '.str_pad($value['return_number'], 6, "0", STR_PAD_LEFT);

                if ($i == 0)
                {
                    $data[] = array("id"=>0, "text"=>'All');
                }

                $data[]         = array("id"=>$value['id'], "text"=>$returnNumber);

                $i++;
            }
       
            return Response::json($data);
        }

        public function allReturnsListSearch($from_date, $to_date, $sr, $issue)
        {
            $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
            $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
            $search_by_sr           = $sr != 0 ? $sr : 0;
            $search_by_issue        = $issue != 0 ? $issue : 0;

            $data           = IssueReturn::leftjoin('customers', 'customers.id', 'issue_return.sr_id')
                                            ->leftjoin('issue_return_entries', 'issue_return_entries.issue_return_id', 'issue_return.id')
                                            ->whereBetween('issue_return.date', [$search_by_from_date, $search_by_to_date])
                                            ->when($search_by_sr != 0, function ($query) use ($search_by_sr) {
                                                return $query->where('customers.id', $search_by_sr);
                                            })
                                            ->when($search_by_issue != 0, function ($query) use ($search_by_issue) {
                                                return $query->where('issue_return.id', $search_by_issue);
                                            })
                                            ->orderBy('issue_return.created_at', 'DESC')
                                            ->select('issue_return.*',
                                                     'customers.name as sr_name',
                                                     'customers.phone as phone')
                                            ->distinct('issue_return.id')
                                            ->take(20)
                                            ->get();

            return Response::json($data);
        }

        public function editReturn($id)
        {
            $find_return            = IssueReturn::find($id);
            $find_ireturn_entries   = IssueReturnEntries::where('issue_return_entries.issue_return_id', $id)->get();
            $entries_count          = $find_ireturn_entries->count();
       
            return view('srissues::edit_return', compact('find_return', 'find_ireturn_entries','entries_count'));
        }

        public function updateReturn(Request $request, $id)
        {
            $rules = array(
                'return_date'   => 'required',
                'sr_id'         => 'required',
            );

            $validation = Validator::make(\Request::all(),$rules);

            if ($validation->fails()) {
                return redirect()->back()->withInput()->withErrors($validation);
            }

            $user_id    = Auth::user()->id;
            $data       = $request->all();

            DB::beginTransaction();

            try{
                $issue_return                  = IssueReturn::find($id);
                $issue_return->issue_id        = $data['issue_id'];
                $issue_return->date            = date('Y-m-d', strtotime($data['return_date']));
                $issue_return->sr_id           = $data['sr_id'];
                $issue_return->updated_by      = $user_id;

                if ($issue_return->save())
                {   
                    $old_issue_return_entries    = IssueReturnEntries::where('issue_return_id', $id)->get();
                    $delete_issue_return_entries = IssueReturnEntries::where('issue_return_id', $id)->delete();
                    $delete_sr_items             = SrItems::where('issue_return_id', $id)->delete();

                    foreach ($data['product_entries'] as $key => $value)
                    {   
                        $product    = ProductEntries::find($value);

                        $conversion_rate_find           = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                    ->where('converted_unit_id', $data['unit_id'][$key])
                                    ->where('product_entry_id', $value)
                                    ->first();

                        $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['return_quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['return_quantity'][$key];

                        $issue_return_entries[] = [
                            'order_id'           => $data['order_id'][$key],
                            'issue_id'           => $data['issue_id'],
                            'issue_return_id'    => $issue_return['id'],
                            'sr_id'              => $data['sr_id'],
                            'product_id'         => $product['product_id'],
                            'product_entry_id'   => $value,
                            'main_unit_id'       => $data['main_unit_id'][$key],
                            'conversion_unit_id' => $data['unit_id'][$key],
                            'store_id'           => $data['store_id'][$key],
                            'quantity'           => round($converted_quantity_to_main_unit, 2),
                            'created_by'         => $user_id,
                            'created_at'         => date('Y-m-d H:i:s'),
                        ];

                        $sr_items[] = [
                            'order_id'              => $data['order_id'][$key],
                            'issue_id'              => $data['issue_id'],
                            'issue_return_id'       => $issue_return['id'],
                            'sr_id'                 => $data['sr_id'],
                            'date'                  => date('Y-m-d', strtotime($data['return_date'])),
                            'product_id'            => $product['product_id'],
                            'product_entry_id'      => $value,
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['unit_id'][$key],
                            'store_id'              => $data['store_id'][$key],
                            'type'                  => 3,
                            'quantity'              => round($converted_quantity_to_main_unit, 2),
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];
                    }

                    DB::table('issue_return_entries')->insert($issue_return_entries);
                    DB::table('sr_items')->insert($sr_items);

                    //Increase Main Stock Start
                        foreach ($old_issue_return_entries as $key => $value)
                        {
                            $old_item_entry_id[]        = $value['product_entry_id'];
                            $old_main_unit_id[]         = $value['main_unit_id'];
                            $old_converted_unit_id[]    = $value['converted_unit_id'];
                            $old_items_stock[]          = $value['quantity'];
                        }

                        foreach ($old_item_entry_id as $key2 => $value2)
                        { 
                            $quantity_add_to_product_entry                   = ProductEntries::find($value2);

                            $conversion_rate_find2              = UnitConversions::where('main_unit_id', $old_main_unit_id[$key2])
                                        ->where('converted_unit_id', $old_converted_unit_id[$key2])
                                        ->where('product_entry_id', $value2)
                                        ->first();

                            $converted_quantity_to_main_unit2   = $conversion_rate_find2 != null ? $old_items_stock[$key2]/$conversion_rate_find2['conversion_rate'] : $old_items_stock[$key2];

                            $quantity_add_to_product_entry->stock_in_hand    = $quantity_add_to_product_entry['stock_in_hand'] - round($converted_quantity_to_main_unit2, 2);
                            $quantity_add_to_product_entry->total_sold       = $quantity_add_to_product_entry['total_sold'] + round($converted_quantity_to_main_unit2, 2);
                            $quantity_add_to_product_entry->save();



                            $store                          = StoreInventories::where('store_id', $data['store_id'][$key2])
                                                                ->where('product_entry_id', $value2)
                                                                ->first();

                            $store->store_stock_in_hand     = $store['store_stock_in_hand'] - round($converted_quantity_to_main_unit2, 2);
                            $store->save();
                        }

                        foreach ($data['product_entries'] as $key4 => $value4)
                        {
                            $product_entries                    = ProductEntries::find($value4);

                            $conversion_rate_find1              = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key4])
                                        ->where('converted_unit_id', $data['unit_id'][$key4])
                                        ->where('product_entry_id', $value4)
                                        ->first();

                            $converted_quantity_to_main_unit1   = $conversion_rate_find1 != null ? $data['return_quantity'][$key4]/$conversion_rate_find1['conversion_rate'] : $data['return_quantity'][$key4];

                            $product_entries->stock_in_hand     = $product_entries['stock_in_hand'] + round($converted_quantity_to_main_unit1, 2);
                            $product_entries->total_sold        = $product_entries['total_sold'] - round($converted_quantity_to_main_unit1, 2);
                            $product_entries->save();


                            //Increase Store Stock Start
                            $store                          = StoreInventories::where('store_id', $data['store_id'][$key4])
                                                                ->where('product_entry_id', $value4)
                                                                ->first();

                            $store->store_stock_in_hand     = $store['store_stock_in_hand'] + round($converted_quantity_to_main_unit1, 2);
                            $store->save();
                            //Increase Store Stock End
                        }
                    //Increase Main Stock End

                    $delete_0 = SrItems::where('quantity', 0)->delete();

                    DB::commit();

                    if ($data['print'] == 1)
                    {
                        return back()->with("success","Products Received Successfully !!");
                    }
                }
            }catch (\Exception $exception){
                DB::rollback();
                dd($exception);
                return back()->with("unsuccess","Not Added");
            }
        }

        public function returnsConvertedUnitList($product_entry_id)
        {
            $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $product_entry_id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

            $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                        ->where('unit_conversions.product_entry_id', $product_entry_id)
                                                        ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

            $data                           = collect(array_merge($data1, $data2));


            return Response::json($data);
        }
    //Return End

    public function printDsmOrderList($issue_id)
    {
        $order_ids  = IssueOrders::where('issue_id', $issue_id)->get();

        foreach ($order_ids as $key => $value)
        {
            $data[$value['order_id']]['invoice']    = Orders::find($value['order_id']);
            $data[$value['order_id']]['entries']    = OrderEntries::where('order_entries.invoice_id', $value['order_id'])->orderBy('order_entries.id', 'DESC')->get();
        }

        if (isset($data))
        {
            $data = $data;
        }
        else
        {
            return back()->with('unsuccess', 'No order found !!');
        }

        return view('srissues::print_order_list', compact('data'));
    }

    public function dsmDailyReport()
    {
        return view('srissues::dsm_daily_report');
    }

    public function dsmDailyReportDetails()
    {
        $date       = date('Y-m-d');
        $from_date  = isset($_GET['from_date']) ? date('Y-m-d', strtotime($_GET['from_date'])) : date('Y-m-d', strtotime($date));
        $sr_id      = isset($_GET['sr_id']) ? $_GET['sr_id'] : 0;
        $sr         = Customers::find($sr_id);

        $receive    = SrItems::where('sr_items.date', $from_date)
                            ->where('sr_items.sr_id', $sr_id)
                            ->where('sr_items.type', 1)
                            ->sum('amount');
        $sales      = SrItems::where('sr_items.date', $from_date)
                            ->where('sr_items.sr_id', $sr_id)
                            ->where('sr_items.type', 2)
                            ->select('sr_items.*')
                            ->sum('amount');
        $return     = SrItems::where('sr_items.date', $from_date)
                            ->where('sr_items.sr_id', $sr_id)
                            ->where('sr_items.type', 3)
                            ->sum('amount');
        $damage     = SrItems::where('sr_items.date', $from_date)
                            ->where('sr_items.sr_id', $sr_id)
                            ->where('sr_items.type', 4)
                            ->sum('amount');    

        return view('srissues::dsm_daily_report_details', compact('sr', 'receive', 'return', 'damage', 'sales'));
    }

    //Convert To Invoice Start
    public function convertToInvoice($issue_id)
    {
        $issue          = Issues::find($issue_id);
        $issue_orders   = IssueOrders::where('issue_id', $issue_id)->get();

        foreach($issue_orders as $key => $issue_order)
        {
            $orders[]   = Orders::find($issue_order['order_id']);
            $return[]   = SrItems::where('sr_items.issue_id', $issue_id)
                            ->where('sr_items.order_id', $issue_order['order_id'])
                            ->where('sr_items.type', 3)
                            ->sum('amount');
            $damage[]   = SrItems::where('sr_items.issue_id', $issue_id)
                            ->where('sr_items.order_id', $issue_order['order_id'])
                            ->where('sr_items.type', 4)
                            ->sum('amount');
            $sales[]    = SrItems::where('sr_items.issue_id', $issue_id)
                            ->where('sr_items.order_id', $issue_order['order_id'])
                            ->where('sr_items.type', 2)
                            ->sum('amount');
        }

        return view('srissues::convert_to_invoice', compact('issue', 'orders', 'return', 'damage', 'sales'));
    }

    public function convertToInvoiceStore(Request $request)
    {
        $rules = array(
            'order_id.*'      => 'required',
            'return_amount.*' => 'required',
            'damage_amount.*' => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            foreach ($data['order_id'] as $key_val => $data_val)
            {   
                // $check_invoice  = Invoices::where('order_id', $data_val)->count();

                // if ($check_invoice == 0)
                // {
                    $order          = Orders::find($data_val);
                    $order_entries  = OrderEntries::where('invoice_id', $data_val)->get();
                    $vat            = $order['vat_amount'];
                 
                    //Calculating Total Discount
                        $discount = 0;
                        foreach ($order_entries as $key => $value) 
                        {
                            if ($value['discount_type'] == 0)
                            {
                                $discount   = $discount + (($value['discount']*$value['rate']*$value['quantity'])/100);
                            }
                            else
                            {
                                $discount   = $discount + $value['discount'];
                            }
                        }

                    //Calculating Total Buy Price
                        $buy_price      = 0;
                        foreach ($order_entries as $key => $value)
                        {
                            if($value['main_unit_id'] == $value['conversion_unit_id'])
                            {
                                $product    = ProductEntries::find($value['product_entry_id']);
                                $buy_price  = $buy_price + ($product['buy_price']*$value['quantity']);
                            }
                            else
                            {
                                $product1   = UnitConversions::where('product_entry_id', $value['product_entry_id'])
                                                                ->where('main_unit_id', $value['main_unit_id'])
                                                                ->where('converted_unit_id', $value['conversion_unit_id'])
                                                                ->first();
                                                        
                                $buy_price  = $buy_price + ($product1['purchase_price']*$value['quantity']);
                            }
                        }

                    $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
                    $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

                    $invoice                            = new Invoices;
                    $invoice->invoice_number            = $invoice_number;
                    $invoice->customer_id               = $order['customer_id'];
                    $invoice->order_id                  = $data_val;
                    $invoice->type                      = 1;
                    $invoice->reference_id              = $order['reference_id'];
                    $invoice->sr_id                     = $order['sr_id'] != 0 ? $order['sr_id'] : Null;
                    $invoice->invoice_date              = date('Y-m-d', strtotime($order['invoice_date']));
                    $invoice->invoice_amount            = $order['invoice_amount'];
                    $invoice->due_amount                = $order['invoice_amount'];
                    $invoice->total_buy_price           = round($buy_price, 2);
                    $invoice->total_discount            = $discount;
                    $invoice->invoice_note              = $order['invoice_note'];
                    $invoice->total_vat                 = $vat;
                    $invoice->vat_type                  = $order['vat_type'];
                    $invoice->total_discount_type       = $order['total_discount_type'];
                    $invoice->total_discount_amount     = $order['total_discount_amount'];
                    $invoice->total_discount_note       = $order['total_discount_note'];
                    $invoice->cash_given                = array_sum($data['paid']);
                    $invoice->change_amount             = $order['change_amount'];
                    $invoice->previous_due              = $order['previous_due'];
                    $invoice->previous_due_type         = $order['previous_due_type'];
                    $invoice->adjusted_amount           = $order['adjustment'];
                    $invoice->account_id                = $order['account_id'];
                    $invoice->created_by                = $user_id;

                    if ($invoice->save())
                    {
                        $order_update       = Orders::find($data_val);
                        $order_update->type = 2;
                        $order_update->save();

                        foreach ($order_entries as $key => $value)
                        {
                            $product_buy_price      = ProductEntries::find($value['product_entry_id']);
                            
                            $conversion_rate_find   = UnitConversions::where('main_unit_id', $value['main_unit_id'])
                                                                    ->where('converted_unit_id', $value['conversion_unit_id'])
                                                                    ->where('product_entry_id', $value['product_entry_id'])
                                                                    ->first();
                                                
                            $invoice_entries[] = [
                                'invoice_id'              => $invoice['id'],
                                'product_id'              => $product_buy_price['product_id'],
                                'product_entry_id'        => $value['product_entry_id'],
                                'customer_id'             => $invoice['customer_id'],
                                'main_unit_id'            => $value['main_unit_id'],
                                'conversion_unit_id'      => $value['conversion_unit_id'],
                                'reference_id'            => $value['reference_id'],
                                'buy_price'               => ($value['main_unit_id'] == $value['conversion_unit_id']) ? round($product_buy_price['buy_price'], 2) : $conversion_rate_find['purchase_price'],
                                'rate'                    => $value['rate'],
                                'quantity'                => $value['quantity'],
                                'total_amount'            => $value['total_amount'],
                                'discount_type'           => $value['discount_type'],
                                'discount_amount'         => $value['discount_amount'],
                                'created_by'              => $user_id,
                                'created_at'              => date('Y-m-d H:i:s'),
                            ];

                            $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $value['quantity']/$conversion_rate_find['conversion_rate'] : $value['quantity'];

                            if (isset($value['free_unit_id']))
                            {
                                $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $value['free_item_main_unit_id'])
                                                ->where('converted_unit_id', $value['free_unit_id'])
                                                ->where('product_entry_id', $value['product_entry_id'])
                                                ->first();

                                $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $value['free_quantity']/$free_conversion_rate_find['conversion_rate'] : $value['free_quantity'];
                            }
                            else
                            {
                                $free_converted_quantity_to_main_unit  = 0;
                            }

                            if ($order['sr_id'] != 0)
                            {
                                $sr_items[] = [
                                    'order_id'              => $value['invoice_id'],
                                    'issue_id'              => $data['issue_id'],
                                    'sales_id'              => $invoice['id'],
                                    'sr_id'                 => $order['dsm_id'],
                                    'date'                  => date('Y-m-d', strtotime($order['invoice_date'])),
                                    'product_id'            => $product_buy_price['product_id'],
                                    'product_entry_id'      => $value['product_entry_id'],
                                    'main_unit_id'          => $value['main_unit_id'],
                                    'conversion_unit_id'    => $value['conversion_unit_id'],
                                    'type'                  => 2,
                                    'quantity'              => $converted_quantity_to_main_unit,
                                    'rate'                  => $value['rate'],
                                    'amount'                => $value['total_amount'],
                                    'created_by'            => $user_id,
                                    'created_at'            => date('Y-m-d H:i:s'),
                                ];
                            }
                        }

                        DB::table('invoice_entries')->insert($invoice_entries);

                        if ($order['sr_id'] != 0)
                        {
                            DB::table('sr_items')->insert($sr_items);
                        }

                        // stockOut($data, $item_id=null);

                        //Financial Accounting Start
                            debit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=8, $amount=$order['invoice_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            credit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=$order['account_id'], $amount=$order['invoice_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                            //Insert into journal_entries 
                            if (isset($data['paid']))
                            {
                                for($i = 0; $i < count($data['paid']); $i++)
                                {
                                    if ($data['paid'][$i] > 0)
                                    {
                                        debit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=$order['current_balance_paid_through'][$i], $amount=$data['paid'][$i], $note=$order['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                        credit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=8, $amount=$data['paid'][$i], $note=$order['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                    }
                                }
                            }

                            customerBalanceUpdate($order['customer_id']);
                        //Financial Accounting End
                    }
                // }
                // else
                // {
                //     $order          = Orders::find($data_val);
                //     $order_entries  = OrderEntries::where('invoice_id', $data_val)->get();
                //     $vat            = $order['vat_amount'];
                 
                //     //Calculating Total Discount
                //         $discount = 0;
                //         foreach ($order_entries as $key => $value) 
                //         {
                //             if ($value['discount_type'] == 0)
                //             {
                //                 $discount   = $discount + (($value['discount']*$value['rate']*$value['quantity'])/100);
                //             }
                //             else
                //             {
                //                 $discount   = $discount + $value['discount'];
                //             }
                //         }

                //     //Calculating Total Buy Price
                //         $buy_price      = 0;
                //         foreach ($order_entries as $key => $value)
                //         {
                //             if($value['main_unit_id'] == $value['conversion_unit_id'])
                //             {
                //                 $product    = ProductEntries::find($value['product_entry_id']);
                //                 $buy_price  = $buy_price + ($product['buy_price']*$value['quantity']);
                //             }
                //             else
                //             {
                //                 $product1   = UnitConversions::where('product_entry_id', $value['product_entry_id'])
                //                                                 ->where('main_unit_id', $value['main_unit_id'])
                //                                                 ->where('converted_unit_id', $value['conversion_unit_id'])
                //                                                 ->first();
                                                        
                //                 $buy_price  = $buy_price + ($product1['purchase_price']*$value['quantity']);
                //             }
                //         }

                //     $invoice                            = Invoices::where('order_id', $data_val)->first();
                //     $invoice->customer_id               = $order['customer_id'];
                //     $invoice->order_id                  = $data_val;
                //     $invoice->type                      = 1;
                //     $invoice->reference_id              = $order['reference_id'];
                //     $invoice->sr_id                     = $order['sr_id'] != 0 ? $order['sr_id'] : Null;
                //     $invoice->invoice_date              = date('Y-m-d', strtotime($order['invoice_date']));
                //     $invoice->invoice_amount            = $order['invoice_amount'];
                //     $invoice->due_amount                = $order['invoice_amount'];
                //     $invoice->total_buy_price           = round($buy_price, 2);
                //     $invoice->total_discount            = $discount;
                //     $invoice->invoice_note              = $order['invoice_note'];
                //     $invoice->total_vat                 = $vat;
                //     $invoice->vat_type                  = $order['vat_type'];
                //     $invoice->total_discount_type       = $order['total_discount_type'];
                //     $invoice->total_discount_amount     = $order['total_discount_amount'];
                //     $invoice->total_discount_note       = $order['total_discount_note'];
                //     $invoice->cash_given                = $order['cash_given'];
                //     $invoice->change_amount             = $order['change_amount'];
                //     $invoice->previous_due              = $order['previous_due'];
                //     $invoice->previous_due_type         = $order['balance_type'];
                //     $invoice->adjusted_amount           = $order['adjustment'];
                //     $invoice->account_id                = $order['account_id'];
                //     $invoice->updated_by                = $user_id;

                //     if ($invoice->save())
                //     {
                //         $item_id            = InvoiceEntries::where('invoice_id', $invoice['id'])->get();
                //         $item_delete        = InvoiceEntries::where('invoice_id', $invoice['id'])->delete();
                //         $delete_sr_items    = SrItems::where('sales_id', $invoice['id'])->delete();

                //         foreach ($order_entries as $key => $value)
                //         {
                //             $product_buy_price      = ProductEntries::find($value['product_entry_id']);
                            
                //             $conversion_rate_find   = UnitConversions::where('main_unit_id', $value['main_unit_id'])
                //                                                     ->where('converted_unit_id', $value['conversion_unit_id'])
                //                                                     ->where('product_entry_id', $value['product_entry_id'])
                //                                                     ->first();
                                                
                //             $invoice_entries[] = [
                //                 'invoice_id'              => $invoice['id'],
                //                 'product_id'              => $product_buy_price['product_id'],
                //                 'product_entry_id'        => $value['product_entry_id'],
                //                 'customer_id'             => $invoice['customer_id'],
                //                 'main_unit_id'            => $value['main_unit_id'],
                //                 'conversion_unit_id'      => $value['conversion_unit_id'],
                //                 'reference_id'            => $value['reference_id'],
                //                 'buy_price'               => ($value['main_unit_id'] == $value['conversion_unit_id']) ? round($product_buy_price['buy_price'], 2) : $conversion_rate_find['purchase_price'],
                //                 'rate'                    => $value['rate'],
                //                 'quantity'                => $value['quantity'],
                //                 'total_amount'            => $value['total_amount'],
                //                 'discount_type'           => $value['discount_type'],
                //                 'discount_amount'         => $value['discount_amount'],
                //                 'created_by'              => $user_id,
                //                 'created_at'              => date('Y-m-d H:i:s'),
                //             ];

                //             $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $value['quantity']/$conversion_rate_find['conversion_rate'] : $value['quantity'];

                //             if (isset($value['free_unit_id']))
                //             {
                //                 $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $value['free_item_main_unit_id'])
                //                                 ->where('converted_unit_id', $value['free_unit_id'])
                //                                 ->where('product_entry_id', $value['product_entry_id'])
                //                                 ->first();

                //                 $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $value['free_quantity']/$free_conversion_rate_find['conversion_rate'] : $value['free_quantity'];
                //             }
                //             else
                //             {
                //                 $free_converted_quantity_to_main_unit  = 0;
                //             }

                //             if ($order['sr_id'] != 0)
                //             {
                //                 $sr_items[] = [
                //                     'order_id'              => $value['invoice_id'],
                //                     'issue_id'              => $data['issue_id'],
                //                     'sales_id'              => $invoice['id'],
                //                     'sr_id'                 => $order['dsm_id'],
                //                     'date'                  => date('Y-m-d', strtotime($order['invoice_date'])),
                //                     'product_id'            => $product_buy_price['product_id'],
                //                     'product_entry_id'      => $value['product_entry_id'],
                //                     'main_unit_id'          => $value['main_unit_id'],
                //                     'conversion_unit_id'    => $value['conversion_unit_id'],
                //                     'type'                  => 2,
                //                     'quantity'              => $converted_quantity_to_main_unit,
                //                     'rate'                  => $value['rate'],
                //                     'amount'                => $value['total_amount'],
                //                     'created_by'            => $user_id,
                //                     'created_at'            => date('Y-m-d H:i:s'),
                //                 ];
                //             }
                //         }

                //         DB::table('invoice_entries')->insert($invoice_entries);

                //         if ($order['sr_id'] != 0)
                //         {
                //             DB::table('sr_items')->insert($sr_items);
                //         }

                //         // stockOut($data, $item_id=null);

                //         $jour_ent_debit     = JournalEntries::where('invoice_id', $invoice->id)
                //                                 ->where('transaction_head', 'sales')
                //                                 ->where('debit_credit', 1)
                //                                 ->first();

                //         $jour_ent_credit    = JournalEntries::where('invoice_id', $invoice->id)
                //                                 ->where('transaction_head', 'sales')
                //                                 ->where('debit_credit', 0)
                //                                 ->first();

                //         //Financial Accounting Start
                //             debitUpdate($jour_ent_debit['id'], $customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=8, $amount=$order['invoice_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //             creditUpdate($jour_ent_credit['id'], $customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=$order['account_id'], $amount=$order['invoice_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //         //Financial Accounting End

                //         $jr_en = JournalEntries::where('invoice_id', $invoice->id)->select('id', 'date')->get()->toArray();
      
                //         //Insert into journal_entries Start
                //             if (isset($data['paid']))
                //             {
                //                 for($i = 0; $i < count($data['paid']); $i++)
                //                 {
                //                     if ($data['paid'][$i] > 0)
                //                     {   
                //                         $pay_debit  = JournalEntries::find($jr_en[$i]['id']);
                //                         $pay_credit = JournalEntries::find($jr_en[$i]['id'] + 1);

                //                         if ($pay_debit != null)
                //                         { 
                //                             debitUpdate($pay_debit['id'], $customer_id=$order['customer_id'], $date=$pay_debit['date'], $account_id=$pay_debit['account_id'], $amount=$data['paid'][$i], $note=$pay_debit['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                         }
                //                         else
                //                         {
                //                             debit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=$order['current_balance_paid_through'][$i], $amount=$data['paid'][$i], $note=$order['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                         }

                //                         if ($pay_credit != null)
                //                         { 
                //                             creditUpdate($pay_credit['id'], $customer_id=$order['customer_id'], $date=$pay_debit['date'], $account_id=8, $amount=$data['paid'][$i], $note=$pay_debit['note'], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                         }
                //                         else
                //                         {
                //                             credit($customer_id=$order['customer_id'], $date=$order['invoice_date'], $account_id=8, $amount=$data['paid'][$i], $note=$order['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //                         }
                //                     }
                //                 }
                //             }

                //             customerBalanceUpdate($order['customer_id']);
                //         //Insert into journal_entries End
                //     }
                // }
            }

            $issue_update               = Issues::find($data['issue_id']);
            $issue_update->has_invoice  = 1;
            $issue_update->save();

            DB::commit();
            return redirect()->route('all_issues')->with("success","Sales Created Successfully !!");
        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }
    //Convert To Invoice End
}
