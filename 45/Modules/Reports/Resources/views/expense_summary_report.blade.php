@extends('layouts.app')

@section('title', 'Expense Summary Report')

@section('styles')
    <style type="text/css">
        @media print {
            a[href]:after {
                content:"" !important;
            }

            header nav, footer {
                display: none;
            }

            @page {
                margin: 0cm ! important;
            }
        }
    </style>
@endsection

<!-- <style type="text/css">
    .select2-container--default .select2-results>.select2-results__options {
        width: 400px;
        background-color: #fff;
    }

    .select2-container--default .select2-search--dropdown {
        width: 400px;
    }
</style> -->

@section('content')
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Expense Summary Report</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Reports</a></li>
                                    <li class="breadcrumb-item active">Expense Summary Report</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body table-responsive">
                            	<div class="row">
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                    <div class="col-md-6 col-xs-12 col-sm-12">
                                        <h2 style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['organization_name'] }}</h2>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['address'] }}</p>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px">{{ $user_info['contact_number'] }}</p>
                                        <h4 style="text-align: center;margin-bottom: 0px;margin-top: 0px">Expense Summary Report</h4>
                                        <p style="text-align: center;margin-bottom: 0px;margin-top: 0px"><strong>From</strong> {{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }} <strong>To</strong> {{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}</p>
                                    </div>
                                    <div class="col-md-3 col-xs-12 col-sm-12"></div>
                                </div>

                                <br>

                                <form method="get" action="{{ route('expense_summary_report_index') }}" enctype="multipart/form-data">
                                    <div class="form-group row mb-12">
                                        <div style="margin-bottom: 10px" class="col-lg-4 col-md-4 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <input id="from_date" name="from_date" type="text" value="{{ isset($_GET['from_date']) ? date('d-m-Y', strtotime($_GET['from_date'])) : date('d-m-Y', strtotime($from_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="from_date">
                                                <input id="to_date" name="to_date" type="text" value="{{ isset($_GET['to_date']) ? date('d-m-Y', strtotime($_GET['to_date'])) : date('d-m-Y', strtotime($to_date)) }}" class="form-control" data-provide="datepicker"  data-date-format="dd-mm-yyyy" data-date-autoclose="true" name="to_date">
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="type" style="width: 100" class="form-control select2" name="category_id">
                                                    <option value="0" selected>--All Category--</option>
                                                    @if(!empty($expense_categories) && ($expense_categories->count() > 0))
                                                    @foreach($expense_categories as $key => $value)
                                                        <option {{ isset($_GET['category_id']) && ($_GET['category_id'] == $value['id']) ? 'selected' : '' }} value="{{ $value['id'] }}">{{ $value['account_name'] }}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-2 col-md-2 col-sm-12 col-12 d-print-none">
                                            <div class="input-daterange input-group">
                                                <select id="customer_id" style="width: 100" class="form-control select2" name="customer_id">
                                                    <option value="{{ $customer_name != null ? $customer_name->id : 0 }}" selected>{{ $customer_name != null ? $customer_name->name : '--All Contacts--' }}</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-6 d-print-none">
                                            <input data-repeater-create type="submit" class="btn btn-info inner" value="Search"/>
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1" title="Print"><i class="fa fa-print"></i></a>
                                            <a onclick="downloadExcel()" class="btn btn-secondary waves-effect waves-light mr-1" title="Download Excel"><i class="far fa-file-excel"></i></a>
                                        </div>
                                    </div>
                                </form>

                                <table id="mainTable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Category</th>
                                            <th style="text-align: right">Amount</th>
                                        </tr>
                                    </thead>


                                    <tbody>

                                        <?php
                                            $serial         = 1; 
                                            $total_amount   = 0; 
                                        ?>
                                        
                                        @if(!empty($expenses) && ($expenses->count() > 0))
                                        @foreach($expenses as $key => $value)
                                            <tr>
                                                <td>{{ $serial }}</td>
                                                <td>{{ $key }}</td>
                                                <td style="text-align: right">{{  number_format($value->sum('amount'),2,'.',',') }}</td>
                                            </tr>

                                            <?php 
                                                $serial++;
                                                $total_amount   = $total_amount + $value->sum('amount');
                                            ?>

                                        @endforeach
                                        @endif

                                    </tbody>
                                    <tr>
                                        <th style="text-align: right"></th>
                                        <th style="text-align: right">Total</th>
                                        <th style="text-align: right">{{ number_format($total_amount,2,'.',',') }}</th>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
    		</div>
		</div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    $( document ).ready(function() {
        
        var site_url            = $('.site_url').val();

        $("#customer_id").select2({
            ajax: { 
            url:  site_url + '/invoices/customer/list/invoices',
            type: "get",
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    searchTerm: params.term // search term
                };
            },
            processResults: function (response) {
                return {
                    results: response
                };
            },
                cache: true
            },

            minimumInputLength: 0,
            escapeMarkup: function(result) {
                return result;
            },
            templateResult: function (result) {
                if (result.loading) return 'Searching...';
 
                return result['text'];
            },
        });
    });
</script>

<script src="{{ url('public/admin_panel_assets/report_download_assets/jquery.min.js') }}"></script>
<script src="{{ url('public/admin_panel_assets/report_download_assets/jquery.table2excel.min.js') }}"></script>

<script type="text/javascript">
    function downloadExcel()
    {   
        $("#mainTable").table2excel({
            filename: "Expense Summary Report.xls"
        });
    }
</script>
@endsection