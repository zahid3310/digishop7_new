<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('salesreturn')->group(function() {
    Route::get('/', 'SalesReturnController@index')->name('sales_return_index');
    Route::post('/store', 'SalesReturnController@store')->name('sales_return_store');
    Route::get('/show/{id}', 'SalesReturnController@show')->name('sales_return_show');
    Route::get('/delete/{id}', 'SalesReturnController@delete')->name('sales_return_delete');
    Route::get('/invoice-list-by-customer/{id}', 'SalesReturnController@invoiceList')->name('sales_return_invoice_list');
    Route::get('/invoice-entries-list-by-invoice/{id}', 'SalesReturnController@invoiceEntriesList')->name('sales_return_invoice__entries_list');
    Route::get('/sales-return/list/load', 'SalesReturnController@salesReturnListLoad')->name('sales_return_list_load');
    Route::get('/sales-return/search/list/{id}', 'SalesReturnController@salesReturnListSearch')->name('sales_return_list_search');
    Route::get('/find-customer-name/{id}', 'SalesReturnController@findCustomerName')->name('sales_return_find_customer_name');
    Route::get('/find-invoice-details/{id}', 'SalesReturnController@findInvoiceDetails')->name('sales_return_find_invoice_details');
    Route::get('/calculate-opening-balance/{id}', 'SalesReturnController@calculateOpeningBalance');
});
