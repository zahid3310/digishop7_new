<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class ParentAccountTypes extends Model
{  
    protected $table = "parent_account_types";
}
