<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IncomeCategories extends Model
{
    protected $table = "income_categories";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }
}
