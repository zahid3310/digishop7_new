<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class MessageDeliveries extends Model
{  
    protected $table = "message_deliveries";

    public function message()
    {
        return $this->belongsTo('App\Models\Messages','message_id');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customers','customer_id');
    }

    public function phoneBook()
    {
        return $this->belongsTo('App\Models\PhoneBook','phone_book_id');
    }

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

}
