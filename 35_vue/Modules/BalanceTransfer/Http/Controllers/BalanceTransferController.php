<?php

namespace Modules\BalanceTransfer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

use App\Models\PaidThroughAccounts;
use App\Models\AccountTransactions;
use Response;
use DB;

class BalanceTransferController extends Controller
{
    public function index()
    {   
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $transfers          = AccountTransactions::where('type', 2)->orderBy('created_at', 'DESC')->get();

        return view('balancetransfer::index', compact('paid_accounts', 'transfers'));
    }

    public function create()
    {
        return view('balancetransfer::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $transfer                   = new AccountTransactions;
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->type             = 2;
            $transfer->created_by       = $user_id;

            if ($transfer->save())
            {
                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {   
        return view('balancetransfer::show');
    }

    public function edit($id)
    {
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $transfers          = AccountTransactions::where('type', 2)->orderBy('created_at', 'DESC')->get();
        $find_transfer      = AccountTransactions::find($id);

        return view('balancetransfer::edit', compact('paid_accounts', 'transfers', 'find_transfer'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $transfer                   = AccountTransactions::find($id);
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->type             = 2;
            $transfer->updated_by       = $user_id;

            if ($transfer->save())
            {
                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }
}
