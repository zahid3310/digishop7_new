<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> @yield('title')</title>
    <meta name="robots" content="noindex, follow" />
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png">

    <!-- All CSS is here
    ============================================ -->

    <script>
        WebFontConfig = {
            google: { families: ['Open+Sans:400,600,700', 'Poppins:400,600,700'] }
        };
        (function (d) {
            var wf = d.createElement('script'), s = d.scripts[0];
            wf.src = 'js/webfont.js';
            wf.async = true;
            s.parentNode.insertBefore(wf, s);
        })(document);
    </script>

    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/animate/animate.min.css') }}">

    <!-- Plugins CSS File -->
    <!-- <link rel="stylesheet" type="text/css" href="vendor/magnific-popup/magnific-popup.min.css"> -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/vendor/owl-carousel/owl.carousel.min.css') }}">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/css/demo1.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/css/demo3.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('public/front_end_asset/css/style.min.css') }}">

    <style type="text/css">
        .dashboard-menu-active
        {
            color: #d26e4b!important;
            font-weight: bold!important;
            font-size: 13px;
        }
    </style>
    @yield('styles')

</head>

<body class="home">
    <div class="page-wrapper">
        @include('frontend::layouts.header')
        @yield('main_body')
        @include('frontend::layouts.footer')
    </div>

    <!-- Scroll Top -->
    <a id="scroll-top" href="#top" title="Top" role="button" class="scroll-top"><i class="fas fa-chevron-up"></i></a>

    @include('frontend::layouts.mobile_menu')
    <div class="itemAddedToCartMessage" style="padding: 10px 20px;background-color: #f6a800;font-size: 20px;text-align: center;position: fixed;left: 0;bottom: 0;z-index: 999;color: white;display: none;"></div>
    <!-- Plugins JS File -->
    <script src="{{ url('public/front_end_asset/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/elevatezoom/jquery.elevatezoom.min.js') }}"></script>
    <!-- <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script> -->

    <script src="{{ url('public/front_end_asset/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ url('public/front_end_asset/vendor/sticky/sticky.min.js') }}"></script>
    <!-- Main JS File -->
    <script src="{{ url('public/front_end_asset/js/main.js') }}"></script>
    <input type="hidden" class="site_url" value="{{ url('/') }}" />

    @yield('scripts')
    @stack('scripts')

    <script type="text/javascript">
        var site_url  = $('.site_url').val();
        // call quick cart
        getQuickCartItems();

        // item add to cart
        $('.addItemToCart').click(function(){
            var url         = $(this).data('url');
            var id          = $(this).data('id');
            var title       = $(this).data('title');
            var quantity    = $(this).data('quantity');




            if (quantity == undefined)
            {
                var get_qty     = $(".cartQty").val();
                var quantity    = parseFloat(get_qty);
            }

            var thumb       = $(this).data('thumb');
            var price       = $(this).data('price');
            var subprice       = quantity*price;

            // cart item set to session
            var items = JSON.parse(localStorage.getItem("cartItems"));

            if (items == null) {
                var items = new Array();
            }

            // var arrayLength = items.length;

                    for(var i = 0; i < items.length; i++)
                    {
                        if(items[i]['productId'] === id)
                        {
                        alert('item already add to cart');
                        exit();
                        }
                
                    }

                    items.push({
                        'productUrl':url,
                        'productId':id,
                        'productTitle':title,
                        'productQuantity':quantity,
                        'productPrice':price,
                        'productThumb': thumb,
                        'productSubTotal': subprice,
                    });


                    localStorage.setItem("cartItems", JSON.stringify(items));

                    // get cart items
                    getQuickCartItems();

                    $('.itemAddedToCartMessage').html('Item Added To Cart');
                    $('.itemAddedToCartMessage').show(1000);

                    // var site_url            = $('.site_url').val();
                    // window.location.href    = site_url + "/cart-details";

                    setInterval(function () {
                     $('.itemAddedToCartMessage').hide(1000);
                    }, 3000);
        });

        // remove item from cart
        function cartItemRemove(id){
            var getItems = JSON.parse(localStorage.getItem("cartItems"));
            if (getItems != null) {
                var items = new Array();
                $(getItems).each(function( index, value ) {
                    if(id == value.productId){
                    }else{
                        items.push({'productUrl':value.productUrl, 'productId':value.productId, 'productTitle':value.productTitle, 'productQuantity':value.productQuantity, 'productPrice':value.productPrice, 'productThumb':value.productThumb}); 
                    }
                });
                if (items.length == 0) {
                    localStorage.removeItem("cartItems");
                }else{
                    localStorage.setItem("cartItems", JSON.stringify(items));
                }
                getQuickCartItems();
            }
        }


        function incrementCartItem(id){


            var quantity_value = parseInt(document.getElementById("productid"+id).value, 10);
            quantity_value = isNaN(quantity_value) ? 0 : quantity_value;
            quantity_value++;

            document.getElementById("productid"+id).value = quantity_value;
              var currentProductPrice  = document.getElementById("currentProductPrice"+id).innerHTML;
              var multiplication = quantity_value*currentProductPrice;
              $('#updatetotalCartPrice'+id).html(multiplication);
              var items = JSON.parse(localStorage.getItem("cartItems"));


             items.forEach(function (item){
                if(item.productId == id) {
                    item.productSubTotal       = parseFloat(multiplication);
                    item.productQuantity       = parseFloat(quantity_value);

                }
            })            
             localStorage.clear();
             localStorage.setItem("cartItems", JSON.stringify(items));

        }
        function deincrementCartItem(id){


            var quantity_value = parseInt(document.getElementById("productid"+id).value, 10);
            quantity_value = isNaN(quantity_value) ? 0 : quantity_value;
            if (1 < quantity_value) {
            quantity_value--;

            document.getElementById("productid"+id).value = quantity_value;
            // var quantity_value =document.getElementById("productid"+id).value;
              var currentProductPrice  = document.getElementById("currentProductPrice"+id).innerHTML;
              var multiplication = quantity_value*currentProductPrice;
              $('#updatetotalCartPrice'+id).html(multiplication);

               var items = JSON.parse(localStorage.getItem("cartItems"));


             items.forEach(function (item){
                if(item.productId == id) {
                    // console.log(item);
                    item.productSubTotal       = parseFloat(multiplication);
                    item.productQuantity       = parseFloat(quantity_value);
                    // localStorage.setItem("cartItems", JSON.stringify(item));
                    // console.log(item);
                }
            })            
             localStorage.clear();
             console.log(items);
             localStorage.setItem("cartItems", JSON.stringify(items));
         }
        }

        // get quick cart item
        function getQuickCartItems(){
            // $('.dropdown-cart-products').empty();
            var getItems = JSON.parse(localStorage.getItem("cartItems"));
            if (getItems == null) {
                $('.showQuickShoppingCart').empty();
                $('.showQuickShoppingCart').append('<li class="noItemToCart"><p style="margin-bottom: 0px;text-align:center;font-size: 14px">No item in cart!</p></li>');
                $('.getCartDetails').empty();
                $('.getCartDetails').append('<li class="noItemToCart"><p style="margin-bottom: 0px;text-align:center;font-size: 14px">No item in cart!</p></li>');
                $('.emptyCartButton').hide();
            }
            else
            {
                $('.showQuickShoppingCart').empty();
                $('.getCartDetails').empty();
                $('.emptyCartButton').show();
                $('.noItemToCart').hide();
            }
            var totalCartPrice = parseFloat(0);
            var cartItemCount = 0;
            $(getItems).each(function( index, value ) {


                // $(document).ready(function() {
                //     $('.quantity_item').click(function() {
                //        var increments_quantity = $('.quantity').val();
                //        alert(increments_quantity);
                //        $('.updatetotalCartPrice').html(value.productPrice*increments_quantity);
                //     });
                // });

                $('.showQuickShoppingCart').append('<div class="product product-cart">' +
                                                        '<div class="product-detail">' + 
                                                            '<a href="#" class="product-name">'+value.productTitle+'</a>' +
                                                            '<div class="price-box">' +
                                                                '<span class="product-quantity">'+value.productQuantity+'</span>' +
                                                                '<span class="product-price">'+value.productPrice+'</span>' +
                                                            '</div>' +
                                                        '</div>' +
                                                        '<figure class="product-media">' +
                                                            '<a href="#">' +
                                                                '<img src="'+value.productThumb+'" alt="product" width="50" height="50" />' +
                                                            '</a>' +
                                                            '<button class="btn btn-link btn-close" onclick="cartItemRemove('+value.productId+')">' +
                                                                '<i class="fas fa-times"></i>' +
                                                            '</button>' +
                                                        '</figure>' +
                                                    '</div>');

                // $('.getCartDetails').append('<tr><td class="product-remove"><a href="javascript:void(0)" onclick="cartItemRemove('+value.productId+')"><i class="dlicon ui-1_simple-remove"></i></a></td><td class="product-img"><a href="javascript:void(0)" onclick="cartItemRemove('+value.productId+')"><img style="height: 80px;width: 80px" src="'+ site_url + '/public/images/products/' + value.productThumb+'" alt=""></a></td><td class="product-name"><a href="javascript:void(0)" onclick="cartItemRemove('+value.productId+')">'+value.productTitle+'</a></td><td class="product-price"><span class="amount">'+value.productPrice+'</span></td><td class="cart-quality"><div class="product-details-quality quality-width-cart"><div class="cart-plus-minus"><input class="cart-plus-minus-box" type="text" name="qtybutton" value="'+value.productQuantity+'"></div></div></td><td class="product-total"><span>'+ value.productPrice +'</span></td></tr>');

                $('.getCartDetails').append('<tr><td class="product-thumbnail"><figure><a href="product-simple.html"><img src="'+value.productThumb+'" width="100" height="100"alt="product"></a><a onclick="cartItemRemove('+value.productId+')"class="product-remove" title="Remove this product"><i class="fas fa-times"></i></a></figure></td><td class="product-name"><div class="product-name-section"><a href="product-simple.html">'+value.productTitle+'</a></div></td><td class="product-subtotal"><span id="currentProductPrice'+value.productId+'">'+value.productPrice+'</span></td><td class="product-quantity"><div class="input-group"><button onclick="deincrementCartItem('+value.productId+')" class="quantity-minus quantity_item d-icon-minus"></button><input class="form-control" type="number" name="qtybutton" id="productid'+value.productId+'" value="'+value.productQuantity+'"><button class="quantity-plus quantity_item d-icon-plus" id="productid'+value.productId+'"  onclick="incrementCartItem('+value.productId+')"></button></div></td><td class="product-price"><span id="updatetotalCartPrice'+value.productId+'">'+value.productSubTotal+'</span></td></tr>');



                $('.checkout_details').append('<li class="list-group-item d-flex justify-content-between align-items-center">' + value.productTitle + ' X ' + value.productQuantity+'<input type="hidden" name="productId[]" value="'+value.productId+'">'+ '<input type="hidden" name="quantity[]" value="'+value.productQuantity+'">'+ '<input type="hidden" name="rate[]" value="'+value.productPrice+'">' +'<span class="badge badge-primary badge-pill">'+ value.productSubTotal +'</span></li>');
              

                totalCartPrice += parseFloat(value.productPrice)*parseFloat(value.productQuantity);
                cartItemCount = cartItemCount+1;
            });
            // cartItemCount
            localStorage.setItem("totalCartItem", cartItemCount);
            $('.totalCartItem').html(cartItemCount + ' items');
            $('.totalCartItemBdt').html(cartItemCount);
            // totalCartPrice
            localStorage.setItem("totalCartPrice", totalCartPrice);
            $('.totalCartPrice').html(totalCartPrice);
            $('.totalCartPrice2').val(totalCartPrice);
            $('.totalCartPriceBdt').html('BDT ' + totalCartPrice);
        }

        // empty cart
        function emptyCart(){
            $('.showQuickShoppingCart').empty();
            $('.showQuickShoppingCart').append('<li class="noItemToCart"><p style="margin-bottom: 0px;text-align:center;font-size: 14px">No item in cart!</p></li>');
            $('.getCartDetails').empty();
            $('.getCartDetails').append('<li class="noItemToCart"><p style="margin-bottom: 0px;text-align:center;font-size: 14px">No item in cart!</p></li>');
            $('.totalCartItem').html('0');
            $('.totalCartPrice').html('0.00');
            $('.emptyCartButton').hide();
            localStorage.removeItem("cartItems")
            localStorage.removeItem("totalCartItem");
            localStorage.removeItem("totalCartPrice");
        }

        function update_cart() {
               window.location.reload();
            }
    </script>
    
</body>

</html>