<?php

namespace Modules\Membership\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Customers;
use App\Models\Users;
use App\Models\Memberships;
use App\Models\MembershipPackages;
use Response;
use DB;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
        $memberships  = Memberships::select('memberships.*','memberships_package.package_name','customers.name as customers_name','customers.phone as customers_phone')
                                    ->leftJoin('memberships_package','memberships_package.id','=','memberships.membership_package_id')
                                    ->leftJoin('customers','customers.id','=','memberships.customer_id')
                                    ->orderBy('created_at', 'DESC')->get();
        // dd($memberships);
        $MembershipPackages  = MembershipPackages::orderBy('created_at', 'DESC')->get();
        return view('membership::index',compact('memberships','MembershipPackages'));
    }

    public function membershipPackage()
    {
        $membershipPackages  = MembershipPackages::all();
        return view('membership::membership_package',compact('membershipPackages'));
    }


    public function membershipStore(Request $request)
    {

        $user_id    = Auth::user()->id;

        DB::beginTransaction();

        try{

            // $data_find                                  = Memberships::orderBy('created_at', 'DESC')->first();

            // $getLastYear = substr ($data_find['card_number'], -4);

            // $currentYear = date('Y');

            // $cardNumber = 0;

            // if ($currentYear > $getLastYear) {
            //     $card_number                                = ++$cardNumber;
            // }else
            // {
            //     $card_number                                = $data_find != null ? $data_find['id'] + 1 : 1;
            // }

            $Memberships                                = new Memberships;
            // $Memberships->card_number                   = $card_number.'/'.$currentYear;card_number
            $Memberships->card_number                   = $request->card_number;
            $Memberships->customer_id                   = $request->customer_id;
            $Memberships->customer_id                   = $request->customer_id;
            $Memberships->membership_package_id         = $request->membership_package_id;
            $Memberships->discount                      = $request->discount;
            $Memberships->created_by                    = $user_id;
            DB::commit();
            $Memberships->save();
            return back()->with("success","Memberships Added Successfully !!");
            
        }catch (\Exception $exception){
            dd($exception);
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }

    }

    public function membershipPackageStore(Request $request)
    {

        $user_id    = Auth::user()->id;

        DB::beginTransaction();

        try{
            $Memberships_Package                                = new MembershipPackages;
            $Memberships_Package->package_name                  = $request->package_name;
            $Memberships_Package->discount                      = $request->discount;
            $Memberships_Package->created_by                    = $user_id;

            DB::commit();

            $Memberships_Package->save();
            return back()->with("success","Memberships Package Added Successfully !!");

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }

    }

    public function membershipPackageEdit($id)
    {
        $data = MembershipPackages::find($id);
        $membershipPackages  = MembershipPackages::all();
        return view('membership::package_edit',compact('data','membershipPackages'));
    }

    public function membershipPackageUpdate(Request $request,$id)
    {
        $user_id    = Auth::user()->id;
        $data = MembershipPackages::find($id);
        $data->package_name                  = $request->package_name;
        $data->discount                      = $request->discount;
        $data->updated_by                    = $user_id;
        $data->save();
        return back()->with("success","Memberships Package update Successfully !!");
    }


    public function membershipdiscount($membershipdiscount)
    {
        $membershipdiscount  = MembershipPackages::find($membershipdiscount);
        return Response::json($membershipdiscount);
    }

    public function show($id)
    {
        return view('membership::show');
    }

    public function edit($id)
    {
        $data = Memberships::select('memberships.*','customers.name as customers_name')
                            ->leftJoin('customers','customers.id','=','memberships.customer_id')
                            ->find($id);
        $memberships  = Memberships::select('memberships.*','memberships_package.package_name','customers.name as customers_name','customers.phone as customers_phone')
                                    ->leftJoin('memberships_package','memberships_package.id','=','memberships.membership_package_id')
                                    ->leftJoin('customers','customers.id','=','memberships.customer_id')
                                    ->orderBy('created_at', 'DESC')->get();


        $MembershipPackages  = MembershipPackages::orderBy('created_at', 'DESC')->get();

        return view('membership::edit',compact('MembershipPackages','memberships','data'));
    }


    public function update(Request $request, $id)
    {
        $data = Memberships::find($id);
        $data->discount     = $request->discount;
        $data->membership_package_id  = $request->membership_package_id;
        $data->card_number  = $request->card_number;
        $data->save();
        return back()->with("success","Memberships update Successfully !!");

    }

    public function destroy($id)
    {
      $info =    DB::table('memberships')->where('id', $id)->delete();
      return redirect()->route('membership_index')->with("success","Memberships update Successfully !!");
    }
}
