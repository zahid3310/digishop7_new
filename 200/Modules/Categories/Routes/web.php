<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('categories')->group(function() {
    Route::get('/', 'CategoriesController@index')->name('categories_index');
    Route::post('/store', 'CategoriesController@store')->name('categories_store');
    Route::get('/edit/{id}', 'CategoriesController@edit')->name('categories_edit');
    Route::post('/update/{id}', 'CategoriesController@update')->name('categories_update');
    Route::get('/list/ajax', 'CategoriesController@listAjax')->name('categories_list_ajax');
    Route::get('/store/ajax', 'CategoriesController@storeAjax')->name('categories_store_ajax');
});
