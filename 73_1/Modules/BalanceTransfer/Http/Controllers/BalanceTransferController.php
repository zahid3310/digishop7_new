<?php

namespace Modules\BalanceTransfer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

use App\Models\PaidThroughAccounts;
use App\Models\AccountTransactions;
use App\Models\CurrentBalance;
use Response;
use DB;

class BalanceTransferController extends Controller
{
    public function index()
    {   
        $branch_id          = Auth::user()->branch_id;
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $transfers          = AccountTransactions::where('type', 2)->where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();

        return view('balancetransfer::index', compact('paid_accounts', 'transfers'));
    }

    public function create()
    {
        return view('balancetransfer::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $branch_id  = Auth::user()->branch_id;
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $transfer                   = new AccountTransactions;
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->branch_id        = $branch_id;
            $transfer->type             = 2;
            $transfer->created_by       = $user_id;

            if ($transfer->save())
            {   
                $transfer_current_balance                           = new CurrentBalance;
                $transfer_current_balance->transaction_date         = date('Y-m-d', strtotime($data['transfer_date']));
                $transfer_current_balance->account_transaction_id   = $transfer->id;
                $transfer_current_balance->transfer_from            = $data['transfer_from'];
                $transfer_current_balance->transfer_to              = $data['transfer_to'];
                $transfer_current_balance->amount                   = $data['amount'];
                $transfer_current_balance->note                     = $data['note'];
                $transfer_current_balance->type                     = 2;
                $transfer_current_balance->branch_id                = $branch_id;
                $transfer_current_balance->created_by               = $user_id;
                $transfer_current_balance->save();

                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Created Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {   
        return view('balancetransfer::show');
    }

    public function edit($id)
    {   
        $branch_id          = Auth::user()->branch_id;
        $paid_accounts      = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')->get();
        $transfers          = AccountTransactions::where('type', 2)->where('branch_id', $branch_id)->orderBy('created_at', 'DESC')->get();
        $find_transfer      = AccountTransactions::find($id);

        return view('balancetransfer::edit', compact('paid_accounts', 'transfers', 'find_transfer'));
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'transfer_date' => 'required',
            'transfer_from' => 'required',
            'transfer_to'   => 'required',
            'amount'        => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $transfer                   = AccountTransactions::find($id);
            $transfer->transaction_date = date('Y-m-d', strtotime($data['transfer_date']));
            $transfer->transfer_from    = $data['transfer_from'];
            $transfer->transfer_to      = $data['transfer_to'];
            $transfer->amount           = $data['amount'];
            $transfer->note             = $data['note'];
            $transfer->type             = 2;
            $transfer->updated_by       = $user_id;

            if ($transfer->save())
            {   
                $transfer_current_balance                           = CurrentBalance::where('account_transaction_id', $id)->first();
                $transfer_current_balance->transaction_date         = date('Y-m-d', strtotime($data['transfer_date']));
                $transfer_current_balance->account_transaction_id   = $transfer->id;
                $transfer_current_balance->transfer_from            = $data['transfer_from'];
                $transfer_current_balance->transfer_to              = $data['transfer_to'];
                $transfer_current_balance->amount                   = $data['amount'];
                $transfer_current_balance->note                     = $data['note'];
                $transfer_current_balance->type                     = 2;
                $transfer_current_balance->updated_by               = $user_id;
                $transfer_current_balance->save();

                DB::commit();
                return redirect()->route('balance_transfer_index')->with("success","Transfer Updated Successfully !!");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }
}
