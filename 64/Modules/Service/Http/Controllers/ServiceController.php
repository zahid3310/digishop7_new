<?php

namespace Modules\Service\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Models\PaidThroughAccounts;
use App\Models\Services;
use App\Models\AccountTransactions;
use App\Models\CurrentBalance;
use App\Models\Customers;
use App\Models\Users;
use Validator;
use Auth;
use DB;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Renderable
     */
    public function index()
    {
         $paid_accounts          = PaidThroughAccounts::orderBy('paid_through_accounts.created_at', 'ASC')
                                        ->get();

         $data = Services::leftjoin('customers','customers.id','service.customer_id')
                            ->select('service.*','customers.name as customer_name','customers.address as customer_address','customers.phone as customer_phone')
                            ->get();
         $customer = Customers::where('contact_type',0)->where('id','!=',1)->get();
        return view('service::index',compact('paid_accounts','data','customer'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Renderable
     */
    public function create()
    {
        return view('service::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Renderable
     */
    public function store(Request $request)
    {
        // $rules = array(
        //     'expense_date'          => 'required',
        //     'expense_category_id'   => 'required',
        //     'amount'                => 'required',
        //     'paid_through'          => 'required',
        // );

        // $validation = Validator::make(\Request::all(),$rules);

        // if ($validation->fails()) {
        //     return redirect()->back()->withInput()->withErrors($validation);
        // }

        $user_id    = Auth::user()->id;
        $branch_id  = Auth::user()->branch_id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $data_find                      = Services::orderBy('created_at', 'DESC')->first();
            $service_number                 = $data_find != null ? $data_find['service_number'] + 1 : 1;
   
            $service                        = new Services;
            $service->service_number        = $service_number;
            $service->service_date          = date('Y-m-d', strtotime($data['service_date']));
            $service->customer_id           = $data['customer_id'];
            $service->service_name          = $data['service_name'];
            $service->service_charge        = $data['service_charge'];
            $service->cash_given            = $data['cash_given'];
            $service->paid_through          = $data['paid_through'];
            $service->description           = $data['description'];
            $service->branch_id             = $branch_id;
            $service->created_by            = $user_id;

            if ($service->save())
            {  
                // $account_transaction                      = new AccountTransactions;
                // $account_transaction->transaction_date    = date('Y-m-d', strtotime($data['service_date']));
                // $account_transaction->amount              = $data['service_charge'];
                // $account_transaction->paid_through_id     = $data['paid_through'];
                // // $account_transaction->account_information = $data['account_information'];
                // // $account_transaction->note                = $data['note'];
                // $account_transaction->type                = 0;
                // $account_transaction->transaction_head    = 'income';
                // $account_transaction->associated_id       = $service->id;
                // $account_transaction->branch_id           = $branch_id;
                // $account_transaction->created_by          = $user_id;
                // $account_transaction->save();

                // $current_balance                      = new CurrentBalance;
                // $current_balance->transaction_date    = date('Y-m-d', strtotime($data['service_date']));
                // $current_balance->amount              = $data['service_charge'];
                // $current_balance->paid_through_id     = $data['paid_through'];
                // // $current_balance->account_information = $data['account_information'];
                // $current_balance->note                = $data['service_name'];
                // $current_balance->type                = 0;
                // $current_balance->transaction_head    = 'income';
                // $current_balance->associated_id       = $service->id;
                // $current_balance->branch_id           = $branch_id;
                // $current_balance->created_by          = $user_id;
                // $current_balance->save();
                DB::commit();
                return back()->with("success","Service Created Successfully !!");
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function show($id)
    {
         $data = Services::leftjoin('customers','customers.id','service.customer_id')
                            ->where('service.id',$id)
                            ->select('service.*','customers.name as customer_name','customers.address as customer_address','customers.phone as customer_phone')
                            ->first();
        $user_info  = Users::find(1);
        return view('service::show',compact('data','user_info'));
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Renderable
     */
    public function edit($id)
    {
        return view('service::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Renderable
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Renderable
     */
    public function destroy($id)
    {
        //
    }
}
