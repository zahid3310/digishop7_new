<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('service')->group(function() {
    Route::get('/', 'ServiceController@index')->name('service_index');
    Route::post('/service-store', 'ServiceController@store')->name('service_store');
    Route::get('/service-show/{id}', 'ServiceController@show')->name('service_show');
});
