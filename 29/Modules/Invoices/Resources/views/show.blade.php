@extends('layouts.app')

@section('title', 'Show')

<style type="text/css">
    @media print {
        #footerInvoice {
            position: fixed;
            bottom: 60;
        }
    }

    .textstyle {
        background-color: white; /* Changing background color */
        font-weight: bold; /* Making font bold */
        border-radius: 20px; /* Making border radius */
        border: 3px solid black; /* Making border radius */
        width: auto; /* Making auto-sizable width */
        height: auto; /* Making auto-sizable height */
        padding: 5px 10px 5px 10px; /* Making space around letters */
        font-size: 18px; /* Changing font size */
    }

    .column-bordered-table thead td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table td {
        border-left: 1px solid #c3c3c3;
        border-right: 1px solid #c3c3c3;
    }

    .column-bordered-table tfoot tr {
        border-top: 1px solid #c3c3c3;
        border-bottom: 1px solid #c3c3c3;
    }

    .table-bordered td, .table-bordered th {
        border: 1px solid #000!important;
    }
</style>

@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Sales</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Sales</a></li>
                                    <li class="breadcrumb-item active">Show</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="row float-right">
                                    <div class="d-print-none">
                                        <div class="float-right">
                                            <a href="javascript:window.print()" class="btn btn-success waves-effect waves-light mr-1"><i class="fa fa-print"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-2">
                                        <img style="height: 100px;" src="{{ url('public/'.userDetails()->logo) }}" alt="">
                                    </div>

                                    <div class="col-md-8">
                                            <h2 style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 55px">{{ $user_info['organization_name'] }}</h2>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['address'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px;font-weight: bold">{{ $user_info['contact_email'] }}</p>
                                            <p style="margin-top: 0px;margin-bottom: 0px;text-align: center;font-size: 18px">{{ $user_info['contact_number'] }}</p>
                                    
                                            <h2 style="font-weight: bold;text-decoration: underline; margin-top: 20px;margin-bottom: 0px;text-align: center;font-size: 26px;" class="">Invoice</h2>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top: 20px" class="row ml-2">
                                    <div style="font-size: 16px" class="col-sm-7 col-6">
                                        <address>
                                            <strong>Customer Name &nbsp;&nbsp;: 
                                            {{ $invoice['customer_name'] != null ? $invoice['customer_name'] : $invoice['contact_name']  }} </strong><br>
                                            <strong>Address &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </strong>
                                            @if($invoice['customer_address'] != null)
                                               <?php echo $invoice['customer_address']; ?> <br>
                                            @else
                                            <?php echo $invoice['address']; ?> <br>
                                            @endif
                                            <strong>Mobile &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: 
                                            {{ $invoice['customer_phone'] != null ? $invoice['customer_phone'] : $invoice['contact_phone']  }}</strong>
                                        </address>
                                    </div>

                                    <div style="font-size: 16px;text-align: right;margin-right: 1px;margin-left: -10px;" class="col-sm-5 col-6">
                                        <address>
                                            <strong>Invoce Genarate &nbsp;&nbsp;:</strong>
                                            {{$invoice['invoice_create_user']}}<br>

                                            <strong>Invoice Number &nbsp;&nbsp;: </strong>
                                            {{ 'INV - ' . str_pad($invoice['invoice_number'], 6, "0", STR_PAD_LEFT) }} <br>
                                            <strong>Date Time: </strong>
                                            {{ date('d/m/Y', strtotime($invoice['invoice_date'])) }} || <span id="currenttime"></span>
                                        </address>
                                    </div>
                                </div>

                                <div style="border: 2px solid black;height: 1180px" class="table-responsive" style="margin-right:10px!important;margin-left: 10px!important;">
                                    <table style="border-bottom: 1px solid gray" class="table column-bordered-table">
                                        <thead style="font-size: 18px">
                                            <tr>
                                                <th style="width: 70px;">SL</th>
                                                <th>Description & Model</th>
                                                <th class="text-right">Sold Qty</th>
                                                <th class="text-right">Unit Price</th>
                                                <th class="text-right">Amount</th>
                                                <th class="text-right">Discount</th>
                                            </tr>
                                        </thead>

                                        <tbody style="font-size: 16px">
                                            @if(!empty($entries) && ($entries->count() > 0))

                                            <?php $sub_total = 0; ?>

                                            @foreach($entries as $key => $value)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>
                                                    @if($value['product_type'] == 1)
                                                        <?php echo $value['product_entry_name'] . ' - ' .  $value['product_model']; ?> 

                                                   
                                                        
                                                    @else
                                                        <?php echo $value['product_entry_name'] . ' - ' . ProductVariationName($value['product_entry_id']) . ' - ' . $value['product_model'] ; ?>

                                                      

                                                    @endif

                                                    <?php
                                                        $w_pr = WarrentyProducts($value['warrenty_product_id']);
                                                    ?>

                                                    @if($w_pr != null)
                                                    <br>
                                                    <br>
                                                    <strong>
                                                        <?php echo $w_pr; ?>
                                                    </strong>
                                                    @endif
                                                </td>

                                                <td class="text-right">{{ number_format($value['quantity'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['rate'],2,'.',',') }}</td>
                                                <td class="text-right">{{ number_format($value['total_amount'],2,'.',',') }}</td>
                                                @if($value['discount_type'] == 0)
                                                <td class="text-right">{{$value['discount_amount']}}</td>
                                                @else
                                                <td class="text-right">{{ number_format($value['discount_amount'],2,'.',',') }}</td>
                                                @endif
                                            </tr>

                                            <?php $sub_total = $sub_total + $value['total_amount']; ?>

                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>

                                    <div style="margin-top: 20px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                            @if($invoice['due_amount'] == 0)
                                            <h1>PAID</h1>
                                            @endif
                                        </div>

                                        <div style="font-size: 16px;border-bottom: 3px solid black" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Total Amount 
                                                <span style="float: right;padding-right: 10px">{{ number_format($sub_total,2,'.',',') }}</span> <br>

                                                Add Vat 
                                                <span style="float: right;padding-right: 10px">
                                                {{ $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : number_format($invoice['total_vat'],2,'.',',') }}
                                               </span> <br>

                                                Less Discount 
                                                <span style="float: right;padding-right: 10px">{{ $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : number_format($invoice['total_discount_amount'],2,'.',',') }}</span> <br>

                                                Add Installation / Service Charge 
                                                <span style="float: right;padding-right: 10px">
                                                    {{ number_format($invoice['service_charge_amount'],2,'.',',') }}
                                                </span> <br>
                                                
                                            </address>
                                        </div>
                                    </div>

                                    <div style="margin-top: 5px" class="row">
                                        <div style="vertical-align: middle;text-align: center" class="col-sm-7 hidden-xs">
                                        </div>

                                        <div style="font-size: 16px;" class="col-sm-5 col-6 text-sm-left">
                                            <address>
                                                Net Payable Amount
                                                <?php 
                                                    $lessDiscount = $invoice['total_discount_type'] == 0 ? ((($sub_total + (($sub_total*$invoice['total_vat'])/100))*$invoice['total_discount_amount'])/100) : $invoice['total_discount_amount'];

                                                    $vatAmount = $invoice['vat_type'] == 0 ? (($sub_total*$invoice['total_vat'])/100) : $invoice['total_vat'];
                                                 ?>
                                                <span style="float: right;padding-right: 10px">{{ number_format($sub_total + $invoice['service_charge_amount'] + $vatAmount -$lessDiscount,2,'.',',') }}</span> 
                                            </address>
                                        </div>
                                    </div>

                                    <span style="    color: black;padding-left: 35px;font-size: 23px;">In Words : {{$inwordsAmount}} Taka only</span>
                                    
                                    <br>
                                    <br>

                                    <div class="row ml-4">
                                        <div class="col-8">
                                            <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Received</th>
                                                    <th scope="col">Due</th>
                                                    <th scope="col">Previous Due</th>
                                                    <th scope="col">Total Due</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        @if ($invoice->cash_given != null)
                                                        {{$invoice->cash_given}}
                                                        @else
                                                        0.00
                                                        @endif
                                                    </td>
                                                    <td>{{$invoice->invoice_amount - $invoice->cash_given}}</td>
                                                    <td>{{$invoice->previous_due}}</td>
                                                    <td>{{$invoice->invoice_amount - $invoice->cash_given + $invoice->previous_due}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>

                                    <br>
                                    <br>


                                    @if($installments_list->count() > 0)
                                    <div class="row ml-4">
                                        <div class="col-8">
                                            <table class="table table-bordered">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Installment Number</th>
                                                    <th scope="col">Date</th>
                                                    <th scope="col">Amount</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($installments_list as $key=> $list)
                                                <tr>
                                                    <td>{{$list->installment_number}}</td>
                                                     <td>{{ date('d/m/Y', strtotime($list->date)) }}</td>
                                                    <td>{{$list->amount}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        </div>
                                    </div>
                                    @endif

                                    <br>
                                    <br>

                                    <div class="row">

                                        <div style="text-align: left" class="col-md-6">
                                            
                                            <span style="border-top: 1px solid black"> Customers Signature  </span>
                                        </div>

                                        <div style="text-align: right" class="col-md-6">

                                            <span style="border-top: 1px solid black"> Authorised Signature </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
    var date =new Date(); 
    var currenttime = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
    document.getElementById("currenttime").innerHTML = currenttime;
</script>
@endsection