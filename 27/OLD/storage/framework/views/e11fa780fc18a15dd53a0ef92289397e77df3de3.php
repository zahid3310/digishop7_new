

<?php $__env->startSection('title', 'Items'); ?>

<?php $__env->startSection('content'); ?>
    <div class="main-content">
    	<div class="page-content">
    		<div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title-box d-flex align-items-center justify-content-between">
                            <h4 class="mb-0 font-size-18">Items</h4>

                            <div class="page-title-right">
                                <ol class="breadcrumb m-0">
                                    <li class="breadcrumb-item"><a href="javascript: void(0);">Items</a></li>
                                    <li class="breadcrumb-item active">Items</li>
                                </ol>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">

                                <h4 class="card-title">Create New Item</h4>
                                <br>

                                <?php if(Session::has('success')): ?>
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('success'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                    			<?php endif; ?>
                    			<?php if(Session::has('unsuccess')): ?>
                    			<div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <?php echo Session::get('unsuccess'); ?>

                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                </div>
                                <?php endif; ?>

            					<form id="FormSubmit" action="<?php echo e(route('items_store')); ?>" method="post" enctype="multipart/formdata">
            					<?php echo e(csrf_field()); ?>


                                <div class="form-group row">
                                    <label for="example-text-input" class="col-md-2 col-form-label">Name</label>
                                    <div class="col-md-10">
                                        <input class="form-control" type="text" value="" name="name" id="name" placeholder="Enter Category Name" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Sub Category</label>
                                    <div class="col-md-10">
                                        <select class="form-control select2" name="sub_category_id">
                                            <option value="">Select Sub Category</option>
                                            <?php if(!empty($sub_categories) && ($sub_categories->count() > 0)): ?>
                                            <?php $__currentLoopData = $sub_categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $sub_category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($sub_category['id']); ?>"><?php echo e($sub_category['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label class="col-md-2 col-form-label">Publication Status</label>
                                    <div class="col-md-10">
                                        <select class="form-control" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group row">
                                	<div class="col-md-2"></div>
                                    <div class="button-items col-md-2 pull-right">
                                    	<button type="submit" class="btn btn-primary waves-effect waves-light enableOnInput" onclick="preventDoubleClick()">Save</button>
                                    	<button type="button" class="btn btn-secondary waves-effect waves-light"><a style="color: white" href="<?php echo e(route('home')); ?>">Close</a></button>
                                    </div>
                                	<div class="col-md-8"></div>
                                </div>

                                </form>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                            	<h4 class="card-title">All Items</h4>
                                <br>

                                <table id="datatable" class="table table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Sub Category</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                    	<?php if(!empty($items) && ($items->count() > 0)): ?>
                                    	<?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	                                        <tr>
	                                            <td><?php echo e($key + 1); ?></td>
                                                <td><?php echo e($item['name']); ?></td>
	                                            <td><?php echo e($item['sub_category_name'] != null ? $item['sub_category_name'] : 'N/A'); ?></td>
	                                            <td><?php echo e($item['status'] == 1 ? 'Active' : 'Inactive'); ?></td>
	                                            <td>
	                                            	<div class="dropdown">
                                                        <a href="#" class="dropdown-toggle card-drop" data-toggle="dropdown" aria-expanded="false">
                                                            <i class="mdi mdi-dots-horizontal font-size-18"></i>
                                                        </a>
                                                        <div class="dropdown-menu dropdown-menu-right" style="">
                                                            <a class="dropdown-item" href="<?php echo e(route('items_edit', $item['id'])); ?>">Edit</a>
                                                        </div>
                                                    </div>
	                                            </td>
	                                        </tr>
	                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	                                    <?php endif; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div> <!-- end col -->
                </div>
    		</div>
		</div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('scripts'); ?>
    <script type="text/javascript">
        function preventDoubleClick()
        {
            $('.enableOnInput').prop('disabled', true)
            $('#FormSubmit').submit();
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /home/digishop7/public_html/66/Modules/Items/Resources/views/index.blade.php ENDPATH**/ ?>