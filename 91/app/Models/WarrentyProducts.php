<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class WarrentyProducts extends Model
{  
    protected $table = "warrenty_products";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function productEntries()
    {
        return $this->belongsTo('App\Models\ProductEntries','product_entry_id');
    }
}
