<?php

namespace Modules\Orders\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Validator;
use Auth;

//Models
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Items;
use App\Models\Products;
use App\Models\ProductEntries;
use App\Models\Customers;
use App\Models\Invoices;
use App\Models\InvoiceEntries;
use App\Models\Orders;
use App\Models\OrderEntries;
use App\Models\Payments;
use App\Models\PaymentEntries;
use App\Models\Users;
use App\Models\SrItems;
use App\Models\FreeItems;
use App\Models\OrderFreeItems;
use App\Models\UnitConversions;
use App\Models\Accounts;
use App\Models\JournalEntries;
use App\Models\OrderJournalEntries;
use App\Models\Stores;
use App\Models\Areas;
use Carbon\Carbon;
use Response;
use DB;
use View;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $invoices           = Invoices::orderBy('id', 'DESC')->first();
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts           = Accounts::where('account_type_id',9)->where('status', 1)->get();
        $areas              = Areas::all();

        return view('orders::index', compact('paid_accounts', 'invoices', 'accounts', 'areas'));
    }

    public function AllSales()
    {
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('orders::all_sales', compact('paid_accounts'));
    }
    
    public function orderListDelivery()
    {
        $paid_accounts      = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();

        return view('orders::all_delivery_list', compact('paid_accounts'));
    }

    public function create()
    {
        return view('orders::create');
    }

    public function store(Request $request)
    {
        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                                                        
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $data_find                          = Orders::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Orders;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'];
            $invoice->address                   = $data['address'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->account_id                = $data['account_id'];
            $invoice->created_by                = $user_id;

            if (isset($data['type']))
            {
                $invoice->type = $data['type'];
            }

            if ($invoice->save())
            {
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);
                    
                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                                            ->where('converted_unit_id', $data['unit_id'][$key])
                                                            ->where('product_entry_id', $value)
                                                            ->first();

                    $invoice_entries[] = [
                        'invoice_id'              => $invoice['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value,
                        'free_product_entry_id'   => isset($data['free_items'][$key]) ? $data['free_items'][$key] : Null,
                        'customer_id'             => $invoice['customer_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key],
                        'conversion_unit_id'      => $data['unit_id'][$key],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key]) ? $data['free_item_main_unit_id'][$key] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key]) ? $data['free_unit_id'][$key] : null,
                        'reference_id'            => $data['reference_id'],
                        'buy_price'               => ($data['main_unit_id'][$key] == $data['unit_id'][$key]) ? round($product_buy_price['buy_price'], 2) : $conversion_rate_find['purchase_price'],
                        'rate'                    => $data['rate'][$key],
                        'quantity'                => $data['quantity'][$key],
                        'free_quantity'           => $data['free_quantity'][$key],
                        'total_amount'            => $data['amount'][$key],
                        'discount_type'           => $data['discount_type'][$key],
                        'discount_amount'         => $data['discount'][$key],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    if (isset($data['free_unit_id'][$key]))
                    {
                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['free_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $free_converted_quantity_to_main_unit,
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('order_entries')->insert($invoice_entries);

                if (isset($free_items))
                {
                    DB::table('order_free_items')->insert($free_items);
                }

                //Financial Accounting Start
                    debitOrder($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditOrder($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debitOrder($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                creditOrder($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return back()->with("success","Order Created Successfully !!");
                }
                else
                {      
                    return redirect()->route('orders_show_pos', $invoice['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function show($id)
    {
        
        $invoice    = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('areas','areas.id','customers.area_id')
                                    ->select('orders.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone',
                                             'areas.name as area_name')
                                    ->find($id);
                                    

        $entries    = OrderEntries::leftjoin('products', 'products.id', 'order_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'order_entries.product_entry_id')
                                    ->where('order_entries.invoice_id', $id)
                                    ->select('order_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();  
                     
        $find_customer  = Customers::find($invoice['customer_id']);
        
        $sr_name_id = Customers::select('customers.sr_id as sr_id')
                            ->where('customers.id',$invoice['customer_id'])
                            ->first();
                            
        $customer_sr_name = Customers::select('customers.name as sr_name')
                            ->where('customers.id',$sr_name_id->sr_id)
                            ->first();

        if ($find_customer['store_id'] != null)
        {
            $user_info  = Stores::find($find_customer['store_id']);
        }  
        else
        {
            $user_info  = userDetails();
        } 

        $previous_due   = customerBalance($invoice['customer_id']);
        


        return view('orders::show', compact('entries', 'invoice', 'user_info','customer_sr_name', 'previous_due'));
    }

    public function showPos($id)
    {
        $invoice    = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('areas','areas.id','customers.area_id')
                                    ->select('orders.*',
                                             'customers.name as customer_name',
                                             'customers.address as address',
                                             'customers.phone as phone',
                                             'areas.name as area_name')
                                    ->find($id);

        $entries    = OrderEntries::leftjoin('products', 'products.id', 'order_entries.product_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'order_entries.product_entry_id')
                                    ->where('order_entries.invoice_id', $id)
                                    ->select('order_entries.*',
                                             'product_entries.product_type as product_type',
                                             'product_entries.name as product_entry_name',
                                             'products.name as product_name')
                                    ->get();   
                     
        $user_infos  = userDetails();

        $find_customer  = Customers::find($invoice['customer_id']);
        
        $sr_name_id = Customers::select('customers.sr_id as sr_id')
                            ->where('customers.id',$invoice['customer_id'])
                            ->first();
                            
        $customer_sr_name = Customers::select('customers.name as sr_name')
                            ->where('customers.id',$sr_name_id->sr_id)
                            ->first();
                            
        // $previous_due   = Invoices::where('invoices.customer_id', $invoice['customer_id'])
        //                             ->where('invoices.id','!=',$id)
        //                             ->sum('invoices.due_amount');

        if ($find_customer['store_id'] != null)
        {
            $user_info  = Stores::find($find_customer['store_id']);
        }  
        else
        {
            $user_info  = userDetails();
        }
        
        $previous_due   = customerBalance($invoice['customer_id']);

        return view('orders::show_pos', compact('entries', 'invoice', 'user_info', 'user_infos','customer_sr_name','previous_due'));
    }

    //Code For Confirm
    public function edit($id)
    {
        $products               = Products::orderBy('products.total_sold', 'DESC')
                                            ->get();
        $product_category       = Products::all();
        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.stock_in_hand', '!=', null)

                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();
      

        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);

        $find_invoice           = Orders::leftjoin('customers as customers', 'customers.id', '=', 'orders.customer_id')
                                            ->leftjoin('customers as srs', 'srs.id', '=', 'orders.sr_id')
                                            ->select('orders.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'customers.code as code',
                                                 'srs.id as sr_id',
                                                 'srs.name as sr_name')
                                            ->find($id);

        $find_invoice_entries   = OrderEntries::leftjoin('customers', 'customers.id', 'order_entries.customer_id')  
                                            ->leftjoin('product_entries AS main_product', 'main_product.id', '=', 'order_entries.product_entry_id')
                                            ->leftjoin('product_entries AS free_product', 'free_product.id', '=', 'order_entries.free_product_entry_id')
                                            ->where('order_entries.invoice_id', $id)
                                            ->select('order_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'main_product.id as item_id',
                                                    'main_product.stock_in_hand as stock_in_hand',
                                                    'main_product.name as item_name',
                                                    'free_product.id as free_item_id',
                                                    'free_product.name as free_item_name')
                                            ->get();


        $entries_count          = $find_invoice_entries->count();

        $current_balance        = OrderJournalEntries::whereIn('transaction_head', ['payment-receive'])
                                            ->where('invoice_id', $id)
                                            ->where('debit_credit', 1)
                                            ->selectRaw('order_journal_entries.*')
                                            ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('orders::edit', compact('products','product_category', 'product_entries', 'find_invoice', 'find_invoice_entries', 'entries_count', 'current_balance', 'current_balance_count', 'paid_accounts', 'accounts'));
    }
    
    public function delivery($id)
    {
        DB::beginTransaction();

        try{
            $delivery           = Orders::find($id);
            $delivery->type     = 3;
            $delivery->status   = 3;
            
            if ($delivery->save())
            {   
                DB::commit();
                return back()->with("success","Order Send To Delivery Successfully !!");
            }
        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function update(Request $request, $id)
    {
        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {   
                    if($data['main_unit_id'][$key] == $data['unit_id'][$key])
                    {
                        $product    = ProductEntries::find($value);
                        $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                    }
                    else
                    {
                        $product1   = UnitConversions::where('product_entry_id', $value)
                                                        ->where('main_unit_id', $data['main_unit_id'][$key])
                                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                                        ->first();
                                                
                        $buy_price  = $buy_price + ($product1['purchase_price']*$data['quantity'][$key]);
                    }
                }

            $data_find                          = Invoices::orderBy('created_at', 'DESC')->first();
            $invoice_number                     = $data_find != null ? $data_find['invoice_number'] + 1 : 1;

            $invoice                            = new Invoices;;
            $invoice->invoice_number            = $invoice_number;
            $invoice->customer_id               = $data['customer_id'];
            $invoice->order_id                  = $id;
            $invoice->type                      = 1;
            // $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'] != 0 ? $data['sr_id'] : Null;
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->previous_due              = $data['previous_due'];
            $invoice->previous_due_type         = $data['balance_type'];
            $invoice->adjusted_amount           = $data['adjustment'];
            $invoice->account_id                = $data['account_id'];
            $invoice->created_by                = $user_id;

            if (isset($data['type']))
            {
                $invoice->type = $data['type'];
            }

            if ($invoice->save())
            {   
                $order_update       = Orders::find($id);
                $order_update->type = 2;
                $order_update->save();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);
                    
                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                                            ->where('converted_unit_id', $data['unit_id'][$key])
                                                            ->where('product_entry_id', $value)
                                                            ->first();
                                        
                    $invoice_entries[] = [
                        'invoice_id'              => $invoice['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value,
                        'free_product_entry_id'   => isset($data['free_items'][$key]) ? $data['free_items'][$key] : Null,
                        'customer_id'             => $invoice['customer_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key],
                        'conversion_unit_id'      => $data['unit_id'][$key],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key]) ? $data['free_item_main_unit_id'][$key] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key]) ? $data['free_unit_id'][$key] : null,
                        // 'reference_id'            => $data['reference_id'],
                        'buy_price'               => ($data['main_unit_id'][$key] == $data['unit_id'][$key]) ? round($product_buy_price['buy_price'], 2) : $conversion_rate_find['purchase_price'],
                        'rate'                    => $data['rate'][$key],
                        'quantity'                => $data['quantity'][$key],
                        'free_quantity'           => $data['free_quantity'][$key],
                        'total_amount'            => $data['amount'][$key],
                        'discount_type'           => $data['discount_type'][$key],
                        'discount_amount'         => $data['discount'][$key],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    if (isset($data['free_unit_id'][$key]))
                    {
                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['free_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($data['sr_id'] != 0)
                    {
                        $sr_items[] = [
                            'sales_id'              => $invoice['id'],
                            'sr_id'                 => $data['sr_id'],
                            'date'                  => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'            => $product_buy_price['product_id'],
                            'product_entry_id'      => $value,
                            'main_unit_id'          => $data['main_unit_id'][$key],
                            'conversion_unit_id'    => $data['unit_id'][$key],
                            'type'                  => 2,
                            'quantity'              => $converted_quantity_to_main_unit,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];
                    }

                    if (isset($data['free_items'][$key]) && ($data['free_items'][$key] != 0))
                    {
                        $sr_items[] = [
                            'sales_id'          => $invoice['id'],
                            'sr_id'             => $data['sr_id'],
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'product_id'        => $product['product_id'],
                            'product_entry_id'  => $data['free_items'][$key],
                            'type'              => 2,
                            'quantity'          => $free_converted_quantity_to_main_unit,
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $free_converted_quantity_to_main_unit,
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('invoice_entries')->insert($invoice_entries);

                if ($data['sr_id'] != 0)
                {
                    DB::table('sr_items')->insert($sr_items);
                }

                if (isset($free_items))
                {
                    DB::table('free_items')->insert($free_items);
                }

                stockOut($data, $item_id=null);

                //Financial Accounting Start
                    debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);

                    //Insert into journal_entries 
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {
                                debit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                credit($customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                            }
                        }
                    }

                    customerBalanceUpdate($data['customer_id']);
                //Financial Accounting End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('invoices_all_sales')->with("success","Sales Created Successfully !!");
                }
                else
                {      
                    return redirect()->route('invoices_show_pos', $invoice['id']);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            return back()->with("unsuccess","Not Added");
        }
    }

    public function destroy($id)
    {
    }

    public function productList()
    {
        $data       = ProductEntries::where('product_entries.stock_in_hand', '>', 0)
                                    ->Where('product_entries.stock_in_hand', '!=', null)
                                    ->orderBy('product_entries.total_sold', 'DESC')
                                    ->select('product_entries.*')
                                    ->get();

        return Response::json($data);
    }

    public function makePayment($id)
    {
        $data       = Orders::find($id);

        return Response::json($data);
    }

    public function storePayment(Request $request)
    {
        $user_id                    = Auth::user()->id;
        $data                       = $request->all();

        DB::beginTransaction();

        try{

            if (isset($data['amount_paid']))
            {
                $data_find        = Payments::orderBy('id', 'DESC')->first();
                $payment_number   = $data_find != null ? $data_find['payment_number'] + 1 : 1;

                for($i = 0; $i < count($data['amount_paid']); $i++)
                {   
                    if ($data['amount_paid'][$i] > 0)
                    {
                        $payments = [
                            'payment_number'        => $payment_number,
                            'customer_id'           => $data['customer_id'],
                            'payment_date'          => date('Y-m-d', strtotime($data['payment_date'])),
                            'amount'                => $data['amount_paid'][$i],
                            'paid_through'          => $data['paid_through'][$i],
                            'account_information'   => $data['account_information'][$i],
                            'note'                  => $data['note'][$i],
                            'type'                  => 0,
                            'created_by'            => $user_id,
                            'created_at'            => date('Y-m-d H:i:s'),
                        ];

                        $payment_id = DB::table('payments')->insertGetId($payments);      

                        if ($payment_id)
                        {   
                            $payment_entries = [
                                    'payment_id'        => $payment_id,
                                    'invoice_id'        => $data['invoice_id'],
                                    'amount'            => $data['amount_paid'][$i],
                                    'created_by'        => $user_id,
                                    'created_at'        => date('Y-m-d H:i:s'),
                            ];

                            DB::table('payment_entries')->insert($payment_entries);  
                        }

                        $update_invoice_dues                = Invoices::find($data['invoice_id']);
                        $update_invoice_dues->due_amount    = $update_invoice_dues['due_amount'] - $data['amount_paid'][$i];
                        $update_invoice_dues->save();

                        $payment_number++;
                    }
                }
            }
            
            DB::commit();
            return Response::json(1);
        }
        catch (\Exception $exception)
        {
            DB::rollback();
            return Response::json(0);
        }
    }

    public function productPriceList($id)
    {
        $data['product_entries']        = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    // ->where('product_entries.product_id', '!=', 1)
                                                    ->selectRaw('product_entries.*, units.name as unit_name')
                                                    ->find($id);

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function productPriceListBySr($product_entry_id)
    {
        $data['product_entries']        = SrItems::leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                                    ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                                    ->groupBy('sr_items.product_entry_id')
                                                    ->select(DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_entry_id) as product_entry_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT sr_items.product_id) as product_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT product_entries.sell_price) as sell_price"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT units.id) as unit_id"),
                                                             DB::raw("GROUP_CONCAT(DISTINCT units.name) as unit_name"),
                                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) -  SUM(IF(sr_items.type='2',sr_items.quantity,0)) - SUM(IF( sr_items.type='3',sr_items.quantity,0)) as stock_in_hand")
                                                            )
                                                    ->first();

        $data1                          = ProductEntries::leftjoin('units', 'units.id', 'product_entries.unit_id')
                                                        ->where('product_entries.id', $product_entry_id)
                                                        ->selectRaw('units.id as unit_id, units.name as unit_name')
                                                        ->get()
                                                        ->toArray();

        $data2                          = UnitConversions::leftjoin('units', 'units.id', 'unit_conversions.converted_unit_id')
                                                    ->where('unit_conversions.product_entry_id', $product_entry_id)
                                                    ->selectRaw('unit_conversions.converted_unit_id as unit_id, units.name as unit_name')
                                                    ->get()
                                                    ->toArray();

        $data['unit_conversions']       = collect(array_merge($data1, $data2));

        return Response::json($data);
    }

    public function invoiceListLoad()
    {
        $data           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                    ->where('orders.type', 1)
                                    ->where('orders.status', 0)
                                    ->orderBy('orders.created_at', 'DESC')
                                    ->select('orders.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('orders.id')
                                    ->get();

        return Response::json($data);
    }

    public function invoiceDeliveryListLoad()
    {
        $data           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                    ->where('orders.type', 3)
                                    ->where('orders.status', 3)
                                    ->orderBy('orders.created_at', 'DESC')
                                    ->select('orders.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone')
                                    ->distinct('orders.id')
                                    ->get();

        return Response::json($data);
    }

    public function invoiceListSearch($from_date, $to_date, $customer, $invoice)
    {
        $search_by_from_date    = $from_date != 0 ? date('Y-m-d', strtotime($from_date)) : 0;
        $search_by_to_date      = $to_date != 0 ? date('Y-m-d', strtotime($to_date)) : 0;
        $search_by_customer     = $customer != 0 ? $customer : 0;
        $search_by_invoice      = $invoice != 0 ? $invoice : 0;

        $data           = Orders::leftjoin('customers', 'customers.id', 'orders.customer_id')
                                        ->leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                        ->where('orders.type', 1)
                                        ->whereBetween('orders.invoice_date', [$search_by_from_date, $search_by_to_date])
                                        ->when($search_by_customer != 0, function ($query) use ($search_by_customer) {
                                            return $query->where('customers.id', $search_by_customer);
                                        })
                                        ->when($search_by_invoice != 0, function ($query) use ($search_by_invoice) {
                                            return $query->where('orders.id', $search_by_invoice);
                                        })
                                        ->orderBy('orders.created_at', 'DESC')
                                        ->select('orders.*',
                                                 'customers.name as customer_name',
                                                 'customers.phone as phone')
                                        ->distinct('orders.id')
                                        ->take(20)
                                        ->get();

        return Response::json($data);
    }

    public function posSearchProduct($id)
    {
        $user_info      = userDetails();
        $data           = ProductEntries::where('product_entries.product_code', $id)
                                        ->where('product_entries.stock_in_hand', '>', 0)
                                        ->first();

        return Response::json($data);
    }

    public function customerStore(Request $request)
    {
        $user_id    = Auth::user()->id;
        $data       = $request->all();

        $customers                              = new Customers;
        $customers->name                        = $data['customer_name'];
        $customers->address                     = $data['address'];
        $customers->phone                       = $data['mobile_number'];
        $customers->contact_type                = $data['contact_type'];
        $customers->created_by                  = $user_id;

        if ($customers->save())
        {   
            return Response::json($customers);
        }
        else
        {
            return Response::json(0);
        }
    }

    public function customersListInvoice()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Customers::orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Customers::where('customers.name', 'LIKE', "%$search%")
                                    ->orWhere('customers.phone', 'LIKE', "%$search%")
                                    ->orderBy('customers.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All', "phone" =>'', "contact_type" =>'');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name'], "phone" =>$value['phone'], "contact_type" =>$value['contact_type']);

            $i++;
        }
   
        return Response::json($data);
    }

    public function couponCode($id)
    {
        $today          = Carbon::today();

        $data           = Discounts::leftjoin('discount_products', 'discount_products.discount_id', 'discounts.id')
                                ->where('discounts.coupon_code', $id)
                                ->orWhere('discounts.card_number', $id)
                                ->whereDate('discounts.expire_date', '>=', $today->format('Y-m-d'))
                                ->where('discounts.status', 1)
                                ->select('discount_products.product_id as product_id', 
                                         'discounts.discount_type as discount_type',
                                         'discounts.discount_amount as discount_amount',
                                         'discounts.expire_date as expire_date'
                                        )
                                ->get();

        return Response::json($data);
    }

    public function printInvoicesList()
    {
        $user_id        = Auth::user()->id;
        $data           = Orders::leftjoin('order_entries', 'order_entries.invoice_id', 'invoices.id')
                                    ->leftjoin('customers', 'customers.id', 'invoices.customer_id')
                                    ->leftjoin('users', 'users.id', 'invoices.created_by')
                                    ->where('invoices.type', 1)
                                    ->where('invoices.created_by', $user_id)
                                    ->orderBy('invoices.created_at', 'DESC')
                                    ->select('invoices.*',
                                            'customers.name as customer_name',
                                            'customers.phone as phone',
                                            'users.name as user_name')
                                    ->distinct('invoices.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function printInvoicesSearch($date,$customer,$invoice_number)
    {
        $search_by_date              = $date != 0 ? date('Y-m-d', strtotime($date)) : 0;
        $search_by_invoice_number    = $invoice_number != 0 ? $invoice_number : 0;
        $search_by_invoice_customer  = $customer != 0 ? $customer : 0;
        $user_info                   = userDetails();

        $data           = Orders::leftjoin('order_entries', 'order_entries.invoice_id', 'orders.id')
                                    ->leftjoin('customers', 'customers.id', 'orders.customer_id')
                                    ->leftjoin('users', 'users.id', 'orders.created_by')
                                    ->where('orders.type', 1)
                                    ->when($search_by_date != 0, function ($query) use ($search_by_date) {
                                        return $query->where('orders.invoice_date', 'LIKE', "%$search_by_date%");
                                    })
                                    ->when($search_by_invoice_number != 0, function ($query) use ($search_by_invoice_number) {
                                        return $query->where('orders.invoice_number', 'LIKE', "%$search_by_invoice_number%");
                                    })
                                    ->when($search_by_invoice_customer != 0, function ($query) use ($search_by_invoice_customer) {
                                        return $query->where('customers.name', 'LIKE', "%$search_by_invoice_customer%");
                                    })
                                    ->orderBy('orders.created_at', 'DESC')
                                    ->select('orders.*',
                                             'customers.name as customer_name',
                                             'customers.phone as phone',
                                             'users.name as user_name')
                                    ->distinct('orders.id')
                                    ->take(100)
                                    ->get();

        return Response::json($data);
    }

    public function calculateOpeningBalance($customer_id)
    {
        $data  = calculateOpeningBalance($customer_id);

        return Response::json($data);
    }

    public function srProductStock($sr_id, $product_entry_id)
    {
        $data       = SrItems::leftjoin('issues', 'issues.id', 'sr_items.issue_id')
                                    ->leftjoin('customers', 'customers.id', 'sr_items.sr_id')
                                    ->leftjoin('product_entries', 'product_entries.id', 'sr_items.product_entry_id')
                                    ->where('sr_items.sr_id', $sr_id)
                                    ->where('sr_items.product_entry_id', $product_entry_id)
                                    ->groupBy('sr_items.product_entry_id')
                                    ->select(DB::raw('group_concat(distinct sr_items.product_entry_id) as product_entry_id'),
                                             DB::raw('group_concat(distinct product_entries.name) as product_name'),
                                             DB::raw("SUM(IF(sr_items.type='1',sr_items.quantity,0)) AS receive_quantity"),
                                             DB::raw("SUM(IF(sr_items.type='2',sr_items.quantity,0)) AS sold_quantity"),
                                             DB::raw("SUM(IF( sr_items.type='3',sr_items.quantity,0)) AS return_quantity")
                                            )
                                    ->first();
        

        return Response::json($data);
    }

    //Code for update order
    public function orderEdit($id)
    {
        $products               = Products::orderBy('products.total_sold', 'DESC')
                                            ->get();
        $product_category       = Products::all();
        $product_entry          = ProductEntries::leftjoin('products', 'products.id', 'product_entries.product_id')
                                            ->leftjoin('units', 'units.id', 'product_entries.unit_id')
                                            ->leftjoin('customers', 'customers.id', 'product_entries.supplier_id')
                                            ->leftjoin('categories', 'categories.id', 'product_entries.brand_id')
                                            ->leftjoin('product_variation_entries', 'product_variation_entries.product_entry_id', 'product_entries.id')
                                            ->leftjoin('product_variation_values', 'product_variation_values.id', 'product_variation_entries.variation_value_id')
                                            ->where('product_entries.stock_in_hand', '!=', null)

                                            ->groupBy('product_entries.id')
                                            ->selectRaw('GROUP_CONCAT(DISTINCT product_entries.id) as id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_id) as product_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.brand_id) as brand_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.supplier_id) as supplier_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_code) as product_code,
                                                         GROUP_CONCAT(DISTINCT product_entries.name) as name,
                                                         GROUP_CONCAT(DISTINCT product_entries.stock_in_hand) as stock_in_hand,
                                                         GROUP_CONCAT(DISTINCT product_entries.opening_stock) as opening_stock,
                                                         GROUP_CONCAT(DISTINCT product_entries.total_sold) as total_sold,
                                                         GROUP_CONCAT(DISTINCT product_entries.buy_price) as buy_price,
                                                         GROUP_CONCAT(DISTINCT product_entries.unit_id) as unit_id,
                                                         GROUP_CONCAT(DISTINCT product_entries.alert_quantity) as alert_quantity,
                                                         GROUP_CONCAT(DISTINCT product_entries.tax_type) as tax_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.selling_price_exclusive_tax) as selling_price_exclusive_tax,
                                                         GROUP_CONCAT(DISTINCT product_entries.vat_percentage) as vat_percentage,
                                                         GROUP_CONCAT(DISTINCT product_entries.service_charge) as service_charge,
                                                         GROUP_CONCAT(DISTINCT product_entries.status) as status,
                                                         GROUP_CONCAT(DISTINCT product_entries.product_type) as product_type,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_by) as created_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_by) as updated_by,
                                                         GROUP_CONCAT(DISTINCT product_entries.created_at) as created_at,
                                                         GROUP_CONCAT(DISTINCT product_entries.updated_at) as updated_at,
                                                         GROUP_CONCAT(DISTINCT product_variation_values.name SEPARATOR " - ") as variations,
                                                         GROUP_CONCAT(DISTINCT units.name) as unit_name,  
                                                         GROUP_CONCAT(DISTINCT customers.name) as supplier_name,  
                                                         GROUP_CONCAT(DISTINCT categories.name) as brand_name,  
                                                         GROUP_CONCAT(DISTINCT products.name) as category_name,
                                                         GROUP_CONCAT(DISTINCT products.id) as product_id
                                                        ')
                                            ->orderBy('product_entries.total_sold', 'DESC')
                                            ->get();
      

        $product_entry          = $product_entry->sortBy('name')->all();
        $product_entries        = collect($product_entry);

        $find_invoice           = Orders::leftjoin('customers as customers', 'customers.id', '=', 'orders.customer_id')
                                            ->leftjoin('customers as srs', 'srs.id', '=', 'orders.sr_id')
                                            ->select('orders.*',
                                                 'customers.id as customer_id',
                                                 'customers.name as contact_name',
                                                 'customers.code as code',
                                                 'srs.id as sr_id',
                                                 'srs.name as sr_name')
                                            ->find($id);

        $find_invoice_entries   = OrderEntries::leftjoin('customers', 'customers.id', 'order_entries.customer_id')  
                                            ->leftjoin('product_entries AS main_product', 'main_product.id', '=', 'order_entries.product_entry_id')
                                            ->leftjoin('product_entries AS free_product', 'free_product.id', '=', 'order_entries.free_product_entry_id')
                                            ->where('order_entries.invoice_id', $id)
                                            ->select('order_entries.*',
                                                    'customers.id as customer_id',
                                                    'customers.name as customer_name',
                                                    'main_product.id as item_id',
                                                    'main_product.stock_in_hand as stock_in_hand',
                                                    'main_product.name as item_name',
                                                    'free_product.id as free_item_id',
                                                    'free_product.name as free_item_name')
                                            ->get();

        $entries_count          = $find_invoice_entries->count();

        $current_balance        = OrderJournalEntries::whereIn('transaction_head', ['payment-receive'])
                                            ->where('invoice_id', $id)
                                            ->where('debit_credit', 1)
                                            ->selectRaw('order_journal_entries.*')
                                            ->get();

        $current_balance_count  = $current_balance->count();

        $paid_accounts          = Accounts::where('account_type_id', 4)
                                            ->whereNotIn('id', [2,3])
                                            ->where('status', 1)
                                            ->get();
        $accounts               = Accounts::where('account_type_id',9)->where('status', 1)->get();

        return view('orders::order_edit', compact('products','product_category', 'product_entries', 'find_invoice', 'find_invoice_entries', 'entries_count', 'current_balance', 'current_balance_count', 'paid_accounts', 'accounts'));
    }

    public function orderUpdate(Request $request, $id)
    {
        $rules = array(
            'selling_date'          => 'required',
            'customer_id'           => 'required',
            'product_entries.*'     => 'required',
            'amount.*'              => 'required',
        );

        $validation = Validator::make(\Request::all(),$rules);

        if ($validation->fails()) {
            return redirect()->back()->withInput()->withErrors($validation);
        }

        $user_id    = Auth::user()->id;
        $data       = $request->all();

        DB::beginTransaction();

        try{
            $vat            = $data['vat_amount'];
             
            //Calculating Total Discount
                $discount = 0;
                foreach ($data['discount_type'] as $key => $value) 
                {
                    if ($value == 0)
                    {
                        $discount   = $discount + (($data['discount'][$key]*$data['rate'][$key]*$data['quantity'][$key])/100);
                    }
                    else
                    {
                        $discount   = $discount + $data['discount'][$key];
                    }
                }

            //Calculating Total Buy Price
                $buy_price      = 0;
                foreach ($data['product_entries'] as $key => $value)
                {
                    $product    = ProductEntries::find($value);
                    $buy_price  = $buy_price + ($product['buy_price']*$data['quantity'][$key]);
                }

            $invoice                            = Orders::find($id);
            $invoice->customer_id               = $data['customer_id'];
            $invoice->reference_id              = $data['reference_id'];
            $invoice->sr_id                     = $data['sr_id'];
            $invoice->invoice_date              = date('Y-m-d', strtotime($data['selling_date']));
            $invoice->invoice_amount            = $data['total_amount'];
            $invoice->due_amount                = $data['total_amount'];
            $invoice->total_buy_price           = round($buy_price, 2);
            $invoice->total_discount            = $discount;
            $invoice->invoice_note              = $data['invoice_note'];
            $invoice->total_vat                 = $vat;
            $invoice->vat_type                  = $data['vat_type'];
            $invoice->total_discount_type       = $data['total_discount_type'];
            $invoice->total_discount_amount     = $data['total_discount_amount'];
            $invoice->total_discount_note       = $data['total_discount_note'];
            $invoice->cash_given                = $data['cash_given'];
            $invoice->change_amount             = $data['change_amount'];
            $invoice->created_by                = $user_id;

            if (isset($data['type']))
            {
                $invoice->type = $data['type'];
            }

            if ($invoice->save())
            {   
                $delete_order_entries     = OrderEntries::where('invoice_id', $id)->delete();
                $delete_order_free_items  = OrderFreeItems::where('invoice_id', $id)->delete();

                foreach ($data['product_entries'] as $key => $value)
                {
                    $product_buy_price = ProductEntries::find($value);

                    $invoice_entries[] = [
                        'invoice_id'              => $invoice['id'],
                        'product_id'              => $product_buy_price['product_id'],
                        'product_entry_id'        => $value,
                        // 'free_product_entry_id'   => $data['free_items'][$key],
                        'customer_id'             => $invoice['customer_id'],
                        'main_unit_id'            => $data['main_unit_id'][$key],
                        'conversion_unit_id'      => $data['unit_id'][$key],
                        'free_main_unit_id'       => isset($data['free_item_main_unit_id'][$key]) ? $data['free_item_main_unit_id'][$key] : null,
                        'free_conversion_unit_id' => isset($data['free_unit_id'][$key]) ? $data['free_unit_id'][$key] : null,
                        'reference_id'            => $data['reference_id'],
                        'buy_price'               => round($product_buy_price['buy_price'], 2),
                        'rate'                    => $data['rate'][$key],
                        'quantity'                => $data['quantity'][$key],
                        'free_quantity'           => $data['free_quantity'][$key],
                        'total_amount'            => $data['amount'][$key],
                        'discount_type'           => $data['discount_type'][$key],
                        'discount_amount'         => $data['discount'][$key],
                        'created_by'              => $user_id,
                        'created_at'              => date('Y-m-d H:i:s'),
                    ];

                    $conversion_rate_find    = UnitConversions::where('main_unit_id', $data['main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                    $converted_quantity_to_main_unit  = $conversion_rate_find != null ? $data['quantity'][$key]/$conversion_rate_find['conversion_rate'] : $data['quantity'][$key];

                    if (isset($data['free_unit_id'][$key]))
                    {
                        $free_conversion_rate_find    = UnitConversions::where('main_unit_id', $data['free_item_main_unit_id'][$key])
                                        ->where('converted_unit_id', $data['free_unit_id'][$key])
                                        ->where('product_entry_id', $value)
                                        ->first();

                        $free_converted_quantity_to_main_unit  = $free_conversion_rate_find != null ? $data['free_quantity'][$key]/$free_conversion_rate_find['conversion_rate'] : $data['free_quantity'][$key];
                    }
                    else
                    {
                        $free_converted_quantity_to_main_unit  = 0;
                    }

                    if ($data['free_quantity'][$key] != 0)
                    {
                        $free_items[] = [
                            'date'              => date('Y-m-d', strtotime($data['selling_date'])),
                            'invoice_id'        => $invoice['id'],
                            'customer_id'       => $data['customer_id'],
                            'sr_id'             => $data['sr_id'],
                            'product_entry_id'  => $value,
                            'total_quantity'    => $free_converted_quantity_to_main_unit,
                            'free_quantity'     => $data['free_quantity'][$key],
                            'purchase_price'    => round($product_buy_price['buy_price'], 2),
                            'sell_price'        => $data['rate'][$key],
                            'created_by'        => $user_id,
                            'created_at'        => date('Y-m-d H:i:s'),
                        ];
                    }
                }

                DB::table('order_entries')->insert($invoice_entries);

                if (isset($free_items))
                {
                    DB::table('order_free_items')->insert($free_items);
                }

                $jour_ent_debit     = OrderJournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 1)
                                        ->first();

                $jour_ent_credit    = OrderJournalEntries::where('invoice_id', $invoice->id)
                                        ->where('transaction_head', 'sales')
                                        ->where('debit_credit', 0)
                                        ->first();
                //Financial Accounting Start
                    debitOrderUpdate($jour_ent_debit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=8, $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                    creditOrderUpdate($jour_ent_credit['id'], $customer_id=$data['customer_id'], $date=$data['selling_date'], $account_id=$data['account_id'], $amount=$data['total_amount'], $note=null, $transaction_head='sales', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                //Financial Accounting End

                //Insert into journal_entries Start
                    if (isset($data['current_balance_amount_paid']))
                    {
                        for($i = 0; $i < count($data['current_balance_amount_paid']); $i++)
                        {
                            if ($data['current_balance_amount_paid'][$i] > 0)
                            {   
                                $pay_debit  = OrderJournalEntries::find($data['current_balance_id'][$i]);
                                $pay_credit = OrderJournalEntries::find($data['current_balance_id'][$i] + 1);

                                if ($pay_debit != null)
                                { 
                                    debitOrderUpdate($pay_debit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    debitOrder($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=$data['current_balance_paid_through'][$i], $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }

                                if ($pay_credit != null)
                                { 
                                    creditOrderUpdate($pay_credit['id'], $customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                                else
                                {
                                    creditOrder($customer_id=$data['customer_id'], $date=$data['current_balance_payment_date'][$i], $account_id=8, $amount=$data['current_balance_amount_paid'][$i], $note=$data['current_balance_note'][$i], $transaction_head='payment-receive', $income_id=null, $expense_id=null, $balance_transfer_id=null, $invoice_id=$invoice->id, $bill_id=null, $payment_id=null, $production_id=null, $settlement_id=null, $sales_return_id=null, $purchase_return_id=null);
                                }
                            }
                            else
                            {
                                $pay_debit  = OrderJournalEntries::where('id', $data['current_balance_id'][$i])->delete();
                                $pay_credit = OrderJournalEntries::where('id', $data['current_balance_id'][$i] + 1)->delete();
                            }
                        }
                    }
                //Insert into journal_entries End

                DB::commit();

                if ($data['print'] == 1)
                {
                    return redirect()->route('orders_all_sales')->with("success","Order Updated Successfully !!");
                }
                else
                {   
                    $find_customer = Customers::find($data['customer_id']);
                    if ($find_customer['store_id'] != null)
                    {
                        $user_info  = Stores::find($find_customer['store_id']);
                    }  
                    else
                    {
                        $user_info  = userDetails();
                    }  
                                 
                    return redirect()->route('orders_show_pos', $invoice->id);
                }
            }
            else
            {
                DB::rollback();
                return back()->with("unsuccess","Something Went Wrong.Please Try Again.");
            }

        }catch (\Exception $exception){
            DB::rollback();
            dd($exception);
            return back()->with("unsuccess","Not Added");
        }
    }

    public function catList()
    {
        if(!isset($_GET['searchTerm']))
        { 
            $fetchData  = Products::orderBy('products.created_at', 'DESC')
                                    ->take(100)
                                    ->get();
        }
        else
        { 
            $search     = $_GET['searchTerm'];   
            $fetchData  = Products::where('products.name', 'LIKE', "%$search%")
                                    ->orderBy('products.created_at', 'DESC')
                                    ->get();
        }

        $data = array();
        $i    = 0;
        foreach ($fetchData as $key => $value)
        {   
            if ($i == 0)
            {
                $data[] = array("id"=>0, "text"=>'All');
            }
            
            $data[] = array("id"=>$value['id'], "text"=>$value['name']);

            $i++;
        }
   
        return Response::json($data);
    }
}