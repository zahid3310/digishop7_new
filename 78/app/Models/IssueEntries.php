<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class IssueEntries extends Model
{  
    protected $table = "issue_entries";

    public function createdBy()
    {
        return $this->belongsTo('App\Models\Users','created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo('App\Models\Users','updated_by');
    }

    public function issue()
    {
        return $this->belongsTo('App\Models\Issues','issue_id');
    }

    public function unit()
    {
        return $this->belongsTo('App\Models\Units','main_unit_id');
    }

    public function convertedUnit()
    {
        return $this->belongsTo('App\Models\Units','conversion_unit_id');
    }

    public function freeConvertedUnit()
    {
        return $this->belongsTo('App\Models\Units','free_conversion_unit_id');
    }
}
