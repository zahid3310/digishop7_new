<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('assets')->group(function() {
    Route::get('/', 'AssetsController@index')->name('assets_index');
    Route::post('/store', 'AssetsController@store')->name('assets_store');
    Route::get('/edit/{id}', 'AssetsController@edit')->name('assets_edit');
    Route::post('/update/{id}', 'AssetsController@update')->name('assets_update');
});
